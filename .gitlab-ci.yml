# SPDX-FileCopyrightText: 2023 Föderales Entwicklungsportal contributors
# SPDX-License-Identifier: EUPL-1.2

variables:
  PROJECT_SLUG: entwicklungsportal
  UPDATE_JOB_GIT_EMAIL: "pipeline-bot@fitko.dev"
  UPDATE_JOB_GIT_USERNAME: "Pipeline Bot"

stages:
  - lint
  - update
  - build
  - deploy
  - post-merge

# The workflow rules define which pipelines are allowed to run.
# We have three types of pipelines:
# 1. Update pipelines (triggered via webhook) - only run specific update jobs based on UPDATE_MODE
#    - UPDATE_MODE=mr: Creates a merge request for review
#    - UPDATE_MODE=direct: Commits directly to main
# 2. Merge request pipelines - run regular jobs for preview deployments
# 3. Main branch pipelines - run regular jobs for production deployments
workflow:
  rules:
    - if: $CI_PIPELINE_SOURCE == "trigger" && $UPDATE_MODE =~ /^(mr|direct)$/
      variables:
        GIT_STRATEGY: clone
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
    - if: $CI_PIPELINE_SOURCE == "web" && $UPDATE_MODE =~ /^(mr|direct)$/
      variables:
        GIT_STRATEGY: clone

# Template for all regular pipeline jobs (build, deploy, lint)
# These jobs should NOT run during update pipelines (trigger)
.regular_pipeline_jobs:
  rules:
    - if: $CI_PIPELINE_SOURCE == "trigger" && $UPDATE_MODE =~ /^(mr|direct)$/
      when: never
    - if: $CI_PIPELINE_SOURCE == "web" && $UPDATE_MODE =~ /^(mr|direct)$/
      when: never
    - when: on_success

reuse:
  extends: .regular_pipeline_jobs
  stage: lint
  image:
    name: fsfe/reuse:latest
    entrypoint: [""]
  script:
    - reuse lint

# This job creates or updates an existing branch and merge request for FIT standards updates
# Triggered when UPDATE_MODE=mr
update-and-create-mr:
  stage: update
  variables:
    GIT_COMMIT_MESSAGE: "FIT-Standards aktualisieren"
    TOKEN: $OpenProject_APIKEY
    UPDATE_FIT_STANDARDS_TOKEN: $UPDATE_FIT_STANDARDS_TOKEN
  image: python:3.10-slim
  rules:
    - if: $CI_PIPELINE_SOURCE == "trigger" && $UPDATE_MODE == "mr"
      when: always
    - if: $CI_PIPELINE_SOURCE == "web" && $UPDATE_MODE == "mr"
      when: always
    - when: never
  script:
    - echo "update-and-create-mr..."
    - apt-get update && apt-get install -y git curl jq
    - git config --global user.email $UPDATE_JOB_GIT_EMAIL
    - git config --global user.name $UPDATE_JOB_GIT_USERNAME
    - git remote set-url origin "https://oauth2:$UPDATE_FIT_STANDARDS_TOKEN@${CI_SERVER_HOST}/${CI_PROJECT_PATH}.git"
    
    # Clean up __pycache__ directories
    - rm -rf scripts/python/__pycache__/
    
    # Export branch name
    - export BRANCH_NAME=${UPDATE_BRANCH:-update-fit-standards-$(date +%Y-%m-%d-%H%M%S)}
    
    # Branch handling
    - git fetch origin $BRANCH_NAME || true
    - |
      if git ls-remote --exit-code origin $BRANCH_NAME; then
        echo "Branch $BRANCH_NAME exists remotely. Checking out and pulling..."
        git checkout -b $BRANCH_NAME origin/$BRANCH_NAME || git checkout $BRANCH_NAME
        git pull origin $BRANCH_NAME --rebase
      else
        echo "Branch $BRANCH_NAME does not exist remotely. Creating new branch..."
        git checkout -b $BRANCH_NAME
        git push -u origin $BRANCH_NAME
      fi
      
    # Run update script once
    - echo "Installing Python requirements..."
    - pip install --root-user-action=ignore --upgrade pip
    - pip install --root-user-action=ignore -r scripts/python/requirements.txt
    - echo "Running update script..."
    - python scripts/python/transfer_fit-standards.py --outfile content/fit-standards/from_openproject.json "$OpenProject_PROJECT" "$TOKEN"
    
    # Check for changes and handle them
    - |
      TARGET_FILE="content/fit-standards/from_openproject.json"
      if git status --porcelain "$TARGET_FILE" | grep -q "^.M\|^M"; then
        echo "Changes detected in $TARGET_FILE, proceeding with commit and push..."
        git add "$TARGET_FILE"
        git commit -m "${GIT_COMMIT_MESSAGE:-Update FIT standards (UPDATE_MODE=mr)}"
        git push origin $BRANCH_NAME
        
        echo "Preparing merge request creation..."
        if [ -z "$CI_MERGE_REQUEST_IID" ]; then
          echo "Creating merge request..."
          RESPONSE=$(curl --verbose --request POST \
            --header "PRIVATE-TOKEN: ${UPDATE_FIT_STANDARDS_TOKEN}" \
            --header "Content-Type: application/json" \
            --data '{
              "source_branch": "'"${BRANCH_NAME}"'",
              "target_branch": "'"${CI_COMMIT_BRANCH}"'",
              "title": "Update FIT standards",
              "description": "Automated update of FIT standards from OpenProject",
              "remove_source_branch": true
            }' \
            "${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/merge_requests")
          echo "Merge request response: $RESPONSE"
        else
          echo "Merge request already exists."
        fi
      else
        echo "No changes detected in $TARGET_FILE. Skipping commit and push."
      fi
    
    # Final cleanup
    - rm -rf scripts/python/__pycache__/


# This job executes the reset script (resets the FIT-standards in OpenProject to 'Produktiv')
# Triggered when merging into main and commit message includes (UPDATE_MODE=mr)
execute-reset-script:
  extends: .regular_pipeline_jobs
  stage: post-merge
  variables:
    TOKEN: $OpenProject_APIKEY
  image: python:3.10-slim
  rules:
    - if: |
          $CI_COMMIT_MESSAGE =~ /^Merge branch 'update-fit-standards-\d{4}-\d{2}-\d{2}-\d{6}'/
      when: on_success
  before_script:
    - apt-get update
    - apt-get install -y git
    - echo "Commit branch = $CI_COMMIT_BRANCH"
    - echo "Default branch = $CI_DEFAULT_BRANCH"
    - echo "MR ID = $CI_MERGE_REQUEST_ID"
    
  script:
    - echo "Branch starts with 'update-fit-standards-', executing reset script..."
    - echo "Installing Python requirements..."
    - pip install -r scripts/python/requirements.txt
    - echo "Running reset.py script..."
    - python scripts/python/reset_status_in_OP.py "$OpenProject_PROJECT" "$TOKEN"
  

# This job commits changes directly to the main branch
# Triggered when UPDATE_MODE=direct
direct-update-and-commit:
  stage: update
  image: python:3.9-slim
  rules:
    - if: $CI_PIPELINE_SOURCE == "trigger" && $UPDATE_MODE == "direct"
      when: always
    - if: $CI_PIPELINE_SOURCE == "web" && $UPDATE_MODE == "direct"
      when: always
    - when: never
  script:
    - apt-get update && apt-get install -y git
    - git config --global user.email $UPDATE_JOB_GIT_EMAIL
    - git config --global user.name $UPDATE_JOB_GIT_USERNAME
    - git remote set-url origin "https://oauth2:${UPDATE_FIT_STANDARDS_TOKEN}@${CI_SERVER_HOST}/${CI_PROJECT_PATH}.git"
    
    # Ensure we're on main branch
    - echo "Checking out main branch..."
    - git checkout $CI_DEFAULT_BRANCH
    
    # Clean up __pycache__ directories
    - rm -rf scripts/python/__pycache__/
    
    # Run your update script
    - echo "Installing Python requirements..."
    - pip install -r scripts/python/requirements.txt
    - echo "Running update script..."
    - python scripts/python/transfer_fit-standards.py --outfile content/fit-standards/from_openproject.json "05_FIT-Standards" "$TOKEN"
    
    # Clean up __pycache__ again after running
    - rm -rf scripts/python/__pycache__/
    
    # Check specifically for changes in the target file
    - |
      TARGET_FILE="content/fit-standards/from_openproject.json"
      if git status --porcelain "$TARGET_FILE" | grep -q "^.M\|^M"; then
        echo "Changes detected in $TARGET_FILE, proceeding with commit and push..."
        
        echo "Adding changes to git..."
        git add "$TARGET_FILE"
        
        echo "Committing changes..."
        git commit -m "Direct update of FIT standards"
        
        echo "Pushing to branch $CI_DEFAULT_BRANCH..."
        git push origin $CI_DEFAULT_BRANCH
        echo "Push completed."
      else
        echo "No changes detected in $TARGET_FILE"
        echo "Current git status:"
        git status
        echo "Job completed successfully - no updates needed"
        exit 0
      fi
  variables:
    TOKEN: $OpenProject_APIKEY
    UPDATE_FIT_STANDARDS_TOKEN: $UPDATE_FIT_STANDARDS_TOKEN

build:
  extends: .regular_pipeline_jobs
  stage: build    
  image: node:lts-alpine
  before_script:
    - apk add --no-cache git python3 py3-pip make g++
    - yarn install
  script:
    # Set base path for preview environments
    - export NEXT_PUBLIC_BASE_PATH="" && [[ "$CI_COMMIT_REF_NAME" != "main" ]] && export NEXT_PUBLIC_BASE_PATH="/$PROJECT_SLUG/$CI_MERGE_REQUEST_IID"
    - echo $NEXT_PUBLIC_BASE_PATH
    - export CURRENT_BRANCH=$CI_COMMIT_REF_NAME
    - yarn export
  artifacts:
    paths:
      - build/
    expire_in: 1 hour
  cache:
    paths:
      - node_modules/

# Deploy to preview environment for merge requests
deploy:preview:
  extends: .regular_pipeline_jobs
  stage: deploy
  variables:
    GIT_STRATEGY: none
  environment:
    name: preview/$CI_MERGE_REQUEST_IID
    on_stop: stop:preview
    auto_stop_in: 2 week
    url: https://preview.docs.fitko.dev/$PROJECT_SLUG/$CI_MERGE_REQUEST_IID
  rules:
    - if: $CI_PIPELINE_SOURCE == "trigger"
      when: never
    - if: $CI_PIPELINE_SOURCE == "web" && $UPDATE_MODE =~ /^(mr|direct)$/
      when: never
    - if: $CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH
  needs:
    - job: build
      artifacts: false
  script:
    - echo "Hey, my name is $CI_JOB_ID! I'm just here so that the upload to the preview environment is triggered."
    - echo "That's a very important task and I'm very proud to help you finish that great feature you're building! :D"
    - echo "There is nothing left to do here for me. Deployment will be triggered via a GitLab webhook."
    - echo "Bye, bye! 👋"

# Deploy to production for main branch
deploy:production:
  extends: .regular_pipeline_jobs
  stage: deploy
  variables:
    GIT_STRATEGY: none
  environment:
    name: production
    url: https://docs.fitko.de/
  rules:
    - if: $CI_PIPELINE_SOURCE == "trigger" && $UPDATE_MODE =~ /^(mr|direct)$/
      when: never
    - if: $CI_PIPELINE_SOURCE == "web" && $UPDATE_MODE =~ /^(mr|direct)$/
      when: never
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      when: on_success
  needs:
    - job: build
      artifacts: false
  script:
    - echo "Hey, my name is $CI_JOB_ID! I'm just here so that the upload to the preview environment is triggered."
    - echo "That's a very important task and I'm very proud to help you finish that great feature you're building! :D"
    - echo "There is nothing left to do here for me. Deployment will be triggered via a GitLab webhook."
    - echo "Bye, bye! 👋"

# Clean up preview environment
stop:preview:
  extends: .regular_pipeline_jobs
  stage: .post
  variables:
    GIT_STRATEGY: none # don't clone git repo
  environment:
    name: preview/$CI_MERGE_REQUEST_IID
    action: stop
  rules:
    - if: $CI_MERGE_REQUEST_ID
      when: manual
  needs:
    - job: deploy:preview
      artifacts: false # don't fetch artifacts from upload:preview job
  script:
    - echo "Hey, my name is $CI_JOB_ID! I'm just here so that the preview environment gets stopped."
    - echo "That's a very important task and I'm very proud to help you finish that great feature you're building! :D"
    - echo "There is nothing left to do here for me. Deployment will be triggered via a GitLab webhook."
    - echo "Bye, bye! 👋"

# Security scanning for yarn.lock
trivy-filesystem:
  extends: .regular_pipeline_jobs
  stage: lint
  image:
    name: aquasec/trivy:latest
    entrypoint: [""]
  cache:
    key: trivy-db-cache
    paths:
      - .trivycache/
    policy: pull-push
    when: 'always'
    untracked: false
  before_script:
    - apk add --no-cache jq
    - mkdir -p .trivycache
    - export TRIVY_CACHE_DIR=.trivycache/
  script:
    # Check DB metadata for age
    - |
      echo "Checking database metadata..."
      
      echo -e "\nRetrieving metadata with error handling..."
      DB_METADATA=$(trivy --cache-dir .trivycache/ version --format json || echo '{"VulnerabilityDB":{"NextUpdate":"1970-01-01T00:00:00Z"}}')
      echo "DB_METADATA=$DB_METADATA"
      
      NEXT_UPDATE=$(echo $DB_METADATA | jq -r '.VulnerabilityDB.NextUpdate' | cut -d'.' -f1)
      echo "NEXT_UPDATE=$NEXT_UPDATE"
      
      # Convert next update time to timestamp (using busybox date)
      NEXT_UPDATE_TS=$(date -D '%Y-%m-%dT%H:%M:%S' -d "$NEXT_UPDATE" +%s 2>/dev/null || echo 0)
      CURRENT_TS=$(date +%s)
      echo "NEXT_UPDATE_TS=$NEXT_UPDATE_TS"
      echo "CURRENT_TS=$CURRENT_TS"
      
      should_download=false
      if [ $NEXT_UPDATE_TS -le $CURRENT_TS ]; then
        echo "Database update is due"
        should_download=true
      else
        MINUTES_UNTIL_UPDATE=$(( ($NEXT_UPDATE_TS - $CURRENT_TS) / 60 ))
        HOURS=$(( MINUTES_UNTIL_UPDATE / 60 ))
        REMAINING_MINUTES=$(( MINUTES_UNTIL_UPDATE % 60 ))
        
        if [ $HOURS -gt 0 ]; then
          if [ $REMAINING_MINUTES -gt 0 ]; then
            echo "Database is current. Next update in $HOURS hours and $REMAINING_MINUTES minutes"
          else
            echo "Database is current. Next update in $HOURS hours"
          fi
        else
          echo "Database is current. Next update in $REMAINING_MINUTES minutes"
        fi
      fi
      
      if [ "$should_download" = true ]; then
        echo "Removing old database..."
        rm -rf .trivycache/db/
        
        echo "Downloading vulnerability database..."
        MAX_RETRIES=10
        RETRY_COUNT=0

        FIB1=1
        FIB2=1
        DELAY=1  # Initial delay

        until [ $RETRY_COUNT -ge $MAX_RETRIES ]
        do
          echo "Download attempt $(($RETRY_COUNT + 1)) of $MAX_RETRIES"
          if trivy filesystem --cache-dir .trivycache/ --download-db-only; then
            echo "Database download completed successfully"
            break
          fi

          RETRY_COUNT=$(($RETRY_COUNT + 1))
          if [ $RETRY_COUNT -lt $MAX_RETRIES ]; then

            # Calculate the next Fibonacci number
            DELAY=$FIB2
            FIB2=$((FIB1 + FIB2))
            FIB1=$DELAY
            echo "Download failed. Waiting $DELAY seconds before retry..."
            sleep $DELAY
          else
            echo "Failed to download database after $MAX_RETRIES attempts"
            exit 1
          fi
        done
      else
        echo "Using cached vulnerability database"
      fi
    # Perform vulnerability scans
    - trivy filesystem --cache-dir .trivycache/ --skip-db-update --scanners vuln,misconfig --exit-code 1 --severity HIGH,CRITICAL yarn.lock
    - trivy filesystem --cache-dir .trivycache/ --skip-db-update --scanners vuln,misconfig --exit-code 0 --severity UNKNOWN,LOW,MEDIUM yarn.lock


yarn-audit:
  extends: .regular_pipeline_jobs
  stage: lint
  image: node:lts-alpine
  script:
    - yarn audit --level high || (test $? -lt 8 && echo "Moderate vulnerabilities found, proceeding anyway.")
