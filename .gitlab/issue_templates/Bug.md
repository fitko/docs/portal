## Bug Description
<!-- Insert here a detailed description of the bug. Please include screenshots or other (visual) aids if this helps to describe the bug. -->

### Steps to reproduce

1. ...
2. ...
3. ...

### Current behavior
<!-- Describe here what actually happens when the bug occurs. -->

### Expected behavior
<!-- Describe here what is actually expected if the bug does not occur. -->

### Additional Information
<!-- Enter further relevant information here, e.g. operating system, browser etc. -->

### Checklist
- [ ] Add Severity label
- [ ] Related/affected issues/stories/epics linked and explained in the bug issue
- [ ] Creation of an automated test
- [ ] Bugfix tested in preview environment
- [ ] Bugfix tested on `prod`

/label ~"type::Bug"