# Federal Portal of Development (Förderales Entwicklungsportal)

![logo](public/img/kabelmaennchen_construct.svg){width=30%}

The developer portal bundles developer resources (documentation, API specifications, standards and code samples) on German government IT systems.

The live version is available here: https://docs.fitko.de

## Contributing
First of all, thank you for considering contributing to this project!

### Contributing code
For code contributions, head over to the next section (`How to build`).
Please get in touch [via the issue tracker](https://gitlab.opencode.de/fitko/docs/portal/-/issues) before putting any significant effort in code contributions. <3

### Contributing content
If you want to help us improve our content, you can find the text segments used on the landing page [here](https://gitlab.opencode.de/fitko/docs/portal/-/blob/main/views/FeaturedApiModules.tsx).

Resources and standards live in Markdown files in the `content/` folder.
To edit specific resources, please edit the markdown files in the `content/resources` folder or navigate to the detail page of that resource and click the `Edit` button.
The markdown files for resources and standards contain machine readable metadata (front matter).
Please create a new merge request for each new resource you want to add.

We also highly appreciate hints on relevant content [via the issue tracker](https://gitlab.opencode.de/fitko/docs/portal/-/issues).

Head over to our [acceptance criteria](https://docs.fitko.de/meta/acceptance-criteria) if you are unsure whether the content you have in mind is relevant for us and feel free to contact us [via the issue tracker](https://gitlab.opencode.de/fitko/docs/portal/-/issues) if you are still unsure!

### Contributing documentation
Documentation sub pages (like, e.g., the [FIT-Connect documentation](https://docs.fitko.de/fit-connect/)) are based on [Docusaurus](https://docusaurus.io/).
Head over to our [content editing guide for Docusaurus instances](https://docs.fitko.de/meta/usage) if you wish to contribute documentation.

If you want to add a new Docusaurus sub page, have a look at our [Docusaurus deployment documentation](https://docs.fitko.de/meta/deploy).

## How to build

To build the developer portal locally [yarn package manager](https://yarnpkg.com/) is recommended.

### Preconditions
To install yarn:
```sh
npm install --global yarn
```

Before proceeding, double check that yarn is installed correctly:
```sh
yarn --version
```

### Start development environment
Let's start the integrated Next.js development server:
```sh
yarn install
yarn dev
```

By default, the server runs on [http://localhost:3000](http://localhost:3000)

### Production build
Build a fully deployable export with:
```sh
yarn install
yarn export
```

The result is stored in the `build` directory.

## Continuous Integration
New commits within a merge request trigger a CI pipeline that creates (and updates) a merge request specific preview environment.
New commits on the `main` branch trigger our deployment pipeline.
For details, check the CI configuration in `.gitlab-ci.yml`.

In order to ensure high code quality, a static code analysis is run before deployment.

This repository is [compliant to the REUSE Specification](https://reuse.software/spec/).
The goal of the specification is to have unambiguous, human- and machine-readable copyright and licensing information for each individual file in the project.
REUSE-compliance is enforced by the `lint` stage of our CI.
New files added to this repository must be annotated with license and copyright information.
See [the REUSE tutorial](https://reuse.software/tutorial/) for how to do so.

## Security
If you want to report security vulnerabilities, please have a look at our [SECURITY.md](./SECURITY.md).

## Licenses
Source code is licensed under the [EUPL](LICENSES/EUPL-1.2.txt).

Unless stated otherwise, the content of this website is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-sa/4.0/).
