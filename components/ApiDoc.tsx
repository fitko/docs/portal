import React, { useEffect } from 'react'

function ApiDoc({ specUrl }) {
    useEffect(() => {
        import('rapidoc')
    }, [])

    return (
        <>
            <rapi-doc
                spec-url={specUrl}
                render-style="view"
                layout="column"
                theme="light"
                schema-description-expanded="true"
                show-info="false"
                show-header="false"
                show-components="false"
                allow-spec-file-load="false"
                info-description-headings-in-navbar="false"
                primary-color="#11171a"
                allow-server-selection="false"
                server-url=""
                allow-try="false"
                allow-authentication="true"
            />
        </>
    )
}

export default ApiDoc
