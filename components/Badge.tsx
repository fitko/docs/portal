/* ===== Render a badge =====
 *
 * Renders a badge, consisting of a symbol and a <label>
 * The colors used can be defined by Tailwind color classes (bg, border and text colors). If not stated, the default color is used.
 * If a link is provided, the badge will be clickable.
 * 
 * Properties:
 * - name: the label text
 * - link: the link to be used
 * - colors: the Tailwind color classes (bg, border and text colors)
 * - symbol: the icon to be rendered before the text (can be a path to an svg file, svg code as string or a Tabler icon name)
 * - altText: the alt text for the icon
 * 
 * The properties foreColor and bgColor are only provided for compatibility reasons. They shouldn't be used and will be ignored if colors is provided.
 * 
 */
import Link from 'next/link'
const { If, Then, Else, When, Unless, Switch, Case, Default } = require('react-if')
import RenderIcon from './RenderIcon'

export type BadgeParams = {
    name: string
    link?: string
    symbol?:  'IconCirclesRelation' |              
              'IconCategory2' |
              'IconFileCheck'
    altText?: string
    foreColor?: string
    bgColor?: string
    colors?: string
}

function RenderSymbol({ symbol, colors, foreColor, bgColor }) {

    let textColor = ''
    if (colors) {
        // Remove any Tailwind background color classes (e.g., bg-red-500)
        textColor = colors.replace(/\b(bg-[a-z0-9-\_]+)\b/g, '');
        // Trim any extra spaces that may have been left after removing the classes
        textColor = textColor.trim()
    }

  const IconComponent = symbol
  if (!IconComponent) return null
  // Check if the icon is a filled icon
  const isFilledIcon = IconComponent.displayName?.includes('Filled')

  return (
      <div className="p-1 z-10 rounded-full bg-white" aria-hidden="true">
          <If condition={colors !== undefined && colors !== null && colors !== ''}>
              <Then>
                  <div className={`p-1 rounded-full ${colors}`}>
                      <div className={textColor}>
                          <RenderIcon icon={symbol} color={textColor.replace('text-', '')} />
                      </div>
                  </div>
              </Then>
              <Else>
                  <div className="p-1 rounded-full" style={{ backgroundColor: bgColor }}>
                  <IconComponent
                          aria-hidden="true"
                          className={isFilledIcon ? 'fill-current text-white' : ''}
                          {...(isFilledIcon 
                              ? { 
                                  fill: foreColor,
                                  color: foreColor,
                                  stroke: 0
                                }
                              : {
                                  color: foreColor,
                                  stroke: 2
                                }
                          )}
                      />
                  </div>
              </Else>
          </If>
      </div>
  )
}


function RenderText({ name, symbol, colors, foreColor, bgColor }) {
  const tabIndexValue = -1;
  const baseClasses = symbol !== undefined ? 'flex items-center -ml-3' : 'flex items-center';
  const textClasses = symbol !== undefined ? 'pl-4 pr-2 whitespace-nowrap font-medium leading-6 mx-auto my-0 rounded-full' : 'px-2 leading-6 mx-auto my-0 rounded-full';

  return (
      <div className={baseClasses}>
          <If condition={colors !== undefined}>
              <Then>
                  <div className={`${textClasses} ${colors}`}>
                      <p tabIndex={tabIndexValue}>{name}</p>
                  </div>
              </Then>
              <Else>
                  <div 
                      className={textClasses}
                      style={{ backgroundColor: bgColor, color: foreColor }}
                  >
                      <p tabIndex={tabIndexValue}>{name}</p>
                  </div>
              </Else>
          </If>
      </div>
  );
}

export default function Badge({ name, link, symbol, altText, colors, foreColor, bgColor }: BadgeParams) {
    let alternateText = ''
    if (colors === undefined && foreColor === undefined && bgColor === undefined) {
        colors = "bg-gray-100 text-black"
    } else if (colors === undefined) {
        if (foreColor == undefined) {
            foreColor = 'black'
        }
        if (bgColor == undefined) {
            bgColor = 'lightgrey'
        }
    }
    if (altText !== undefined) {
        // eslint-disable-next-line no-unused-vars
        alternateText = altText
    }

    return (
        (link != null
            ? <Link href={link} className='text-xs px-2 inline-flex'>
                <RenderSymbol symbol={symbol} colors={colors} foreColor={foreColor} bgColor={bgColor} />
                <RenderText name={name} symbol={symbol} colors={colors} foreColor={foreColor} bgColor={bgColor} />
            </Link>
            : <div className='text-xs px-2 inline-flex relative'>
                <RenderSymbol symbol={symbol} colors={colors} foreColor={foreColor} bgColor={bgColor} />
                <RenderText name={name} symbol={symbol} colors={colors} foreColor={foreColor} bgColor={bgColor}/>
            </div>
        )
    )
}
