type ChipOptions = {
  text: string,
  style: string
}

export default ({ text, style }: ChipOptions) => {
    return (
        <div className={`m-px px-3 py-1 text-xs font-bold rounded-full text-white ${style} w-auto self-start shadow-sm`}>
            {text}
        </div>
    )
}
