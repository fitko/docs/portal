import useBasePath from '../shared/use-base-path'

export default () => {
    const { basePath } = useBasePath()
    return (
        <link rel="shortcut icon" href={`${basePath}/favicon-32x32.png`} />
    )
}
