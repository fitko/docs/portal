import React, { useState, ReactElement } from 'react'
import { IconChevronUp, IconChevronDown, IconAdjustmentsHorizontal, IconFilterOff } from '@tabler/icons-react'

type FilterAccordionOptions = {
  title: string;
  children?: ReactElement[];
  className?: string;
};

const FilterAccordion: React.FC<FilterAccordionOptions> = ({ title, children, className }) => {
    const [isFiltersOpen, setIsFiltersOpen] = useState(false)

    function handleResetFilters(): boolean {
        const checkboxes: HTMLInputElement[] = Array.from(document.querySelectorAll('form input[type="checkbox"]'))

        checkboxes.forEach(elem => {
            if (elem.checked) {
                elem.click()
            }
        })

        setIsFiltersOpen(false)

        return false
    }

    function handleOpenCloseFilters() {
        setIsFiltersOpen(!isFiltersOpen)
    }

    return (
        <>
            <div className="filter-wrap my-8 md:my-0 md:p-0 rounded-md">
                <div className="flex flex-row justify-between items-center p-2 md:mb-4 bg-yellow-400 text-white rounded-md md:hidden" onClick={handleOpenCloseFilters}>
                    <div className="flex flex-row items-center">
                        <IconAdjustmentsHorizontal className="mr-1"/>
                        <div className={`font-semibold ${className}`}>{title}</div>
                        <IconFilterOff aria-label="Filter zurücksetzen" className="hidden mx-2 cursor-pointer" onClick={handleResetFilters}/>
                    </div>
                    <div className="flex flex-row">
                        {isFiltersOpen ? <IconChevronUp aria-label="Filter-Anzeige schließen" className="cursor-pointer"/> : <IconChevronDown aria-label="Filter anzeigen" className="cursor-pointer"/>}
                    </div>
                </div>

                <aside aria-label="Sidebar" className={`${isFiltersOpen ? 'block' : 'hidden'} md:block md:sticky md:h-screen md:w-64 top-8 p-2 md:pt-0 md:pl-0 md:pr-0 divide-y divide-gray-300`}>
                    {children}
                </aside>
            </div>
        </>
    )
}

export default FilterAccordion
