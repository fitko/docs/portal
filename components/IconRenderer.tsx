// IconRenderer.tsx
import React from 'react'
import { IconSend, IconRoute, IconSteeringWheel, IconManualGearbox, IconBrandOpenSource, IconRocket, IconArrowBigRight } from '@tabler/icons-react'

const iconMapping = {
    IconRoute,
    IconSend,
    IconSteeringWheel,
    IconManualGearbox,
    IconBrandOpenSource,
    IconRocket,
    IconArrowBigRight,
}

interface IconRendererProps {
    iconName: keyof typeof iconMapping;
}

const IconRenderer: React.FC<IconRendererProps> = ({ iconName }) => {
    const IconComponent = iconMapping[iconName] || IconBrandOpenSource
    return <IconComponent className='h-6 w-6 text-white' />
}

export default IconRenderer
