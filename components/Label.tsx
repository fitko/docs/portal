/* ===== Render a text label =====
 *
 * The label may be a scoped label (delimited by '::')
 * The component also supports rendering a symbol before the text starts.
 * The label color can be stated as Tailwind color classes (bg, border and text colors). If not stated, the default color is used.
 * If a link is provided, the label will be clickable.
 *
 * Input:
 * - name: the label text (use '::' for scoped labels )
 * - link: the link to be used
 * - colors: the Tailwind color classes (bg, border and text colors)
 * - icon: the icon to be rendered before the text (can be a path to an svg file, svg code as string or a Tabler icon name)
 * 
 */

import {If, Then, Else} from 'react-if'
import Link from 'next/link'
import MY_CONSTANTS from '@/lib/constants-standards.js'
import { useEffect, useState } from 'react'
import RenderIcon from './RenderIcon'
import useImgPath from 'shared/use-img-path'

const { getImgPath } = useImgPath()

export type LabelParams = {
    name: string
    role?: string
    link?: string
    colors?: string
    icon?: string
}

export type RenderTextParams = {
  hasScope: boolean
  isScope: boolean
  text: string
  colors: string
  icon?: string
}


/* Render text label
 * - Can be the scope or just the label
 * - Can have an icon
 * 
 * Input:
 *  - hasScope: true if it's about a scoped label
 *  - isScope: true if we currently printing the scope
 *  - text: the text to print
 *  - colors: the color classes (Tailwind) to be used
 *  - icon: the icon to be used (if any); path to svg file
 */
function RenderText(props: RenderTextParams): JSX.Element {
    const tabIndexValue = -1;
    let borders = 'rounded-full';
    const textColorClass = props.colors.match(/\btext-([^\s]+)/)?.[0] || 'text-black' // Extract text color class

    if (props.hasScope) {
        if (props.isScope) {  // Render Scope!
            borders = 'rounded-l-2xl'
        } else {  // Render Label!
            borders = 'rounded-r-2xl'
        }
    }

    return (
        <div className="flex items-center">
            <div
                className={`flex flex-nowrap text-xs font-medium px-1 leading-6 my-0 border-2 ${borders} ${props.colors}`}
            >
                {props.icon && (
                    <div className="w-4 max-h-4 mr-1 flex items-center">
                        <RenderIcon icon={props.icon} color={textColorClass.replace('text-', '')} />
                    </div>
                )}
                <p tabIndex={tabIndexValue} className="leading-normal">
                    {props.text}
                </p>
            </div>
        </div>
    )
}

export default function Label({ name, link, colors, icon}: LabelParams) {
    let scope = ''
    // If colors are not defined, use default colors
    if (colors == undefined) {
      colors = MY_CONSTANTS.COLORS.DEFAULT // Use default colors
    }
    let colorClassesName = colors, colorClassesScope = colors

    // Are we dealing with a scoped label?
    const pos = name.indexOf('::')
    if (pos >= 0) {
        scope = name.substring(0, pos)
        name = name.substring(pos + 2)
        colorClassesName = colorClassesName
            .replace(/\bbg-[^\s]+/, 'bg-white')       // Remove background color, if scoped label
            .replace(/\btext-[^\s]+/, 'text-black')   // Remove text color, if scoped label
    }

    return (
      link != undefined ? (
          <Link href={link} className="text-xs px-2 py-1 my-1 inline-flex">
              <If condition={scope != ''}> {/* If scoped label */}
                  <Then>
                      {/* Render the scope part */}
                      <RenderText
                          hasScope={scope != ''}
                          isScope={true}
                          text={scope}
                          colors={colorClassesScope}
                          icon={icon}
                      />
                      {/* Render the label part */}
                      <RenderText
                          hasScope={scope != ''}
                          isScope={false}
                          text={name}
                          colors={colorClassesName}
                          icon={undefined} // Ensure icon is not passed to the label part
                      />
                  </Then>
                  <Else>  {/* If not scoped label */}
                      <RenderText
                          hasScope={scope != ''}
                          isScope={false}
                          text={name}
                          colors={colorClassesName}
                          icon={icon}
                      />
                  </Else>
              </If>
          </Link>
      ) : (
          <div className="text-xs mb-2 px-2 inline-flex relative">
              <If condition={scope != ''}>
                  <Then>
                      <RenderText
                          hasScope={scope != ''}
                          isScope={true}
                          text={scope}
                          colors={colorClassesScope}
                          icon={icon}
                      />
                      <RenderText
                          hasScope={scope != ''}
                          isScope={false}
                          text={name}
                          colors={colorClassesName}
                          icon={undefined}
                      />
                  </Then>
                  <Else>
                      <RenderText
                          hasScope={scope != ''}
                          isScope={false}
                          text={name}
                          colors={colorClassesName}
                          icon={icon}
                      />
                  </Else>
              </If>
          </div>
      )
  )
}