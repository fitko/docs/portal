import { useState } from 'react'
import { useRouter } from 'next/router'
import useBasePath from 'shared/use-base-path'
import { IconWorld } from '@tabler/icons-react'
import useTranslation from 'shared/use-translation'

const LanguageChooser = () => {
    const [isOpen, setIsOpen] = useState(false)
    const { relocalizePath, getLocaleFromPath } = useBasePath()
    const router = useRouter()
    const { asPath } = router

    const currentLocale = getLocaleFromPath(asPath)
    const { translate } = useTranslation(currentLocale)

    const supportedLocales = ['de', 'en']

    return (
        <div className="relative">
            <button onClick={() => setIsOpen(!isOpen)} className="flex items-center pr-4 py-2 text-gray-700 rounded-md focus:outline-none">
                <IconWorld className="mr-1" style={{ height: '1.5em' }} />
                {currentLocale.toUpperCase()}
            </button>
            {isOpen && (
                <div className="absolute right-0 w-40 mt-2 origin-top-right bg-white border border-gray-200 divide-y divide-gray-100 rounded-md shadow-lg">
                    <ul className="py-1">
                        {supportedLocales.map((locale) => (
                            <li key={locale}>
                                <a href={relocalizePath(asPath, locale)} className="block px-4 py-2 text-sm text-gray-700 hover:bg-gray-100">
                                    {translate(`language.${locale}`)}
                                </a>
                            </li>
                        ))}
                    </ul>
                </div>
            )}
        </div>
    )
}

export default LanguageChooser
