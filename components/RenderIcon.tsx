/*
 * ***** Flexible, extensible icon rendering component *****
 *
 * renders either a tabler icon, svg content or an image based on the provided `icon` prop.
 * For a tabler icon, it is sufficient to pass the React icon name (e.g., 'IconUser'). The icon will be loaded dynamically.
 * svg content can be passed directly as a string.
 * For an image, the image path can be provided. The image path can be relative or absolute. 
 * If the image path starts witha '/', it will be adjusted by calling getImgPath().
 * Additional props will be passed to the rendered icon or image.
 * The component is designed to be flexible and can be extended to support other icon libraries or image sources.
 * 
 */

import React, { useEffect, useState } from 'react'
import dynamic from 'next/dynamic'
import useImgPath from 'shared/use-img-path'

const { getImgPath } = useImgPath()

// Dynamically load Tabler icons
const dynamicImport = (iconName: string) => {
  return dynamic(
    () => import(`@tabler/icons-react`).then(mod => mod[iconName] as React.ComponentType<any>),
    { ssr: false }
  )
}

interface RenderIconProps {
  icon: string
  className?: string
  stroke?: number
  [key: string]: any // Allow additional props
}

const RenderIcon: React.FC<RenderIconProps> = ({ icon, className = '', stroke = 2, ...props }) => {
    const [IconComponent, setIconComponent] = useState<React.ComponentType<any> | null>(null)

  useEffect(() => {
      if (icon.startsWith('Icon')) { // Check if the icon is a Tabler icon
          // Import the icon component and set it to the state
          const loadIcon = async () => {
              const Icon = await dynamicImport(icon)
              setIconComponent(Icon)
          }
          loadIcon().catch(() => {
              console.error(`Failed to load icon: ${icon}`)
          })
      } else {
          setIconComponent(null)
      }
  }, [icon])

  if (icon.startsWith('/')) { // Check if the icon is an image path
      // Call getImgPath to get the adjusted image path
      const imagePath = getImgPath(icon)
      return <img src={imagePath} alt="Icon" className={`${className}`} />
  } else if (icon.startsWith('Icon')) { // Check if the icon is a Tabler icon
      return IconComponent ? <IconComponent stroke={stroke} className={`${className}`} {...props} /> : null
  } else if (icon.startsWith('<svg')) { // Check if the icon is an SVG string
      return <div dangerouslySetInnerHTML={{ __html: icon }} className={`${className}`} />
  } else if (icon) { // If icon is not empty, treat it as a relative image path
      return <img src={icon} alt="Icon" className={`${className}`} />
  } else {
      return null
  }
}

export default RenderIcon