import React, { ReactElement, Fragment } from 'react'
import { IconBooks, IconPencil } from '@tabler/icons-react'
import ApiDoc from '@/components/ApiDoc'
import Link from 'next/link'
import useEditPath from 'shared/use-edit-path'
import useTranslation from '../shared/use-translation'
import useImgPath from 'shared/use-img-path'
const { getImgPath } = useImgPath()
const { getContentEditUrl } = useEditPath()

type ResourceCardOptions = {
  title: string
  description: string
  logo?: {
    path: string
    title?: string
  }
  apiSpecURL?: string
  children?: ReactElement[]
  slug: string
  lang: string
  childResources?: any[]
}

export default ({ title, description, logo, slug, lang, apiSpecURL, children, childResources }: ResourceCardOptions) => {
    const { translate } = useTranslation(lang)
    return (
        <section className="bg-white md:mt-10 w-full mx-auto relative z-10 lg:pb-32" aria-labelledby="heading">
            <h2 className="sr-only" id="heading">
                {title}
            </h2>
            <div className="flex flex-col bg-white rounded-b-lg shadow-xl">
                <div className="pt-6 px-4 pb-6 md:flex-1 md:pt-5 md:px-6 md:pb-8">
                    <div className="pb-12 md:inline-block md:transform md:translate-y-0 md:h-32 md:grow md:p-4 md:mb-12 w-full">
                        <header className={'flex flex-col md:flex-row justify-between items-center'}>
                            <Link href={getContentEditUrl(slug, lang)} passHref legacyBehavior>
                                <a href={getContentEditUrl(slug, lang)} target={'_blank'} className="flex ml-auto whitespace-nowrap items-center md:order-last self-start text-gray-700 p-2 mb-4">
                                    <IconPencil className="w-5 h-5 mr-2" />
                                    { translate('resourcedetail.buttons.edit') }
                                </a>
                            </Link>
                            {logo && logo.path
                                ? (
                                    <img
                                        className={'h-24'}
                                        src={getImgPath(logo.path)}
                                        alt={logo.title}
                                    />
                                )
                                : (
                                    <IconBooks className='w-12 h-12 mx-auto'/>
                                )}
                            <h1 className="text-2xl lg:text-3xl xl:text-5xl tracking-tight text-gray-900 text-center md:text-left overflow-hidden mt-4 md:mt-0 md:mx-6">
                                {title}
                            </h1>
                        </header>
                    </div>
                    <div className="flex flex-col md:flex-row md:items-center mt-4 md:mt-0">
                        <div
                            className="cms-blog-text text-lg md:text-xl text-gray-800 md:flex-1"
                            dangerouslySetInnerHTML={{ __html: description }}
                        />
                    </div>
                    {apiSpecURL && (
                        <div className="flex flex-col md:flex-row md:items-center my-5 !h-full">
                            <ApiDoc specUrl={apiSpecURL}/>
                        </div>
                    )}
                    {childResources.length > 0 && (
                        <Fragment>
                            <h2 className="mt-6 mb-4 text-3xl font-medium text-gray-900">{ translate('resourcedetail.labels.subresources') }</h2>
                            <div className="flex flex-wrap gap-6">
                                {childResources.map((resource, index) => (
                                    <Link key={index} href={`/resources/${resource.slug}`} passHref legacyBehavior>
                                        <a className="flex flex-col flex-grow basis-1/4 text-gray-900 p-4 text-center border border-gray-300 shadow">
                                            {resource.logo && resource.logo.path
                                                ? (
                                                    <img
                                                        className={'max-h-24 self-center mx-auto mt-auto mb-2'}
                                                        src={getImgPath(resource.logo.path)}
                                                        alt={resource.logo.title}
                                                    />
                                                )
                                                : (
                                                    <IconBooks className='w-8 h-8 mx-auto my-auto'/>
                                                )
                                            }
                                            {resource.name}
                                        </a>
                                    </Link>
                                ))}
                            </div>
                        </Fragment>
                    )}
                </div>
                <div className="px-4 py-6 bg-gray-50 rounded-b-lg md:px-8 flex flex-wrap md:flex-nowrap items-center gap-4 md:gap-16">
                    {children}
                </div>
            </div>
        </section>

    )
}
