/* eslint-disable id-length */
/*
 *      Implements a scroll to top button
 */

import { useEffect, useCallback } from 'react'
import { IconArrowUp } from '@tabler/icons-react'

const isBrowser = () => typeof window !== 'undefined' // The approach recommended by Next.js

/* Scroll to top function in client */
function scrollToTop() {
    if (!isBrowser()) return
    // eslint-disable-next-line id-length
    window.scrollTo({ top: 0, behavior: 'smooth' })
}

/* The button */
export const ScrollToTopButton = () => {
    const onScroll = useCallback(event => {
        const { scrollY, innerHeight } = window
        const elements = document.getElementsByClassName('scrollToTopButton')
        const ourButton = elements[0]
        if ((scrollY / innerHeight) > 1.25) {
            ourButton.setAttribute('style', 'visibility: visible; opacity: 1;')
            // ourButton.setAttribute('style', 'display: inline')
        } else {
            ourButton.setAttribute('style', 'opacity: 0; vsibility: hidden')
            // ourButton.setAttribute('style', 'display: none')
        }
    }, [])

    useEffect(() => {
        // Test via a getter in the options object to see if the passive property is accessed
        let supportsPassive = false
        try {
            const opts = Object.defineProperty({}, 'passive', {
                get: function () {
                    supportsPassive = true
                },
            })
            window.addEventListener('testPassive', null, opts)
            window.removeEventListener('testPassive', null, opts)
        } catch (e) {}

        // add eventlistener to window
        window.addEventListener('scroll', onScroll, supportsPassive ? { passive: true } : false)
        // remove event on unmount to prevent a memory leak with the cleanup
        return () => {
            window.removeEventListener('scroll', onScroll)
        }
    }, [])

    return (
        <button
            className="fixed bottom-0 lg:bottom-4 right-4 md:right-8 mt-4 justify-self-center
                        p-2 rounded-md bg-slate-400 text-white font-black invisible opacity-0
                        hover:bg-slate-600 hover:opacity-100
                        transition ease-in-out duration-300 scrollToTopButton"
            title="Zum Seitenanfang scrollen"
            onClick={scrollToTop}
        >
            <IconArrowUp className="stroke-3 stroke-[3px]" />
        </button>
    )
}
