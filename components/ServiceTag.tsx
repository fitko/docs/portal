import Chip from '@/components/Chip'
import useTranslation from 'shared/use-translation'
import { Tag } from 'types/content/'

type ServiceTagOptions = {
    lang: string,
    tag: Tag,
    style: string
}

export default ({ lang, tag, style }: ServiceTagOptions) => {
    const { translateLabel, translateTag } = useTranslation(lang)

    return (
        <Chip text={`${translateLabel(tag?.name)}: ${translateTag(tag)}`} style={`${style} capitalize ${tag.name == 'label' && tag.value == 'external' ? 'hidden' : ''}`} />
    )
}
