import React, { useState, ReactElement } from 'react'

type TooltipOptions = {
  children?: ReactElement[] | ReactElement
  content: string
  delayIn?: number
  delayOut?: number
  direction?: string // not yet implemented
}

export default ({ delayIn, delayOut, direction, children, content }: TooltipOptions) => {
    let timeout
    const [active, setActive] = useState(false)

    const showTooltip = () => {
        clearInterval(timeout)
        timeout = setTimeout(() => {
            setActive(true)
        }, delayIn || 400)
    }

    const hideTooltip = () => {
        clearInterval(timeout)
        timeout = setTimeout(() => {
            setActive(false)
        }, delayOut || 400)
    }

    return (
        <div
            className="tooltip-wrapper"
            onMouseEnter={showTooltip}
            onMouseLeave={hideTooltip}
        >
            {children}
            <div className={`tooltip-content ${direction || 'top-left'} ${active ? 'active' : ''}`}>
                <div className="tooltip-arrow"></div>
                {content}
            </div>
        </div>
    )
}
