import Link from 'next/link'

// Display the breadcrumb navigation
export default (props) => {
    return (
        <header className="px-3 lg:px-8 flex flex-row text-sm sm:text-base pb-8 font-medium">
            <div>
                <Link className="font-semibold text-blue-700 underline-offset-2 hover:underline " href={'.'}>Übersicht</Link>
            </div>
            <div>
                &nbsp;&rarr;&nbsp;{props.navItem}
            </div>
        </header>
    )
}
