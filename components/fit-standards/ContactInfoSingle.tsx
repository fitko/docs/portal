import MY_CONSTANTS from '@/lib/constants-standards.js'
import Link from 'next/link'
import useImgPath from 'shared/use-img-path'

const { getImgPath } = useImgPath()

export type Contact = {
    name: string
    link: string
    mail: string
    phone: string
    contactform: string
}

type ContactInfoSingleOptions = {
    contactType: string
    contact: Contact
}

function getContactTypeName(contactType: string) {
    const found = MY_CONSTANTS.CONTACT_TYPES.filter((item) => item.id === contactType)[0]
    return (found !== undefined ? found.text : '#INVALID')
}

export function ContactInfoSingle({ contactType, contact }: ContactInfoSingleOptions) {
    const imagePath = `/img/fit-standards/roles/${contactType}.svg`
    return (
        (contact && contact.name != null && contact.name != ''
            ? <div className="contact-single text-sm p-1 mb-2 mt-1 leading-6">
                {/* Role icon and contact type row */}
                <div className="flex grow align-bottom bg-neutral-100 items-center">

                    {/* Role icon */}
                    <div className="w-4 ml-1 mr-2 max-h-5 flex items-center">
                        <img
                            src={getImgPath(imagePath)}
                            alt={`${contactType}`}
                            aria-hidden='true'
                        />
                    </div>

                    {/* Contact type */}
                    <div className="font-semibold">
                        {getContactTypeName(contactType)}
                    </div>
                </div>

                {/* organisation row */}
                {
                    contact.link != null && contact.link != ''
                        ? <div className="leading-5 mb-2">
                            <Link href = {contact.link} target="_blank" rel="noopener noreferrer">
                                {contact.name}
                            </Link>
                        </div>
                        : <div className="leading-5 mb-2">{contact.name}</div>
                }

                {/* E-Mail row */}
                {
                    contact.mail != null && contact.mail != ''
                        ? <div className="flex leading-5 mb-2 flex-wrap">                            
                            <div>
                                <Link href = {'mailto:' + contact.mail} className="break-anywhere">
                                    {contact.mail}
                                </Link>
                            </div>
                        </div>
                        : null
                }

                {/* Phone no. */}
                {
                    contact.phone != null && contact.phone != ''
                        ? <div className="flex leading-5 mb-2">
                            <div>
                                <Link href = {'tel:' + contact.phone}>
                                    {contact.phone}
                                </Link>
                            </div>
                        </div>
                        : null
                }

                {/* Contact form {contact.contactform} */}
                {
                    contact.contactform != null && contact.contactform != ''
                        ? <div className="flex">
                            <div className="font-semibold">
                                <Link href = {contact.contactform} target="_blank" rel="noopener noreferrer">
                                    Kontaktformular
                                </Link>
                            </div>
                        </div>
                        : null
                }
                <hr aria-hidden='true'/>
            </div>
            : null
        )
    )
}

export default ContactInfoSingle
