//  Creates the content for Info box 'Kontaktinformationen'
//  JVo, 2025.02.05
//

import { Contact, ContactInfoSingle } from './ContactInfoSingle'
import MY_CONSTANTS from '@/lib/constants-standards.js'

type ContactDetailsOptions = {
    initiator: Contact
    productOwner: Contact
    steeringCommittee: Contact
    developer: Contact
    operator: Contact
    contact: Contact
    funder: Contact
}

export function ContactInformation({
    initiator,
    productOwner,
    steeringCommittee,
    developer,
    operator,
    contact,
    funder
}: ContactDetailsOptions) {
    return (
        <div className="sm:w-96 md:w-80 border-2 mb-4 rounded-md">
            <div className={`${MY_CONSTANTS.COLORS.ROLES} p-1 font-bold`}>Informationen zu den Rollen</div>
            <div className={`${MY_CONSTANTS.INFOBOX_1ST_COLUMN_WIDTH} font-semibold`}></div>

            {/* Use contact only, if not empty */}
            <ContactInfoSingle
                contactType = {'initiator'}
                contact = { initiator }
            />
            <ContactInfoSingle
                contactType = {'productOwner'}
                contact = { productOwner }
            />
            <ContactInfoSingle
                contactType = {'developer'}
                contact = { developer }
            />
            <ContactInfoSingle
                contactType = {'operator'}
                contact = { operator }
            />
            <ContactInfoSingle
                contactType = {'contact'}
                contact = { contact }
            />
            <ContactInfoSingle
                contactType = {'steeringCommittee'}
                contact = { steeringCommittee }
            />
            <ContactInfoSingle
                contactType = {'funder'}
                contact = { funder }
            />
        </div>
    )
}
