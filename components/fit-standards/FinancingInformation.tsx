//  Creates the content for Info box 'Finanzierung'
//  JVo, 2025.01.31
//

import { Contact, ContactInfoSingle } from './ContactInfoSingle'
import MY_CONSTANTS from '@/lib/constants-standards.js'
import useImgPath from 'shared/use-img-path'

const { getImgPath } = useImgPath()

type FundingDetailsOptions = {
    funder: Contact
}

export function FinancingInformation({
    funder
}: FundingDetailsOptions) {
  const contactType = 'funder'
  const contact = funder
  const imagePath = `/img/fit-standards/roles/${contactType}.svg`
  return (
    (contact && contact.name != null && contact.name != ''
      ? <div className="sm:w-96 md:w-80 border-2 mb-4 rounded-md">
            <div className={`${MY_CONSTANTS.BG_COLORS.DEFAULT} p-1 font-bold`}>
            {/* Role icon */}
            <div className="w-4 ml-1 mr-2 max-h-5 flex items-center">
              <img
                  src={getImgPath(imagePath)}
                  alt={`${contactType}`}
                  aria-hidden='true'
              />
              <div className="ml-2">Finanzierung</div>
            </div>
          </div>
          <div className={`${MY_CONSTANTS.INFOBOX_1ST_COLUMN_WIDTH} font-semibold`}></div>

          {/* Contact name */}
          <div className="leading-5 mb-2 text-sm p-1">{contact.name}</div>
        </div>
      : null
    )
  )
}
