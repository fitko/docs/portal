/*
 * ***** Flexible, extensible filter component *****
 *
 * This component implements a flexible filter which can be extended easily.
 *
 * BASIC CONCEPT
 * =============
 * A data array (array of objects) is filtered by configurable filter criteria.
 *
 * The filter criteria must be specified prior of using the component.
 * he filter criteria configuration is handed over to the component as a prop (as well
 * as the data array and a callback function, which will be called whenever
 * the resulting array of filtered data changes.
 *
 * The component itself renders the filter UI.
 * A function returns the filtered data array as a JSON string
 * according to the selections the user has made in the UI.
 * To return the data to the parent component, useState() is used.
 *
 * Beside that, the component offers a way to call a function of the component (reset all filters)
 * from a parent component. This is based on
 * https://dev.to/cleverzone/child-s-function-calling-from-parent-component-reactjs-nextjs-23b1
 *
 * Filters are organised in filter groups. A filter group has a specific type.
 * The group type defines, what filters will be displayed and how they are presented.
 * There are two filter group types implemented currently in the FlexFilter component:
 *
 * - BoolForcedFilterGroup
 *   The BoolForcedFilterGroup represents criteria which can be matched or not.
 *   Therefore, checboxes are used to display a filter criteria.
 *   Within a BoolForcedFilterGroup, each filter criteria must be specified.
 *
 * - BoolAutoFilterGroup
 *   The BoolAutoFilterGroup is pretty similar to the BoolForcedFilterGroup.
 *   The difference is, that the filter criteria (labels) are collected from the objects itself.
 *
 */
'use client'

import React, { useEffect, forwardRef, useImperativeHandle } from 'react'
import { useRouter } from 'next/router'
import { IconChevronUp, IconChevronDown } from '@tabler/icons-react'
import { BooleanOption, BoolAutoFilterGroup, FilterGroup } from 'types/filters/'
import { OpenClose } from 'types/filters/FilterGroup'
import { validateQueryParameter, updateQueryParameter, toSafeString, getQueryParams } from 'lib/utils/handleUrlParameters'
import RenderIcon from 'components/RenderIcon'
import dynamic from 'next/dynamic'

const QUERY_PARAM = 'filters'
const DEFAULT_COLORS = 'bg-itplr_grau-4 border-itplr_grau-4 text-black'

interface BlockHeaderProps {
    id: string
    label: string
    color: string
    icon: string
    openCloseMode: OpenClose
}

// Dynamically load Tabler icons
const dynamicImport = (iconName: string) => {
  return dynamic(
    () => import(`@tabler/icons-react`).then(mod => mod[iconName] as React.ComponentType<any>),
    { ssr: false }
  )
}

export type FlexFilterOptions = {
    theObjects: Array<any>
    filters: Array<FilterGroup>
    parentCallback: (json: string) => void
}

function getInitialCount(contents: Array<any>, filters: Array<FilterGroup>) {
    const router = useRouter()
    // Check router.query for our parameter
    const param = validateQueryParameter(router.query, QUERY_PARAM)
    param.forEach(function (value, index) {
        const keyValue = value.split(':')
        param[keyValue[0]] = keyValue[1]
    })

    // Check filters for status
    filters.forEach((filter) => {
        // Get options for auto filter
        if (filter.type == 'BoolAuto' && !filter.options.length) {
            const myFilter = filter as BoolAutoFilterGroup
            const labels: string[] = myFilter.getElementsAll(contents)
                .filter(label => label !== undefined && label !== null && label !== '')

            labels.forEach((label) => {
                const id = toSafeString(label)
                const evalFunc = myFilter.getLabelsOfElementFunc
                const parentFilterType = 'BoolAuto'
                filter.options.push(new BooleanOption({
                    id,
                    label,
                    evalFunc,
                    parentFilterType,
                })
                )
            })
        }
        // Count objects matching the criteria
        filter.options.forEach((option) => {
            option.initialCount = 0
            switch (option.parentFilterType) {
            case '':
                contents.forEach((content) => {
                    if (option.evalFunc(content)) {
                        option.initialCount++
                    }
                })
                break

            case 'BoolAuto':
                contents.forEach((content) => {
                    const contacts = option.evalFunc(content)
                    if (typeof contacts == 'object' && contacts.includes(option.label)) {
                        option.initialCount++
                    }
                })
                break
            }
        })
        if (filter.openCloseMode == OpenClose.DefaultClose && param[filter.id] !== undefined) {
            filter.openCloseMode = OpenClose.DefaultOpen
        }
    })
}

// Checks, if a specific object matches a specific filter
function isMatching(object: any, thisCB: HTMLElement, filter: FilterGroup): boolean {
  // Get the option ID
  const optionId = thisCB.id.split('-')[1]

  // locate the option
  const myOption = filter.options.find(myObj => myObj.id == optionId)

  const myResult = myOption.evalFunc(object)

  // If it's already a boolean, return it directly
  if (typeof myResult === 'boolean') {
      return myResult
  }

  // If it's an array, check if it includes the option's label
  if (Array.isArray(myResult)) {
      return myResult.includes(myOption.label)
  }

  // If we get here, something is wrong with the data type
  console.warn(`Unexpected result type from evalFunc: ${typeof myResult}`)
  return false
}


// Handle opening/closing of filter group by clicking the corresponding chevron
function handleChevronClick(event) {
    event.preventDefault()

    // Get the main element
    const filterGroupElem = event.target.closest('[id^="headline-"]')

    // Find wrapper of all items
    let myElem: HTMLElement = filterGroupElem.parentElement.querySelector('.items-wrap')
    const currentVisibility = myElem.checkVisibility()

    filterGroupElem.querySelector('.message').textContent = (!currentVisibility ? 'Geöffnet' : 'Geschlossen')

    myElem.style.display = (currentVisibility ? 'none' : 'block')

    // Find and adjust chevron 'up'
    myElem = filterGroupElem.querySelector('.chevronUp-wrap')
    myElem.style.display = (myElem.checkVisibility() ? 'none' : 'block')

    // Chevron 'down' must be the opposite
    filterGroupElem.querySelector('.chevronDown-wrap').style.display = (myElem.checkVisibility() ? 'none' : 'block')

    if (currentVisibility) {
        filterGroupElem.querySelector('.chevronDown-wrap').focus()
    } else {
        myElem.focus()
    }
}

//  Build the header row of a filter group (label, maybe icon, color, chevrons for opening/closing the group)
const BlockHeader: React.FC<BlockHeaderProps> = ({ id, label, color, icon, openCloseMode }) => {

  return (
    <div className={`flex flex-nowrap justify-between items-center p-1 w-full ${color}`} id={`headline-${id}`}>
      <div className="flex items-center">
          {icon && (
          <div className="h-5">
              <RenderIcon icon={icon} className="mr-2" stroke={2} size={20} />
          </div>
          )}
          <div>
              <div className="message sr-only" role="alert"></div>
              <legend className="font-semibold text-sm w-full">
                {label}
              </legend>
          </div>
      </div>
      {(openCloseMode === OpenClose.DefaultClose) && (
        <div>
          <button type="button" onClick={handleChevronClick} className="hidden chevronUp-wrap" aria-label={`Filtergruppe ${label} schließen`}>
            <IconChevronUp stroke={2} id={`${id}-chevron`} />
          </button>
          <button onClick={handleChevronClick} className="chevronDown-wrap" aria-label={`Filtergruppe ${label} öffnen`}>
            <IconChevronDown stroke={2} id={`${id}-chevron`} />
          </button>
        </div>
      )}
      {(openCloseMode === OpenClose.DefaultOpen) && (
        <div>
          <button type="button" onClick={handleChevronClick} className="chevronUp-wrap" aria-label={`Filtergruppe ${label} schließen`}>
            <IconChevronUp stroke={2} id={`${id}-chevron`} />
          </button>
          <button type="button" onClick={handleChevronClick} className="hidden chevronDown-wrap" aria-label={`Filtergruppe ${label} öffnen`}>
            <IconChevronDown stroke={2} id={`${id}-chevron`} />
          </button>
        </div>
      )}
    </div>
  )
}

// eslint-disable-next-line id-length
export const FlexFilter = forwardRef((props : FlexFilterOptions, ref) => {
    // External reference => functions that can be called from outside
    useImperativeHandle(ref, () => ({
        resetFilters: () => resetFilters(),
    }))
    const theObjects = props.theObjects
    const filters = props.filters
    const parentCallback = props.parentCallback

    const router = useRouter()
    const { pathname } = useRouter()

    // The regular event handler (onClick)
    function handleChange(event) {
        event.preventDefault()

        // Invisible element?
        if (event.target.classList.contains('invisible')) {
            return false // Invisible element, we're done here!
        }
        const value = (event.target as HTMLInputElement).value
        processChange(value)
    }

    // Used to align with URL parameter
    function handleChange2(value: string) {
        processChange(value, true)
    }

    // Handles any change event
    const processChange = (value: string, forced?: Boolean) => {
        const filterQueryStrings: { [key: string]: string } = {}

        // Get all our checkboxes
        const checkboxes = document.querySelectorAll('form[name="filters"] input[type="checkbox"]')

        // Find out, if at least one checkbox is checked
        const checkedOneAtLeast = Array.prototype.slice.call(checkboxes).some(elem => elem.checked)

        // Start with all objects
        const FilteredObjectsIndices: number[] = []
        theObjects.map(function (object, index) {
            FilteredObjectsIndices.push(index)
        })

        // Is there one checkbox selected at least?
        if (checkedOneAtLeast) {
            // ===== Need to find the filtered (remaining) objects =====

            // Walk trough all filters
            filters.forEach((filter) => {
                // Get checked checkboxes of this filter
                const queryStr = 'form[name="filters"] #headline-' + filter.id + '+div input[type="checkbox"]:checked'
                const filterCB = document.querySelectorAll<HTMLElement>(queryStr)

                // Is this filter used?
                if (filterCB.length) {
                    filterQueryStrings[filter.id] = filter.id + ':' // For URL query string
                    for (const thisCB of filterCB) {
                        filterQueryStrings[filter.id] += thisCB.id.split('-')[1] + ';'
                    }

                    // Walk through all remaining objects
                    // eslint-disable-next-line id-length
                    for (let i = FilteredObjectsIndices.length - 1; i >= 0; i--) {
                        const objIndex = FilteredObjectsIndices[i]
                        // Check if object matches any of the checked options
                        let keepObject = false
                        for (const thisCB of filterCB) {
                            if (isMatching(theObjects[objIndex], thisCB, filter)) {
                                keepObject = true
                                break
                            }
                        }
                        if (!keepObject) {
                            // Remove index from array
                            const index = FilteredObjectsIndices.indexOf(objIndex, 0)
                            if (index > -1) {
                                FilteredObjectsIndices.splice(index, 1)
                            }
                        }
                    }
                }
            })
        }
        // Start with empty array
        const theFilteredObjects: any[] = []

        // Add remaining objects
        for (const index of FilteredObjectsIndices) {
            theFilteredObjects.push(theObjects[index])
        }
        parentCallback(JSON.stringify(theFilteredObjects))

        if (forced) {
            return
        }

        // we always track filters in query param
        let queryParam = ''
        for (const key in filterQueryStrings) {
            queryParam += filterQueryStrings[key]
        }
        if (queryParam != '') {
            queryParam = queryParam.slice(0, -1)
        }
        const newQueryParams = updateQueryParameter(getQueryParams(router.query, router.asPath), QUERY_PARAM, queryParam)

        // If anything has changed, then set new param string
        if (newQueryParams != false) {
            router.push({ pathname, query: newQueryParams }, undefined, { shallow: true })
        }
    }

    // Resets all checkboxes
    const resetFilters = () => {
        // Get all checked checkboxes
        const checkboxes: HTMLInputElement[] = document.querySelectorAll('form[name="filters"] input[type="checkbox"]:checked') as any
        checkboxes.forEach(elem => {
            elem.checked = false
        })
        processChange('')
    }

    // Initialize filters, e.g. count matching objects
    getInitialCount(theObjects, filters)
    // const { filteredServices, activeTagFilters, filterToParams /**/ } = useServiceFilter(theObjects, router.query.filter)

    useEffect(() => {
        // Check router.query for our parameter
        const param = validateQueryParameter(router.query, QUERY_PARAM)

        // Reset all checkboxes
        const checkboxes = document.querySelectorAll('form[name="filters"] input[type="checkbox"]')
        checkboxes.forEach(function (thisCheckbox: HTMLInputElement) {
            thisCheckbox.checked = false
        })

        let filterGroup = ''
        param.forEach(function (myString) {
            // Maybe filter group + 1st value
            const filter1stvalue = myString.split(':')

            // New filter group?
            if (filter1stvalue.length == 2) {
                filterGroup = filter1stvalue.shift()
            }
            const cbId = filterGroup + '-' + filter1stvalue[0]
            const elem = document.getElementById(cbId) as HTMLInputElement | null
            if (elem != null) {
                elem.checked = true
            }
        })
        handleChange2(checkboxes[0].id)
    }, [router.query])

    return (
        <div className="mr-2">
            <form name="filters" className="md:block md:mt-4">
                { filters.map((filter, index) => (
                    <div className={`border-2 mb-${(filters.length - index) > 1 ? '4' : 'px'} rounded-md`} key={filter.id}>
                        { /* Überschrift für Filterkategorie */}
                        <fieldset>
                            <BlockHeader 
                              id={filter.id} 
                              label={filter.label} 
                              color={filter.color || DEFAULT_COLORS} 
                              icon={filter.icon} 
                              openCloseMode={filter.openCloseMode} 
                            />

                            { /* Block with items */}
                            <div className={`items-wrap text-sm p-1 transition-all ${filter.openCloseMode == OpenClose.DefaultClose ? 'hidden' : ''}`}>
                                { filter.options.map((option) =>
                                    // Do we want to show the checkbox?
                                    (option.initialCount > 0
                                        ? <div key={option.id} className="flex px-0.5 py-1 hover:cursor-pointer">
                                            <input
                                                id={`${filter.id}-${option.id}`}
                                                name={`${filter.id}[]`}
                                                defaultValue={`${filter.id}:${option.id}`}
                                                defaultChecked={router.query.filter ? router.query.filter.includes(`${filter.id}:${option.id}`) : false}
                                                type="checkbox"
                                                className="h-4 w-4 mt-0.5 border-gray-300 rounded text-indigo-600 focus:ring-indigo-500  hover:cursor-pointer"
                                                aria-label={option.label}
                                                onChange={handleChange}
                                            />
                                            <label
                                                htmlFor={`${filter.id}-${option.id}`}
                                                className="ml-3 text-sm text-gray-500  hover:cursor-pointer"
                                            >
                                                {option.label}<span aria-hidden='true'> (</span>{option.initialCount}<span aria-hidden='true'>)</span>
                                            </label>
                                        </div>
                                        : <div key={option.id} className="flex px-0.5 py-1">
                                            <input
                                                id={`${filter.id}-${option.id}`}
                                                name={`${filter.id}[]`}
                                                type="checkbox"
                                                className="h-4 w-4 mt-0.5 border-gray-300 rounded text-indigo-600 focus:ring-indigo-500 invisible"
                                                aria-label={`${option.label}, nicht aktiviert`}
                                                aria-disabled="true"
                                            />
                                            <label
                                                htmlFor={`${filter.id}-${option.id}`}
                                                className="ml-3 text-sm text-gray-500"
                                                aria-disabled="true"
                                            >
                                                {option.label}<span aria-hidden='true'> (</span>{option.initialCount}<span aria-hidden='true'>)</span>
                                            </label>
                                        </div>
                                    )
                                )}
                            </div>
                        </fieldset>
                    </div>
                ))}
            </form>
        </div>
    )
})
