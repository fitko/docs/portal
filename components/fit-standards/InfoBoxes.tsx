/* eslint-disable id-length */
import MY_CONSTANTS from '@/lib/constants-standards.js'
import { ContactInformation } from '@/components/fit-standards/ContactInformation'
import { FinancingInformation } from '@/components/fit-standards/FinancingInformation'
import { ChildResource, LinkItem, Liability, Framework } from 'types/content'
import { UNDEFINED_LIABILITY } from 'types/content/Liability'
import {  IconCirclesRelation,
  IconCategory2,
  IconFileCheck
} from '@tabler/icons-react'
import Link from 'next/link'
import useImgPath from 'shared/use-img-path'

import { licenses, frameworks } from 'lib/assets/data/fit-standards/FilterSortDefs'

const { getImgPath } = useImgPath()

type Contact = {
    name: string
    link: string
    mail: string
    phone: string
    contactform: string
}

type InfoboxOptions = {
    funder: Contact
    productOwner: Contact
    developer: Contact
    operator: Contact
    contact: Contact
    initiator: Contact
    steeringCommittee: Contact
    supportInfo: LinkItem[]
    framework: Framework
    liability: Liability[]
    artefacts: LinkItem[]
    combinedStandards: ChildResource[]
    slugMaster: string
    currentSlug: string
    lastUpdate: Date
}

function getArtefactName(artefact: LinkItem) {
    const found = MY_CONSTANTS.ARTEFACTS.filter((item) => item.id === artefact.name)[0]
    return (found === undefined ? artefact.name : found.text)
}

export function InfoBoxes({
    funder,
    initiator,
    productOwner,
    steeringCommittee,
    developer,
    operator,
    contact,
    supportInfo,
    artefacts,
    combinedStandards,
    slugMaster,
    currentSlug,
    lastUpdate,
    framework,
    liability,
}: InfoboxOptions) {
    framework = new Framework(framework)
    return (
        <div className= "infoboxes" >

            {/* INFORMATION RE. ROLES */}
            <ContactInformation
                initiator = { initiator }
                productOwner={ productOwner }
                steeringCommittee = { steeringCommittee }
                developer = { developer }
                operator = { operator }
                contact = { contact }
                funder = { funder }
            />

            {/* SUPPORT LINKS */}
            {supportInfo.length > 0 && supportInfo[0].title != null && supportInfo[0].title != '' && (
                <div className="sm:w-96 md:w-80 border-2 mb-4 rounded-md">
                    <div className={`${MY_CONSTANTS.COLORS.DEFAULT} p-1 font-bold`}>Änderungsmanagement</div>
                    <div className="text-sm p-1">
                        {
                            (supportInfo.map((myItem: LinkItem, key) => {
                                return (
                                    <div className="flex" key={`${key}`}>
                                        { (myItem.url != null && myItem.url != '' && myItem.url != 'n.v.'
                                            ? <div className={'leading-7  text-gray-900 ' + `${myItem.title}`}>
                                                <Link href={myItem.url} target="_blank">{myItem.title}</Link>
                                            </div>
                                            : null
                                        )}
                                    </div>
                                )
                            }))
                        }
                    </div>
                </div>
            )}

            {/* ARTEFACTS */}
            {artefacts.length > 0 && artefacts[0].name != '' && (
                <div className="sm:w-96 md:w-80 border-2 mb-4 rounded-md">
                    <div className={`${MY_CONSTANTS.COLORS.DEFAULT} p-1 font-bold`}>Artefakte</div>
                    <div className="text-sm p-1">
                        {
                            (artefacts.map((myItem: LinkItem, key) => {
                                return (
                                    <div className="flex justify-between mt-2" key={`${key}`}>
                                        <div className="">
                                            { myItem.url === 'n.v.'
                                                ? <div className="text-slate-300" aria-disabled='true'>{getArtefactName(myItem)}</div>
                                                : (myItem.hasLink
                                                    ? <Link href={myItem.url} target="_blank">{getArtefactName(myItem)}</Link>
                                                    : ''
                                                )
                                            }
                                        </div>
                                        { myItem.hasLicense
                                            ? <div className="license mt-1 shrink-0" aria-disabled='true'>
                                                <Link
                                                    href={ (licenses.find((myObj) => { return myObj.licenseID === myItem.license })).licenseURL}
                                                    target="_blank"
                                                >
                                                    <img
                                                        src={ getImgPath('public/img/licenses/' + myItem.license + '.png')}
                                                        className="h-6 -mt-1"
                                                        title={ (licenses.find((myObj) => { return myObj.licenseID == myItem.license })).tooltipText }
                                                        alt={ (licenses.find((myObj) => { return myObj.licenseID == myItem.license })).fullTitle }
                                                    />
                                                </Link>
                                            </div>
                                            : ''
                                        }
                                    </div>
                                )
                            }))
                        }
                    </div>
                </div>
            )}

            {/* COMBINED STANDARDS */}
            {combinedStandards.length > 0 && combinedStandards[0].name != null && combinedStandards[0].name != '' && (
                <div className="sm:w-96 md:w-80 border-2 mb-4 rounded-md">
                    <div className={`${MY_CONSTANTS.COLORS.COMBINED_STANDARDS} p-1 font-bold flex items-center`}>
                    <IconCirclesRelation className="mr-2" size={20} />
                      Verbundene IT-Standards
                    </div>
                    <div className="text-sm p-1">
                        {
                            (combinedStandards.filter((item) => item.slug != currentSlug))
                                .map((myItem: ChildResource, key) => {
                                    return (
                                        <div className="flex" key={`${key}`}>
                                            {myItem.slug == slugMaster
                                                ? <div className={'leading-7 ' + 'font-bold ' + `${myItem.name}`}>
                                                    <Link href={myItem.slug} passHref>
                                                        {myItem.name}
                                                    </Link>
                                                </div>
                                                : <div className={'leading-7 ' + `${myItem.name}`}>
                                                    <Link href={myItem.slug}>{myItem.name}</Link>
                                                </div>
                                            }
                                        </div>
                                    )
                                })
                        }
                    </div>
                </div>
            )}

            {/* FRAMEWORK */}
            {framework.initialized && (
                <div className="sm:w-96 md:w-80 border-2 mb-4 rounded-md">
                    <div className={`${MY_CONSTANTS.COLORS.DEFAULT} p-1 font-bold`}>Methode/Framework</div>
                    <div className="text-sm p-1 text-gray-900">
                        <div className="flex justify-between mt-2" key={`${framework.id}`}>
                            <div className="">
                                <Link href={ (frameworks.find((myObj) => { return myObj.frameworkID === framework.id })).frameworkURL } target="_blank">
                                    <div className="">{ (frameworks.find((myObj) => { return myObj.frameworkID === framework.id })).fullTitle }</div>
                                </Link>
                            </div>
                            <div className="framework mt-1 shrink-0" aria-disabled='true'>
                                <Link
                                    href={ (frameworks.find((myObj) => { return myObj.frameworkID === framework.id })).frameworkURL}
                                    target="_blank"
                                >
                                    <img
                                        src={ getImgPath('public/img/frameworks/' + framework.id + '.png')}
                                        className="h-6 -mt-1"
                                        title={ (frameworks.find((myObj) => { return myObj.frameworkID == framework.id })).tooltipText }
                                        alt={ (frameworks.find((myObj) => { return myObj.frameworkID == framework.id })).fullTitle }
                                    />
                                </Link>
                            </div>
                        </div>
                    </div>
                </div>
            )}

            {/* LIABILITY */}
            {liability.length > 0 && liability[0].initialized && (
                <div className="sm:w-96 md:w-80 border-2 mb-4 rounded-md">
                  <div className={`${MY_CONSTANTS.COLORS.LIABILITY} p-1 font-bold rounded-t-md flex items-center`}>
                    <IconFileCheck className="mr-2" size={20} />
                      Gültigkeit
                  </div>
                  <div className="text-sm p-1 text-gray-900">
                    {liability
                      .filter((item, index) => {
                        // Always show the first item
                        if (index === 0) return true;
                        // Show second item only if first item is not "Nicht definiert"
                        const firstLiability = liability[1].liability;
                        return firstLiability !== UNDEFINED_LIABILITY;
                      })
                      .map((item, index) => (
                        <div className={`flex ${index > 0 ? 'mt-1' : ''}`} key={index}>
                          <div>
                            {item.hasLink
                              ? <Link href={item.url} target="_blank">
                                  <div className="font-semibold">{item.liability}</div>
                                </Link>
                              : <div className="font-semibold">{item.liability}</div>
                            }
                            {item.hasDate && (
                              <div className="flex">
                                <div className={`${MY_CONSTANTS.INFOBOX_1ST_COLUMN_WIDTH} font-semibold`}>
                                  Ab/seit
                                </div>
                                <div>
                                  {item.dateDate.toLocaleDateString('de-DE', {
                                    year: 'numeric',
                                    month: 'long',
                                    day: '2-digit'
                                  })}
                                </div>
                              </div>
                            )}
                          </div>
                        </div>
                      ))}
                  </div>
                </div>
            )}
            
            {/* LAST UPDATE */}
            {lastUpdate && (
                <div className="sm:w-96 md:w-80 border-2 mb-4 rounded-md">
                    <div className={`${MY_CONSTANTS.COLORS.DEFAULT} p-1 font-bold`}>Letzte Aktualisierung</div>
                    <div className="text-sm p-1 text-gray-900">
                        <div className="flex">
                            <div className={`${MY_CONSTANTS.INFOBOX_1ST_COLUMN_WIDTH} font-semibold`}>Datum</div>
                            <div>{lastUpdate.toLocaleDateString('de-DE', { year: 'numeric', month: 'long', day: '2-digit' })}</div>
                        </div>
                    </div>
                </div>
            )}
        </div>
    )
}
