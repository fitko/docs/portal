/* ===== Render a text label =====
 *
 * The label may be a scoped label (delimited by '::')
 * The component also supports rendering a symbol before the text starts.
 * The label color can be stated as Tailwind color classes (bg, border and text colors)
 * - if not stated, the default color is used
 * 
 */

import { IconThumbUpFilled, IconExclamationMark, IconCirclesRelation } from '@tabler/icons-react'
import {If, Then, Else} from 'react-if'
import Link from 'next/link'
import MY_CONSTANTS from '@/lib/constants-standards.js'

export type LabelParams = {
    name: string
    role?: string
    link?: string
    colors?: string
    icon?: string
}

export type RenderTextParams = {
  hasScope: boolean
  isScope: boolean
  text: string
  colors: string
  icon?: string
}


/* Render text label
 * - Can be the scope or just the label
 * - Can have an icon
 * 
 * Input:
 *  - hasScope: true if it's about a scoped label
 *  - isScope: true if we currently printing the scope
 *  - text: the text to print
 *  - colors: the color classes (Tailwind) to be used
 *  - icon: the icon to be used (if any)
 */
function RenderText(props: RenderTextParams): JSX.Element {
    const tabIndexValue = -1
    let icon = props.icon
    let borders = 'rounded-full'
    if (props.hasScope) {
        if (props.isScope) {  // Render Scope!
           borders = 'rounded-l-2xl'
        } else {  // Render Label!
           borders = 'rounded-r-2xl'
           icon = undefined
        }
    }
    return (
        <div className="flex items-center">
            <div
                className={`flex flex-nowrap text-xs font-medium px-1 leading-6 my-0 border-2 ${borders} ${props.colors}`}
            >
              <If condition={props.icon != undefined}>
                  <Then>
                    <div className="flex-shrink-0 w-4 ml-1 mr-2 flex align-bottom">
                      <img
                        src={icon}
                        alt="Icon"
                      />
                    </div>
                  </Then>
              </If>
              <p tabIndex={tabIndexValue} className='leading-normal'>{props.text}</p>
            </div>
        </div>
    )
}


export default function Label({ name, link, colors, icon}: LabelParams) {
    let scope = ''
    // If colors are not defined, use default colors
    if (colors == undefined) {
      colors = MY_CONSTANTS.COLORS.DEFAULT // Use default colors
    }
    let colorClassesName = colors, colorClassesScope = colors

    // Are we dealing with a scoped label?
    const pos = name.indexOf('::')
    if (pos >= 0) {
        scope = name.substring(0, pos)
        name = name.substring(pos + 2)
        colorClassesName = colorClassesName
            .replace(/\bbg-[^\s]+/, 'bg-white')       // Remove bnackground color, if scoped label
            .replace(/\btext-[^\s]+/, 'text-black')   // Remove text color, if scoped label
    }

    return (
        (link != undefined
            ? <Link href={link} className='text-xs px-2 py-1 my-1 inline-flex'>
                <If condition={scope != ''}>
                    <Then>
                        <RenderText hasScope={scope!=''} isScope={true} text={scope} colors={colorClassesScope} icon={icon} />
                    </Then>
                </If>
                <RenderText hasScope={scope!=''} isScope={false} text={name} colors={colorClassesName} icon={icon} />
            </Link>
            : <div className='text-xs mb-2 px-2 inline-flex relative'>
                <If condition={scope != ''}>
                    <Then>
                        <RenderText hasScope={scope!=''} isScope={true} text={scope} colors={colorClassesScope} icon={icon} />
                    </Then>
                </If>
                <RenderText hasScope={scope!=''} isScope={false} text={name} colors={colorClassesName} icon={icon} />
            </div>
        )
    )

    /*
    <div className={`${MY_CONSTANTS.BG_COLORS.ROLES} inline-flex items-center text-xs px-2 py-1 my-2 mr-4 rounded-full`}>
            <div className="flex-shrink-0 w-4 ml-1 mr-2 max-h-3 flex align-bottom">
                <img
                    src={getImgPath(`/img/${rootFolder}/roles/${MY_CONSTANTS.CONTACT_TYPES[3].id}.svg`)}
                    alt={`${MY_CONSTANTS.CONTACT_TYPES[3].text}`}
                />
            </div>
            <div>{service.operator.organisation}</div>
        </div>
    </div>
    */

}
