import useImgPath from 'shared/use-img-path'
import MY_CONSTANTS from '@/lib/constants-standards.js'

const { getImgPath } = useImgPath()

type LifecylceOptions = {
    status: string
  }
// Display the image corresponding to the lifecycle phase
export default (props: LifecylceOptions) => {
    const status = props.status
    const imagePath = `/img/fit-standards/lifecycle/${status}.svg`

    if (MY_CONSTANTS.LYFE_CYCLE_PHASES.includes(status)) {
        return (
            <div className="border border-gray-300 rounded-lg overflow-hidden pt-1">
                {/* Die Next.js Image-Komponente verwendet das "src" Attribut */}
                <img
                    src={getImgPath(imagePath)}
                    alt={`Der FIT-Standard befindet sich in der Phase ${status}`}
                    width={500} // Breite des Bildes
                    height={0} // Höhe des Bildes
                />
            </div>
        )
    } else {
        return (
            <div className="border border-gray-300 rounded-lg overflow-hidden p-1">
                #INVALID! (tags.status)
            </div>

        )
    }
}
