/*
 *  ***** Configurable list sorter component *****
 *
 *  This component implements a configurable list sorter.
 *
 *  BASIC CONCEPT
 *  =============
 *  A data array (array of objects) is sorted by configurable sort criterias.
 *
 *  The available sort criterias must be defined prior of using the component.
 *  They are handed over to the component by a parameter.
 *
 *  The component itself renders the dropdown list for selecting the sort order.
 *  It returns the sorted data array according to the selected sort criteria.
 *
 */
'use client'

import React, { useEffect } from 'react'
import { useRouter } from 'next/router'
import { validateQueryParameter, updateQueryParameter } from 'lib/utils/handleUrlParameters'

export enum SortOrder {
    // eslint-disable-next-line no-unused-vars
    ascending,
    // eslint-disable-next-line no-unused-vars
    descending
}

export type SortCriteria = {
    id: string
    label: string
    sortOrder: SortOrder
    getValueFunc: (elem:any) => any
}

export type ListSortOptions = {
    theObjects: Array<any>
    sortOrders: SortCriteria[]
    parentCallback: (json: string) => void
}

const QUERY_PARAM = 'sortby'

export function ListSort({
    theObjects,
    sortOrders,
    parentCallback,
}: ListSortOptions) {
    const router = useRouter()
    const { pathname } = useRouter()

    // The regular event handler (onClick)
    function handleChange(event) {
        event.preventDefault()
        const value = (event.target as HTMLInputElement).value
        processChange(value)
    }

    // Used to align with URL parameter
    // eslint-disable-next-line no-unused-vars
    function handleChange2(value: string) {
        processChange(value, true)
    }

    function processChange(value: string, forced?: boolean) {
        const sortC = sortOrders.find((order) => {
            return order.id == value
        })
        if (sortC) {
            theObjects.sort(function (aVal, bVal) {
                if (sortC.getValueFunc(aVal) < sortC.getValueFunc(bVal)) {
                    return (sortC.sortOrder == SortOrder.ascending ? -1 : 1)
                } else if (sortC.getValueFunc(aVal) == sortC.getValueFunc(bVal)) {
                    return 0
                } else {
                    return (sortC.sortOrder == SortOrder.ascending ? 1 : -1)
                }
            })
            parentCallback(JSON.stringify(theObjects))

            if (forced) {
                return
            }

            // we always track sort order in query param
            const newQueryParams = updateQueryParameter(router.query, QUERY_PARAM, sortC.id)

            // If anything has changed, then set new param string
            if (newQueryParams != false) {
                router.push({ pathname, query: newQueryParams }, undefined, { shallow: true })
            }
        }
    }

    useEffect(() => {
        // Check router.query for our parameter
        const param = validateQueryParameter(router.query, QUERY_PARAM)

        // Parameter found?
        if (param.length) {
            // Get SELECT element and set value
            const selectElem: HTMLSelectElement | null = document.querySelector('#list-sort-dropdown')
            if (selectElem) {
                selectElem.value = param[0]
                handleChange2(param[0])
            }
        }
    }, [router.query])

    return (
        <form className="list-sort-form w-100 md:w-auto mb-4">
            <label className="list-sort-label text-xs sm:mx-2" htmlFor="list-sort-dropdown">Sortieren nach:</label>
            <select className="list-sort-dropdown text-xs md:text-sm px-1 sm:px-4 w-full sm:w-fit rounded focus:ring-gray-400" name="list-sort-dropdown" id="list-sort-dropdown" onChange={handleChange}>
                {sortOrders.map((order) => (
                    <option key={order.id} value={order.id} >{order.label}</option>
                ))}
            </select>
        </form>
    )
}

export function UpdateListSort(
    theObjects: Array<any>,
    sortOrders: SortCriteria[],
    parentCallback: (arg0: string) => void
) {
    const value = (document.querySelector('.list-sort-dropdown') as HTMLInputElement).value

    const sortC = sortOrders.find((order) => {
        return order.id == value
    })
    if (sortC) {
        theObjects.sort(function (aVal, bVal) {
            if (sortC.getValueFunc(aVal) < sortC.getValueFunc(bVal)) {
                return (sortC.sortOrder == SortOrder.ascending ? -1 : 1)
            } else if (sortC.getValueFunc(aVal) == sortC.getValueFunc(bVal)) {
                return 0
            } else {
                return (sortC.sortOrder == SortOrder.ascending ? 1 : -1)
            }
        })
        parentCallback(JSON.stringify(theObjects))
    }
}
