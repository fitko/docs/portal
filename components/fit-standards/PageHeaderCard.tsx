import React, { ReactElement } from 'react'
import { IconBooks } from '@tabler/icons-react'
import LifecyclePicture from '@/components/fit-standards/LifecyclePicture'
import Label from '../Label'
import MY_CONSTANTS from '@/lib/constants-standards.js'
import { Context } from 'types/content'

const iconSvgCategory2Path = '/img/fit-standards/tabler/icons-react/category-2.svg'

type HeaderOptions = {
  title: string
  description: string
  contextRelation: string[]
  contextContent: string[]
  img?: {
    src: string
    alt?: string
  }
  status: string
  typeOfStandard: string
  children?: ReactElement[]
}

export default ({ title, contextRelation, contextContent, description, img, status, typeOfStandard, children }: HeaderOptions) => {
    return (
        <section className="flex stretch bg-white w-full mx-auto relative z-10" aria-labelledby="heading">
            <h2 className="sr-only" id="heading">{title}</h2>
            <div className="flex flex-col bg-white">
                <div className="flex-1 relative py-4 px-2 sm:px-6 md:px-8">

                    {/* Icon + Title */}
                    <header className={'flex pb-2 sm:items-center flex-col sm:flex-row align-bottom'}>
                        {img && img.src
                            ? (
                                <img
                                    className={'h-8 w-min sm:h-10 pr-4'}
                                    src={img.src}
                                    alt={img.alt}
                                    aria-hidden='true'
                                />
                            )
                            : (
                                <IconBooks className='hidden w-24 h-24'/>
                            )}
                        <h1 className="text-2xl sm:text-3xl lg:text-4xl tracking-tight text-gray-900 overflow-hidden break-words hyphens-auto">
                            {title}
                        </h1>
                    </header>

                    {/* Life cycle phase */}
                    <div className="flex">
                        <LifecyclePicture status = {status} />
                    </div>

                    <hr className="my-6" />

                    {/* Labels */}
                    {(contextRelation.length > 0 || contextContent.length > 0 || typeOfStandard) && (
                      <div className='flex flex-wrap mt-2 -mx-2 justify-end'>
                        {/* Beziehungskontext */}
                        {contextRelation.length>0 && contextRelation.map((id) => (
                          <Label key={id} name={'Beziehung::' + id} colors={MY_CONSTANTS.COLORS.CONTEXT_RELATION}  />
                        ))}
                        
                        {/* Inhaltlicher Kontext */}
                        {contextContent.length>0 && contextContent.map((id) => (
                          <Label key={id} name={'Inhalt::' + (MY_CONSTANTS.CONTEXT_CONTENT[id] || id)} colors={MY_CONSTANTS.COLORS.CONTEXT_CONTENT}  />
                        ))}

                        {/* Standard Type */}
                        {typeOfStandard && (
                          <Label key="type" name={'Typ::' + typeOfStandard} colors={MY_CONSTANTS.COLORS.STANDARD_TYPE} icon="IconCategory2" />
                        )}

                      </div>
                    )}

                    <div className="flex flex-col md:flex-row md:items-center">
                        <div
                            className="cms-blog-text text-xl text-gray-800 mt-2 md:flex-1"
                            dangerouslySetInnerHTML={{ __html: description }}
                        />
                    </div>
                </div>
                {
                    (children !== undefined
                        ? <div className="p-6 bg-gray-50 rounded-b-lg md:px-8 flex flex-row items-center gap-16">
                            {children}
                        </div>
                        : null
                    )
                }
            </div>
        </section>

    )
}
