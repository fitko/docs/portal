import React, { ReactElement } from 'react'
import { IconBooks } from '@tabler/icons-react'
import LifecyclePicture from '@/components/fit-standards/LifecyclePicture'
import Label from './Label'
import MY_CONSTANTS from '@/lib/constants-standards.js'
import { Context } from 'types/content'

type HeaderOptions = {
  title: string
  description: string
  contextRelation: string[]
  contextContent: string[]
  img?: {
    src: string
    alt?: string
  }
  status: string
  children?: ReactElement[]
}

export default ({ title, contextRelation, contextContent, description, img, status, children }: HeaderOptions) => {
    return (
        <section className="flex stretch bg-white w-full mx-auto relative z-10" aria-labelledby="heading">
            <h2 className="sr-only" id="heading">{title}</h2>
            <div className="flex flex-col bg-white">
                <div className="flex-1 relative py-4 px-2 sm:px-6 md:px-8">

                    {/* Icon + Title */}
                    <header className={'flex space-x-6 pb-2 items-center'}>
                        {img && img.src
                            ? (
                                <img
                                    className={'h-8 sm:h-10'}
                                    src={img.src}
                                    alt={img.alt}
                                    aria-hidden='true'
                                />
                            )
                            : (
                                <IconBooks className='hidden w-24 h-24'/>
                            )}
                        <h1 className="text-2xl sm:text-3xl lg:text-4xl tracking-tight text-gray-900 overflow-hidden">
                            {title}
                        </h1>
                    </header>

                    {/* Life cycle phase */}
                    <div className="flex">
                        <LifecyclePicture status = {status} />
                    </div>

                    <hr className="mt-6 mb-4" />

                    {/* Detailed description */}
                    <div className="text-2xl font-semibold">Detailinformationen</div>

                    {/* Labels */}
                    {(contextRelation.length > 0 || contextContent.length > 0) && (
                      <div className='flex flex-wrap mt-2'>
                        {/* Beziehungskontext */}
                        {contextRelation.length>0 && contextRelation.map((id) => (
                          <Label key={id} name={'Beziehung::' + id} colors={MY_CONSTANTS.COLORS.CONTEXT_RELATION}  />
                        ))}
                        
                        {/* Inhaltlicher Kontext */}
                        {contextContent.length>0 && contextContent.map((id) => (
                          <Label key={id} name={'Inhalt::' + MY_CONSTANTS.CONTEXT_CONTENT[id]} colors={MY_CONSTANTS.COLORS.CONTEXT_CONTENT}  />
                        ))}
                      </div>
                    )}

                    <div className="flex flex-col md:flex-row md:items-center">
                        <div
                            className="cms-blog-text text-xl text-gray-800 mt-5 md:flex-1"
                            dangerouslySetInnerHTML={{ __html: description }}
                        />
                    </div>
                </div>
                {
                    (children !== undefined
                        ? <div className="p-6 bg-gray-50 rounded-b-lg md:px-8 flex flex-row items-center gap-16">
                            {children}
                        </div>
                        : null
                    )
                }
            </div>
        </section>

    )
}
