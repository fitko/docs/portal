import { useRouter } from 'next/router'
import { Footer } from '@/views/layout/Footer'
import { useEffect } from 'react'
import NavigationHeader from '../views/layout/NavigationHeader'
import useBasePath from 'shared/use-base-path'
import useTranslation from 'shared/use-translation'

export function Layout({ children }) {
    const { getLocaleFromPath } = useBasePath()
    const router = useRouter()
    const { asPath } = router

    const currentLocale = getLocaleFromPath(asPath)
    const { translate } = useTranslation(currentLocale)

    useEffect(() => {
        // Do NOT add class 'absolute' if FIT standard
        if (!router.asPath.includes('/fit-standards/')) {
            document.body.classList.add('absolute')
        } else {
            document.body.classList.remove('absolute') // Might have been set before, so remove it!
            // Set language (workaround, since using i18n in next.config.js currently isn't working as expected)
            document.documentElement.lang = currentLocale
            if (document.title === '') {
                // Default title FIT-standards
                document.title = translate('fitstandards.common.title')
            }
        }
        document.body.classList.add('lg:relative')
    })

    return (
        <>
            <NavigationHeader/>
            {/* Don't keep distance if FIT standard */}
            { router.asPath.includes('/fit-standards/')
                ? <main className=''>{children}</main>
                : <main className='lg:py-10'>{children}</main>
            }
            <Footer
                lang={currentLocale}
                logo={{
                    img: 'fitko-main-logo.svg',
                    alt: translate('alt.fitko'),
                }}/>
        </>
    )
}
