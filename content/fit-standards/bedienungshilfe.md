---
Seiteninhalt: Inhalte für Hilfeseite, Infoportal (#376)
---

# Bedienungshilfe und Erläuterungen

Auf dieser Seite erfahren Sie alles über die Bedienung der
<span class="font-semibold">Informationsplattform für Föderale IT-Standards (FIT-Standards)</span>.

Im einzelnen werden die folgenden Themen behandelt:

<ul class="font-semibold">
    <li><a href="#uebersicht">Übersicht</a></li>
    <li><a href="#stdag">Zentrale Elemente der Standardisierungsagenda</a>
        <ul>
            <li><a href="#bedeutung_lz">Das Lebenszyklusmodell</a></li>
            <li><a href="#bedeutung_rollen">Relevante Rollen</a></li>
            <li><a href="#verbundene_standards">Verbundene FIT-Standards</a></li>
            <li><a href="#bedeutung_gueltigkeit">Gültigkeit eines FIT-Standards</a></li>
        </ul>
    </li>
    <li><a href="#listenansicht">Die Listenansicht</a>
        <ul>
            <li><a href="#filtern">Filterfunktionen</a></li>
            <li><a href="#sortieren">Sortieren der Liste</a></li>
            <li><a href="#struktur_listenansicht">Struktur eines Eintrags in der Listenansicht</a></li>
        </ul>
    </li>
    <li><a href="#detailansicht">Die Detailansicht</a>
        <ul>
            <li><a href="#info-blocks">Informationsboxen und ihr Inhalt</a></li>
        </ul>
    </li>
    <li><a href="#dokumentationsportal">Das Dokumentationsportal der Standardisierungsagenda</a></li>
    <li><a href="#more-nav">Weitere Navigationsmöglichkeiten</a></li>
</ul>

<h2 id="uebersicht">Übersicht</h2>

Die Informationsplattform bietet in der <span class="font-semibold">Listenansicht</span> eine Auflistung
aller FIT-Standards. Die Aufnahme eines FIT-Standards erfolgt, sobald
für diesen eine Bedarfsmeldung vorliegt. Ab diesem Zeitpunkt begleitet
die Informationsplattform einen FIT-Standard über dessen gesamten
[Lebenszyklus](https://docs.fitko.de/standardisierungsagenda/docs/einfuehrung/#lebenszyklus), bis zu seinem Lebensende: der finalen Dekommissionierung. Dabei sind die verschiedenen Phasen des Lebenszyklus definiert.

Klicken Sie auf den Namen oder das Logo eines FIT-Standards, um zur
<span class="font-semibold">Detailansicht</span> dieses FIT-Standards zu gelangen.\
Die Detailansicht enthält umfassende Informationen zu dem jeweiligen
FIT-Standard, beispielsweise Beschlusslage, Kontaktdaten,
Supportangebote sowie verfügbare Artefakte.

<h2 id="stdag">Zentrale Elemente der Standardisierungsagenda</h2>

Die Standardisierungsagenda dient der Priorisierung und Steuerung von Aktivitäten sowie Schaffung von Transparenz rund um den Lebenszyklus föderaler IT-Standards.  
Dieses Informationsportal baut auf dem Konzept der Standardisierungsagenda auf. Wesentliche Elemente der Standardisierungsagenda erläutern wir daher kurz.

<h3 id="bedeutung_lz">Das Lebenszyklusmodell</h2>

Innerhalb der Standardisierungsagenda ist der Lebenszyklus für einen
FIT-Standard das zentrale Element. Die folgende Grafik benennt die
Phasen und ordnet die Verantwortung jeweils einer bestimmten Rolle zu.

<div class="img-wrap p-2 hidden sm:block">
    <img src="../../img/fit-standards/lifecycle/Lebenszyklus_new_Rollen%20-%20no%20links.png" alt="Lebenszyklusmodell der Standardisierungsagenda" />
</div>
<img class="sm:hidden mb-2" src="../../img/fit-standards/lifecycle/Lebenszyklus_new_Rollen%20-%20no%20links.png" alt="Lebenszyklusmodell der Standardisierungsagenda" />

[Vertiefende Informationen zum
Lebenszyklus](https://docs.fitko.de/standardisierungsagenda/docs/einfuehrung/)
halten wir im Dokumentationsportal der Standardisierungsagenda für Sie
bereit.

<h3 id="bedeutung_rollen">Relevante Rollen</h3>

Für die Informationsplattform sind besonders diese Rollen relevant:

<div class="sm:ml-4 mb-4 max-w-2xl ">
    <div class="flex flex-row bg-slate-100">
        <div class="flex justify-center w-24 p-1 sm:p-2 font-semibold">Symbol</div>
        <div class="p-1 sm:p-2 font-semibold">Bedeutung</div>
    </div>
    <div class="flex flex-row border-b">
        <div class="flex flex-none justify-center basis-24 p-2">
            <img
                class="h-6"
                src="../../img/fit-standards/roles/productOwner.svg"
                alt="Symbol für den Bedarfsträger"
            />
        </div>
        <div class="flex flex-col p-2">
            <div class="font-semibold">Bedarfsträger</div>
            <div class="text-xs sm:text-sm hyphens-auto">Ein Standardisierungs&shy;bedarf wird durch einen Bedarfs&shy;träger identi&shy;fi&shy;ziert und ver&shy;antwort&shy;lich bearbeitet.</div>
        </div>
    </div>
    <div class="flex flex-row border-b">
        <div class="flex flex-none justify-center basis-24 p-2">
            <img
                class="h-6"
                src="../../img/fit-standards/roles/developer.svg"
                alt="Symbol für die Umsetzung"
            />
        </div>
        <div class="flex flex-col p-2">
            <div class="font-semibold">Umsetzung</div>
            <div class="text-xs sm:text-sm hyphens-auto">Das Umsetzungs&shy;team realisiert einen Standardisierungs&shy;bedarf.</div>
        </div>
    </div>
    <div class="flex flex-row border-b">
        <div class="flex flex-none justify-center basis-24 p-2">
            <img
                class="h-6"
                src="../../img/fit-standards/roles/operator.svg"
                alt="Symbol für den Betrieb"
            />
        </div>
        <div class="flex flex-col p-2">
            <div class="font-semibold">Betrieb</div>
            <div class="text-xs sm:text-sm hyphens-auto">Der Betreiber überführt ge&shy;mein&shy;sam mit dem Umsetzungs&shy;team einen föderalen IT-Standard in den Regel&shy;betrieb und betreibt diesen anschließend. Am Ende des Lebenszyklus und nach Genehmigung des IT-Planungsrats führt der Betreiber die Dekommissionierung durch.</div>
        </div>
    </div>
    <div class="flex flex-row border-b">
        <div class="flex flex-none justify-center basis-24 p-2">
            <img
                class="h-6"
                src="../../img/fit-standards/roles/contact.svg"
                alt="Symbol für den Support"
            />
        </div>
        <div class="flex flex-col p-2">
            <div class="font-semibold">Support</div>
            <div class="text-xs sm:text-sm hyphens-auto">Der Support dient als Ansprechpartner für den IT-Standard und unterstützt diesen.</div>
        </div>
    </div>
    <div class="flex flex-row border-b">
        <div class="flex flex-none justify-center basis-24 p-2">
            <img
                class="h-6"
                src="../../img/fit-standards/roles/steeringCommittee.svg"
                alt="Symbol für die Steuerung"
            />
        </div>
        <div class="flex flex-col p-2">
            <div class="font-semibold">Steuerung</div>
            <div class="text-xs sm:text-sm hyphens-auto">Eine Steuerungsinstanz (Steuerungsprojekt, Steuerungsgremium, Arbeitskreis ...) sorgt für die Planung und die Umsetzung in den unterschiedlichen Lebenszyklusphasen eines einzelnen IT-Standards oder eines Verbunds von IT-Standards.</div>
        </div>
    </div>
    <div class="flex flex-row border-b">
        <div class="flex flex-none justify-center basis-24 p-2">
            <img
                class="h-6"
                src="../../img/fit-standards/roles/funder.svg"
                alt="Symbol für die Finanzierung"
            />
        </div>
        <div class="flex flex-col p-2">
            <div class="font-semibold">Finanzierung</div>
            <div class="text-xs sm:text-sm hyphens-auto">Mit der Finanzierung stellt der Auftraggeber die Umsetzung und den Regelbetrieb eines IT-Standards sicher.</div>
        </div>
    </div>
</div>

Mehr über die Beteiligten, ihre Aufgaben und Rollen im Lebenszyklus
von FIT-Standards erfahren Sie in entsprechenden [Erklärungen im Dokumentationsportal](https://docs.fitko.de/standardisierungsagenda/docs/rollen/).

<h3 id="bedeutung_gueltigkeit">Gültigkeit eines FIT-Standards</h3>

Die Verbindlichkeit eines FIT-Standard wird typischerweise durch den IT-Planungsrat festgelegt. Über die Gültigkeit hinaus ist in Klammern teilweise das beschlussfassende Gremium angegeben. So kann ein FIT-Standard auch vom Föderalen IT-Standardisierungsboard zur Anwendung empfohlen werden. Weiterhin können Rechtsverordnungen oder andere Vorgaben zur Anwendung des FIT-Standards verpflichten (beispw. OZG RV - Rechtsverordnung nach §6 OZG).

<h3 id="kontext">Kontext des FIT-Standards</h3>

Der **Beziehungskontext** gibt an in welchen Kommunikationsbeziehungen der IT-Standard angewendet wird (beispielsweise Government-to-Government, Business-to-Government, Citizen-to-Government, Business-to-Business).

Der **inhaltliche Kontext** zeigt an in welchem Umfeld der IT-Standard angewendet wird. Beispielsweise im Umfeld Beschaffung oder Registermodernisierung.


<h3 id="verbundene_standards">Verbundene FIT-Standards</h3>

Einige FIT-Standards sind zu einem sogenannten *Verbund* zusammengefasst. Das bedeutet, dass diese IT-Standards gemeinsam entwickelt und betrieben werden, um Synergieeffekte zu heben.

Konkret kann es sich dabei beispielsweise um eine einheitliche Basis handeln, auf der speziellere FIT-Standards aufbauen. Auch ein gleiches Themenfeld, bei dem ein abgestimmtes Vorgehen die Arbeit an allen verbundenen Standards erleichtert, ist ein Grund für einen solchen Verbund.

<h2 id="listenansicht">Die Listenansicht</h2>

Die Listenansicht bildet den Startpunkt, um FIT-Standards auf der
Informationsplattform zu finden. Sie vermittelt einen schnellen
Überblick über die vorhandenen FIT-Standards und deren Phase im
Lebenszyklus.

<div class="img-wrap img-box-shadow hidden sm:block">
    <img
        class=""
        src="../../img/fit-standards/InfoP-ListView.png"
        alt="Listenansicht"
    />
</div>

In der Listenansicht können Sie die erfassten FIT-Standards nach verschiedenen Kriterien <a href="#filtern">filtern</a>, beziehungsweise <a href="#sortieren">sortieren</a>, um schnell zu dem von Ihnen gesuchten FIT-Standard zu navigieren. Mit der Suchfunktion des Browsers (STRG-F) können Sie direkt im Text der Seite suchen.

<h3 id="struktur_listenansicht">Struktur eines Eintrags in der Listenansicht</h3>

Jeder Eintrag zu einem FIT-Standard enthält in der Listenansicht die
folgenden Elemente:

- Name des FIT-Standards mit Logo (soweit vorhanden)
- Ggf. Hinweis auf die [Zugehörigkeit zu einer Gruppe verbundener
    FIT-Standards (*Verbund*)](#verbundene_standards)
- Die [Gültigkeit](#bedeutung_gueltigkeit) des FIT-Standards
- Der [Kontext](#kontext) in dem der FIT-Standard angewendet wird
- Die aktuelle [Lebenszyklusphase](#bedeutung_lz) des FIT-Standards
- Die aktuell hinterlegten [Rollen](#bedeutung_rollen) als Ansprechpartner zum jeweiligen FIT-Standard
- Eine Kurzbeschreibung des FIT-Standards  sowie
- Das Datum der letzten Aktualisierung des Eintrags in der
    Informationsplattform

Um zur [Detailansicht](#detailansicht) zu
wechseln, klicken Sie auf das Logo oder den Namen des FIT-Standards.

<h3 id="filtern">Filterfunktionen</h3>

Mit Hilfe der zur Verfügung stehenden Filterfunktionen kann die Liste der angezeigten FIT-Standards aufgrund verschiedener Kriterien eingegrenzt werden. Die Kriterien sind in sogenannten _Filterblöcken_ organisiert. Innerhalb eines Blocks werden Ihnen die verschiedenen Ausprägungen als Optionen angezeigt.  
Wenn Sie mehrere Kontrollkästchen innerhalb eines Filterblocks aktivieren, werden die einzelnen Optionen ODER-verknüpft.
Beispielsweise können Sie im Filterblock **Lebenszyklusphasen** eine oder mehrere Lebenszyklusphasen durch Aktivieren der gewünschten Kontrollkästchen auswählen, in denen ein FIT-Standard sich befinden muss, damit er in der resultierenden Liste angezeigt wird.  
Die Aktualisierung der Liste erfolgt bei jeder Änderung der ausgewählten Optionen.

Sind Kontrollkästchen in verschiedenen Filterblöcken aktiviert, erfolgt eine UND-Verknüpfung zwischen den Filterblöcken. Das bedeutet, es muss mindestens eine der aktivierten Optionen in jedem Filterblock erfüllt sein, damit ein FIT-Standard in der resultierenden Liste angezeigt wird.

Der Filterblock **Verbundene FIT-Standards** bietet die Möglichkeit, die Übersichtlichkeit durch ausblenden untergeordneter FIT-Standards zu erhöhen. Weitere Kriterien in entsprechenden Filterblöcken sind:

- An einem FIT-Standard beteiligte **Organisationen**
- **Gültigkeit** eines FIT-Standards
- **Beziehungskontext** in dem der FIT-Standard steht
- **Inhaltlicher Kontext** in welchem der FIT-Standard eingeordnet werden kann
- **Methoden/Framework** in die der FIT-Standard eingebettet ist

<div class="flex justify-center">
    <img
        src="../../img/fit-standards/InfoP-Filterboxes-xs-new.png"
        alt="Filterboxen"
    />
</div>

Falls in der Kopfzeile eines Filterblocks ein Chevron-Symbol dargestellt ist, kann der betreffende Filterblock durch Anklicken des Symbols geschlossen, beziehungsweise geöffnet werden.

<h3 id="sortieren">Sortieren der Liste</h3>

Durch Auswahl einer der in der Dropdown-Liste _Sortieren nach_ zur Verfügung stehenden Optionen lässt sich die Reihenfolge beeinflussen, in welcher die FIT-Standards angezeigt werden.

<div class="flex justify-center">
    <img
        src="../../img/fit-standards/InfoP-SortFunctions.png"
        alt="Sortieransicht"
    />
</div>

<h2 id="detailansicht">Die Detailansicht</h2>

Die Detailansicht zeigt die
[Lebenszyklusphase](https://docs.fitko.de/standardisierungsagenda/docs/einfuehrung/#lebenszyklus)
des ausgewählten FIT-Standards an, gefolgt von einer umfassenden
Beschreibung.

<div class="img-wrap img-box-shadow">
    <img
        class="hidden sm:block"
        src="../../img/fit-standards/InfoP-DetailView.png"
        alt="Detailansicht"
    />
</div>
<img
    class="sm:hidden mb-2"
    src="../../img/fit-standards/InfoP-DetailView-xs_cropped.png"
    alt="Detailansicht"
/>

Im Abschnitt ***Detailinformationen*** werden Kontextinformationen zu Beziehungen und Inhalten des FIT-Standards in Badges symbolhaft angezeigt.

Der Abschnitt ***Überblick*** vermittelt Ihnen einen Überblick über den FIT-Standard.

Der Abschnitt ***Beschlüsse*** listet alle zum FIT-Standard gehörigen
Beschlüsse des IT-Planungsrates chronologisch auf, beginnend mit dem
jeweils neuesten. Jeder Eintrag ist mit einem Link versehen, den Sie
anklicken können, um den Beschluss auf der Webseite des IT-Planungsrates
einzusehen.

Der Abschnitt ***Weiterführende Informationen*** listet Webseiten mit
vertiefenden Informationen auf. Durch Anklicken erreichen Sie dieses
externe Informationsangebot.

<h3 id="info-blocks">Informationsboxen und ihr Inhalt</h3>

Abhängig von der Auflösung Ihres Bildschirms befinden sich unterhalb der umfassenden Beschreibung oder in der rechten Seitenleiste sogenannte ***Informationsboxen***, beginnend mit der Info-Box ***Informationen zu den Rollen***. In dieser sind aktuelle Ansprechpartner für den FIT-Standard angegeben.  
Je nach Lebenszyklusphase und Bedarf/Verfügabrkeit werden dabei Ínformationen zu den Rollen angezeigt.
Die Einträge ermöglichen Ihnen per Link einen direkten
Zugriff auf Informationen, die den Betrieb eines FIT-Standards unterstützen. Sie werden abhängig von der Lebenszyklusphase
bereitgestellt.

Die Info-Box ***Artefakte*** verlinkt zu vorhandenen/verfügbaren
Dokumenten und weiteren Artefakten des FIT-Standards. Sie können diese aufrufen oder herunterladen.  
Optional wird durch ein Symbol die Lizenz visualisiert, welche für das betreffende Artefakt gilt. Das Anklicken des Lizenzsymbols öffnet ein neues Fenster/Tab mit der entsprechenden Lizenzinformation.

Bei Bedarf oder Verfügbakeit wird in der Info-Box ***Methode/Framework*** die Methode oder das Framework genannt und verlinkt, auf der der FIT-Standard basiert.

Wenn der gewählte FIT-Standard mit anderen FIT-Standards verbunden oder diesen
untergeordnet ist, wird die Info-Box ***Verbundene IT-Standards*** mit
entsprechenden Einträgen angezeigt. Jeden verbundenen FIT-Standard
können Sie über den zugehörigen Link des Eintrags direkt aufrufen.

Die Info-Box ***Gültigkeit*** wird nur bei Bedarf/Verfügbarkeit eingeblendet. Sie enthält Angaben darüber, ob es sich um einen empfohlenen oder sogar einen verbindlichen FIT-Standard handelt und welche Organisation dessen Gültigkeit festgelegt hat. Es kann auch ein Link zu einem entsprechenden Beschluss hinterlegt sowie ein Gültigkeitsdatum (ab wann/seit wann gültig) angegeben sein.

Die Info-Box ***Letzte Aktualisierung*** zeigt Ihnen, wann der Eintrag
auf der Informationsplattform zuletzt aktualisiert wurde.

<h2 id="dokumentationsportal">Das Dokumentationsportal der Standardisierungsagenda</h2>

Um mehr über Aufgaben und Ziele der
[Standardisierungsagenda](https://docs.fitko.de/standardisierungsagenda/docs/einfuehrung/)
zu erfahren, folgen Sie dem entsprechenden Link in der Kopfzeile. Dieser
verzweigt direkt zu den Informationen des Dokumentationsportals der
Standardisierungsagenda. Zu diesen gehören neben der [Definition eines
föderalen
IT-Standards](https://docs.fitko.de/standardisierungsagenda/docs/fit-standard/)
und den [Beteiligten
(Rollen)](https://docs.fitko.de/standardisierungsagenda/docs/rollen/)
auch der
[Lebenszyklus](https://docs.fitko.de/standardisierungsagenda/docs/einfuehrung/#lebenszyklus),
[vertiefende
Informationen](https://docs.fitko.de/standardisierungsagenda/docs/details/)
und
[Hilfsmittel](https://docs.fitko.de/standardisierungsagenda/docs/tools/)
in Form von Dokumentenvorlagen sowie ein
[Glossar](https://docs.fitko.de/standardisierungsagenda/docs/glossary),
das verwendete Begriffe erklärt.

<h2 id="more-nav">Weitere Navigationsmöglichkeiten</h2>

Nutzen Sie für weitere Navigationsmöglichkeiten die Standardfunktionen
Ihres Browsers.
