---
Seiteninhalt: Changelog der Informationsplattform für FIT-Standards
---

# Changelog

Alle wesentlichen funktionalen und inhaltlichen Änderungen an der Informationsplattform für FIT-Standards werden auf dieser Seite dokumentiert.

Das Format dieser Datei basiert auf [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),

## 2025-02-13

### Hinzugefügt

- Die bisherige Informationsbox mit Kontaktdaten wurde in "Informationen zu Rollen" umbenannt und umfasst nun auch Beteiligte, die nicht direkt erreichbar sind, um eine bessere Übersicht der Beteiligungen zu geben.  
- Die Verbindlichkeit der Einträge wurde erweitert: Neben "Verbindlich (IT-PLR)" und "Empfohlen (IT-PLR)" gibt es nun auch "Empfohlen (FIT-SB)", "OZG RV - Rechtsverordnung nach §6 OZG", "Rechtsverordnung" und "Andere Vorgabe".
- Einträge können nun Kategorien zugeordnet werden, z. B. für Beziehungskontext, inhaltliche oder technische Aspekte.
- Die technischen Voraussetzungen für diese Änderungen wurden umgesetzt, inhaltliche Anpassungen folgen in Kürze.

## 2024-06-04

### Hinzugefügt

- Informationen bezüglich der verwendeten Spezifikationsmethode/Frameworks eines FIT-Standards hinzugefügt.
- Filterbox auf jeden Fall geöffnet darstellen, wenn beim Aufruf der Seite eine Option aus der Gruppe ausgewählt ist
- Barrierefreiheit: Ergänzung/Optimierung der Ausgabe beim Einsatz eines Screenreaders

## 2024-05-02

### Hinzugefügt

- Dieses Changelog hinzugefügt.
- Informationen zur Gültigkeit eines FIT-Standards hinzugefügt, einschließlich [Erläuterungen dazu](../hilfe/#bedeutung_gueltigkeit).
- Informationen bezüglich der Lizensierung von Artefakten hinzugefügt.
- Erweiterung angezeigter Informationen in der [Listenansicht](../hilfe/#listenansicht) und der [Detailansicht](../hilfe/#detailansicht):
- [Filterfunktionen erweitert](../hilfe/#filtern)
- [Sortiermöglichkeit der Liste in der Listenansicht](../hilfe/#sortieren) nach verschiedenen Kriterien

## 2023-11-14

### Initiale Version veröffentlicht

<!--

## [Unveröffentlicht]

### Hinzugefügt
- ...

### Geändert

- ...

### Gefixt

- ...

### Entfernt

- ...

-->
