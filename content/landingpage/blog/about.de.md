---
title: "Über das Föderale Entwicklungsportal"
icon: "IconManualGearbox"
---

Das Föderale Entwicklungsportal stellt eine **zentrale Anlaufstelle für Softwareentwickler:innen** dar. Bei der Umsetzung von digitalen Anwendungen für die Verwaltung spielen Entwickler:innen eine zentrale Rolle. Sie müssen in die Lage versetzt werden, schnell und effizient nachnutzbare Lösungen zu schaffen. Primäre Zielgruppe des föderalen Entwicklungsportals sind daher Softwareentwickler:innen, die Software für die öffentliche Verwaltung im Kontext **föderaler IT-Infrastrukturen von Bund und Ländern** entwickeln.


Um die bestehenden Hürden bei der **Beschaffung von Informationen über technische Rahmenbedingungen sowie existierender Standards und Schnittstellen** zu reduzieren, werden bestehende Informationsangebote in diesem Portal gebündelt und durch zusätzliche Informationsangebote ergänzt. Eine Öffnung der bestehenden Bemühungen für möglichst alle Akteur:innen sowie deren effektive Koordination und Kooperation ist nötig, um die Stärken der föderal organisierten deutschen Verwaltung mit ihren verteilten Ressourcen effizient zu nutzen.


Das Föderale Entwicklungsportal **senkt damit Entwicklungsaufwände** bei allen Beteiligten und beschleunigt dadurch Umsetzungsprojekte. Damit wird die Entwicklung eines innovativen Lösungsökosystems gefördert und ein „Single-Point-of-Truth“ geschaffen, an dem sich alle Entwickler:innen, Leistungs- und Umsetzungsverantwortliche orientieren können. Das Föderale Entwicklungsportal ist Teil des IT-Planungsrats-Projekts [FIT-Connect](https://www.fitko.de/projektmanagement/fit-connect) und wird aus Mitteln des [Digitalisierungsbudgets](https://www.fitko.de/foederale-koordination/digitalisierungsbudget) von der [Föderalen IT-Kooperation (FITKO)](https://www.fitko.de/) entwickelt.
