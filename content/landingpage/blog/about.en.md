---
title: "About the FPoD"
icon: "IconManualGearbox"
---

The Federal Development Portal serves as a **central point of contact for software developers**. In the implementation of digital applications for administration, developers play a key role. They need to be empowered to create reusable solutions quickly and efficiently. Therefore, the primary target group of the federal development portal are software developers who develop software for public administration in the context of **federal IT infrastructures of the federal and state governments**.


To reduce existing hurdles in **procuring information about technical conditions as well as existing standards and interfaces**, existing information offers are bundled in this portal and supplemented by additional information offers. An opening of the existing efforts for as many actors as possible, as well as their effective coordination and cooperation, is necessary to efficiently utilize the strengths of the federally organized German administration with its distributed resources.


The Federal Development Portal thereby **reduces development efforts** for all parties involved and accelerates implementation projects. This promotes the development of an innovative solution ecosystem and creates a "single point of truth" where all developers, service, and implementation managers can orient themselves. The Federal Development Portal is part of the IT Planning Council's project [FIT-Connect](https://www.fitko.de/projektmanagement/fit-connect) and is developed with funds from the [Digital Budget](https://www.fitko.de/foederale-koordination/digitalisierungsbudget) by the [Federal IT Cooperation (FITKO)](https://www.fitko.de/).