---
title: "Du möchtest Fehler oder Verbesserungsvorschläge melden?"
icon: "IconBrandOpenSource"
---

Im Auftrag des [IT-Planungsrats](https://www.it-planungsrat.de/) betreibt die Föderale IT-Kooperation (FITKO) eine Reihe von [Produkten](https://www.fitko.de/produktmanagement) und steuert [Projekte](https://www.fitko.de/projektmanagement), die noch zu Produkten werden wollen.

Um dabei immer besser zu werden, sind wir auf Dein Feedback angewiesen! In unserem Issue-Tracker im *Open Source Code Repository der öffentlichen Verwaltung* kannst Du Fehler oder Verbesserungsvorschläge zu Komponenten der Föderalen IT-Infrastruktur melden.

Der Issue-Tracker soll bestehende Feedback-Möglichkeiten nicht ersetzen, sondern diese ergänzen. Wir versuchen dennoch gerne, Dein Anliegen an die richtige Ansprechperson zu vermitteln. Bitte habe Verständnis, wenn dies manchmal etwas Zeit in Anspruch nimmt. Wir experimentieren hier gerade mit neuen Feedback-Möglichkeiten und bekommen im Alltagsgeschäft natürlich auch Feedback über etablierte Kanäle, das es ebenfalls zu bedienen gilt.

Bitte beachte, dass Dein Feedback öffentlich einsehbar sein wird.

Schau vor Deinem Feedback bitte [in die Liste der bestehenden Issues](https://gitlab.opencode.de/fitko/feedback/-/issues) und erstelle erst dann [ein neues Issue](https://gitlab.opencode.de/fitko/feedback/-/issues/new).

Danke! 😊
