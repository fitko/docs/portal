---
title: "Would you like to report bugs or suggest improvements?"
icon: "IconBrandOpenSource"
---

On behalf of the [IT Planning Council](https://www.it-planungsrat.de/), the Federal IT Cooperation (FITKO) operates a range of [products](https://www.fitko.de/produktmanagement) and manages [projects](https://www.fitko.de/projektmanagement) that are intended to become products.

To continuously improve, we rely on your feedback! In our issue tracker at the *Open Source Code Repository of Public Administration*, you can report bugs or suggest improvements for components of the Federal IT Infrastructure.

The issue tracker is not meant to replace existing feedback mechanisms but to supplement them. We still attempt to direct your concern to the correct contact person. Please understand if this sometimes takes a bit of time. We are currently experimenting with new feedback opportunities and, of course, also receive feedback through established channels that need to be addressed.

Please note that your feedback will be publicly visible.

Before providing feedback, please check [the list of existing issues](https://gitlab.opencode.de/fitko/feedback/-/issues) and only then [create a new issue](https://gitlab.opencode.de/fitko/feedback/-/issues/new).

Thank you! 😊
