---
title: "Du hast Fragen oder neue Ideen, die Du gerne diskutieren möchtest?"
icon: "IconBrandOpenSource"
---

Im [Forum des Open Source Code Repository](https://discourse.opencode.de/) ist Platz dafür. Wir freuen uns auf Deinen Beitrag!

