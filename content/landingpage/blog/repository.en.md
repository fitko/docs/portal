---
title: "Any questions or ideas?"
icon: "IconBrandOpenSource"
---

The [forum of the Open Source Code Repository](https://discourse.opencode.de/) is the place for that. We look forward to your contribution!
