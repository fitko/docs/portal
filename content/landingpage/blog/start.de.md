---
title: "Direkt loslegen!"
icon: "IconRocket"
link: "/de/resources"
linkText: "Zu den Entwicklungsressourcen"
---

Mit den folgenden Ressourcen kannst Du direkt loslegen und mehr über die Föderale IT-Infrastruktur von Bund und Ländern erfahren und Anwendungen für die Öffentliche Verwaltung erstellen. Perspektivisch werden an dieser Stelle konkrete Entwicklungsszenarien mit den jeweils dafür benötigten Entwicklungsressourcen zusammengestellt.
