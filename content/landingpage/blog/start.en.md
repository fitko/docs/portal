---
title: "Get started right away!"
icon: "IconRocket"
link: "/en/resources"
linkText: "Development Resources"
---

With the following resources, you can get started right away, learn more about the federal IT infrastructure of states and the federal government, and create applications for public administration. In the future, this section will compile specific development scenarios with the required development resources.
