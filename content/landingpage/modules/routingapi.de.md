---
title: 'Standards und Schnittstellen'
link: 'https://docs.fitko.de/standards-und-schnittstellen/'
linkText: 'Mehr erfahren'
icon: 'IconRoute'
---
Im Rahmen der Umsetzung des Portalverbunds wurden die Kommunikationsbeziehungen zwischen den verschiedenen Basisdiensten wie Servicekonten, Postfächern und ePayment-Lösungen analysiert und in einer Übersicht der eingesetzten Standards und Schnittstellen dokumentiert.