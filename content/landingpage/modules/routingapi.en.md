---
title: 'Routing-API'
link: 'https://docs.fitko.de/fit-connect/docs/apis/routing-api'
linkText: 'Learn more'
icon: 'IconSteeringWheel'
---
To submit an application in a machine-readable format to the competent authority, information on responsibilities and technical parameters can be determined via the Routing API. The retrieval of these parameters is carried out through a request to the FIT-Connect routing service.
