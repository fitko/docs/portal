---
title: 'Standards and Interfaces'
link: 'https://docs.fitko.de/standards-und-schnittstellen/'
linkText: 'Learn more'
icon: 'IconRoute'
---
As part of the implementation of the portal network, the communication relationships between various basic services such as service accounts, mailboxes, and ePayment solutions were analyzed and documented in an overview of the standards and interfaces used.
