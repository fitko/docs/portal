---
title: 'Submission-API'
link: 'https://docs.fitko.de/fit-connect/docs/apis/submission-api'
linkText: 'Mehr erfahren'
icon: 'IconSend'
---
Über den FIT-Connect Zustelldienst können Anträge für Verwaltungsleistungen maschinenlesbar an die fachlich zuständige Behörde übermittelt werden. Der Zustelldienst stellt hierfür die Submission API bereit. Sendende Systeme können sich über eine erfolgreiche Zustellung über ein Ereignisprotokoll informieren.