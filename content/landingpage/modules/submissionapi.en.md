---
title: 'Submission-API'
link: 'https://docs.fitko.de/fit-connect/docs/apis/submission-api'
linkText: 'Learn more'
icon: 'IconSend'
---
Applications for administrative services can be transmitted to the competent authority in a machine-readable format via the FIT-Connect delivery service. The delivery service provides the Submission API for this purpose. Sending systems can be informed of a successful delivery via an event log.
