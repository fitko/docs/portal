---
contactInformation:
  mail: support@ausweisapp.de
  name: Governikus GmbH & Co. KG
developer:
  name: Bundesamt für Sicherheit in der Informationstechnik
  url: https://www.bsi.bund.de
docsUrl: https://www.ausweisapp.bund.de/fuer-diensteanbieter
logo:
  path: public/img/resources/ausweisapp2/AusweisApp2.png
  title: AusweisApp2
name: AusweisApp2
sourceCodeUrl: https://github.com/Governikus/AusweisApp2
tags:
- label:external
- type:software
- status:production
---

Die AusweisApp2 kann genutzt werden, um in eigenen Dienste die eID-Funktion unter Nutzung eines eID-Diensteproviders oder eines eigenen eID Servers anzubinden.