---
contactInformation:
  mail: support@ausweisapp.de
  name: Governikus GmbH & Co. KG
developer:
  name: Federal Office for Information Security (Bundesamt für Sicherheit in der Informationstechnik)
  url: https://www.bsi.bund.de
docsUrl: https://www.ausweisapp.bund.de/en/serviceproviders
logo:
  path: public/img/resources/ausweisapp2/AusweisApp2.png
  title: AusweisApp2
name: AusweisApp2
sourceCodeUrl: https://github.com/Governikus/AusweisApp2
tags:
- label:external
- type:software
- status:production
---

AusweisApp2 can be used to integrate the eID function into your own services, utilizing an eID service provider or your own eID server.
