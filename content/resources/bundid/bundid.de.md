---
contactInformation:
  mail: bundID@bmi.bund.de
  name: BMI Referat DVI5 "Digitale Identitäten; Authentifizierung"
developer:
  name: BMI
  url: https://www.bmi.bund.de/
docsUrl: https://ssp.id.bund.de/ip?id=downloads
name: BundID
shortDescription: Die BundID realisiert das Bürgerkonto (inkl. Postfach) gemäß § 2 OZG
tags:
- type:base
- label:external
- status:production
---

Die BundID realisiert das Bürgerkonto (inkl. Postfach) gemäß [§ 2 Onlinezugangsgesetz (OZG)](https://www.gesetze-im-internet.de/ozg/__2.html).
Die BundID wird als Basisdienst im Sinne des OZG zentral vom Bundesministerium des Innern und für Heimat betrieben und weiterentwickelt. 

Behörden des Bundes, der Länder, der Kommunen sowie der mittelbaren Verwaltungen binden ihre Onlinedienste an die BundID an, womit ein zentraler Identifizierungs- und Authentifizierungsdienst für die Nutzenden deutschlandweit genutzt wird.
Das BundID-Postfach erleichtert zudem die Kommunikation mit Bürger:innen, da rechtskräftige Bescheide nicht postalisch, sondern mit Zustimmung der Nutzenden digital versendet werden können.

Weitere Informationen zur BundID finden sich [auf der Webseite des BMI](https://www.digitale-verwaltung.de/Webs/DV/DE/digitale-identitaeten/bundid/bundid-node.html).