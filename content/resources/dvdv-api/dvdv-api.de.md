---
contactInformation:
  mail: dvdv@fitko.de
  name: DVDV Produktteam (FITKO) 
developer:
  name: IT-Planungsrat
  url: https://www.it-planungsrat.de/
docsUrl: https://docs.fitko.de/dvdv/spec
apiSpecUrl:
logo:
  path: public/img/resources/dvdv/logo_dvdv.png
  title: Logo DVDV
name: DVDV API
shortDescription: Die REST-API des DVDV.
sourceCodeUrl: https://git.fitko.de/dvdv/dvdv-api
tags:
- type:api
- status:production
---

Über das Deutsche Verwaltungsdiensteverzeichnis (DVDV) werden Verbindungsparameter bereitgestellt, um eine rechtssichere elektronische Kommunikation von und mit Behörden zu ermöglichen. Für die Anbindung an das DVDV werden kostenfrei nutzbare Software-Bibliotheken in Java und .Net bereitgestellt. Alternativ (z.B. zur Nutzung in anderen Programmiersprachen) kann die REST-API des DVDV genutzt werden.