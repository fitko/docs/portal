---
childResources:
- name: DVDV API
  slug: dvdv-api
contactInformation:
  mail: dvdv@fitko.de
  name: DVDV Produktteam (FITKO) 
developer:
  name: IT-Planungsrat
  url: https://www.it-planungsrat.de/
docsUrl: https://docs.fitko.de/dvdv/
logo:
  path: public/img/resources/dvdv/logo_dvdv.png
  title: Logo DVDV
name: DVDV
tags:
- status:production
- type:base
---

Über das Deutsche Verwaltungsdiensteverzeichnis (DVDV) werden Verbindungsparameter bereitgestellt, um eine rechtssichere elektronische Kommunikation von und mit Behörden zu ermöglichen. Für die Anbindung an das DVDV werden kostenfrei nutzbare Software-Bibliotheken in Java und .Net bereitgestellt.