---
contactInformation:
  mail: digit-semic-team@ec.europa.eu
  name: SEMIC Support Center
developer:
  name: European Commission
  url: https://ec.europa.eu/info/index_en
docsUrl: https://joinup.ec.europa.eu/collection/semantic-interoperability-community-semic/core-vocabularies
name: e-Government Core Vocabularies
shortDescription: Auf EU-Ebene vereinheitlichtes Vokabular zu Begriffen im Kontext
  der öffentlichen Verwaltung (english)
tags:
- type:information-assistance
- status:production
- label:external
---

Auf EU-Ebene vereinheitlichtes Vokabular zu Begriffen im Kontext der öffentlichen Verwaltung (english).

[Das FAQ](https://joinup.ec.europa.eu/collection/semic-support-centre/helpdesk-faq) enthält einige häufig gestellte Fragen zum Core-Vocabulary. Änderungsvorschläge können als Issue im [SEMIC-GitHub-Repository](https://github.com/SEMICeu) eingereicht werden.