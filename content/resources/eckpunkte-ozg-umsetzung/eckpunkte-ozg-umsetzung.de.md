---
contactInformation:
  mail: ozg@fitko.de
  name: Koordination OZG-Programmmanagement
developer:
  name: BMI
  url: https://www.bmi.bund.de/
docsUrl: https://docs.fitko.de/arc/policies/eckpunkte-umsetzung-ozg/
logo:
  path: public/img/resources/eckpunkte-ozg-umsetzung/titelbild.jpg
  title: Logo OZG
name: Eckpunkte zur OZG-Umsetzung
tags:
- type:directive-guideline
- status:production
---

Mit dem [Beschluss 2020/39](https://www.it-planungsrat.de/beschluss/beschluss-2020-39) des IT-Planungsrat schafft die Bundesregierung mit einem Konjunkturprogramm einen neuen Handlungsrahmen, um schnell ein
flächendeckendes digitales Verwaltungsangebot in Deutschland zu aufzubauen – und dabei
Länder und Kommunen gezielt zu entlasten. Die Voraussetzungen für die Nutzung dieser Mittel sind im Eckpunktepapier zur OZG-Umsetzung beschrieben.