---
contactInformation:
  mail: architekturboard@fitko.de
  name: Koordination Föderales IT-Architekturboard
developer:
  name: Arbeitsgruppe „Rahmenbedingungen für den Betrieb von EfA-Services“ (AG RaBe)
docsUrl: https://docs.fitko.de/arc/policies/efa-mindestanforderungen-betrieb
name: EfA-Mindestanforderungen Betrieb
shortDescription: Die „Einer für Alle“-Mindestanforderungen Betrieb definieren
  Vorgaben zu Rollen und Verantwortlichkeiten für Betrieb und Support von
  EfA-Online-Services nach dem „Einer für Alle“-Prinzip. 
tags:
- type:directive-guideline
- status:production
---

Die Digitalisierung der deutschen Verwaltungsleistungen kann nur arbeitsteilig gelingen.
Als umfassende Form der Arbeitsteilung und Zusammenarbeit wurde das „Einer für Alle“-Prinzip etabliert.
„Einer für Alle“-Services sind flächendeckend einsetzbare Lösungen, die einmal nutzerzentriert konzipiert und entwickelt, fachlich betreut und technisch betrieben werden.

Die EfA-Mindestanforderungen Betrieb definieren Vorgaben zu Rollen und Verantwortlichkeiten für Betrieb und Support von EfA-Online-Services nach dem „Einer für Alle“-Prinzip.
