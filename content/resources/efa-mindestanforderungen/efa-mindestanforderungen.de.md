---
contactInformation:
  mail: architekturboard@fitko.de
  name: Koordination Föderales IT-Architekturboard
developer:
  name: Föderales IT-Architekturboard
  url: https://www.fitko.de/foederale-koordination/gremienarbeit/foederales-it-architekturboard
docsUrl: https://docs.fitko.de/arc/policies/efa-mindestanforderungen
name: EfA-Mindestanforderungen
shortDescription: Die „Einer für Alle“-Mindestanforderungen definieren die Mindestanforderungen
  an Online-Dienste zur elektronischen Abwicklung von Verwaltungsleistungen, damit
  diese als „EfA-konform“ gelten.
tags:
- type:directive-guideline
- status:production
---

Die Digitalisierung der deutschen Verwaltungsleistungen kann nur arbeitsteilig gelingen.
Als umfassende Form der Arbeitsteilung und Zusammenarbeit wurde das „Einer für Alle“-Prinzip etabliert.
„Einer für Alle“-Services sind flächendeckend einsetzbare Lösungen, die einmal nutzerzentriert konzipiert und entwickelt, fachlich betreut und technisch betrieben werden.

Die EfA-Mindestanforderungen definieren die Mindestanforderungen an einen Online-Dienst zur elektronischen Abwicklung von Verwaltungsleistungen, damit dieser als „EfA-konform“ gelten kann.