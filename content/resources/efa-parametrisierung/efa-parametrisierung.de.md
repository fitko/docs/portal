---
contactInformation:
  mail: efa-parametrisierung@fitko.de
  name: Team EfA-Parametrisierung der FITKO
developer:
  name: IT-Planungsrat
  url: https://www.it-planungsrat.de/
docsUrl: https://docs.fitko.de/efa-parametrisierung/
name: Dokumentation zur EfA-Parametrisierung
shortDescription: Die Parametrisierung von EfA-Onlinediensten unterstützt
  eine bundesweite Nachnutzung durch flexible Anpassung der Onlinedienste auf
  regionale Gegebenheiten.
tags:
- status:production
- type:information-assistance
---

Die Parametrisierung von „Einer-für-Alle“-Onlinediensten (EfA-Onlinedienste) dient dazu, eine bundesweite Nachnutzung der Dienste zu ermöglichen trotz landes- und satzungsrechtlicher Unterschiede.
Die Flexibilität von EfA-Onlinediensten soll durch die Verwendung von Variablen (Parametern) sichergestellt werden.
Dies soll standardisiert und gleichartig für möglichst viele Dienste der öffentlichen Verwaltung erfolgen.

Die Dokumentation zur EfA-Parametrisierung gibt Hinweise, wie die Parametrisierung von Diensten erfolgen soll, wie die Parameter definiert, gepflegt und abgerufen werden sollen und welche Parameter verwendet werden sollen.