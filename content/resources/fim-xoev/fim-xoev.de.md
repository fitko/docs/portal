---
contactInformation:
  mail: it-standards@fitko.de
  name: FITKO Koordination IT-Standards
developer:
  name: FITKO Koordination IT-Standards
  url: https://www.fitko.de/foederale-it-architektur/standards
docsUrl: https://docs.fitko.de/fim-xoev
name: Dokumentation zur Interoperabilisierung von FIM und XÖV
shortDescription: FIM-Datenfelder beschreiben den Bereich der Antragserfassung
  und XÖV-Vorhaben stellen die Vorgaben für die Kommunikation zwischen den
  nachfolgenden Fachverfahren. Ihr möglichst ideales Zusammenspiel ist also
  kritisch für die Digitalisierung - und das nicht nur im OZG-Umfeld.
sourceCodeUrl: https://git.fitko.de/foederale-it/fim-xoev/fim-xoev-doc
tags:
- type:information-assistance
- status:production
---

FIM-Datenfelder beschreiben den Bereich der Antragserfassung und XÖV-Vorhaben stellen die Vorgaben für die Kommunikation zwischen den nachfolgenden Fachverfahren.
Ihr möglichst ideales Zusammenspiel ist also kritisch für die Digitalisierung - und das nicht nur im OZG-Umfeld.

Ziele der FIM-XÖV-Interoperabilisierung ist das Herbeiführen einer nahtlosen Zusammenarbeit zwischen FIM und XÖV.