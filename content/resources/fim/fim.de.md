---
contactInformation:
  mail: fim@fitko.de
  name: FIM Produktteam (FITKO)
developer:
  name: IT-Planungsrat
  url: https://www.it-planungsrat.de/
docsUrl: https://fimportal.de
logo:
  path: public/img/resources/fim/fim-logo.svg
  title: FIM Logo
name: FIM-Portal
shortDescription: Das Föderale Informationsmanagement (FIM) liefert standardisierte
  Informationen für Verwaltungsleistungen (Metadaten, Prozessbeschreibungen und Datenformate).
tags:
- type:base
- status:production
---

Das Föderale Informationsmanagement (FIM) liefert standardisierte Informationen für Verwaltungsleistungen. FIM besteht aus drei Bausteinen:

* FIM Leistungen: Strukturierte Beschreibung einer Verwaltungsleistung
* FIM Prozesse: Prozessbeschreibungen einschließlich BPMN-Modellen einer Verwaltungsleistung
* FIM Datenfelder: Maschinenlesbare Definition von Input- und Outputobjekten (bspw. Anträge) einer Verwaltungsleistung.

Über das FIM-Portal können veröffentliche Informationen nach einer Registrierung abgerufen werden.