---
contactInformation:
  mail: fit-connect@fitko.de
  name: FIT-Connect Produktteam (FITKO)
developer:
  name: IT-Planungsrat
  url: https://www.it-planungsrat.de/
docsUrl: https://docs.fitko.de/fit-connect/docs/apis/self-service-api
logo:
  path: public/img/resources/fit-connect-self-service-api/df94f47069316480.jpeg
  title: Fit-Connect Logo
name: FIT-Connect Self-Service API
sourceCodeUrl: https://git.fitko.de/fit-connect/self-service-api/
tags:
- type:api
- status:development
---

Über die Self-Service API von FIT-Connect können automatisiert Fachverfahren für den Antragsempfang konfiguriert werden.