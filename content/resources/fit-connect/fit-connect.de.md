---
childResources:
- name: FIT-Connect Routing API
  slug: routing-api
- name: FIT-Connect Submission API
  slug: submission-api
contactInformation:
  mail: fit-connect@fitko.de
  name: FIT-Connect Produktteam (FITKO)
developer:
  name: IT-Planungsrat
  url: https://www.it-planungsrat.de/
docsUrl: https://docs.fitko.de/fit-connect/docs
logo:
  path: public/img/resources/fit-connect/df94f47069316480.jpeg
  title: FIT-Connect Logo
name: FIT-Connect
shortDescription: FIT-Connect stellt eine Infrastruktur, APIs und SDKs zum Versand
  von Anträgen an die zuständigen Fachbehörden in Bund, Ländern und Kommunen bereit.
  FIT-Connect vereinfacht dabei nicht nur den Datentransport, sondern unterstützt
  auch bei Umsetzung weiterer, verfahrensunabhängiger Anforderungen wie z.B. der Zuständigkeitsermittlung,
  der Aushandlung von Fachdatenschemata und Rückkanaloptionen und der Übermittlung
  von Bezahlinformationen.
sourceCodeUrl: https://git.fitko.de/fit-connect
tags:
- status:production
- type:platform
- type:base
---

FIT-Connect ist eine Plattform, die über einheitliche APIs eine skalierbare und einfache Übermittlung von Anträgen und Berichtsmeldung zwischen sendenden Systemen (bspw. Onlineantragsdiensten) und empfangenden Systemen in Behörden und anderen hoheitlichen Stellen ermöglicht. Die API-Infrastruktur wird als Service zur Verfügung gestellt und erfordert keinen Infrastrukturaufbau auf Seiten der API-Nutzer.

FIT-Connect kombiniert dazu bestehende Produkte und Standards des IT-Planungsrates bedarfsgerecht und zielt auf die ganzheitliche Betrachtung aller Schritte der Antragstellung im Rahmen des Onlinezugangsgesetzes. Durch die Vereinheitlichung und Dokumentation von Prozessschritten der Antragsstellung senkt FIT-Connect die Umsetzungsaufwände für zukünftige Digitalisierungsvorhaben. Herausfordernde Problemstellungen wie z.B. das Antragsrouting werden einmal bearbeitet; entwickelte Lösungen werden allen Verfahrensverantwortlichen zur Verfügung gestellt.