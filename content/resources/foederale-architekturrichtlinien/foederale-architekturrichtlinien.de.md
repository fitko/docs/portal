---
contactInformation:
  mail: architekturmanagement@fitko.de
  name: FITKO Architekturmanagement
developer:
  name: IT-Planungsrat
  url: https://www.it-planungsrat.de/
docsUrl: https://docs.fitko.de/arc/policies/foederale-it-architekturrichtlinien
name: Föderale Architekturrichtlinien
shortDescription: Die vom Architekturboard des IT-Planungsrat definierten Architekturrichtlinien
  helfen Architekturentscheidungen bei der Entwicklung von Softwarelösungen effizient
  zu treffen und wiederkehrende Grundsatzdiskussionen zu vermeiden. Die verpflichtende
  Anwendung wurde vom [IT-Planungsrat](https://www.it-planungsrat.de/) mit [Beschluss
  2021/37](https://www.it-planungsrat.de/beschluss/beschluss-2021-37) verbindlich
  festgelegt.
tags:
- status:production
- type:directive-guideline
---

Architekturrichtlinien helfen Architekturentscheidungen bei der Entwicklung von Softwarelösungen effizient zu treffen und wiederkehrende Grundsatzdiskussionen zu vermeiden. Somit werden Auswahlprozesse beschleunigt, Fehlentscheidungen reduziert und Beschlussergebnisse vereinheitlicht.

Das Architekturboard definiert die föderalen IT-Architekturrichtlinien und entwickelt diese weiter. Die Richtlinien gelten für alle laufenden und neuen Projekte und Vorhaben, die Einfluss auf die föderale IT-Landschaft haben.

In der vorliegenden Version werden die strategischen Architekturrichtlinien behandelt. Sie stellen den strategischen Rahmen der Architekturentwicklung dar. Die Einhaltung dieser Vorgaben dient unmittelbar der Erreichung der vereinbarten Ziele zur Digitalisierung der Verwaltung. Die strategischen Architekturvorgaben können sich aufgrund neuer Zielsetzungen ändern, sind also nicht als unveränderbar anzusehen.

Die verpflichtende Anwendung der Architekturrichtlinien wurde vom [IT-Planungsrat](https://www.it-planungsrat.de/) mit [Beschluss 2021/37](https://www.it-planungsrat.de/beschluss/beschluss-2021-37) verbindlich festgelegt.