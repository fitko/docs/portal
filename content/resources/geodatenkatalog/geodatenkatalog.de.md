---
contactInformation:
  mail: mailbox@bkg.bund.de
  name: GDI-DE
developer:
  name: Geodateninfrastruktur Deutschland
  url: https://www.gdi-de.org/
docsUrl: https://gdk.gdi-de.org/
name: Geodatenkatalog
shortDescription: Der Geodatenkatalog der [Geodateninfrastruktur Deutschland (GDI-DE)](https://www.gdi-de.org/) stellt
  Metadaten über Geodaten, Geodatendienste und weitere Geodatenanwendungen deutschlandweit
  über eine einheitliche Schnittstelle bereit.
tags:
- label:external
- status:production
- type:information-assistance
---

Der Geodatenkatalog der [Geodateninfrastruktur Deutschland (GDI-DE)](https://www.gdi-de.org/) stellt Metadaten über Geodaten, Geodatendienste und weitere Geodatenanwendungen deutschlandweit über eine einheitliche Schnittstelle bereit. Der Geodatenkatalog bezieht die in ihm enthaltenen Metadaten durch Zugriff auf andere Kataloge des Bundes und der Länder über eine standardisierte Austauschschnittstelle (Catalogue Service, CSW) und baut daraus einen konsolidierten, übergreifenden Metadatenbestand auf.

Die Inhalte des Geodatenkatalog sind auch [über eine REST-Schnittstelle](https://gdk.gdi-de.org/gdi-de/doc/api/) abrufbar.