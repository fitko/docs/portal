---
contactInformation:
  mail: fit-connect@fitko.de
  name: FIT-Connect Produktteam (FITKO)
developer:
  name: IT-Planungsrat
  url: https://www.it-planungsrat.de/
docsUrl: https://docs.fitko.de/fit-connect/docs/glossary
name: Glossar im Projekt FIT-Connect
tags:
- status:production
- type:information-assistance
---

Glossar zu Begriffen im Kontext von FIT-Connect