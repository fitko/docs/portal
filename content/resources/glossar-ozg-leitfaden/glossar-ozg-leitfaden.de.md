---
contactInformation:
  mail: DVII2@bmi.bund.de
  name: BMI | Referat DV II 2
developer:
  name: BMI
  url: https://www.bmi.bund.de/
docsUrl: https://www.digitale-verwaltung.de/Webs/DV/DE/aktuelles-service/faq-lexikon/glossar/glossar-node.html
name: Glossar auf digitale-verwaltung.de
tags:
- type:information-assistance
- status:production
- label:external
---

Glossar zu Begriffen im Kontext des Onlinezugangsgesetz (OZG), der Registermodernisierung (RegMo) und weiterer Themen rund um die digitale Verwaltung.