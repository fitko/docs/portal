---
contactInformation:
  mail: info@govdata.de
  name: GovData Produktteam (FITKO)
developer:
  name: IT-Planungsrat
  url: https://www.it-planungsrat.de/
docsUrl: https://www.govdata.de/
logo:
  path: public/img/resources/govdata/GovData.png
  title: GovData
name: GovData
shortDescription: GovData ist das nationale Metadatenportal, über das Bund, Länder
  und Kommunen ihre Verwaltungsdaten als Offene Daten (Open Data) auffindbar und zugänglich
  machen.
tags:
- type:base
- label:external
- status:production
---

GovData ist das nationale Metadatenportal, über das Bund, Länder und Kommunen ihre Verwaltungsdaten als Offene Daten (Open Data) auffindbar und zugänglich machen. Zentraler Bestandteil von GovData ist ein Katalog, der mit Metadaten befüllt wird, die in Bund, Ländern und Kommunen im standardisierten Austauschformat [DCAT-AP.de](https://www.dcat-ap.de/def/) ([Issue-Tracker](https://github.com/GovDataOfficial/DCAT-AP.de/issues)) bereitgestellt werden. Die durch die Metadaten beschriebenen Verwaltungsdaten selbst werden von den datenbereitstellenden dezentral vorgehalten und gepflegt.

GovData selbst basiert auf der Software [CKAN](https://ckan.org/). Der auf GovData veröffentlichte [Metadatenkatalog](https://www.govdata.de/daten/-/details/govdata-metadatenkatalog) lässt sich nicht nur über die Funktion auf der Website durchsuchen, 
sondern natürlich auch maschinell abfragen und verarbeiten. Hierfür stehen ein [SPARQL Endpunkt](https://www.govdata.de/sparql) sowie die [CKAN API](https://docs.ckan.org/en/2.8/api/index.html) (<https://www.govdata.de/ckan/api>) zur Verfügung.

Die von GovData entwickelte Software selbst steht [auf GitHub](https://github.com/govdataofficial) zur Verfügung.