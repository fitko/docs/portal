---
contactInformation:
  mail: kontakt@bfit-bund.de
  name: BFIT-Bund
developer:
  name: Überwachungsstelle des Bundes für Barrierefreiheit von Informationstechnik
  url: https://www.bfit-bund.de/
docsUrl: https://handreichungen.bfit-bund.de/
name: Handreichungen zur Barrierefreiheit
shortDescription: Alle digitalen Dienste müssen verabschiedete Barrierefreiheitsanforderungen
  erfüllen. Die Überwachungsstelle des Bundes für Barrierefreiheit von Informationstechnik
  (BFIT) hat entsprechende Handreichungen zur Erreichung der Barrierefreiheit entwickelt.
tags:
- type:directive-guideline
- type:information-assistance
- label:external
- status:production
---

Es ist gesetzlicher Auftrag für Bund, Länder und Kommunen, dass alle digitalen Dienste die verabschiedeten Barrierefreiheitsanforderungen erfüllen müssen. Die [Überwachungsstelle des Bundes für Barrierefreiheit von Informationstechnik](https://www.bfit-bund.de/DE/Home/home_node.html) (BFIT) hat entsprechende Handreichungen zur Erreichung der Barrierefreiheit entwickelt. 

Folgende Handreichungen stehen zur Verfügung:

* [Handlungsleitfaden zur Gestaltung barrierefreier Software](https://handreichungen.bfit-bund.de/ag02/1.0/)
* [Barrierefreie mobile Apps](https://handreichungen.bfit-bund.de/ag03/1.1/)

Beim Bund sowie in den Bundesländern wurden Überwachungsstellen zur Umsetzung der Barrierefreiheitsanforderungen eingerichtet. Eine Übersicht ist [auf der Webseite der BFIT](https://www.bfit-bund.de/DE/Kontakt/Ueberwachungsstellen-der-Laender/ueberwachungsstelle_laender_node.html) hinterlegt.

Um Kontakt mit der BFIT-Bund aufzunehmen, senden Sie gerne eine E-Mail an [kontakt@bfit-bund.de](mailto:kontakt@bfit-bund.de) oder melden sich telefonisch unter +49 30 8441489-0.