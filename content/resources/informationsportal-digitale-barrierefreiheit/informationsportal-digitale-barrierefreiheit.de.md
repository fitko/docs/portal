---
contactInformation:
  mail: DGII1@bmi.bund.de
  name: BMI | Referat DG II 1
developer:
  name: BMI
  url: https://www.bmi.bund.de/
docsUrl: https://www.barrierefreiheit-dienstekonsolidierung.bund.de
name: Informationsportal zur Digitalen Barrierefreiheit
shortDescription: Digitale Barrierefreiheit betrifft uns alle und ist Voraussetzung
  für die Teilhabe aller Menschen. Das Informationsportal lädt dazu ein, zur Umsetzung
  barrierefreier IT-Lösungen beizutragen und gibt praxisnahe Hilfestellungen zu den
  Anforderungen der IT-Barrierefreiheit und deren Umsetzung.
tags:
- type:information-assistance
- label:external
- status:production
---

Digitale Barrierefreiheit betrifft uns alle und ist Voraussetzung für die Teilhabe aller Menschen. Das Informationsportal lädt dazu ein, zur Umsetzung barrierefreier IT-Lösungen beizutragen und gibt praxisnahe Hilfestellungen zu den Anforderungen der IT-Barrierefreiheit und deren Umsetzung.

Das [Bundesministerium des Innern und für Heimat (BMI)](https://www.bmi.bund.de/) hat in Zusammenarbeit mit der [Hessischen Landesbeauftragten für barrierefreie IT](https://lbit.hessen.de/Landesbeauftragte-fuer-barrierefreie-IT-0) und dem [Informationstechnikzentrum Bund](https://www.itzbund.de/) ein Informationsportal zur Digitalen Barrierefreiheit online gestellt. So wie die Umsetzung der IT-Barrierefreiheit ein dauerhafter Prozess ist, wird auch das Portal laufend ergänzt und verbessert, um aktuelle Informationen liefern zu können.

Der auf dem Informationsportal bereitgestellte [Standardanforderungskatalog](https://www.barrierefreiheit-dienstekonsolidierung.bund.de/Webs/PB/DE/standardanforderungskatalog/standardanforderungskatalog-node.html) bietet einen Überblick über alle Anforderungen an die Barrierefreiheit aus der [Barrierefreie-Informationstechnik-Verordnung (BITV 2.0)](https://www.barrierefreiheit-dienstekonsolidierung.bund.de/Webs/PB/DE/gesetze-und-richtlinien/bitv2-0/bitv2-0-node.html), der [Europäischen Norm EN 301 549](https://www.barrierefreiheit-dienstekonsolidierung.bund.de/Webs/PB/DE/gesetze-und-richtlinien/en301549/en301549-node.html) und den [Web Content Accessibility Guidelines (WCAG) 2.1](https://www.barrierefreiheit-dienstekonsolidierung.bund.de/Webs/PB/DE/gesetze-und-richtlinien/wcag/wcag-node.html) des World Wide Web Konsortiums unterstützt bei einer Vorauswahl der Barrierefreiheitsanforderungen.