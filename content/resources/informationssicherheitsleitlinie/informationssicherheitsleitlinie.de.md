---
contactInformation:
  mail: it-sicherheit@fitko.de
  name: Informationssicherheitsmanagement FITKO / IT-Planungsrat
developer:
  name: IT-Planungsrat
  url: https://www.it-planungsrat.de/
docsUrl: https://docs.fitko.de/arc/policies/informationssicherheitsleitlinie
name: Leitlinie für die Informationssicherheit in der öffentlichen Verwaltung
shortDescription: Mit der Entscheidung hin zur Digitalisierung der Verwaltung öffnen
  sich Bund, Länder und Kommunen für Angriffe aus dem Cyberraum. Mit der Leitlinie
  für die Informationssicherheit in der öffentlichen Verwaltung sollen für betroffenen
  Institutionen grundlegende Sicherheitsstandards und Maßnahmen zum Schutz etabliert
  werden.
tags:
- type:directive-guideline
- status:production
---

Um die Chancen zu nutzen, die sich aus einer stärkeren Vernetzung der IT-Systeme von Bund und Ländern ergeben können, ist es notwendig alle beteiligten Partner auf ein angemessenes Sicherheitsniveau zu bringen. In dieser Fortschreibung der Leitlinie aus dem Jahr 2013 soll sich nun verstärkt auf die Wirkung von Sicherheitsmaßnahmen, insbesondere auf die Frage einer lückenlosen Umsetzung von Sicherheitskonzepten, und deren Messbarkeit fokussiert werden.