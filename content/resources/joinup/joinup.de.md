---
contactInformation:
  mail: sg-acc-doc@ec.europa.eu
  name: Europäische Komission
developer:
  name: European Commission
  url: https://ec.europa.eu/info/index_en
docsUrl: https://joinup.ec.europa.eu/
logo:
  path: public/img/resources/joinup/190404-logo-JOINUP-blue.png
  title: JOINUP Logo
name: Joinup
tags:
- label:external
- status:production
- type:platform
- type:information-assistance
---

Joinup ist eine kollaborative Plattform zur Unterstützung von E-Government-Fachleuten.
Die Europäische Kommission hat Joinup ins Leben gerufen, um einen gemeinsamen Ort zu schaffen, der es öffentlichen Verwaltungen, Unternehmen und Bürgern ermöglicht, IT-Lösungen und bewährte Verfahren auszutauschen und wiederzuverwenden sowie die Kommunikation und Zusammenarbeit bei IT-Projekten in ganz Europa zu erleichtern.