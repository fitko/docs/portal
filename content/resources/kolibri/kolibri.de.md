---
contactInformation:
  mail: kolibri@itzbund.de
  name: KoliBri-Team des ITZBund
developer:
  name: ITZBund
  url: https://www.itzbund.de/
docsUrl: https://public-ui.github.io
logo:
  path: public/img/resources/kolibri/KoliBri.jpg
  title: KoliBri
name: KoliBri - Komponenten-Bibliothek für die Barrierefreiheit
shortDescription: KoliBri ist eine Komponenten-Bibliothek mit dem Ziel, gemeinsam
  einen barrierefreien und wiederverwendbaren Standards für alle webbasierten Benutzeroberflächen
  zur Verfügung zu stellen.
sourceCodeUrl: https://github.com/public-ui/
tags:
- label:external
- type:software
- status:production
---

KoliBri ist eine Komponenten-Bibliothek mit dem Ziel, gemeinsam einen barrierefreien und wiederverwendbaren Standards für alle webbasierten Benutzeroberflächen zur Verfügung zu stellen.

Basierend auf dem W3C-Standard für Web Components wird eine maximale Wiederverwendbarkeit und Plattformunabhängigkeit gewährleistet. Durch die Möglichkeit verschiedene Styleguides in Form von Themes zu hinterlegen, ist KoliBri extrem flexibel und kann für nahezu alle Design Systeme wiederverwendet werden.