---
developer:
  name: Arbeitsgruppe "openDVA" der Uni Jena
  url: https://www.opendva.de/
docsUrl: https://docs.fitko.de/kompass/
name: Kompass der föderalen IT-Architektur
tags:
- type:information-assistance
- status:production
- label:external
---

Der Kompass der föderalen IT-Architektur gibt einen Überblick über alle wesentlichen Bestandteile der föderalen IT-Architektur: von verwaltungsrechtlichen und politischen Fragen, über Zugangsportale, Basisdienste bis hin zu Datenstandards und Transportsystemen.
Ziel ist es, die verschiedenen Bereiche inhaltlich in einen Zusammenhang zu setzen und ein erstes Verständnis für das komplexe Projekt der föderalen Verwaltungsdigitalisierung zu schaffen.
Um die Aktualität der Inhalte zu gewährleisten, verfolgt dieses Format einen kollaborativen Ansatz, der jeden interessierten Menschen dazu einläd, daran mitzuwirken.