---
contactInformation:
  mail: DVII3@bmi.bund.de
  name: BMI Referat DVII3 "Programmmanagement Verwaltungsdigitalisierung Bund"
developer:
  name: BMI
  url: https://www.bmi.bund.de/
docsUrl: https://feedback.gov.de/setup
name: Nationale Feedback-Komponente (NFK)
shortDescription: Die Nationale Feedback-Komponente (NFK) ist die nationale Lösung in Deutschland zur
  Erhebung, Sammlung und Auswertung von Nutzenden-Feedback.
tags:
- type:base
- label:external
- status:production
---

Die Nationale Feedback-Komponente (NFK) ist die nationale Lösung in Deutschland zur Erhebung, Sammlung und Auswertung von Nutzenden-Feedback gemäß den Vorgaben der Single Digital Gateway-Verordnung - [SDG-VO (EU) 2018/1724](https://eur-lex.europa.eu/legal-content/DE/TXT/HTML/?uri=CELEX:32018R1724&from=DE#art_17).

Die NFK bietet der öffentlichen Verwaltung die Möglichkeit, standardisiert und einfach, die Rückmeldungen ihrer Nutzer:innen einzuholen und auszuwerten.
Auf diese Weise können Online-Verfahren kontinuierlich in ihrem Angebot, der Qualität und Verfügbarkeit verbessert werden.

Weitere Informationen zur NFK finden sich [auf der Webseite des BMI](https://www.digitale-verwaltung.de/SharedDocs/kurzmeldungen/Webs/DV/DE/2025/02_nfk.html).