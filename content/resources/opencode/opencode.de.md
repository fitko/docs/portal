---
contactInformation:
  mail: info@opencode.de
  name: Open CoDE Kontakt
developer:
  name: IT-Planungsrat
  url: https://www.it-planungsrat.de/
docsUrl: https://opencode.de/
logo:
  path: public/img/resources/opencode/logo-opencode.svg
  title: Open Code Repository Logo
name: Open Source Code Repository der öffentlichen Verwaltung
tags:
- status:production
- label:external
- type:platform
- type:information-assistance
---

Open CoDE ist die gemeinsame Plattform der Öffentlichen Verwaltung für den Austausch von Open Source Software. Durch die zentrale Ablage von offenem Quellcode soll die Wiederverwendung und gemeinsame Arbeit an Softwarelösungen der Öffentlichen Verwaltung zwischen Verwaltung, Industrie und Gesellschaft gefördert werden. Die Plattform befindet sich derzeit im erweiterten Probebetrieb.