---
contactInformation:
  mail: ozg@fitko.de
  name: Koordination OZG-Programmmanagement (FITKO)
developer:
  name: Bundestag
  url: https://www.bundestag.de/
docsUrl: https://docs.fitko.de/arc/policies/ozg
logo:
  path: public/img/resources/ozg/titelbild.jpg
  title: Logo OZG
name: Gesetz zur Verbesserung des Onlinezugangs zu Verwaltungsleistungen (OZG)
tags:
- type:directive-guideline
- status:production
---

Das Onlinezugangsgesetz (OZG) verpflichtet Bund, Länder und Gemeinden, bis spätestens Ende 2022 ihre Verwaltungsleistungen auch elektronisch online über Verwaltungsportale anzubieten.