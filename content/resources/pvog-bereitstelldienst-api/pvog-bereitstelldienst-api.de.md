---
contactInformation:
  mail: pvog@fitko.de
  name: PVOG Produktteam (FITKO)
developer:
  name: IT-Planungsrat
  url: https://www.it-planungsrat.de/
docsUrl: https://anbindung.pvog.cloud-bdc.dataport.de/api/bereitstelldienst/
name: PVOG Bereitstelldienst API
shortDescription: Die vom Bereitstelldienst des PVOG bereitgestellte REST-API ermöglicht
  einen kontinuierlichen Abruf des Gesamtdatenbestands an Verwaltungsleistungen und
  dessen Aktualisierung.
tags:
- type:api
- status:production
---

Das Portalverbund-Onlinegateway (PVOG) stellt zentral die Verwaltungsleistungen von Bund und Ländern bereit. Die Aufgabe des Bereitstelldienstes ist es, den gesamten, im PVOG gesammelten Datenbestand für Verwaltungsportale und weitere Plattformen 
bereitzustellen. Die vom Bereitstelldienst bereitgestellte REST-API ermöglicht einen kontinuierlichen Abruf des Gesamtdatenbestands an Verwaltungsleistungen und dessen Aktualisierung.