---
contactInformation:
  mail: pvog@fitko.de
  name: PVOG Produktteam (FITKO)
developer:
  name: IT-Planungsrat
  url: https://www.it-planungsrat.de/
docsUrl: https://anbindung.pvog.cloud-bdc.dataport.de/api/sammlerdienst/
name: PVOG Sammlerdienst API
shortDescription: Die vom Sammlerdienst des PVOG bereitgestellte REST-API ermöglicht
  es den Redaktionssystemen Änderungen an den Zuständigkeiten zu Verwaltungsleistungen
  an das PVOG zu übermitteln.
tags:
- type:api
- status:production
---

Das Portalverbund-Onlinegateway (PVOG) stellt zentral die Verwaltungsleistungen von Bund und Ländern bereit. Die Aufgabe des Sammlerdienstes ist es, die Beschreibungen der Verwaltungsleistungen aus den verschiedenen Redaktionssystemen von Bund und Ländern 
einzusammeln. Eine vom Sammlerdienst bereitgestellte REST-API ermöglicht es den Redaktionssystemen Änderungen an den Zuständigkeiten zu Verwaltungsleistungen an das PVOG zu übermitteln.