---
contactInformation:
  mail: pvog@fitko.de
  name: PVOG Produktteam (FITKO)
developer:
  name: IT-Planungsrat
  url: https://www.it-planungsrat.de/
docsUrl: https://anbindung.pvog.cloud-bdc.dataport.de/api/suchdienst/
name: PVOG Suchdienst API
shortDescription: "Die vom Suchdienst des PVOG bereitgestellte REST-API bietet verschiedene Endpunkte zum Durchsuchen des Datenbestands des PVOG."
tags:
- type:api
- status:production
---

Das Portalverbund-Onlinegateway (PVOG) stellt zentral die Verwaltungsleistungen von Bund und Ländern bereit. Die Aufgabe des Suchdienstes ist es, die bereitgestellten Verwaltungsleistungen anwendergerecht und öffentlich durchsuchbar und auffindbar zu machen. Die vom Suchdienst bereitgestellte REST-API bietet verschiedene 
Endpunkte zum Durchsuchen des Datenbestands des PVOG.
