---
childResources:
- name: PVOG Bereitstelldienst API
  slug: pvog-bereitstelldienst-api
- name: PVOG Sammlerdienst API
  slug: pvog-sammlerdienst-api
- name: PVOG Suchdienst API
  slug: pvog-suchdienst-api
contactInformation:
  mail: pvog@fitko.de
  name: PVOG Produktteam (FITKO)
developer:
  name: IT-Planungsrat
  url: https://www.it-planungsrat.de/
docsUrl: https://anbindung.pvog.cloud-bdc.dataport.de/
name: Portalverbund Onlinegateway (PVOG)
shortDescription: Das Portalverbund Online-Gateway (PVOG) verknüpft die Verwaltungsportale
  des Bundes und der Länder und ermölicht einen einfachen und maschinenlesbaren Zugang
  zu Zuständigkeitsinformationen aller Verwaltungsleistungen auf allen föderalen Ebenen
  (Bund, Länder und Kommunen).
tags:
- type:base
- type:platform
- status:production
---

Das Portalverbund Online-Gateway (PVOG) verknüpft die Verwaltungsportale des Bundes und der Länder und ermöglicht einen einfachen und maschinenlesbaren Zugang zu Zuständigkeitsinformationen aller Verwaltungsleistungen auf allen föderalen Ebenen (Bund, Länder und Kommunen).
Über das PVOG sind in den verknüpften Portalen die Daten aller teilnehmenden Portale verfügbar, so dass Bürger:innen bundesweit alle 
Online-Verwaltungsleistungen erreichen können.
Das PVOG bietet diese Daten über verschiedene für Mensch oder Maschine ausgelegte Schnittstellen an.

Weitere Informationen zum Produkt finden sich [auf der Webseite der FITKO](https://www.fitko.de/produktmanagement/pvog).
