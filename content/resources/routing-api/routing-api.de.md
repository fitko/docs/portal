---
contactInformation:
  mail: fit-connect@fitko.de
  name: FIT-Connect Produktteam (FITKO)
developer:
  name: IT-Planungsrat
  url: https://www.it-planungsrat.de/
docsUrl: https://docs.fitko.de/fit-connect/docs/apis/routing-api/
apiSpecUrl:
logo:
  path: public/img/resources/routing-api/df94f47069316480.jpeg
  title: Fit-Connect Logo
name: FIT-Connect Routing API
shortDescription: API für die Ermittlung notwendiger Routinginformationen anhand von
  FIM-[Leistungsschlüsseln](https://de.wikipedia.org/wiki/LeiKa#Schl%C3%BCsselsystematik)
  (ehemals. LeiKa-Schlüssel) und einem [Amtlichen Regionalschlüssel](https://de.wikipedia.org/wiki/Amtlicher_Gemeindeschl%C3%BCssel#Regionalschl%C3%BCssel)
  eines Gebietes (Bundesland, Landkreis, Kreisfreie Stadt, etc.).
sourceCodeUrl: https://git.fitko.de/fit-connect/routing-api
tags:
- type:api
- status:production
---

API für die Ermittlung notwendiger Routinginformationen anhand von FIM-[Leistungsschlüsseln](https://de.wikipedia.org/wiki/LeiKa#Schl%C3%BCsselsystematik) (ehemals. LeiKa-Schlüssel) und einem [Amtlichen Regionalschlüssel](https://de.wikipedia.org/wiki/Amtlicher_Gemeindeschl%C3%BCssel#Regionalschl%C3%BCssel) eines Gebietsobjektes (Bundesland, Landkreis, Kreisfreie Stadt, etc.).
