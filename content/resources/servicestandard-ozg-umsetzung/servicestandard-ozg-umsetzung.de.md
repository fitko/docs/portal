---
contactInformation:
  mail: ozg@fitko.de
  name: Koordination OZG-Programmmanagement (FITKO)
developer:
  name: BMI
  url: https://www.bmi.bund.de/
docsUrl: https://docs.fitko.de/arc/policies/servicestandard-ozg-umsetzung
name: Servicestandard für die OZG-Umsetzung
shortDescription: Die Nutzerorientierung ist das oberste Prinzip bei der Verwaltungsdigitalisierung,
  denn schließlich ist die OZG-Umsetzung nur dann erfolgreich, wenn sowohl Bürger:innen,
  als auch Unternehmen die Online-Services tatsächlich nutzen. Die Anwendung des programmübergreifenden
  Servicestandards soll sicherstellen, dass die Digitalisierung von Verwaltungsleistungen
  nutzerfreundlich vonstattengeht.
tags:
- type:directive-guideline
- type:information-assistance
- status:production
---

Die Nutzerorientierung das oberste Prinzip bei der Verwaltungsdigitalisierung. Eine OZG-Umsetzung ist nur dann erfolgreich, wenn sowohl Bürger:innen, als auch Unternehmen die Online-Services von Bund, Ländern und Kommunen tatsächlich nutzen. Als Empfehlung für die Digitalisierung von Verwaltungsleistungen ist der Servicestandard eine Hilfestellung für alle Beteiligten bei der Entwicklung und Optimierung digitaler Verwaltungsangebote im Rahmen der OZG-Umsetzung und darüber hinaus. Die Anwendung des programmübergreifenden Servicestandards soll dabei sicherstellen, dass die Digitalisierung von Verwaltungsleistungen nutzerfreundlich vonstatten geht.