---
contactInformation:
  mail: architekturmanagement@fitko.de
  name: FITKO Architekturmanagement
developer:
  name: IT-Planungsrat
  url: https://www.it-planungsrat.de/
docsUrl: https://docs.fitko.de/standards-und-schnittstellen/
name: Dokumentation der Standards & Schnittstellen des Portalverbund
tags:
- status:production
- type:information-assistance
---

Das Portal Standards und Schnittstellen dokumentiert im Bereich des Antragsmanagements alle Kommunikationsbeziehung zwischen Anwendungen und Basisdiensten. Für diese Kommunikationsbeziehungen werden existierende Kommunikations-/Schnittstellenstandards sowie Standardisierungsbedarfe und laufende Standardisierungsvorhaben dokumentiert und soweit vorhanden, auf weiterführende Informationen verlinkt.