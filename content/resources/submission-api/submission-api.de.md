---
contactInformation:
  mail: fit-connect@fitko.de
  name: FIT-Connect Produktteam (FITKO)
developer:
  name: IT-Planungsrat
  url: https://www.it-planungsrat.de/
docsUrl: https://docs.fitko.de/fit-connect/docs/apis/submission-api
apiSpecUrl:
logo:
  path: public/img/resources/submission-api/df94f47069316480.jpeg
  title: Fit-Connect Logo
name: FIT-Connect Submission API
tags:
- type:api
- status:production
---

Zentrale Komponente der FIT-Connect-Infrastruktur. Die Submission API ermöglicht im Kontext des Onlinezugangsgesetzes (OZG) die maschinenlesbare Übermittlung von Anträgen oder Berichten an Fachverfahren der jeweils zuständigen Fachbehörden auf allen föderalen Ebenen.
