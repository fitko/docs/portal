---
contactInformation:
  mail: ozg@fitko.de
  name: Koordination OZG-Programmmanagement (FITKO)
developer:
  name: IT-Planungsrat
  url: https://www.it-planungsrat.de/
docsUrl: https://docs.fitko.de/arc/policies/verwaltungsabkommen-ozg
name: Verwaltungsabkommen zur Umsetzung des Onlinezugangsgesetzes
shortDescription: Der Bund, die Länder und Kommunen haben sich darauf verständigt
  bei der Digitalisierung von Verwaltungsleistungen zu kooperieren. Im Zuge dieses
  Verwaltungsabkommens werden Ziele, Umfang und Rahmenbedingungen die dieser Kooperation
  festgehalten.
tags:
- type:directive-guideline
- status:production
---

Bund und Länder streben die kooperative, einheitliche, zukunftsweisende und effiziente Umsetzung des OZGs an und haben sich aus diesem Grund zu diesem Verwalungsabkommen geeinigt. Es wird ein interdisziplinäres Arbeiten, eine agile Arbeitsweise, arbeitsteiliges Vorgehen und die konsequente Nutzerzentrierung zugrunde gelegt. Die Digitalisierung erfolgt nach dem Modell „Einer für Alle/Einer für Viele“.