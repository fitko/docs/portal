---
contactInformation:
  mail: kosit@finanzen.bremen.de
  name: Koordinierungsstelle für IT Standards (KoSIT)
developer:
  name: Koordinierungsstelle für IT Standards (KoSIT)
  url: https://www.xoev.de/
docsUrl: https://docs.xoev.de/
logo:
  path: public/img/resources/xoev/IT-03-09-XOEV_02_372x248.jpg
  title: XÖV Logo
name: XÖV - XML in der öffentlichen Verwaltung
tags:
- status:production
---

Für die Entwicklung und den Betrieb von IT-Standards für den Datenaustausch zwischen Fachverfahren in der öffentlichen Verwaltung stellt das XÖV-Rahmenwerk praxiserprobte Methoden und Produkte bereit. Es wird von der Koordinierungsstelle für IT-Standards (KoSIT) betrieben und weiterentwickelt.