---
contactInformation:
  mail: kosit@finanzen.bremen.de
  name: Koordinierungsstelle für IT Standards (KoSIT)
developer:
  name: Koordinierungsstelle für IT Standards (KoSIT)
  url: https://www.xoev.de/
docsUrl: https://www.xrepository.de/
logo:
  path: public/img/resources/xrepository/IT-03-09-XOEV_02_372x248.jpg
  title: XÖV Logo
name: XRepository
tags:
- status:production
- type:platform
---

Das XRepository wurde realisiert, um einen zentralen und verlässlichen Ort für die Ablage, Verwaltung und öffentliche Bereitstellung XÖV-konformer Standards und Codelisten zu schaffen. Nutzerinnen und Nutzer der Plattform können sich über die bereitgestellten Inhalte informieren, deren Beziehungen zu anderen Inhalten recherchieren und Inhalte beziehen.