---
contactInformation:
  mail: zsk@bmi.bund.de
  name: BMI Referat DVII1 "Steuerung Digitalprogramme und Verwaltungsdigitalisierungsvorhaben"
developer:
  name: BMI
  url: https://www.bmi.bund.de/
docsUrl: https://statistik.ozg-umsetzung.de/
name: Zentrale Statistik-Komponente (ZSK)
shortDescription: Die ZSK sammelt nicht-personenbezogene Nutzungsdaten von elektronischen
  Verwaltungsleistungen und stellt diese dar.
tags:
- type:base
- label:external
- status:production
---

Die Zentrale Statistik-Komponente (ZSK) aggregiert nicht-personenbezogenen Nutzungsdaten von Onlineservices und stellt diese dar.
Als zentraler Indikator zieht sie die monatliche, regional differenzierte Transaktionszahl eines Onlineservices heran.
Als Transaktionen zählen die initiale Antragstellung, Anzeige oder Meldung Nutzender gegenüber einer Behörde.
Die Plattform bietet mehrere Möglichkeiten der Datenübermittlung, um die Bereitstellung der Nutzungsdaten für die Betreiber:innen von Onlineservices so einfach wie möglich zu gestalten.

Weitere Informationen zur ZSK finden sich [auf der Webseite des BMI](https://www.digitale-verwaltung.de/Webs/DV/DE/onlinezugangsgesetz/rahmenarchitektur/basisdienste-basiskomponenten/zentrale-statistik-komponente/zentrale-statistik-komponente-node.html).