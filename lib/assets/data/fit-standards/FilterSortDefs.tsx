/*
 ****** Filter definitions for FlexFilter for FIT-Standards *****
 *
 */

import { ResourceContent } from 'types/ResourceContent'

// Our new filter component
import { BooleanOption, BoolAutoFilterGroup, BoolForcedFilterGroup, FilterGroup } from 'types/filters/'
import { OpenClose } from 'types/filters/FilterGroup'

// Our list sort component
import { SortCriteria, SortOrder } from '@/components/fit-standards/ListSort'

// Our constants
import MY_CONSTANTS from '@/lib/constants-standards.js'
import { LiabilityType } from 'types/content/Liability'

export const licenses = [
    {
        licenseID: 'CC-BY',
        tooltipText: 'CC BY 4.0',
        licenseURL: 'https://creativecommons.org/licenses/by/4.0/',
        fullTitle: 'Creative Commons Attribution 4.0 International',
    },
    {
        licenseID: 'CC-BY-NC-ND',
        tooltipText: 'CC BY-NC-ND 4.0',
        licenseURL: 'https://creativecommons.org/licenses/by-nc-nd/4.0/',
        fullTitle: 'Creative Commons Attribution-NonCommercial-NoDerivs 4.0 International',
    },
    {
        licenseID: 'EUPL',
        tooltipText: 'OPEN-SOURCE-LIZENZ FÜR DIE EUROPÄISCHE UNION',
        licenseURL: 'https://joinup.ec.europa.eu/collection/eupl/eupl-guidelines-faq-infographics',
        fullTitle: 'OPEN-SOURCE-LIZENZ FÜR DIE EUROPÄISCHE UNION',
    },
    {
        licenseID: 'Apache',
        tooltipText: 'Apache License 2.0',
        licenseURL: 'https://www.apache.org/licenses/LICENSE-2.0.html',
        fullTitle: 'Apache License 2.0',
    },
    {
        licenseID: 'AGPL',
        tooltipText: 'GNU Affero General Public License',
        licenseURL: 'https://www.gnu.org/licenses/agpl-3.0.html',
        fullTitle: 'GNU AFFERO GENERAL PUBLIC LICENSE',
    },
    {
        licenseID: 'GFDL',
        tooltipText: 'GNU Free Documentation License',
        licenseURL: 'https://www.gnu.org/licenses/fdl-1.3#license-text',
        fullTitle: 'GNU Free Documentation License, version 1.3',
    },
    {
        licenseID: 'LGPL',
        tooltipText: 'GNU Lesser General Public License (LGPL), version 2.1 v',
        licenseURL: 'https://www.gnu.org/licenses/lgpl-3.0.html',
        fullTitle: 'GNU Lesser General Public License (LGPL), version 2.1 v',
    },
]

export const frameworks = [
    {
        frameworkID: 'OpenAPI',
        tooltipText: 'OpenAPI Specification (OAS)',
        frameworkURL: 'https://www.openapis.org/',
        fullTitle: 'OpenAPI',
    },
    {
        frameworkID: 'RDF',
        tooltipText: 'Resource Description Framework (RDF)',
        frameworkURL: 'https://www.w3.org/RDF/',
        fullTitle: 'Resource Description Framework',
    },
    {
        frameworkID: 'XÖV',
        tooltipText: 'XÖV-Rahmenwerk',
        frameworkURL: 'https://docs.xoev.de/xoev-handbuch/',
        fullTitle: 'XÖV-Rahmenwerk',
    },
]

// Our filter functions, used later on in the filter configuration.
// They basically answer the question 'does a given object (FIt standard) meet the filter criteria?'
function isBedarfsmeldung(elem: ResourceContent): boolean {
    return elem.status === 'bedarfsmeldung'
}
function isPrüfung(elem: ResourceContent): boolean {
    return elem.status === 'prüfung'
}
function isUmsetzung(elem: ResourceContent): boolean {
    return elem.status === 'umsetzung'
}
function isGenehmigung(elem: ResourceContent): boolean {
    return elem.status === 'genehmigung_plr'
}
function isBetriebsüberführung(elem: ResourceContent): boolean {
    return elem.status === 'betriebsüberführung'
}
function isRegelbetrieb(elem: ResourceContent): boolean {
    return elem.status === 'regelbetrieb'
}
function isDekommissionierung(elem: ResourceContent): boolean {
    return elem.status === 'dekommissionierung'
}
function isSlave(elem: ResourceContent): boolean {
    return elem.slugMaster == '' || elem.slugMaster == elem.slug
}
function isG2g(elem: ResourceContent): boolean {
  return elem.contextRelation.length && elem.contextRelation.includes('G2G')
}
function isB2g(elem: ResourceContent): boolean {
  return elem.contextRelation.length && elem.contextRelation.includes('B2G')
}
function isC2g(elem: ResourceContent): boolean {
  return elem.contextRelation.length && elem.contextRelation.includes('C2G')
}
function isB2b(elem: ResourceContent): boolean {
  return elem.contextRelation.length && elem.contextRelation.includes('B2B')
}

// Collects all stakeholder organisationss (product owner, developer, operator, contact)
// from a given FIT standard
interface Stakeholder {
  organisation?: string
}

function getStakeholder(elem: ResourceContent): Array<string> {
  return MY_CONSTANTS.CONTACT_TYPES
    .map(({ id }) => {
      const stakeholder = elem[id as keyof ResourceContent] as Stakeholder
      return stakeholder?.organisation
    })
    .filter((org): org is string => !!org)
}

// Collects all context content from a given FIT standard
function getContextContent(elem: ResourceContent): Array<string> {
  const found: Array<string> = []
  if (Array.isArray(elem.contextContent)) {
      elem.contextContent.forEach(value => {
          if (typeof value === 'string' && value.trim() !== '') {
              found.push(MY_CONSTANTS.CONTEXT_CONTENT[value])
          }
      })
  }
  return found
}

function getFramework(elem: ResourceContent): Array<string> {
    const found: Array<string> = []
    if (typeof elem.framework != 'undefined') {
        found.push(frameworks.find((myObj) => { return myObj.frameworkID === elem.framework.id }).fullTitle)
    }
    return found
}

function isRequiredByPlr(elem: ResourceContent): boolean {
  return Array.isArray(elem.liability) && 
         elem.liability.length > 0 && 
         elem.liability.some(item => LiabilityType.Verbindlich_PLR === item.liability);
}

function isRecommendedByPlr(elem: ResourceContent): boolean {
  return Array.isArray(elem.liability) && 
         elem.liability.length > 0 && 
         elem.liability.some(item => LiabilityType.Empfohlen_PLR === item.liability);
}

function isRecommendedByFitSb(elem: ResourceContent): boolean {
  return Array.isArray(elem.liability) && 
         elem.liability.length > 0 && 
         elem.liability.some(item => LiabilityType.Empfohlen_FitSb === item.liability);
}

function isOzgRv(elem: ResourceContent): boolean {
  return Array.isArray(elem.liability) && 
         elem.liability.length > 0 && 
         elem.liability.some(item => LiabilityType.OzgRv === item.liability);
}

function isRv(elem: ResourceContent): boolean {
  return Array.isArray(elem.liability) && 
         elem.liability.length > 0 && 
         elem.liability.some(item => LiabilityType.Rv === item.liability);
}

function isOther(elem: ResourceContent): boolean {
  return Array.isArray(elem.liability) && 
         elem.liability.length > 0 && 
         elem.liability.some(item => LiabilityType.Other === item.liability);
}

function isUndefined(elem: ResourceContent): boolean {
  if (!Array.isArray(elem.liability) || elem.liability.length === 0) {
    return false;
  }
  
  return elem.liability[0].liability === LiabilityType.NOT_DEFINED;
}

// Configuration of the filters for the FlexFilter component
export const myFilters: Array<FilterGroup> = [
    new BoolForcedFilterGroup(
        {
            id: 'status',
            label: 'Lebenszyklusphasen',
            openCloseMode: OpenClose.ForcedOpen,
            options: [
                new BooleanOption({
                    label: 'Identifizierung und Bedarfsmeldung',
                    id: 'bedarfsmeldung',
                    evalFunc: isBedarfsmeldung,
                }),
                new BooleanOption({
                    label: 'Prüfung und Bewertung',
                    id: 'prüfung',
                    evalFunc: isPrüfung,
                }),
                new BooleanOption({
                    label: 'Umsetzung',
                    id: 'umsetzung',
                    evalFunc: isUmsetzung,
                }),
                new BooleanOption({
                    label: 'Genehmigung',
                    id: 'genehmigung_plr',
                    evalFunc: isGenehmigung,
                }),
                new BooleanOption({
                    label: 'Überführung in den Regelbetrieb',
                    id: 'betriebsüberführung',
                    evalFunc: isBetriebsüberführung,
                }),
                new BooleanOption({
                    label: 'Regelbetrieb und Monitoring',
                    id: 'regelbetrieb',
                    evalFunc: isRegelbetrieb,
                }),
                new BooleanOption({
                    label: 'Dekommissionierung',
                    id: 'dekommissionierung',
                    evalFunc: isDekommissionierung,
                }),
            ],
        }),
    new BoolForcedFilterGroup(
        {
            id: 'slugMaster',
            label: 'Verbundene FIT-Standards',
            color: MY_CONSTANTS.COLORS.COMBINED_STANDARDS,
            icon: 'IconCirclesRelation',
            openCloseMode: OpenClose.DefaultOpen,
            options: [
                new BooleanOption({
                    label: 'Untergeordnete FIT-Standards ausblenden',
                    id: 'parent',
                    evalFunc: isSlave,
                }),
            ],
        }
    ),
    new BoolForcedFilterGroup(
      {
        id: 'contextRelations',
        label: 'Beziehungskontext',
        openCloseMode: OpenClose.DefaultClose,
        options: [
          new BooleanOption({
            label: 'Government-to-Government',
            id: 'g2g',
            evalFunc: isG2g,
          }),
          new BooleanOption({
            label: 'Business-to-Business',
            id: 'b2b',
            evalFunc: isB2b,
          }),
          new BooleanOption({
            label: 'Citizen-to-Government',
            id: 'c2g',
            evalFunc: isC2g,
          }),
          new BooleanOption({
            label: 'Business-to-Government',
            id: 'b2g',
            evalFunc: isB2g,
          }),
        ],
      },
    ),
    new BoolAutoFilterGroup(
        {
          id: 'contextContent',
          label: 'Inhaltlicher Kontext',
          getLabelsOfElementFunc: getContextContent,
          openCloseMode: OpenClose.DefaultClose,        
        },
    ),
    new BoolAutoFilterGroup(
        {
            getLabelsOfElementFunc: getStakeholder,
            id: 'organisations',
            label: 'Organisationen',
            openCloseMode: OpenClose.DefaultClose,
        }
    ),
    new BoolForcedFilterGroup(
        {
          id: 'liability',
          label: 'Gültigkeit',
          color: MY_CONSTANTS.COLORS.LIABILITY,
          icon: 'IconFileCheck',
          openCloseMode: OpenClose.DefaultOpen,
          options: [
            new BooleanOption({
              label: 'Verbindlich (IT-PLR)',
              id: 'requiredByPlr',
              evalFunc: isRequiredByPlr,
            }),
            new BooleanOption({
              label: 'Empfohlen (IT-PLR)',
              id: 'recommendedByPlr',
              evalFunc: isRecommendedByPlr,
            }),
            new BooleanOption({
              label: 'Empfohlen (FIT-SB)',
              id: 'recommendedByFitSb',
              evalFunc: isRecommendedByFitSb,
            }),
            new BooleanOption({
              label: 'OZG RV - Rechtsverordnung nach §6 OZG',
              id: 'ozgRv',
              evalFunc: isOzgRv
            }),
            new BooleanOption({
              label: 'Rechtsverordnung',
              id: 'rv',
              evalFunc: isRv,
            }),
            new BooleanOption({
              label: 'Andere Vorgabe',
              id: 'other',
              evalFunc: isOther,
            }),  
            new BooleanOption({
              label: 'Nicht definiert',
              id: 'undefined',
              evalFunc: isUndefined,
            }),  
          ]
        }
    ),
    new BoolAutoFilterGroup(
        {
            getLabelsOfElementFunc: getFramework,
            id: 'frameworks',
            label: 'Methode/Framework',
            openCloseMode: OpenClose.DefaultClose,
        }
    ),
]

// Get values from FIT standards used for sorting
function getName(elem: ResourceContent): string {
    return elem.name.toLocaleLowerCase()
}
function getLastUpdate(elem: ResourceContent): Date {
    return elem.dateLastUpdate
}
function getLifecycleIndex(elem: ResourceContent): number {
    const statusArray = ['bedarfsmeldung', 'prüfung', 'umsetzung', 'genehmigung_plr',
        'betriebsüberführung', 'regelbetrieb', 'dekommissionierungsempfehlung', 'dekommissionierung']
    return statusArray.indexOf(elem.status)
}

// Specify sort criteria employed by the ListSort component
export const mySortCriteria: SortCriteria[] = [
    {
        id: 'alphabet',
        label: 'Alphabetisch',
        sortOrder: SortOrder.ascending,
        getValueFunc: getName,
    },
    {
        id: 'last-update',
        label: 'Letzte Aktualisierung: Neueste zuerst',
        sortOrder: SortOrder.descending,
        getValueFunc: getLastUpdate,
    },
    {
        id: 'lifecycle-asc',
        label: 'Lebenszyklusphase: Beginnend bei Bedarfsmeldung',
        sortOrder: SortOrder.ascending,
        getValueFunc: getLifecycleIndex,
    },
    {
        id: 'lifecycle-desc',
        label: 'Lebenszyklusphase: Beginnend bei Dekommissionierung',
        sortOrder: SortOrder.descending,
        getValueFunc: getLifecycleIndex,
    },
]
