/*
 *  All the constants for the Federal IT-Standards
 *
 *  Author:         Jürgen Voskuhl, itcv GmbH
 *  Last update:    2023-09-25
 *
 * CHANGES
 * =======
 *  Date        | Name      | remarks
 *  ============+==========================================================
 *  2025-02-03  | JVo       | Added tailwind Color defintions
 *              |           |
 *  2025-02-15  | JVo       | Text list for standard type
 *              |           |
 *              |           |
 *
*/

module.exports = Object.freeze({
    // Width of 1st column in info boxes (Tailwind)
    INFOBOX_1ST_COLUMN_WIDTH: 'w-20',

    // Background colors used for info boxes and chips/badges (Tailwind)
    BG_COLORS: {
        ROLES: 'bg-red-100',
        COMBINED_STANDARDS: 'bg-yellow-100',
        DEFAULT: 'bg-slate-100',

    },
    // Color definitions used for info boxes and chips/labels/badges (Tailwind)
    COLORS: {
      ROLES: 'bg-itplr_blau-6 border-itplr_blau-6 text-black',
      CONTEXT_CONTENT: 'bg-itplr_blau-4 border-itplr_blau-4 text-black',
      CONTEXT_RELATION: 'bg-itplr_blau-1 border-itplr_blau-1 text-white',
      COMBINED_STANDARDS: 'bg-itplr_senf border-itplr_senf text-black',
      LIABILITY: 'bg-itplr_opal bg-itplr_opal text-white',
      STANDARD_TYPE: 'bg-itplr_malve border-itplr_malve text-white',
      DEFAULT: 'bg-itplr_grau-4 border-itplr_grau-4 text-black',
    },


    // All possible life cycles phases
    LYFE_CYCLE_PHASES: [
        'bedarfsmeldung',
        'prüfung',
        'umsetzung',
        'genehmigung_plr',
        'betriebsüberführung',
        'regelbetrieb',
        'dekommissionierungsempfehlung',
        'genehmigung_dekommissionierung',
        'dekommissionierung',
    ],

    // All possible artefacts
    ARTEFACTS: [
        { id: 'artefaktSpec', text: 'Spezifikation' },
        { id: 'artefaktDocumentation', text: 'Benutzerdokumentation' },
        { id: 'artefaktTestumgebung', text: 'Testumgebung' },
        { id: 'artefaktRefImplementation', text: 'Referenzimplementierung' },
        { id: 'artefaktBetriebskonzept', text: 'Betriebskonzept' },
    ],

    // Contact types
    CONTACT_TYPES: [
      { id: 'initiator', text: 'Initiierung' },
      { id: 'productOwner', text: 'Bedarfsträger' },
      { id: 'steeringCommittee', text: 'Steuerung' },
      { id: 'developer', text: 'Umsetzungsteam' },
      { id: 'operator', text: 'Betreiber' },
      { id: 'contact', text: 'Support' },
      { id: 'funder', text: 'Finanzierung' },
    ],

    // Standard types
    STANDARD_TYPES: [
      { id: 'architektur', text: 'Architektur-Standard' },
      { id: 'taxonomie', text: 'Taxonomie' },
      { id: 'tech_fach', text: 'Techn.-fachliche Klassifizierungen' },
      { id: 'datenstruktur', text: 'Datenstruktur' }, 
      { id: 'design', text: 'Designsprachen und -systeme' },
      { id: 'datenaustausch', text: 'Datenaustausch-Standard' },
      { id: 'tsp_sich', text: 'Transport- und Sicherheits-Standard' },
      { id: 'api', text: 'APIs' },
    ],

    // Inhaltlicher Kontext
    CONTEXT_CONTENT: {
      procurement: 'Beschaffung',
      ozg: 'Onlineservice (OZG)',
      reg_modernisation: 'Registermodernisierung',
      other: 'Anderer Kontext',
      undefined: 'Nicht definiert'
    },

    // Liability
    LIABILITY: [
      { id: 'Verbindlich (IT-PLR)', colors: 'bg-itplr_opal text-white' },
      { id: 'Empfohlen (IT-PLR)', colors: 'bg-itplr_opal text-white' },
      { id: 'Empfohlen (FIT-SB)', colors: 'bg-itplr_opal text-white' },
      { id: 'OZG RV - Rechtsverordnung nach §6 OZG', colors: 'bg-itplr_opal text-white' },
      { id: 'Rechtsverordnung', colors: 'bg-itplr_opal text-white' },
      { id: 'Andere Vorgabe', colors: 'bg-itplr_opal text-white' },
      { id: 'Nicht definiert', colors: 'bg-itplr_opal text-white' },
  ]
})
