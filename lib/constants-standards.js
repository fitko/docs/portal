/*
 *  All the constants for the it-standards
 *
 *  Author:         Jürgen Voskuhl, itcv GmbH
 *  Last update:    2023-09-25
 *
 * CHANGES
 * =======
 *  Date        | Name      | remarks
 *  ============+==========================================================
 *  2025-02-03  | JVo       | Added tailwind Color defintions
 *              |           |
 *              |           |
 *              |           |
 *
*/

module.exports = Object.freeze({
    // Width of 1st column in info boxes (Tailwind)
    INFOBOX_1ST_COLUMN_WIDTH: 'w-20',

    // Background colors used for info boxes and chips/badges (Tailwind)
    BG_COLORS: {
        ROLES: 'bg-red-100',
        COMBINED_STANDARDS: 'bg-yellow-100',
        DEFAULT: 'bg-slate-100',

    },
    // Color definitions used for info boxes and chips/labels/badges (Tailwind)
    COLORS: {
      ROLES: 'bg-itplr_blau-6 border-itplr_blau-6 text-black',
      CONTEXT_CONTENT: 'bg-itplr_blau-4 border-itplr_blau-4 text-black',
      CONTEXT_RELATION: 'bg-itplr_blau-1 border-itplr_blau-1 text-white',
      COMBINED_STANDARDS: 'bg-itplr_senf border-itplr_senf text-black',
      LIABILITY: 'bg-itplr_opal bg-itplr_opal text-white',
      DEFAULT: 'bg-itplr_grau-4 border-itplr_grau-4 text-black',
    },


    // All possible life cycles phases
    LYFE_CYCLE_PHASES: [
        'bedarfsmeldung',
        'prüfung',
        'umsetzung',
        'genehmigung_plr',
        'betriebsüberführung',
        'regelbetrieb',
        'dekommissionierungsempfehlung',
        'genehmigung_dekommissionierung',
        'dekommissionierung',
    ],

    // All possible artefacts
    ARTEFACTS: [
        { id: 'artefaktSpec', text: 'Spezifikation' },
        { id: 'artefaktDocumentation', text: 'Benutzerdokumentation' },
        { id: 'artefaktTestumgebung', text: 'Testumgebung' },
        { id: 'artefaktRefImplementation', text: 'Referenzimplementierung' },
        { id: 'artefaktBetriebskonzept', text: 'Betriebskonzept' },
    ],

    // Contact types
    CONTACT_TYPES: [
      { id: 'initiator', text: 'Initiierung' },
      { id: 'productOwner', text: 'Bedarfsträger' },
      { id: 'steeringCommittee', text: 'Steuerung' },
      { id: 'developer', text: 'Umsetzungsteam' },
      { id: 'operator', text: 'Betreiber' },
      { id: 'contact', text: 'Support' },
      { id: 'funder', text: 'Finanzierung' },
    ],

    // Inhaltlicher Kontext
    CONTEXT_CONTENT: {
      procurement: 'Beschaffung',
      ozg: 'Onlineservice (OZG)',
      reg_modernisation: 'Registermodernisierung',
      other: 'Anderer Kontext',
      undefined: 'Nicht definiert'
    },

    // Liability
    LIABILITY: [
      { id: 'Verbindlich (IT-PLR)',
        colors: 'bg-itplr_opal text-white',
        icon: 'IconExclamationMark',
      },
      { id: 'Empfohlen (IT-PLR)',
        colors: 'bg-itplr_opal text-white',
        icon: 'IconThumbUpFilled',
      },
      { id: 'Empfohlen (FIT-SB)',
        colors: 'bg-itplr_opal text-white',
        icon: 'IconThumbUpFilled',
      },
      { id: 'OZG RV - Rechtsverordnung nach §6 OZG', 
        colors: 'bg-itplr_opal text-white',
        icon: 'IconSectionSign', 
      },
      { id: 'Rechtsverordnung', 
        colors: 'bg-itplr_opal text-white',
        icon: 'IconSectionSign',
       },
       { id: 'Andere Vorgabe',
        colors: 'bg-itplr_opal text-white',
        icon: 'IconQuestionMark',
       },
      { id: 'Nicht definiert',
        colors: 'bg-itplr_opal text-white',
        icon: 'IconQuestionMark',
       },
    ]
})
