/*
 * Helper funtions for URL parameter handling
 *
 */
import querystring, { ParsedUrlQuery } from 'querystring'

/**
 * validate query parameter: the @param is in @query and it's value is not empty
 * @param query - useRouter query
 * @param parameter - the parameter name in question
 * @returns empty array or array of strings
 *
 * Based on https://stackoverflow.com/questions/76133097/how-to-best-write-next-router-query-in-typescript
 *
 */
export function validateQueryParameter(
    query: ParsedUrlQuery,
    parameter: string
): string[] {
    // eslint-disable-next-line no-prototype-builtins
    if (!query.hasOwnProperty(parameter)) return []
    if (query[parameter] === '') return []
    const queryString: string = query[parameter] as string
    if (typeof queryString == 'object') {
        return queryString
    } else {
        return queryString.split(';')
    }
}

/**
 * Update a query parameter
 * @param query - useRouter query
 * @param param - the parameter name in question
 * @param value - the new value
 *
 * Return value:
 * A string with the new parameter string or false, if nothing has changed
 *
 * Written by J. Voskuhl, itcv
 *
 */
export function updateQueryParameter(
    query: ParsedUrlQuery | String,
    param: string,
    value: string
): string | false {
    let newParamString = ''
    let myKey: keyof ParsedUrlQuery
    let hasBeenReplaced = false
    let isDifferent = false

    if (typeof query == 'string') {
        query = querystring.parse(query)
    }

    // Walk trough all params and build new params string
    for (myKey in query) {
        // Do we want to replace this parameter with our new value?
        if (myKey == param) {
            hasBeenReplaced = true
            if (query[myKey] != value) {
                isDifferent = true
                newParamString += '&' + myKey + '=' + value
            }
        } else {
            newParamString += '&' + myKey + '=' + query[myKey]
        }
    }
    // Didn't we came across our parameter so far?
    if (!hasBeenReplaced) {
        // Obviously not, so append it!
        newParamString += '&' + param + '=' + value
        isDifferent = true
    }

    // Has anything changed?
    if (isDifferent) {
        // Remove first ampersand
        newParamString = newParamString.substring(1)

        // Return the new parameter string
        return newParamString
    }
    // No changes at all!
    return false
}

// eslint-disable-next-line no-unused-vars
export function toObject(paramString: string) {
    const params: { [key: string]: string } = {}

    if (paramString == '') {
        return params
    }

    const keyValuePairs = paramString.split('&')
    keyValuePairs.forEach(function (thisPair) {
        const keyValue = thisPair.split('=')
        if (keyValue[1] != '') {
            params[keyValue[0]] = keyValue[1]
        }
    })
    return params
}

export function toSafeString(myString: string) {
    return myString
        .replace(' ', '_')
        .replace('-', '_')
        .replace('&', '_')
        .toLowerCase()
}

export function getQueryParams(query: ParsedUrlQuery, asPath: string): string {
    let retVal = ''

    // No parameters in query object?
    if (!query) {
        // Look for Parameters at the end of the URL string
        const found = asPath.indexOf('?')
        // Did we find at least a question mark?
        if (found != -1) {
            retVal = decodeURIComponent(asPath.substring(found + 1))
        }
    } else {
        retVal = decodeURIComponent(querystring.stringify(query))
    }
    return retVal
}
