import { MouseEvent } from 'react'


interface JsonSerializable {  // For type safety, ensure the object is JSON serializable
  [key: string]: any;
}

export function removeFromObjRecursive<T extends JsonSerializable>(nodeName: string, nextJsDataObject: T): Partial<T> {
  return JSON.parse(
      JSON.stringify(nextJsDataObject, (key, value) => {
          return key !== nodeName ? value : undefined;
      })
  );
}

export function replaceEmailsRecursive(name: string, nextJsDataObject: any) {
    return JSON.parse(
        JSON.stringify(nextJsDataObject, function (key, value) {
            if (key === name) return encodeEmail(value)
            return value
        })
    )
}

export function encodeEmail(emailAddress: string) {
    return btoa(emailAddress)
}

export function decodeEmail(encodedEmailAddress: string) {
    return atob(encodedEmailAddress)
}

export function handleMailtoEvent(mouseEvent: MouseEvent<HTMLAnchorElement>) {
    const targetElement = mouseEvent.currentTarget
    const base = targetElement.dataset.base
    const mail = decodeEmail(base)
    targetElement.href = 'mailto:' + mail
    return true
}

export default {
    removeFromObjRecursive,
    replaceEmailsRecursive,
    encodeEmail,
    decodeEmail,
    handleMailtoEvent,
}
