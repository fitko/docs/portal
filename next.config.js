const webpack = require('webpack')

const conf = {
    output: 'export',
    distDir: 'build',
    assetPrefix: `${process.env.NEXT_PUBLIC_BASE_PATH ? process.env.NEXT_PUBLIC_BASE_PATH : '/'}`,
    basePath: `${process.env.NEXT_PUBLIC_BASE_PATH ? process.env.NEXT_PUBLIC_BASE_PATH : ''}`,
    trailingSlash: !process.env.TRAILING_SLASH || process.env.TRAILING_SLASH !== 'off',
    publicRuntimeConfig: {
        CURRENT_BRANCH: process.env.CURRENT_BRANCH,
    },
    webpack: (config) => {
        config.plugins.push(
            new webpack.DefinePlugin({
                'process.env.CURRENT_BRANCH': JSON.stringify(process.env.CURRENT_BRANCH),
            })
        )
        return config
    },
}

module.exports = conf
