import {
    IconBookmark,
    IconBook,
    IconRss,
    IconListDetails,
    IconChevronRight
} from '@tabler/icons-react'

export default function NotFound() {
    const links = [
        {
            title: 'Documentation',
            description: 'Learn how to integrate our tools with your app',
            icon: IconBook,
        },
        {
            title: 'API Reference',
            description: 'A complete API reference for our libraries',
            icon: IconListDetails,
        },
        {
            title: 'Guides',
            description: 'Installation guides that cover popular setups',
            icon: IconBookmark,
        },
        {
            title: 'Blog',
            description: 'Read our latest news and articles',
            icon: IconRss,
        },
    ]

    return (
        <div className="bg-white">
            <main className="max-w-7xl w-full mx-auto px-4 sm:px-6 lg:px-8">
                <div className="max-w-xl mx-auto py-16 sm:py-24">
                    <div className="text-center">
                        <p className="text-sm font-semibold text-yellow-600 uppercase tracking-wide">
              404 Fehler
                        </p>
                        <h1 className="mt-2 text-4xl font-extrabold text-gray-900 tracking-tight sm:text-5xl">
              Diese Seite konnte nicht gefunden werden
                        </h1>
                        <p className="mt-2 text-lg text-gray-500">
              Die von Ihnen gesuchte Seite konnte nicht gefunden werden.
                        </p>
                    </div>
                    <div className="mt-12">
                        <h2 className="text-sm font-semibold text-gray-500 tracking-wide uppercase">
              Andere besuchte Seiten
                        </h2>
                        <ul
                            role="list"
                            className="mt-4 border-t border-b border-gray-200 divide-y divide-gray-200"
                        >
                            {links.map((link, linkIdx) => (
                                <li
                                    key={linkIdx}
                                    className="relative py-6 flex items-start space-x-4"
                                >
                                    <div className="flex-shrink-0">
                                        <span className="flex items-center justify-center h-12 w-12 rounded-lg bg-yellow-50">
                                            <link.icon
                                                className="h-6 w-6 text-yellow-700"
                                                aria-hidden="true"
                                            />
                                        </span>
                                    </div>
                                    <div className="min-w-0 flex-1">
                                        <h3 className="mb-0 text-base font-medium text-gray-900">
                                            <span className="rounded-sm focus-within:ring-2 focus-within:ring-offset-2 focus-within:ring-yellow-500">
                                                <a href="#" className="focus:outline-none">
                                                    <span
                                                        className="absolute inset-0"
                                                        aria-hidden="true"
                                                    />
                                                    {link.title}
                                                </a>
                                            </span>
                                        </h3>
                                        <p className="mb-0 text-base text-gray-500">
                                            {link.description}
                                        </p>
                                    </div>
                                    <div className="flex-shrink-0 self-center">
                                        <IconChevronRight
                                            className="h-5 w-5 text-gray-400"
                                            aria-hidden="true"
                                        />
                                    </div>
                                </li>
                            ))}
                        </ul>
                        <div className="mt-8">
                            <a
                                href="#"
                                className="text-base font-medium text-yellow-600 hover:text-yellow-500"
                            >
                Or go back home<span aria-hidden="true"> &rarr;</span>
                            </a>
                        </div>
                    </div>
                </div>
            </main>
        </div>
    )
}
