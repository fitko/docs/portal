import Head from 'next/head'
import Hero from '@/views/Hero'
import FeaturedApiModules from '@/views/FeaturedApiModules'
import Organisators from '@/views/Organisators'

import useTranslation from 'shared/use-translation'
import { useLandingPageContent } from 'shared/use-content'

import { ScrollToTopButton } from '@/components/ScrollToTopButton'

export async function getStaticProps({ params }) {
    const { lang } = params
    const featuredModulesContent = await useLandingPageContent(lang)
    return {
        props: {
            featuredModulesContent,
            lang,
        },
    }
}

export async function getStaticPaths() {
    const languages = ['en']

    const paths = languages.map((lang) => ({
        params: { lang },
    }))

    return {
        paths,
        fallback: false,
    }
}

export default function IndexPage({ featuredModulesContent, lang }) {
    const { translate } = useTranslation(lang)
    return (
        <>
            <Head>
                <title>{translate('landingpage.headlines.title')}</title>
                <meta
                    name="description"
                    content={translate('landingpage.headlines.title')}
                />
                <meta name="viewport" content="width=device-width, initial-scale=1"/>
            </Head>
            <Hero lang={lang}/>

            <Organisators lang={lang}/>

            <FeaturedApiModules content={featuredModulesContent} lang={lang}/>

            { ScrollToTopButton() }
        </>
    )
}
