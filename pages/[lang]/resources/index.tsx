import { useRouter } from 'next/router'
import { IconArrowBigRight, IconMail, IconBooks, IconBook2 } from '@tabler/icons-react'

import { handleMailtoEvent, encodeEmail } from '@/lib/utils'
// TODO: reorganise imports
import { remark } from 'remark'
import html from 'remark-html'
import Tooltip from '@/components/Tooltip'
import ServiceTag from '@/components/ServiceTag'
import FilterAccordion from '@/components/FilterAccordion'
import Chip from '@/components/Chip'
import Link from 'next/link'

import useServiceFilter from 'shared/use-service-filter'
import { ResourceContent } from 'types/ResourceContent'
import { Tag } from 'types/content/'
import { useResourceContent } from 'shared/use-content'
import useImgPath from 'shared/use-img-path'
import useTranslation from '../../../shared/use-translation'
import { ScrollToTopButton } from '@/components/ScrollToTopButton'
import useBasePath from 'shared/use-base-path'
import useFilterData from 'shared/use-filter-data'

export default function ({ contentsString, lang }) {
    const contents: ResourceContent[] = JSON.parse(contentsString)
    const router = useRouter()
    const filterData = useFilterData(lang)
    const filters = filterData.filter
    const { filteredServices, activeTagFilters, filterToParams } = useServiceFilter(contents, router.query.filter)
    const { getImgPath } = useImgPath()
    const { translate, translateToArray } = useTranslation(lang)
    const { localizePath } = useBasePath()

    function handleFilters(event) {
        const filter = event.target.id.split('-')[0]
        if (event.target.checked) {
            activeTagFilters[filter].push(event.target.value)
        } else if (activeTagFilters[filter].indexOf(event.target.value) >= 0) {
            activeTagFilters[filter].splice(activeTagFilters[filter].indexOf(event.target.value), 1)
        }
        // we always track filters in query params, param change will trigger filtering
        router.push(localizePath(`resources?filter=${encodeURIComponent(filterToParams())}`, lang), undefined, { shallow: true })
    }

    function prepareShortDescription(service: ResourceContent): any {
        return service.shortDescription !== '' ? remark().use(html).processSync(service.shortDescription) : service.text
    }

    const hasContentTag = (content: ResourceContent, tagName: string) => {
        const tags: Tag[] = content.tags.filter(tag => tag.name + ':' + tag.value === tagName)
        return tags.length > 0
    }

    const additionalContentTranslation = translateToArray('resources.additionalContent.text', { existingIssuesLink: '', newIssueLink: '' })

    return (
        <div className='container px-3 lg:px-8  max-w-7xl md:mx-auto pt-8 pb-2 text-2xl md:text-3xl font-medium'>
            <div className="max-w-2xl mx-auto px-3 sm:px-6 lg:px-8 lg:max-w-7xl lg:grid lg:grid-cols-12 lg:gap-8">
                <header className="col-span-12">
                    <h1>
                        <span className="overflow-hiddenmt-1 block text-2xl tracking-tight xl:text-3xl">
                            <span className="block text-gray-900">{translate('resources.headlines.title')}</span>
                        </span>
                    </h1>
                    <hr className="my-4 border-t-2 border-gray-300" aria-hidden="true"/>
                </header>

                {/* filters */}
                <div className="lg:block lg:col-span-3">
                    <FilterAccordion title={translate('resources.headlines.filter')} className='text-lg'>
                        <form name="filters">
                            <nav
                                aria-label="Sidebar"
                                className="sticky top-4 divide-y divide-gray-300"
                            >
                                {filters.map((filter) => (
                                    <div className="pt-2 md:pt-6 mt-4" key={filter.id}>
                                        <p
                                            className="text-xs font-semibold text-gray-500 uppercase tracking-wider"
                                            id={`${filter.id}-headline`}
                                        >
                                            {filter.label}
                                        </p>
                                        <div
                                            className="pl-3 mt-3 space-y-2"
                                            aria-labelledby={`${filter.id}-headline`}
                                        >
                                            {filter.options.map((option) => (
                                                <div key={option.value} className="flex items-center">
                                                    <input
                                                        onClick={handleFilters}
                                                        id={`${filter.id}-${option.id}`}
                                                        name={`${filter.id}[]`}
                                                        defaultValue={`${filter.id}:${option.id}`}
                                                        defaultChecked={router.query.filter ? router.query.filter.includes(`${filter.id}:${option.id}`) : false}
                                                        type="checkbox"
                                                        className="h-4 w-4 border-gray-300 rounded text-yellow-400 focus:ring-yellow-400"
                                                    />
                                                    <label
                                                        htmlFor={`${filter.id}-${option.id}`}
                                                        className="ml-3 text-sm text-gray-500"
                                                    >
                                                        {option.label}
                                                    </label>
                                                </div>
                                            ))}
                                        </div>
                                    </div>
                                ))}
                            </nav>
                        </form><br/>
                    </FilterAccordion>
                </div>

                {/* filtered list */}
                <main className="lg:col-start-4 lg:col-span-8">
                    <div className="mt-4">
                        <ul role="list" className="space-y-4">
                            {filteredServices.map((service: ResourceContent, key) => (
                                <li
                                    key={service.name}
                                    className="bg-white px-4 pt-3 pb-6 shadow sm:p-6 sm:rounded-lg"
                                >
                                    <div className="text-right md:float-right">
                                        {hasContentTag(service, 'label:external')
                                            ? <Tooltip
                                                delayIn={200}
                                                delayOut={600}
                                                content="Bei diesem Inhalt handelt es sich um einen externen Inhalt, der keiner Qualitätssicherung seitens der FITKO unterliegt."
                                            >
                                                <Chip text={translate('resources.filterLabels.externalvalue')} style={'bg-indigo-500 mb-5 md:mb-1'} />
                                            </Tooltip>
                                            : ''}
                                    </div>
                                    <div aria-labelledby={'service-' + key}>
                                        <div className="flex justify-between">
                                            <div className="flex space-x-3">
                                                <div className="flex-shrink-0 xl:py-6">
                                                    {service.logo.path !== '' && service.logo.title !== ''
                                                        ? (
                                                            <img
                                                                className="max-w-[100px] h-10"
                                                                src={getImgPath(service.logo.path)}
                                                                alt={service.logo.title}
                                                            />
                                                        )
                                                        : (
                                                            <IconBooks
                                                                className={'h10'}
                                                            />
                                                        )}
                                                </div>
                                                <div className="min-w-0 flex-1 xl:py-6">
                                                    <p className="text-lg font-medium text-gray-900">
                                                        <Link href={localizePath(`/resources/${service.slug}`, lang)} passHref legacyBehavior>
                                                            <a className="hover:underline">
                                                                {service.name}
                                                            </a>
                                                        </Link>
                                                    </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="container flex flex-col md:flex-row my-2">
                                            {service.tags.map((tag: Tag, key: number) => {
                                                return <ServiceTag key={key} tag={tag} style={'bg-yellow-400'} lang={lang} />
                                            })}
                                        </div>
                                        <div
                                            className="mt-2 text-sm text-gray-700 space-y-4"
                                            dangerouslySetInnerHTML={{
                                                __html: prepareShortDescription(service),
                                            }}
                                        />
                                        <div className="mt-6 flex justify-end space-x-8">
                                            <div className="flex text-sm">
                                                <span className="inline-flex items-center text-sm mx-5 xl:mx-12">
                                                    <Link href={`${service.docsUrl}`} passHref legacyBehavior>
                                                        <a className="inline-flex space-x-2 text-gray-400 hover:text-gray-800">
                                                            <span className="font-medium">{translate('resources.buttons.toresource')}</span>
                                                            <IconBook2
                                                                className="h-5 w-5"
                                                                aria-hidden="true"
                                                            />
                                                        </a>
                                                    </Link>
                                                </span>
                                                <span className="inline-flex items-center text-sm">
                                                    <Link href={localizePath(`/resources/${service.slug}`, lang)} passHref legacyBehavior>
                                                        <a className="inline-flex space-x-2 text-gray-400 hover:text-gray-800">
                                                            <span className="font-medium">{translate('resources.buttons.more')}</span>
                                                            <IconArrowBigRight
                                                                className="h-5 w-5"
                                                                aria-hidden="true"
                                                            />
                                                        </a>
                                                    </Link>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            ))}
                            <li
                                key='Additional-Resource'
                                className="bg-white px-4 py-6 shadow sm:p-6 sm:rounded-lg"
                            >
                                <div aria-labelledby={'service-add-on'}>
                                    <div className="min-w-0 flex-1">
                                        <p className="text-sm font-medium text-gray-900">
                                            {translate('resources.headlines.futurecontent')}
                                        </p>
                                    </div>
                                    <div className="mt-2 text-sm text-gray-700 space-y-4">
                                        {additionalContentTranslation[0]}
                                        <a className='text-blue-500' href='https://gitlab.opencode.de/fitko/feedback/-/issues'>{translate('resources.additionalContent.existingIssuesLink')}</a>
                                        {additionalContentTranslation[2]}
                                        <a className='text-blue-500' href='https://gitlab.opencode.de/fitko/feedback/-/issues/new'>{translate('resources.additionalContent.newIssueLink')}</a>
                                        {additionalContentTranslation[4]}
                                    </div>
                                    <div className="mt-6 flex justify-end space-x-8" id="kontakt-link">
                                        <div className="flex text-sm">
                                            <span className="inline-flex items-center text-sm">
                                                <a
                                                    href="#kontakt-link"
                                                    onMouseDown={handleMailtoEvent}
                                                    data-base={`${encodeEmail('architekturmanagement@fitko.de')}`}
                                                    className="inline-flex space-x-2 text-gray-400 hover:text-gray-800"
                                                >
                                                    <span className="font-medium">{translate('resources.buttons.contact')}</span>
                                                    <IconMail
                                                        className="h-5 w-5"
                                                        aria-hidden="true"
                                                    />
                                                </a>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                </main>
            </div>
            { ScrollToTopButton() }
        </div>
    )
}

export async function getStaticProps({ params }) {
    const lang = params?.lang as string || 'de'
    const { contents } = useResourceContent('resources', lang)

    return {
        props: {
            contentsString: JSON.stringify(contents),
            lang,
        },
    }
};

export async function getStaticPaths() {
    return {
        paths: [
            { params: { lang: 'de' } },
            { params: { lang: 'en' } },
        ],
        fallback: false,
    }
}
