import { AppProps } from 'next/app'
import { Layout } from '@/components/layout'
import PlausibleProvider from 'next-plausible'

import '../styles/style.scss'

export default function App({ Component, pageProps }: AppProps) {
    return (
        <Layout>
            <PlausibleProvider domain="docs.fitko.de">
                <Component {...pageProps} />
            </PlausibleProvider>
        </Layout>
    )
}
