import Document, { Html, Head, Main, NextScript } from 'next/document'
import Favicon from '@/components/Favicon'

class CustomizedDocument extends Document {
    static async getInitialProps(context) {
        const initialProps = await Document.getInitialProps(context)
        return { ...initialProps }
    }

    render() {
        return (
            <Html className='scroll-smooth' style={{ scrollBehavior: 'smooth' }}>
                <Head>
                    <Favicon />
                </Head>
                <body>
                    <Main />
                    <NextScript />
                </body>
            </Html>
        )
    }
}

export default CustomizedDocument
