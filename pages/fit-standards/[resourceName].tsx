import { useEffect } from 'react'
import { Header } from '@/views/fit-standards/Resource'

import { ResourceContent } from '../../types/ResourceContent'
import { Liability, LinkItem } from 'types/content'

import { InfoBoxes } from '@/components/fit-standards/InfoBoxes'
import BreadcrumbNav from '@/components/fit-standards/BreadcrumbNav'

import { useResourceContent } from 'shared/use-content'
import useImgPath from 'shared/use-img-path'

import { ScrollToTopButton } from '@/components/ScrollToTopButton'

const initializedUsedContent = useResourceContent('fit-standards')
const { getImgPath } = useImgPath()

export default function ({ resourceName }) {
    const content: ResourceContent = JSON.parse(resourceName)

    // console.log(content)

    // Make properties and methods work by creating instance of origin class
    content.artefacts = content.artefacts.map(artefact => new LinkItem(artefact))
    content.liability = content.liability.map(item => new Liability(item))

    content.dateLastUpdate = new Date(content.dateLastUpdate)

    // Set our document title
    useEffect(() => {
        document.title = 'Detailansicht - ' + content.name
    })

    return (
        <div className="relative md:mt-16 overflow-hidden md:overflow-visible">
            <div className="it-standard max-w-7xl md:mx-auto">

                {/* Breadcrumb line */}
                <BreadcrumbNav navItem="Föderaler IT-Standard - Detailansicht" />

                <div className="md:flex md:flex-row gap-2">
                    <main className="flex flex-col border-r-2">
                        <Header
                            contact={{
                                name: content.contactInformation.name,
                                mail: content.contactInformation.mail,
                            }}
                            img={{
                                src: getImgPath(content.logo.path == null ? '' : content.logo.path),
                                alt: content.logo.title == null ? '' : content.logo.title,
                            }}
                            title={content?.name}
                            contextContent={content?.contextContent}
                            contextRelation={content?.contextRelation}
                            description={content?.text}
                            tags = {content.tags}
                            developer={{
                                name: content?.developer.name,
                                link: content?.developer.url,
                            }}
                            statusLabel={content.status}
                            documentationURL={content.docsUrl}
                            sourceURL={content.sourceCodeUrl}
                            resources={content.childResources}
                        />
                    </main>
                    <aside role='region' aria-label="Seitenleiste mit Informationsboxen" className="mt-8 p-2 md:sticky top-8">

                        <InfoBoxes
                            funder ={{
                              name: typeof content.funder === 'string' 
                                ? content.funder 
                                : content.funder?.organisation ?? '',
                              mail: content.funder.mail == null ? '' : content.funder.mail,
                              link: content.funder.url == null ? '' : content.funder.url,
                              phone: content.funder.phone == null ? '' : content.funder.phone,
                              contactform: content.funder.contactform == null ? '' : content.funder.contactform,
                          }}
                            productOwner={{
                                name: content.productOwner.organisation == null ? '' : content.productOwner.organisation,
                                mail: content.productOwner.mail == null ? '' : content.productOwner.mail,
                                link: content.productOwner.url == null ? '' : content.productOwner.url,
                                phone: content.productOwner.phone == null ? '' : content.productOwner.phone,
                                contactform: content.productOwner.contactform == null ? '' : content.productOwner.contactform,
                            }}
                            developer={{
                                name: content.developer.organisation,
                                mail: content.developer.mail,
                                link: content.developer.url,
                                phone: content.developer.phone,
                                contactform: content.developer.contactform,
                            }}
                            operator={{
                                name: content.operator.organisation,
                                mail: content.operator.mail,
                                link: content.operator.url,
                                phone: content.operator.phone,
                                contactform: content.operator.contactform,
                            }}
                            initiator={{
                              name: content.initiator.organisation,
                              mail: content.initiator.mail,
                              link: content.initiator.url,
                              phone: content.initiator.phone,
                              contactform: content.initiator.contactform,
                            }}
                            steeringCommittee={{
                              name: content.steeringCommittee.organisation,
                              mail: content.steeringCommittee.mail,
                              link: content.steeringCommittee.url,
                              phone: content.steeringCommittee.phone,
                              contactform: content.steeringCommittee.contactform,
                            }}
                            contact={{
                                name: content.contactInformation.organisation,
                                mail: content.contactInformation.mail,
                                link: content.contactInformation.url,
                                phone: content.contactInformation.phone,
                                contactform: content.contactInformation.contactform,
                            }}
                            framework={content.framework}
                            liability={content.liability}
                            supportInfo={content.support}
                            artefacts={content.artefacts}
                            combinedStandards={content.childResources}
                            slugMaster={content.slugMaster == null ? '' : content.slugMaster}
                            currentSlug={content.slug}
                            lastUpdate={content.dateLastUpdate}
                        />

                    </aside>
                </div>
            </div>
            {/* incorporate our scroll to top button */}
            { ScrollToTopButton() }
        </div>

    )
}

export async function getStaticProps({ params }) {
    const content = initializedUsedContent.contentByResourceName(params.resourceName)
    return {
        props: {
            resourceName: JSON.stringify(content),
        },
    }
}

export async function getStaticPaths() {
    const { contents } = useResourceContent('fit-standards')
    const servicePaths = contents.map((content) => {
        return { params: { resourceName: content.slug } }
    })

    return {
        paths: servicePaths,
        fallback: false,
    }
}
