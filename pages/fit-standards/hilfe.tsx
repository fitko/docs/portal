/*
 *      Reads the help file, provides the contents on the help page
 */

/* Needed for scroll to top button */

import BreadcrumbNav from '@/components/fit-standards/BreadcrumbNav'
import { ScrollToTopButton } from '@/components/ScrollToTopButton'

import loadAndPrepareHelpfileMarkdown from 'shared/fit-standards/get-helpfile-content'

export default function ({ contentsString }) {
    return (
        <div className="grid relative md:mt-8 overflow-hidden">
            <div className="helpfile max-w-7xl md:mx-auto">

                {/* Breadcrumb line */}
                <BreadcrumbNav navItem='Bedienungshilfe' />

                {/* We need to incorporate all Tailwind classed used throughout
                    the markdown file in advance.
                    This is the place we do that. */}
                <div
                    className="font-semibold h-6 flex flex-row flex-none bg-slate-100
                    sm:block sm:ml-4 p-2 w-24 w-80 max-w-xl basis-24
                    sm:text-sm text-xs sm:text-sm hyphens-auto
                    hidden sm:hidden"
                />

                <main className='px-3 lg:px-8 max-w-5xl text-gray-900 -mt-8'>
                    <div dangerouslySetInnerHTML={{ __html: contentsString }} />

                </main>
            </div>
            {/* incorporate our scroll to top button */}
            { ScrollToTopButton() }
        </div>
    )
}

export async function getStaticProps() {
    const contents = loadAndPrepareHelpfileMarkdown()

    return {
        props: {
            contentsString: contents,
        },
    }
}
