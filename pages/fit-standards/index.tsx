/* eslint-disable id-length */
/*
 * ***** Controls the list view of FIT-standards *****
 *
 * BASIC CONCEPT
 * =============
 * The page employs already available functionalitity from the 'Föderales Entwicklungsportal'.
 * This is flavoured with additional functionality, e.g. 
 * - reading data from a JSON file (read from OpenProject API) 
 * - filtering and 
 * - sorting 
 * before displaying a list of FIT standards.
 *
 * User interface
 * --------------
 * After some intro text, the page displays
 * - a side bar for filtering all available FIT-standards by various criteria.
 * - a dropdown to select the sort order of the remaining (after filtering) FIT-Standards.
 * - the list of the remaining FIT standards, sorted by the selected sort criteria.
 *
 * For filtering and sorting, two dedicated components are incorporated.
 *
 */

import React, { useEffect, useState, useRef } from 'react'

import TitleRow from '@/views/fit-standards/TitleRow'
import Intro from '@/views/fit-standards/Intro'
import { useRouter } from 'next/router'
import { IconChevronUp, IconChevronDown, IconAdjustmentsHorizontal, IconFilterOff, IconCategory2 } from '@tabler/icons-react'

import { remark } from 'remark'
import html from 'remark-html'
import Link from 'next/link'

import { ScrollToTopButton } from '@/components/ScrollToTopButton'

import MY_CONSTANTS from '@/lib/constants-standards.js'

import { ResourceContent } from 'types/ResourceContent'
import { UNDEFINED_LIABILITY } from 'types/content/Liability'
import { Tag } from 'types/content/'
import { useResourceContent } from 'shared/use-content'
import useImgPath from 'shared/use-img-path'
import LifecyclePicture from '@/components/fit-standards/LifecyclePicture'

import Badge from '@/components/Badge'
import { BadgeParams } from '@/components/Badge'
// The only allowed icon types for the badge component
type BadgeIconType = "IconSectionSign" | "IconThumbUpFilled" | "IconExclamationMark" | "IconQuestionMark" | "IconCirclesRelation"

import Label from '@/components/Label'

// The filter component
import { FlexFilter } from '@/components/fit-standards/FlexFilter'

// Our list sort component
import { ListSort, UpdateListSort } from '@/components/fit-standards/ListSort'

import { myFilters, mySortCriteria } from 'lib/assets/data/fit-standards/FilterSortDefs'


const isBrowser = () => typeof window !== 'undefined' // The approach recommended by Next.js

export default function ({ contentsString }) {
    const router = useRouter()

    // Get the contents
    const contents: ResourceContent[] = JSON.parse(contentsString)

    // For reference to filter component function resetFilters()
    const filterRef = useRef(null)

    const { getImgPath } = useImgPath()

    // Für Link auf Detailseite root ermitteln
    const currentPath = router.asPath // Aktuellen Pfad holen
    const pathSegments = currentPath.split('/') // Pfad in Segmente aufteilen

    // Der Ordnername befindet sich in pathSegments[1]
    const rootFolder = '/' + pathSegments[1] + '/'

    // For data transfer from the filter
    const [filteredObjectsJSON, setData] = useState(contentsString)

    const FlexFilterCallback = (childdata: string) => {
        setData(childdata)
        openCloseFilters()
    }

    const [sortedObjectsJSON, setSorted] = useState(contentsString)

    const ListSortCallback = (childdata: string) => {
        setSorted(childdata)
    }

    useEffect(() => {
        UpdateListSort(JSON.parse(filteredObjectsJSON), mySortCriteria, ListSortCallback)
    }, [filteredObjectsJSON])

    // Set our document title
    useEffect(() => {
        document.title = 'Informationsplattform FIT-Standards'
    })

    const getStatusTag = (content: ResourceContent) => {
        const tags: Tag[] = content.tags.filter(tag => tag.name === 'status')
        return (tags.length > 0 ? tags[0].value : '#INVALID')
    }

    const isPartOfGroup = (content: ResourceContent) => {
        return content.childResources.length > 0 && content.childResources[0].name != null
    }

    // Add event listener for window resize events
    useEffect(() => {
        window.addEventListener('resize', handleResize)
        return () => {
            window.removeEventListener('resize', handleResize)
        }
    })

    // Handles the request to reset all filters
    function handleResetFilters(event): void {
        event.preventDefault()
        if (filterRef && filterRef.current) {
            filterRef.current.resetFilters()
            // Close filters
            openCloseFilters(0)
        }
    }

    // Handles the clicks on open/close chevron to show or hide the filter section
    function handleOpenCloseFilters(event: React.MouseEvent) {
        const elementIds = ['filters-close', 'filters-open']

        // Frontend?
        if (!isBrowser()) return

        // Get element handling this event
        const thisElement = event.currentTarget as HTMLElement

        // Which one was it?
        const foundIdx = elementIds.findIndex((element) => element === thisElement.id)
        if (foundIdx == -1) return false

        openCloseFilters(foundIdx)
        const elemFilterTitleText = document.getElementById('filters-title')
        elemFilterTitleText.focus()

        return false
    }

    // Opens (operation > 0) or closes (operation undefined or < 0) the filter section
    // Sets the filter mode as well (nothing or active/reset), according to checkboxes
    function openCloseFilters(operation?: number) {
        const elementIds = ['filters-close', 'filters-open']

        // Find the elements we need to modify in the next steps
        const elemFilters = document.querySelectorAll('aside')[0]
        const elemFilterTitleRow = document.querySelectorAll('.filter-wrap>div')[0]
        const elemFilterTitleText = document.getElementById('filters-title')
        const elemFilterOffIcon = document.getElementById('filter-off')
        const elemFilterMessage = document.querySelectorAll('.filter-wrap>div .message')[0]

        // Get all filter checkboxes
        const checkboxes = document.querySelectorAll('form[name="filters"] input[type="checkbox"]')

        // Find out, if at least one checkbox is checked
        const checkedOneAtLeast = Array.prototype.slice.call(checkboxes).some(elem => elem.checked)

        if (checkedOneAtLeast) { // Is there one checkbox selected at least?
            // Show, that a filter is active
            elemFilterTitleRow.classList.add('is-filtered')
            if (elemFilterTitleText != null) {
                elemFilterTitleText.textContent = 'Filter aktiv!'
            }
            if (elemFilterOffIcon != null) {
                elemFilterOffIcon.style.display = 'block'
            }
        } else {
            // No filter active
            elemFilterTitleRow.classList.remove('is-filtered')
            if (elemFilterTitleText != null) {
                elemFilterTitleText.textContent = 'FIT-Standards filtern'
            }
            if (elemFilterOffIcon != null) {
                elemFilterOffIcon.style.display = 'none'
            }
        }

        // Show or hide the filter section?
        if (operation !== undefined) {
            // Check boundaries
            if (operation < 0) { operation = 0 }
            if (operation > 0) { operation = 1 }

            elemFilters.style.display = (operation ? 'block' : 'none')
            elemFilterMessage.textContent = (operation ? 'Filter werden angezeigt.' : 'Filter ausgeblendet.')

            // Show/hide the correct chevron
            let elem = document.getElementById(elementIds[1 - operation])
            if (elem) {
                elem.style.display = 'block'
            }
            elem = document.getElementById(elementIds[operation])
            if (elem) {
                elem.style.display = 'none'
            }

            // Make it nice (e.g. color) by adding dedicated CSS class
            if (operation) {
                elemFilterTitleRow.classList.add(elementIds[1])
            } else {
                elemFilterTitleRow.classList.remove(elementIds[1])
            }
        }
    }

    // We need to check if filters should be hidden or shown when crossing 640 px breakpoint
    function handleResize() {
        const docElem = document.documentElement
        const body = document.getElementsByTagName('body')[0]
        // eslint-disable-next-line id-length
        const x = window.innerWidth || docElem.clientWidth || body.clientWidth

        const elemFilters = document.querySelectorAll('aside')[0]
        if (x >= 640) { // Force showing sidebar?
            elemFilters.style.display = 'block'
        } else {
            const elemFilterOffIcon = document.getElementById('filters-close')
            elemFilters.style.display = (elemFilterOffIcon && elemFilterOffIcon.style.display === 'block' ? 'block' : 'none')
        }
        return false
    }

    function prepareShortDescription(service: ResourceContent): any {
        return service.shortDescription !== '' ? remark().use(html).processSync(service.shortDescription) : service.text
    }

    // counter for the given filter (works for tags and slugMaster)
    // eslint-disable-next-line no-unused-vars
    const countFilter = (filterID : string) => {
        const filterParts = filterID.split(':')
        let filteredArray = []
        if (filterParts[0] === 'slugMaster') {
            // filters all slugMasters out and get an array of subordinated services
            filteredArray = contents.filter((service: ResourceContent) => {
                const istSlugMasterVorhanden = service.slugMaster === '' || service.slugMaster === service.slug
                return !istSlugMasterVorhanden
            })
        } else {
            // get an array of services with the given tag
            filteredArray = contents.filter((service: ResourceContent) => {
                const contents = service.tags.filter((tag: Tag) => filterID.includes(`${tag.name}:${tag.value}`))
                return contents.length > 0
            })
        }
        const count = filteredArray.length
        return count
    }

    return (
        <div className="overflow-hidden sm:overflow-visible relative">
            <TitleRow />
            <div className='it-standards max-w-7xl md:mx-auto'>
                <div className="px-3 lg:px-8 ">

                    {/* Introtext / minimum explanation */}
                    <Intro />

                    <div className="md:flex gap-4">
                        {/* Filter wrapper */}
                        <div className ="filter-wrap my-8 md:my-0 md:p-0 rounded-md border-2 md:border-0">
                            {/* Filter header to switch between show/hide */}
                            <div className="flex flex-row p-2 md:mb-4 bg-gray-600 text-white rounded-md justify-between md:hidden">

                                {/* Filter show/hide title */}
                                <div className="flex flex-row">
                                    <IconAdjustmentsHorizontal className="mr-1" aria-hidden="true"/>
                                    <div id="filters-title" className="font-semibold">FIT-Standards filtern</div>
                                    <button type="button" id="filter-off" aria-label="Filter zurücksetzen" onClick={handleResetFilters} className="hidden"><IconFilterOff className="mx-2 cursor-pointer"/></button>
                                </div>
                                {/* Filter show/hide chevrons */}
                                <div className="flex flex-row">
                                    <button type="button" id="filters-close" aria-label="Filter-Anzeige schließen" onClick={handleOpenCloseFilters} className="hidden"><IconChevronUp className="cursor-pointer"/></button>
                                    <button type="button" id="filters-open" aria-label="Filter anzeigen" onClick={handleOpenCloseFilters}><IconChevronDown className="cursor-pointer"/></button>
                                </div>
                                <div className="message sr-only" role="alert"></div>
                            </div>

                            {/* The sidebar: filters form */}
                            <aside role='form' aria-label="Sidebar mit Filtergruppen" className="hidden md:block md:sticky md:h-[calc(100vh-2rem)] md:w-72 top-8 p-2 md:pt-0 md:pl-0 md:pr-0 divide-y divide-gray-300 bg-white overflow-y-auto">
                                <FlexFilter
                                    theObjects = { contents }
                                    filters = { myFilters }
                                    parentCallback = { FlexFilterCallback }
                                    ref = { filterRef }
                                />
                            </aside>
                        </div>
                        {/* Main part: the standards list */}
                        <main className="">
                            <div className="mt-4">
                                <div className="flex flex-row justify-start md:justify-end">
                                    <ListSort
                                        theObjects = { JSON.parse(filteredObjectsJSON) }
                                        sortOrders = { mySortCriteria }
                                        parentCallback = { ListSortCallback }
                                    />
                                </div>
                                <ul role="list" className="space-y-4">
                                    {JSON.parse(sortedObjectsJSON).map((service: ResourceContent, key) => (
                                        // *** A list entry
                                        <li
                                            key={service.slug}
                                            className="bg-white px-2 sm:px-4 pb-6 shadow sm:p-6 sm:pb-4 sm:rounded-b-lg"
                                        >

                                            {/* Render LOGO & TITLE  */}
                                            <div aria-labelledby={'service-' + key}>
                                                <div className="flex justify-between flex-wrap">
                                                    <Link href={`${rootFolder}${service.slug}`} passHref legacyBehavior>
                                                        <a className="flex md:whitespace-nowrap underline underline-offset-2 hover:no-underline">

                                                            <div className="flex space-x-3 items-center">
                                                                {service.logo.path != null && service.logo.path !== '' && service.logo.title != null && service.logo.title !== ''
                                                                    ? (
                                                                        <div className="flex-shrink-0 xl:pt-6" aria-hidden='true'>
                                                                            <img
                                                                                className="h-6"
                                                                                src={getImgPath(service.logo.path)}
                                                                                alt=''
                                                                            />
                                                                        </div>
                                                                    )
                                                                    : ''
                                                                }
                                                                <div className="min-w-0 flex-1 xl:pt-6 break-words hyphens-auto">
                                                                    <p className="text-xl font-bold text-gray-900">
                                                                        {service.name}
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </a>
                                                    </Link>

                                                    {/* ***** Render badges ***** */}
                                                    <div className="flex flex-wrap justify-end items-center ml-auto">
                                                        {/* Render group tag, if applicable */}
                                                        {isPartOfGroup(service)
                                                            ? <Badge
                                                                name={`Verbund: ${service.slugMaster}`}
                                                                link={`${rootFolder}${service.slugMaster}`}
                                                                symbol="IconCirclesRelation"
                                                                colors="bg-itplr_senf text-black"
                                                            />
                                                            : null
                                                        }

                                                        {/* Render liability badges */}
                                                        {service.liability && service.liability.length > 0 && (
                                                            <div className="flex">
                                                              {MY_CONSTANTS.LIABILITY
                                                                .filter(constantItem => {
                                                                  // Skip if first liability is UNDEFINED_LIABILITY
                                                                  const firstLiabilityValue = service.liability[0].liability;
                                                                  if (firstLiabilityValue === UNDEFINED_LIABILITY) return false;
                                                                  
                                                                  // Check if any non-UNDEFINED liability matches the current constant
                                                                  return service.liability.some(liabilityObj =>
                                                                    liabilityObj.liability === constantItem.id &&
                                                                    liabilityObj.liability !== UNDEFINED_LIABILITY
                                                                  );
                                                                })
                                                                .sort((a, b) => {
                                                                  // Sort so that the first liability value appears first
                                                                  const firstLiabilityValue = service.liability[0].liability;
                                                                  if (a.id === firstLiabilityValue) return -1;
                                                                  if (b.id === firstLiabilityValue) return 1;
                                                                  return 0;
                                                                })
                                                                .map(constantItem => (
                                                                  <Badge
                                                                    key={constantItem.id}
                                                                    name={constantItem.id}
                                                                    symbol="IconFileCheck"
                                                                    colors={constantItem.colors}
                                                                    altText={`Die Gültigkeit dieses FIT-Standards ist ${constantItem.id}`}
                                                                  />
                                                                ))}
                                                            </div>
                                                        )}

                                                        {/* Render type tag badge */}
                                                        {service.tags.filter(tag => tag.name === 'type').map(tag => {
                                                            const standardType = MY_CONSTANTS.STANDARD_TYPES.find(type => type.id === tag.value)
                                                            return (
                                                                <Badge
                                                                    key={tag.value}
                                                                    name={standardType ? standardType.text : tag.value}
                                                                    symbol="IconCategory2" 
                                                                    colors="bg-itplr_malve text-white"
                                                                />
                                                            )
                                                        })}

                                                    </div>

                                                </div>

                                                {/* Render CURRENT LIFECYCLE PHASE */}
                                                <div className="container flex flex-col md:flex-row my-4">
                                                    <LifecyclePicture
                                                        status = { getStatusTag(service) }
                                                    />
                                                </div>

                                                {/* Render the tag line */}
                                                <div className="inline-flex flex-wrap">
                                                    {/* Render contact tags */}
                                                    {[
                                                        { role: 'initiator', index: 0 },
                                                        { role: 'productOwner', index: 1 },
                                                        { role: 'steeringCommittee', index: 2 },
                                                        { role: 'developer', index: 3 },
                                                        { role: 'operator', index: 4 },
                                                        { role: 'contactInformation', index: 5 },
                                                        { role: 'funder', index: 6 },
                                                    ].map(({ role, index }) => {
                                                        const org = service[role]?.organisation
                                                        const icon = `/img/fit-standards/roles/${MY_CONSTANTS.CONTACT_TYPES[index].id}.svg`
                                                        return org && org !== '' ? (
                                                            <Label
                                                                key={role}
                                                                name={org}
                                                                colors={MY_CONSTANTS.COLORS.ROLES}
                                                                icon={icon}
                                                            />
                                                        ) : null
                                                    })}
                                                </div>

                                                {/* Kurzbeschreibung */}
                                                <div
                                                    className="mt-2 text-sm text-gray-700 space-y-4"
                                                    dangerouslySetInnerHTML={{ __html: prepareShortDescription(service) }}
                                                />

                                                {/* Letzte Aktualisierung  */}
                                                <div className="mt-2 text-xs text-gray-700 space-y-4">
                                                    Letzte Aktualisierung: {
                                                        new Date(service.dateLastUpdate).toLocaleDateString('de-DE', { year: 'numeric', month: 'long', day: '2-digit' })
                                                    }
                                                </div>
                                            </div>
                                        </li>
                                    ))}
                                    <li
                                        key='Element-Count'
                                        className="bg-white px-4 pb-4 shadow sm:p-6 sm:rounded-lg"
                                    >
                                        <div aria-labelledby={'service-add-on'}>
                                            <div className="advice text-xs font-semibold text-gray-700">
                                                <p>{JSON.parse(sortedObjectsJSON).length} von insgesamt {contents.length} Einträgen.</p>
                                            </div>
                                        </div>
                                    </li>
                                    <li
                                        key='Additional-Resource'
                                        className="bg-white px-4 pb-4 shadow sm:p-6 sm:rounded-lg"
                                    >
                                        <div aria-labelledby={'service-add-on'}>
                                            <div className="advice mt-2 text-sm text-gray-700 space-y-4">
                                                <p className="text-xl font-semibold">Nicht das Gewünschte gefunden?</p>
                                                <p>Neben den hier aufgelisteten Föderalen IT-Standards des IT-Planungsrats finden Sie weitere Spezifikationen insbesondere für den fachspezifischen Einsatz auf folgenden Seiten:</p>
                                                <ul className="list-disc list-outside">
                                                    <li className="mb-2">
                                                        <a href='https://www.xrepository.de/' target='_blank' rel='noopener' className="font-semibold">
                                                            XRepository
                                                        </a> - XÖV-Standards und Codelisten
                                                    </li>
                                                    <li className="mb-2">
                                                        <a href='https://fimportal.de/kataloge' target='_blank' rel='noopener' className="font-semibold">
                                                            FIM Kataloge
                                                        </a> - Kataloge zu FIM Leistungen, Datenfeldern und Prozessen
                                                    </li>
                                                </ul>
                                                <p>Viele weitere Informationen finden Sie auch im <a href='https://docs.fitko.de/' target='_blank' rel='noopener'>föderalen Entwicklungsportal</a>.</p>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </main>
                    </div>
                </div>
            </div>
            {/* incorporate our scroll to top button */}
            { ScrollToTopButton() }
        </div>
    )
}

// Get our contents
export async function getStaticProps() {
    const { contents } = useResourceContent('fit-standards')

    return {
        props: {
            contentsString: JSON.stringify(contents),
        },
    }
}
