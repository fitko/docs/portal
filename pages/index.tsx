import Head from 'next/head'
import Hero from '@/views/Hero'
import FeaturedApiModules from '@/views/FeaturedApiModules'
import Organisators from '@/views/Organisators'

import useTranslation from 'shared/use-translation'
import { useLandingPageContent } from 'shared/use-content'

import { ScrollToTopButton } from '@/components/ScrollToTopButton'

export async function getStaticProps() {
    const featuredModulesContent = await useLandingPageContent('de')
    return {
        props: {
            featuredModulesContent,
        },
    }
}

export default function IndexPage({ featuredModulesContent }) {
    const { translate } = useTranslation()
    return (
        <>
            <Head>
                <title>{translate('landingpage.headlines.title')}</title>
                <meta
                    name="description"
                    content={translate('landingpage.headlines.title')}
                />
                <meta name="viewport" content="width=device-width, initial-scale=1"/>
            </Head>
            <Hero lang={'de'}/>
            <Organisators lang={'de'}/>
            <FeaturedApiModules content={featuredModulesContent} lang={'de'}/>
            { ScrollToTopButton() }
        </>
    )
}
