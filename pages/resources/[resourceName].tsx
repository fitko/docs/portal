import Link from 'next/link'
import { Resource } from '@/views/Resource'
import { ResourceContent } from '../../types/ResourceContent'
import { useResourceContent } from 'shared/use-content'
import { ScrollToTopButton } from '@/components/ScrollToTopButton'
import useBasePath from 'shared/use-base-path'
import useTranslation from 'shared/use-translation'

const initializedUsedContent = useResourceContent()

export default function ({ resourceJSON }) {
    const content: ResourceContent = JSON.parse(resourceJSON)
    const { localizePath } = useBasePath()
    const { translate, translateStatusLableValue } = useTranslation()
    return (
        <div className={'mx-auto max-w-screen-xl'}>
            <nav className="relative max-w-7xl flex">
                <div className="flex items-center flex-1">
                    <div className="flex items-center justify-between w-full md:w-auto">
                        <div className="hidden lg:flex">
                            <Link href={localizePath('/resources')} passHref legacyBehavior>
                                <a className={'text-lg font-bold text-yellow-400 hover:text-gray-900'}>
                                    {translate('resources.navigation.back')}
                                </a>
                            </Link>
                        </div>
                    </div>
                </div>
            </nav>
            <Resource
                contact={{
                    name: content.contactInformation.name,
                    mail: content.contactInformation.mail,
                }}
                logo={content?.logo}
                title={content?.name}
                description={content?.text}
                slug={content?.slug}
                developer={{
                    name: content?.developer.name,
                    link: content?.developer.url,
                }}
                statusLabel={translateStatusLableValue(content.status)}
                documentationURL={content.docsUrl}
                apiSpecURL={content.apiSpecUrl}
                sourceURL={content.sourceCodeUrl}
                childResources={content.childResources}
            />
            { ScrollToTopButton() }
        </div>
    )
}

export async function getStaticProps({ params }) {
    const { resourceName } = params
    const content = initializedUsedContent.contentByResourceName(resourceName)
    return {
        props: {
            resourceJSON: JSON.stringify(content),
        },
    }
}

export async function getStaticPaths() {
    const { contents } = useResourceContent()
    const servicePaths = contents.map((content) => {
        return { params: { resourceName: content.slug } }
    })

    return {
        paths: servicePaths,
        fallback: false,
    }
}
