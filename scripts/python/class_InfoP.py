#!/usr/bin/env python

""" A class to interact with Information platform md files
    
"""
import os
import json
import yaml
import re 
from typing import List, Dict, Any

__author__ = "Jürgen Voskuhl, itcv GmbH"
__license__ = "GPL"
__version__ = "2024-12-05"
__maintainer__ = "Jürgen Voskuhl"
__email__ = "jvo@itcv-software.com"
__status__ = "Draft"


import os
import yaml
import re
from typing import List, Dict, Any

class InfoP:
    def __init__(self, folder_path: str):
        self.folder_path = folder_path
        print(f"Initializing InfoP with folder path: {folder_path}... ", end="")
        # Check if folder exists
        if not os.path.exists(folder_path):
            raise ValueError(f"  Folder '{folder_path}' does not exist.")

        # Validate folder structure
        self.validate_folder_structure()
        print(f"O.k.!")


    def validate_folder_structure(self):
        """Validate that each subfolder contains at least one .md file."""
        for root, dirs, files in os.walk(self.folder_path):
            for dir_name in dirs:
                dir_path = os.path.join(root, dir_name)
                md_files = [f for f in os.listdir(dir_path) if f.endswith('.md')]
                if not md_files:
                    raise ValueError(f"\n  Subfolder '{dir_name}' does not contain any .md files.")

    def list_folders(self) -> List[str]:
        """Return a list of all subfolders."""
        folders = []
        for root, dirs, _ in os.walk(self.folder_path):
            for dir_name in dirs:
                folders.append(os.path.join(root, dir_name))

        return folders

    def read_md_file(self, folder_path: str) -> Dict[str, Any]:
        """Read the .md file in the given folder and return both front matter and document content as a JSON object."""
        md_files = [f for f in os.listdir(folder_path) if f.endswith('.md')]
        if not md_files:
            raise ValueError(f"No .md file found in folder '{folder_path}'.")

        # Assuming there is only one .md file per folder
        md_file_path = os.path.join(folder_path, md_files[0])
        with open(md_file_path, 'r', encoding='utf-8') as file:
            content = file.read()

        # Split front matter and document content
        front_matter_match = re.match(r'^---\s*([\s\S]*?)\s*---\s*(.*)$', content, re.DOTALL)
        if not front_matter_match:
            raise ValueError(f"Invalid .md file format in '{md_file_path}'.")

        front_matter = yaml.safe_load(front_matter_match.group(1))
        front_matter['slug'] = os.path.basename(folder_path)  # Add slug to front matter
        front_matter['tags'] = self.process_tags(front_matter.get('tags', []))  # Process tags

        document_content = front_matter_match.group(2).strip()

        return {
            "front_matter": front_matter,
            "document_content": document_content
        }
    
    def process_tags(self, tags):
        processed_tags = {}
        for tag in tags:
            if isinstance(tag, str) and ':' in tag:
                key, value = tag.split(':', 1)
                processed_tags[key.strip()] = value.strip()
            elif isinstance(tag, dict):
                if len(tag) == 1:
                    # If the dictionary has only one key-value pair, add it as a tag
                    key, value = next(iter(tag.items()))
                    processed_tags[key.strip()] = value
                else:
                    # If the dictionary has multiple key-value pairs, update the processed tags
                    processed_tags.update(tag)
        return processed_tags

    def isStandalone(self, tag_list):
      if 'slugMaster' in tag_list:
        slugMaster_value = tag_list['slugMaster']
        print(f"slugMaster value = {slugMaster_value}")
        if slugMaster_value == 'None':
          return True
      return False


# Example usage:
# ip = InfoP('content/fit-standards')
# folders = ip.list_folders()
# for folder in folders:
#     print(f"Folder: {folder}")
#     md_content = ip.read_md_file(folder)
#     print(f"Front Matter: {md_content['front_matter']}")
#     print(f"Document Content: {md_content['
   

