#!/usr/bin/env python

"""	A class to make OpenProject data structures used for FIT-SB
    available for the Informationsplattform. 
    
"""
import sys
import json
import copy
from urllib.parse import quote, quote_plus, unquote
import requests

__author__ = "Jürgen Voskuhl, itcv GmbH"
__license__ = "GPL"
__version__ = "2024-12-14"
__maintainer__ = "Jürgen Voskuhl"
__email__ = "jvo@itcv-software.com"
__status__ = "Production"



class OpenProject2Infoplattform:
  
  # Create new instance with parameter 'work_packages' (JSON object read from OpenProject)
  def __init__(self, work_packages):
    # Define constants
    self.EMPTY_IP_OBJECT = {
      "funder": "",
	    "contactInformation": {
	      "organisation": None,
	      "url": None,
	      "name": None,
	      "phone": None,
	      "contactform": None,
	      "mail": None,
	    },
	    "developer": {
	      "organisation": None,
	      "url": None,
	      "name": None,
	      "phone": None,
	      "contactform": None,
	      "mail": None,
	    },
	    "steeringCommittee": {
	      "organisation": None,
	      "url": None,
	      "name": None,
	      "phone": None,
	      "contactform": None,
	      "mail": None,
	    },
	    "operator": {
	      "organisation": None,
	      "url": None,
	      "name": None,
	      "phone": None,
	      "contactform": None,
	      "mail": None,
	    },
	    "productOwner": {
	      "organisation": None,
	      "url": None,
	      "name": None,
	      "phone": None,
	      "contactform": None,
	      "mail": None,
	    },
	    "initiator": {
	      "organisation": None,
	      "url": None,
	      "name": None,
	      "phone": None,
	      "contactform": None,
	      "mail": None,
	    },
	    "docsUrl": "",
	    "apiSpecUrl": "",
	    "name": "",
	    "shortDescription": "",
	    "sourceCodeUrl": "",
	    "status": "",
	    "tags": [
	      {
	        "name": "status",
	        "status": "",
	        "value": "",
	      },
	      {
	        "name": "type",
	        "type": "",
	        "value": "",
	      },
	      {
	        "name": "slug_master",
	        "slug": "",
	        "value": "",
	      },
	    ],
	    "initialized": True,
	    "text": "",
	    "childResources": [
	    ],
	    "support": [
	    ],
	    "artefacts": [
	      {
	        "title": None,
	        "name": "artefaktSpec",
	        "url": "",
	        "license": "",
	      },
	      {
	        "title": None,
	        "name": "artefaktDocumentation",
	        "url": "n.v.",
	        "license": "",
	      },
	      {
	        "title": None,
	        "name": "artefaktTestumgebung",
	        "url": "n.v.",
	        "license": "",
	      },
	      {
	        "title": None,
	        "name": "artefaktRefImplementation",
	        "url": "n.v.",
	        "license": "",
	      },
	      {
	        "title": None,
	        "name": "artefaktBetriebskonzept",
	        "url": "",
	        "license": "",
	      },
	    ],
	    "slugMaster": "",
	    "logo": None,
	    "slug": "",
	    "artefactNames": [
	      "artefaktSpec",
	      "artefaktDocumentation",
	      "artefaktTestumgebung",
	      "artefaktRefImplementation",
	      "artefaktBetriebskonzept",
	    ],
	    "framework": None,
	    "liability": None,
	    "dateLastUpdate": None,
      "contextRelation": [],
      "contextContent": [],
	  }
	  

    # Store OpenProject data
    self.work_packages = work_packages
    self.wp_array = work_packages['_embedded']['elements']
    self.currentWP = None
    
    # Array for data from last export
    self.lastResult = []
    
    # Array for Informationsplattform objects
    self.ip_array = []
    
  # Return the list containing the actually creeated objects
  def getInfoPlattformObjects(self):
    return self.ip_array


  # Read data previously stored from file and store them in our class instance for later use
  def readLastResult(self, filename: str) -> bool:
    try:
      # Open the file in read mode with UTF-8 encoding
      with open(filename, "r", encoding='utf-8') as in_file:
	      # Load JSON data from the file
        self.lastResult = json.load(in_file)
        return True  # Return True if the file is successfully read
    except Exception as e:
        return False  # Return False if an error occurs, e.g. no file present


  # Locate object in previous data; add it to our current array if found
  def useLastResult(self, slug: str) -> bool:
    for standard in self.lastResult:
      if standard.get('slug') == slug:
        self.ip_array.append(standard)
        return True
    return False
          	
  	
  # Create a new FIT-Standard object
  def createNewObject(self):
    # Start with empty FIT-Standard object
    myObj = copy.deepcopy(self.EMPTY_IP_OBJECT)

    # Append our new object to array
    self.ip_array.append(myObj)

  # Lookup for existing object by slug
  def getObjectBySlug(self, slug):
    for obj in self.ip_array:
      if obj.get("slug") == slug:
        return obj
    return None
        
  # Set a parameter to a certain value		
  def setValue(self, par_name: str, value, force_array: bool = False):
    if (force_array):
      # Check if the parameter already exists and is a list
      if isinstance(self.ip_array[-1].get(par_name), list):
        self.ip_array[-1][par_name].append(value)
      else:
        # Ensure the value is a list, even if a single item is provided
        if not isinstance(value, list):
          value = [value]
        self.ip_array[-1][par_name] = value
    else:
      self.ip_array[-1][par_name] = value


  # Set a contact parameter to a certain value		
  def setContactValue(self, contact_type, par_name, value):
    self.ip_array[-1][contact_type][par_name] = value


  # Set a tag to a certain value
  def setTag(self, tag_name: str, par_name: str, value: str) -> bool:
      for tag in self.ip_array[-1]["tags"]:
          if tag.get('name') == tag_name:
              if par_name in tag:
                  tag[par_name] = value
                  return True
              else:
                  print(f"  Parameter name '{par_name}' does not exist in tag '{tag_name}'.")
                  sys.exit(1)
      print(f"  Tag name '{tag_name}' not found.")
      sys.exit(1)


  # Set an artefact to a certain value
  def setArtefact(self, tag_name: str, par_name: str, value: str, force_add: bool = False) -> bool:
      for tag in self.ip_array[-1]["artefacts"]:
          if tag.get('name') == tag_name:
              if par_name in tag:
                  tag[par_name] = value
                  return True
              elif (force_add):
                # print(f"  Forced: Add param. '{par_name}'.")
                tag[par_name] = value
                return True
              else:
                  print(f"  Parameter name '{par_name}' does not exist in tag '{tag_name}'.")
                  sys.exit(1)
      if (force_add):
        # print(f"  Forced: Add object '{tag_name}'.")
        self.ip_array[-1]["artefacts"].append( {'title': None, 'name': tag_name })
        self.setArtefact(tag_name, par_name, value, True)
        return True
              
      print(f"  Tag name '{tag_name}' not found.")
      sys.exit(1)


  # Add a child resource
  def addChildResource(self, name, slug):
    myObj = { "name": name,
              "slug": slug
            }
    self.ip_array[-1]['childResources'].append(myObj)


  # Add a supporting resource
  def addSupportResource(self, title, url):
    myObj = { "title": title,
              "url": url
            }
    self.ip_array[-1]['support'].append(myObj)
