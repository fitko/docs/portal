#!/usr/bin/env python

""" A class to interact with OpenProject API v3
    
"""
import json, inspect
from typing import Union
from urllib.parse import quote, quote_plus, unquote
from copy import deepcopy
import requests
import sys
import re

__author__ = "Jürgen Voskuhl, itcv GmbH"
__license__ = "GPL"
__version__ = "2024-12-14"
__maintainer__ = "Jürgen Voskuhl"
__email__ = "jvo@itcv-software.com"
__status__ = "Production"


class OpenProject:

  headers = {
      'Accept': 'application/json',
      'Content-Type': 'application/json',  # Add Content-Type
  }    


  # Create new instance with parameter 'host' (the host name) and a valid access token
  def __init__(self, host: str, token: str) -> None:
    # Define constants
    self.PROTOCOL = 'https://'
    self.API = '/api/v3/'
    self.HEADERS = {
        'Accept': 'application/json',
        'Content-Type': 'application/json',  # Add Content-Type
    }    

    self.host = host
    self.token = token
    self.auth = ('apikey', token)
    
    self.currentWP = None		# The workpackage we are currently processing
    self.updateWP  = {}     # The workpackage we are going to update (empty dictionary for now)

    self.createWPForm = {}  # The create workpackage form (empty dictionary for now)



  def getAttributeByName(self, elements: list, search_name: str, attribute_name: str) -> any:
    """
    Find an element by name and return the specified attribute value.
    
    Args:
        data: The data containing the elements
        search_name: The name to search for
        attribute_name: The attribute to return
        
    Returns:
        The value of the specified attribute if found, None otherwise
    """
    try:
        for element in elements:
            if element.get('name') == search_name:
                return element.get(attribute_name)
        return None
    except (KeyError, TypeError):
        return None


  # Get Project id from identifier
  # Returns the numeric project ID corresponding to indentifier. Returns False, if project couldn't be found.
  def getProjectId(self, identifier: str) -> int:
    full_url = self.PROTOCOL + self.host + self.API + 'projects' + '?&pageSize=-1'
    # print("Full request: " + unquote(full_url))

    response = requests.get(full_url, auth=self.auth)
    # print(f"Return code is {response.status_code}")
    if response.status_code == 200:
      data = response.json()
      key = self.getAttributeByName(data['_embedded']['elements'], identifier, 'id')
      return key
    else:
       print(f"Error: {response.status_code}")
      
    return False

  
  # Read workpackages of a specific project
  def getProjectWorkpackages(self, project_id: int, filters: list = []):
    request = self.PROTOCOL + self.host + self.API + 'projects/' + str(project_id) + '/work_packages'
    
    full_url = f"{request}?pageSize=1000"
    if len(filters):
      params = quote(json.dumps(filters))
      full_url = f"{full_url}&filters={params}"
      
    # print("Full request: " + unquote(full_url))
    response = requests.get(full_url, auth=self.auth)
    # print(f"Return code is {response.status_code}")
    if response.status_code == 200:
      # Store OpenProject data
      json_obj = response.json()
      self.work_packages = json_obj
      self.wp_array = json_obj['_embedded']['elements']
      self.schema_array = json_obj['_embedded']['schemas']['_embedded']['elements']
      self.currentWP = None
      
      return json_obj

    return None


  # Read the types of a specific project
  def getTypes(self, project_id: int, filters: list = []) -> list:
    request = self.PROTOCOL + self.host + self.API + 'projects/' + str(project_id) + '/types'
    
    if len(filters):
      params = quote(json.dumps(filters))
      full_url = f"{request}?filters={params}&pageSize=1000"
    else:
      full_url = f"{request}?pageSize=1000"
      
    # print("Full request: " + unquote(full_url))
    response = requests.get(full_url, auth=self.auth)
    # print(f"Return code is {response.status_code}")
    if response.status_code == 200:
      return response.json()
    else:
       print(f"Error: {response.status_code}")

    return None
  
  # Store a specific work package for future use
  def setCurrentWP(self, wp: list):
    self.currentWP = wp


  def find_attribute_key(self, data: list, target_name: str) -> any:
    # Iterate through the schema object, excluding _attributeGroups
    for key, value in data.items():
        if key != '_attributeGroups':
            # Check if the current attribute has a 'name' field
            if isinstance(value, dict) and 'name' in value:
                # If the name matches the target_name, return the key
                if value['name'] == target_name:
                    return key
    # If no matching attribute is found, return None
    return None

    
  def getValue(self, param_name: str, sub: str = "", wp: Union[list, str] = None) -> any:

    if (not wp and not self.currentWP):
      raise ValueError("Current work package not set.")
    
    if (not wp):
      wp = self.currentWP
      
    # Step 1a: Check if the parameter is a direct attribute of any element
    if param_name in wp and not sub:
      # print(f"  '{param_name}'/'{sub}': No sub.")
      # print(f"  Returning '{self.currentWP[param_name]}'.")
      return wp[param_name]

    # Step 1b: Check if the parameter is an object with several attributes
    if param_name in wp and sub in wp[param_name]:
      # print(f"  '{param_name}'/'{sub}': Sub requested, found it.")
      # print(f"  Returning '{self.currentWP[param_name][sub]}'.")
      return wp[param_name][sub]
    
    # Step 2: If not found, identify the schema used for the object
    schema_url = wp.get("_links", {}).get("schema", {}).get("href")
    # print("Schema: " + schema_url)
      
    if not schema_url:
      raise ValueError("Schema URL not found in the JSON data.")

    # Step 3: Locate the used schema
    for schema in self.schema_array:
      if schema['_links']['self']['href'] == schema_url:
        mySchema = schema
        break  # Exit the loop once the schema is found
    else:
      # Handle the case where no matching schema was found
      raise ValueError(f"No schema found with URL: {schema_url}.")
        
    # Step 4: Identify the custom field by name
    custom_field_id = self.find_attribute_key(mySchema, param_name)
    if not custom_field_id:
      raise ValueError(f"Custom field with name '{param_name}' not found in the schema.")
    # print("custom_field_id: " + custom_field_id)
    
    # Step 5: Check if the custom field ID is an attribute of any element
    if custom_field_id in wp:
      if not sub:
        return wp[custom_field_id]
      else:
        return wp[custom_field_id][sub]
    
    # Step 6: Must be a list entry then! Check if the custom field ID is an attribute of any element
    if custom_field_id in wp["_links"]:
      # print(f"\nparam_name: ", param_name)
      # print("custom_field_id: " + custom_field_id)
      # print(self.currentWP["_links"][custom_field_id])

      # Are we dealing with a list?
      if isinstance(self.currentWP["_links"][custom_field_id], list):
        # print("List detected!")
        # Check if the list is empty
        if not self.currentWP["_links"][custom_field_id]:
          # print("List is empty")
          return None
        else:
          # print("List is not empty")
          # Return all elements in the list, with their titles if available
          result = []
          for item in self.currentWP["_links"][custom_field_id]:
            if "title" in item:
              result.append(item["title"])
            else:
              result.append(item)
          return result
      
      else:
        # print("Not a list")
        # Check if the element has a "title" attribute
        if "title" in self.currentWP["_links"][custom_field_id]:
          # print("Element has 'title' attribute")
          # Return the "title" attribute of the element
          return self.currentWP["_links"][custom_field_id]["title"]
        else:
          # print("Element does not have 'title' attribute")
          # Return the element
          return self.currentWP["_links"][custom_field_id]
      # print("List value: " + self.currentWP["_links"][custom_field_id]["title"])
      # return wp["_links"][custom_field_id]["title"]

    # If the parameter is neither a direct attribute nor a custom field, raise an error
    raise ValueError(f"Parameter '{param_name}' not found as either a direct attribute or a custom field.")
         


  # Sets a parameter to a specific value in a create workpackage form
  def setValueUponCreate(self, param_name: str, sub: str, value: any, form: list = None):

    if (not form and not self.createWPForm):
      raise ValueError("Current form not set.")
    
    if (not form):
      form = deepcopy(self.createWPForm)

    href = form['_links']['self']['href']
    method = form['_links']['self']['method']

    # === Now we have the create form! ===
    fd = self.createWPForm['_embedded']['payload']

    # Step 1a: Check if the parameter is a direct attribute
    if param_name in fd and not sub:
      # print(f"  '{param_name}'/'{sub}': No sub.")
      self.createWPForm['_embedded']['payload'][param_name] = value
      return True

    # Step 1b: Check if the parameter is an object with several attributes
    if param_name in fd and sub in fd[param_name]:
      # print(f"  '{param_name}'/'{sub}': Sub to be assigned, found it.")
      self.createWPForm['_embedded']['payload'][param_name][sub] = value
      return True
      
    # print(f"schema = '{self.createWPForm['schema']}'")
    
    # Step 2: Try to identify the field by name
    field_id = self.find_attribute_key(self.createWPForm['_embedded']['schema'], param_name)
    if not field_id:
        raise ValueError(f"  Field with name '{param_name}' not found in the schema.")

    fld = self.createWPForm['_embedded']['schema'][field_id]
    match fld['type']:
      case 'CustomOption':
        for currentValue in fld['_links']['allowedValues']:
          # Check if the current value is the one we're looking for
          if currentValue['title'] == value:
            # We are there!
            # Add href to response and return
            # print(f"Filed ID: {field_id}, currentValue.title = {currentValue['title']}, currentValue.href = {currentValue['href']}")
            self.createWPForm['_embedded']['payload']['_links'][field_id] = {"href": currentValue['href']}
            return True
        
        raise ValueError(f"  Field '{field_id}/'{param_name}' (Type '{fld['type']}'): value '{value}' not found.")	

      case 'Date':
          value = value.isoformat()
          self.createWPForm['_embedded']['payload'][field_id] = value
          return True
       
      case 'Type':
          allowedValues = self.createWPForm['_embedded']['schema'][field_id]['_embedded']['allowedValues']
          # print(json.dumps(allowedValues, indent=2))
                
          for currentValue in allowedValues:
            # Check if the current value is the one we're looking for
            if currentValue['name'] == value:
              # We are there!
              # Add href to response and return
              self.createWPForm['_embedded']['payload']['_links'][field_id] = {
                 "href": currentValue['_links']['self']['href'],
                 "title": currentValue['_links']['self']['title']
              }
              return True
          
      case 'Status':
          allowedValues = self.createWPForm['_embedded']['schema'][field_id]['_embedded']['allowedValues']
          # print(json.dumps(allowedValues, indent=2))
                
          for currentValue in allowedValues:
            # Check if the current value is the one we're looking for
            if currentValue['name'] == value:
               # We are there!
               # Add href to response and return
               self.createWPForm['_embedded']['payload']['_links'][field_id] = {
                  "href": currentValue['_links']['self']['href'],
                  "title": currentValue['_links']['self']['title']
                  }
               return True

      case 'Formattable':
          self.createWPForm['_embedded']['payload'][field_id]['raw'] = value
          return True
          
      case 'String' | 'Link':
          self.createWPForm['_embedded']['payload'][field_id] = value
          return True

      case 'WorkPackage':
          self.createWPForm['_embedded']['payload']['_links'][field_id] = value
          return True

      case _:
        raise ValueError(f"  Type '{fld['type']}' of field '{field_id}/'{param_name}' not implemented yet.")	
    
  # Sets a parameter to a specific value in an update form
  def setValueUponUpdate(self, param_name: str, sub: str, value: any, wp: list = None):

    if (not wp and not self.currentWP):
      raise ValueError("Current work package not set.")
    
    if (not wp):
      wp = self.currentWP

    href = wp['_links']['update']['href']
    method = wp['_links']['update']['method']

    # Do we need to fetch the update form or do we have it already?
    if not self.updateWP or self.updateWP['href'] != href:
      if (not self.fetchUpdateForm(href, method)):
        return False
    
    # === Now we have the update form! ===
    fd = self.updateWP['formdata']    
    
    # Step 1a: Check if the parameter is a direct attribute
    if param_name in fd and not sub:
      # print(f"  '{param_name}'/'{sub}': No sub.")
      self.updateWP['response'][param_name] = value
      return True

    # Step 1b: Check if the parameter is an object with several attributes
    if param_name in fd and sub in fd[param_name]:
      # print(f"  '{param_name}'/'{sub}': Sub to be assigned, found it.")
      self.updateWP['response'][param_name][sub] = value
      return True
      
    # print(f"schema = '{self.updateWP['schema']}'")
    
    # Step 2: Try to identify the field by name
    field_id = self.find_attribute_key(self.updateWP['schema'], param_name)
    if not field_id:
      raise ValueError(f"  Field with name '{param_name}' not found in the schema.")

    # print(f"  '{param_name}'/'{sub}': Found it (field_id = '{field_id}').")
    
    fld = self.updateWP['schema'][field_id]
    match fld['type']:
      case 'CustomOption':
        for currentValue in fld['_links']['allowedValues']:
          # Check if the current value is the one we're looking for
          if currentValue['title'] == value:
            # We are there!
            # Add href to response and return
            self.updateWP['response']['_links'][field_id] = {"href": currentValue['href']}
            return True
        
        raise ValueError(f"  Field '{field_id}/'{param_name}' (Type '{fld['type']}'): value '{value}' not found.")	

      case _:
        raise ValueError(f"  Type '{fld['type']}' of field '{field_id}/'{param_name}' not implemented yet.")	
    
        
  # Sends the update data to OpenProject
  def commitUpdate(self) -> bool:
    full_url = self.PROTOCOL + self.host + self.updateWP['links']['commit']['href']
    payload = self.updateWP['response']
    
    # print(f"{self.updateWP['response']}")
    # print(f"{full_url}")
    # print("performPatchRequest(): Full request = " + unquote(full_url))
    
    response = requests.patch(full_url, auth=self.auth, headers=self.headers, json=payload)
    if response.status_code == 200:
      # print(f"  {inspect.currentframe().f_code.co_name}(): O.k.")
      return True
    else: 
      print(f"  {inspect.currentframe().f_code.co_name}(): HTTP Status code is {response.status_code}")
      return False


  # Requests the update form from OP, if needed
  # Prepares the response
  def fetchUpdateForm(self, href: str, method: str) -> bool:
    
    full_url = self.PROTOCOL + self.host + href
    # print("fetchUpdateForm(): Full request = " + unquote(full_url))
    
    if method == 'post':
        response = requests.post(full_url, auth=self.auth, headers=self.HEADERS)
    else:
        response = requests.get(full_url, auth=self.auth, headers=self.HEADERS)
    
    
    if response.status_code == 200:
      # print(f"  {inspect.currentframe().f_code.co_name}(): O.k.")
      # store response in JSON file
      data = response.json()
      """
      out_file = open("response.json", "w", encoding='utf-8')
      json.dump(data, out_file, ensure_ascii=False, indent = 2)
      out_file.close()
      """

      # Store update form
      self.updateWP['href'] = href
      self.updateWP['formdata'] = data['_embedded']['payload']
      self.updateWP['schema'] = data['_embedded']['schema']
      self.updateWP['links'] = data['_links']
      
      # Initialise response dictionary
      self.updateWP['response'] = { 
        "lockVersion": self.updateWP['formdata']['lockVersion'],
        "_links": {},
      }
      return True

    print(f"  {inspect.currentframe().f_code.co_name}(): HTTP Status code is {response.status_code}")
    return False
    

  def fetchCreateWorkpackageForm(self, project_id: int) -> Union[list, bool]:
    full_url = self.PROTOCOL + self.host + self.API + f"projects/{project_id}/work_packages/form"
    # print("fetchCreateWorkpackageForm(): Full request = " + unquote(full_url))

    response = requests.post(full_url, auth=self.auth, headers=self.headers)
    if response.status_code == 200:
      self.createWPForm = response.json()
      return self.createWPForm
    else:
      print(f"  {inspect.currentframe().f_code.co_name}(): HTTP Status code is {response.status_code}")
      return False


  def createWorkpackage(self, theForm) -> Union[list, bool]:
    # full_url = self.PROTOCOL + self.host + theForm['_links']['validate']['href']
    full_url = self.PROTOCOL + self.host + theForm['_embedded']['payload']['_links']['project']['href'] + "/work_packages"
    # print(f"  {inspect.currentframe().f_code.co_name}(): Full request = " + unquote(full_url))
  
    response = requests.post(full_url, auth=self.auth, headers=self.headers, json = theForm['_embedded']['payload'])

    if response.status_code == 201:
       print(f"  {inspect.currentframe().f_code.co_name}(): 201 Created", end="")
       return response.json() # return the response
    else:
      print(f"  {inspect.currentframe().f_code.co_name}(): HTTP Status code is {response.status_code}")
      with open('response.json', 'w') as f:
        json.dump(response.json(), f, indent=2)
      return False

  
  # Looks for a workpackage with a specific id in our list
  def getWorkpackageById(self, id: int) -> list:
      for standard in self.wp_array:
          if (standard.get('id') == id):
              return standard
      return None

  def getWorkpackageBySlug(self, slug: str) -> list:
      for wp in self.wp_array:
         wpSlug = self.getValue('Slug', "", wp)
         if (wpSlug == slug):
              return wp
      return None


  # Looks for the root work package (the first one under the project with 0 ancestors)
  def getRootWorkpackage(self) -> list:
      for standard in self.wp_array:
          links = standard.get("_links")
          ancestors = links.get('ancestors')
          if ancestors is not None and len(ancestors) == 0:
              return standard
      return None


  def getChildWorkpackageByTitle(self, root_wp: list, title: str):
      root_wp_id = root_wp.get('id')
      for standard in self.wp_array:
          ancestors = standard.get('_links').get('ancestors')
          if ancestors is not None:
            for ancestor in ancestors:
              if standard.get('subject') == title and ancestor['href'] == (f'/api/v3/work_packages/{root_wp_id}'):
                return standard
      return None

  def getChildResources(self):
      # Access the type element within _links
      links = self.currentWP.get('_links', {})

      type_element = links.get('type', {})
      wpType = type_element.get('title', '')

      # Are we dealing with a standalone standard?
      if (wpType == 'FIT-STD'):
          return False # No linked standard, we are done!
      
      # Is it a child?
      elif (wpType == 'FIT-STD-Modul'):
          # Access the parent element within _links
          parent_element = links.get('parent', {})
          href = parent_element.get('href', '')

          # Extract the last numerical bit from the href
          match = re.search(r'/(\d+)$', href)
          if match:
             parent_id = int(match.group(1))

             # Find parent standard
             theParent = self.getWorkpackageById(parent_id)
             if (not theParent):
                 print("")
                 print(f"  Unexpected error in getParentId(): parent not found!")
                 sys.exit(1)
             
          else:
            print("")
            print(f"  Unexpected error in getParentId(): no match!")
            sys.exit(1)
            
      # We are at the parent already!
      else:
        theParent = self.currentWP
      
      slug = self.getValue('Slug', "", theParent)

      childResources = [ {'name': theParent.get('subject'), 'slug': slug}]
        
      children = theParent["_links"]["children"]
      # print(children)
      
      for child in children:
          match = re.search(r'/(\d+)$', child.get('href'))
          childId = int(match.group(1))
          childObj = self.getWorkpackageById(childId)
          if (childObj):
              slug = self.getValue('Slug', "", childObj)
              childResources.append({'name': child.get('title'), 'slug': slug})
      
      return childResources

  def deleteWorkpackage(self, id: int) -> bool:
    full_url = self.PROTOCOL + self.host + self.API + f"work_packages/{id}"
    # print("Full request: " + unquote(full_url))

    response = requests.delete(full_url, auth=self.auth, headers=self.HEADERS)

    if response.status_code == 204:
        print(f"  Work package {id} deleted successfully.")
        return True
    else:
        print(f"  Failed to delete work package {id}. Status code: {response.status_code}")
        return False

