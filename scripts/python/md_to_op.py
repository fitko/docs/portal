#!/usr/bin/env python

"""	md_to_op.py: Reads the FIT-Standards files from Informationsplattform (docs.fitko.de/fit-standards/)
    and transfers them to an OpenProject instance.
    Appropriate work packages will be created or updated.

    *** THIS FILE IS NOT MENT TO RUN IN GITLAB ENVIRONMENT ***
    *** USE A LOCAL ENVIRONMENT INSTEAD                    ***

    
    Security
    ========
    An OpenProject apikey is used for authorization.
    
    Usage
    ===== 
    md_to_op.py [-h] PATH_TO_FOLDER project_id token
    
    Parameters
    ==========
    PATH_TO_FOLDER    Path to the folder containing the subfolders and files 
    									of the Informationsplattform. Mandatory.
                
		project_id				The ID (numerical or project name) of the OpenProject project 
											to retrieve the data from. Mandatory.

    token       			OpenProject apikey. Mandatory.
      
    	
"""
from copy import deepcopy
import os
import sys
from typing import Union, List, Any
import argparse
import json
from datetime import datetime, timezone

from class_OpenProject import OpenProject
# from class_OP2Infoplattform import OpenProject2Infoplattform
from class_InfoP import InfoP

__author__ = "Jürgen Voskuhl, itcv GmbH"
__license__ = "GPL"
__version__ = "2024-12-14"
__maintainer__ = "Jürgen Voskuhl"
__email__ = "jvo@itcv-software.com"
__status__ = "Draft"

# ========== Define constants ==========

# The host where the OpenProject instance lives
HOST = 'project.fitko.opendesk.live'

# Our table for any required translations
MAP_TABLE = {
    "Art": [
        {"OP": "Bedarfsträger", "IP": "productOwner"},
        {"OP": "Umsetzungsteam", "IP": "developer"},
        {"OP": "Betreiber", "IP": "operator"},        
        {"OP": "Ansprechpartner", "IP": "contactInformation"},
    ],
    "Kontakt": [
        {"OP": "Bezeichnung", "IP": "organisation"},
        {"OP": "Website", "IP": "url"},
        {"OP": "E-Mail", "IP": "mail"},
        {"OP": "Kontaktformular (Link)", "IP": "contactform"},
        {"OP": "Rufnummer", "IP": "phone"},
    ],
    "Status": [
      {"OP": "Identifizierung und Bedarfsmeldung", "IP": "bedarfsmeldung"},
      {"OP": "Prüfung und Bewertung", "IP": "prüfung"},
      {"OP": "Umsetzung", "IP": "umsetzung"},
      {"OP": "Überführung in Regelbetrieb", "IP": "genehmigung_plr"},
      {"OP": "Regelbetrieb und Monitoring", "IP": "regelbetrieb"},
      {"OP": "Dekommissionierungsempfehlung", "IP": "dekommissionierung"},
      {"OP": "Dekommissionierung", "IP": "dekommissionierung"},
      {"OP": "Dekommissioniert", "IP": "dekommissionierung"},
    ],
    "Type": [
      {"OP": "Architektur-Standard", "IP": "architektur"},
      {"OP": "Taxonomie", "IP": "taxonomie"},
      {"OP": "Techn.-fachliche Klassifizierungen", "IP": "tech_fach"},
      {"OP": "Datenstruktur", "IP": "datenstruktur"},
      {"OP": "Designsprachen und -systeme", "IP": "design"},
      {"OP": "Datenaustausch-Standard", "IP": "datenaustausch"},
      {"OP": "Transport- und Sicherheits-Standard", "IP": "tsp_sich"},
      {"OP": "APIs", "IP": "api"},
    ],
    "Artefakte": [
       {"OP": "Spezifikation", "IP": "artefaktSpec"},
       {"OP": "Benutzerdokumentation", "IP": "artefaktDocumentation"},
       {"OP": "Testumgebung", "IP": "artefaktTestumgebung"},
       {"OP": "Referenzimplementierung", "IP": "artefaktRefImplementation"},
       {"OP": "Betriebskonzept", "IP": "artefaktBetriebskonzept"},
    ]

}

# ========== No editing required below this line! ==========

# Sometimes we must "translate" a value. This function performs the operation.
def getMappedValue(type: str, opValue: str, falseOnError: bool = False) -> Union[str, bool]:
    try:
        # Check if the type exists in MAP_TABLE
        if type not in MAP_TABLE:
            if (falseOnError):
              return False
            else:
              print(f"Error: Type '{type}' not found in MAP_TABLE.")
              sys.exit(1)

        # Lookup opValue in the list of dictionaries under the given type
        for mapping in MAP_TABLE[type]:
            if mapping["OP"] == opValue:
                return mapping["IP"]
            elif mapping["IP"] == opValue:
                return mapping["OP"]

        # If opValue not found, handle the error
        if (falseOnError):
            return False
        else:
            print(f"Error: OP value '{opValue}' not found for type '{type}'.")
            sys.exit(1)

    except Exception as e:
        # General error handling
        if (falseOnError):
            return False
        else:
            print(f"An unexpected error occurred: {e}")
            sys.exit(1)


# Transfers the data from the md file into the OpenProject form
def fillForm(md_content: list, theForm: list) -> None:
    fm = md_content['front_matter']
  
    slug = fm['slug']
    isStandalone = fm['tags']['slugMaster'] == 'None' or fm['tags']['slugMaster'] is None
    isMaster = fm['tags']['slugMaster'] == slug

    # === Fill the form with our data ===

    # Get the standards type and set the parent for standalones and masters
    if isStandalone:
      stdType = 'FIT-STD'
      op.setValueUponCreate('Übergeordnetes Arbeitspaket', '', wpParentOfStandalones['_links']['self'], theForm)
    elif isMaster:
      stdType = 'FIT-STD-Verbund'
      op.setValueUponCreate('Übergeordnetes Arbeitspaket', '', wpRoot['_links']['self'], theForm)
    else:
      stdType = 'FIT-STD-Modul'
    
    # Set the standards type  
    op.setValueUponCreate('Typ', "", stdType, theForm)

    # Publish on information platform always as 'Preview'
    op.setValueUponCreate('Status auf Infoplattform', "", "Preview", theForm)

    # Set the standard's name
    op.setValueUponCreate('subject', "", fm['name'], theForm)

    # Set the description
    op.setValueUponCreate('description', 'raw', md_content['document_content'], theForm)

    # Set the short description
    op.setValueUponCreate('Kurzbeschreibung', 'raw', fm['shortDescription'], theForm)

    # Set the standard's slug
    op.setValueUponCreate('Slug', "", slug, theForm)

    # Date of last update
    op.setValueUponCreate('Letzte Aktualisierung', '', fm['dateLastUpdate'], theForm)

    # Status
    value = getMappedValue('Status', fm['tags']['status'])
    # print(f"From md: {fm['tags']['status']}, value = {value}")
    op.setValueUponCreate('Status', '', value, theForm)

    # Type of standard
    if 'type' in fm['tags'] and fm['tags']['type'] != None:
      value = getMappedValue('Type', fm['tags']['type'])
      if value:
        op.setValueUponCreate('Typ des Standards', '', value, theForm)

    # === Contacts ===
    contactTypes = ['productOwner', 'developer', 'operator', 'contactInformation']
    contactCount = 1
    for contactType in contactTypes:
        if not contactType in fm:
           continue
        
        if fm[contactType]['organisation']:
          if contactCount > 2:
            raise ValueError(f"  More than 2 contacts.")	

          op.setValueUponCreate(f'Organisation {contactCount}: Bezeichnung', '', fm[contactType]['organisation'], theForm)  
          op.setValueUponCreate(f'Organisation {contactCount}: Art', '', getMappedValue('Art', contactType), theForm)  
          op.setValueUponCreate(f'Organisation {contactCount}: Website', '', fm[contactType]['url'], theForm)  
          op.setValueUponCreate(f'Organisation {contactCount}: E-Mail', '', fm[contactType]['mail'], theForm)  
          op.setValueUponCreate(f'Organisation {contactCount}: Rufnummer', '', fm[contactType]['phone'], theForm)  
          op.setValueUponCreate(f'Organisation {contactCount}: Kontaktformular (Link)', '', fm[contactType]['contactform'], theForm)
          contactCount += 1

    # Liability
    value = 'Nicht definiert'        
    if 'liability' in fm and 'liability' in fm['liability']:
      value = fm['liability']['liability']
      if not value:
        value = 'Nicht definiert'        
      else:
          op.setValueUponCreate('Gültig ab/seit', '', fm['liability']['date'], theForm)
          op.setValueUponCreate('Link zum Beschluss', '', fm['liability']['url'], theForm)
    op.setValueUponCreate('Verbindlichkeit', '', value, theForm)

    # Framework
    if 'framework' in fm and 'id' in fm['framework']:
      value = fm['framework']['id']
      if value:
          op.setValueUponCreate('Framework', '', value, theForm)

    # Logo
    value = fm['logo']['path']
    if value != None and value.startswith('public/'):
      value = 'https://docs.fitko.de/' + value[len('public/'):]
      op.setValueUponCreate('Link zum Logo', '', value, theForm)

    # Artefakte
    items = [
        'artefaktSpec', 
        'artefaktDocumentation', 
        'artefaktTestumgebung',
        'artefaktRefImplementation',
        'artefaktBetriebskonzept',
    ]
    for item in items:
      itemOp = getMappedValue('Artefakte', item)

      value = fm[item]['url']
      if value != 'n.v.':
        op.setValueUponCreate(f"{itemOp}: Link", '', value, theForm)
        if 'license' in fm[item]:
          value = fm[item]['license']
          if value != None:
            op.setValueUponCreate(f"{itemOp}: Lizenz", '', value, theForm)

    # Standard-spezifische Artefakte
    # print(fm['artefaktUserDefined'])
    udaCount = 1
    for uda in fm['artefaktUserDefined']:
      if udaCount > 4:
        raise ValueError(f"  More than 3 user defined artefacts.")	

      op.setValueUponCreate(f"Artefakt {udaCount}: Bezeichnung", '', uda['name'], theForm)  
      op.setValueUponCreate(f"Artefakt {udaCount}: Link", '', uda['url'], theForm)
      if 'license' in uda and uda['license']:
        op.setValueUponCreate(f"Artefakt {udaCount}: Lizenz", '', uda['license'], theForm)  
      udaCount += 1

    # Support
    supportCount = 1
    for support in fm['support']:
      if supportCount > 4:
        raise ValueError(f"  More than 4 support entries.")	

      op.setValueUponCreate(f"Support {supportCount}: Bezeichnung", '', support['title'], theForm)  
      op.setValueUponCreate(f"Support {supportCount}: Link", '', support['url'], theForm)
      if 'license' in support:
        op.setValueUponCreate(f"Support {supportCount}: Lizenz", '', support['license'], theForm)  
      supportCount += 1



# ========== Here the fun begins! ==========

# Let's get the OpenProject API token and optional filename
parser = argparse.ArgumentParser(		
		description='Reads information stored in OpenProject via API and prepare it to be used in Informationsplattform.'
)
parser.add_argument("folder", help="Folder containing FIT-Standard folders and md files. Required.")
parser.add_argument("project_id", help="project ID in OpenProject (project id or project name). Required.")
parser.add_argument("token", help="OpenProject API Token. Required.")
try:
    args = parser.parse_args()
except SystemExit:
    # argparse automatically exits if required args are missing
    sys.exit(1)


ip = InfoP(args.folder)
folders = ip.list_folders()
print(f"Found {len(folders)} folders in '{args.folder}'.")


# Get the files to process

# Create our OpenProject object
op = OpenProject(HOST, args.token)

project_id = args.project_id

if isinstance(project_id, str) and not project_id.isdigit():
  project_id = op.getProjectId(project_id)

if not project_id:
    print(f"Project ID '{args.project_id}' not found!")
    sys.exit(1)

# The ID of the project where we're going to write data to
print(f"Project ID = '{project_id}'")

# get all types related to the project in question
types = op.getTypes(project_id)

# We want only the list of elements (work package types)
typeElements = types['_embedded']['elements']

wp = op.getProjectWorkpackages(project_id)

# Check if the result is None
if wp is None:
    print("Can't get work packages from OpenProject.")
    sys.exit(1)

# Get our root work package
wpRoot = op.getRootWorkpackage()
if wpRoot is None:
    print("Can't get root work package from OpenProject.")
    sys.exit(1)
else:
   wpRootHref = wpRoot.get('_links').get('self').get('href')

print(f"Root ID = {wpRoot.get('id')}")

wpParentOfStandalones = op.getChildWorkpackageByTitle(wpRoot, "Ohne Verbund")
if wpParentOfStandalones is None:
    print("Can't get parent of standalone standards from OpenProject.")
    sys.exit(1)
else:
   wpParentOfStandalonesHref = wpParentOfStandalones.get('_links').get('self').get('href')

print(f"ID of parent for standalone standards = {wpParentOfStandalones.get('id')}")

# Delete all existing FIT-STD work packages except 
# root work package and the parent work package of standalone standards
deleteLater = []
for child in wp['_embedded']['elements']:
    type = op.getValue("_links", "type", child)

    if type.get('title').startswith('FIT-STD') and (child.get('id') != wpRoot.get('id') and child.get('id') != wpParentOfStandalones.get('id')):
        if type.get('title') == 'FIT-STD-Modul':    # Child standard?
           deleteLater.append(child.get('id'))
           continue # Will be deleted if parent is deleted!
   
        print(f"Deleting work package {child.get('id')} ({child.get('subject')})... ", end='')
        result = op.deleteWorkpackage(child.get('id'))
        if result == False:
           deleteLater.append(child.get('id'))


if len(deleteLater) > 0:
  # Get workpackages again (there should be only the root and the parent of standalone standards left now)
  wp = op.getProjectWorkpackages(project_id)
  for child in wp['_embedded']['elements']:
    type = op.getValue("_links", "type", child)
    if type.get('title').startswith('FIT-STD') and (child.get('id') != wpRoot.get('id') and child.get('id') != wpParentOfStandalones.get('id')):
      print(f"Deleting work package {child.get('id')} ({child.get('subject')})... ", end='')
      result = op.deleteWorkpackage(child.get('id'))

childs = []

# Process all folders
for folder in folders:
    print(f"Processing folder '{os.path.basename(folder)}'... ", end="")
    md_content = ip.read_md_file(folder)
    fm = md_content['front_matter']
    slug = fm['slug']
    # print(f"\nFront Matter: {fm}")

    isMaster = False
    isStandalone = fm['tags']['slugMaster'] == 'None' or fm['tags']['slugMaster'] is None
    # print(f"slugMaster = {fm['tags']['slugMaster']}, Standalone = {isStandalone}")
    if not isStandalone:
      isMaster = fm['tags']['slugMaster'] == slug

    if isStandalone:
        print("Standalone standard.")
    elif isMaster:
        print("Standard is master.")
    else:
        print("Standard is child.")

    # === Process the standard ===
          
    # Create a new work package
    theForm = op.fetchCreateWorkpackageForm(project_id)
    if theForm == False:
      print(f"Can't get create work package form from OpenProject.")
      sys.exit(1)

    # Fill the form with our data
    fillForm(md_content, theForm)

    # Create the work package?
    if isStandalone or isMaster:
      # Create the work package
      wpStandard = op.createWorkpackage(theForm)
      if wpStandard == False:
        print(f"Can't create work package for standard '{slug}'.")
        sys.exit(1)

    else:
        # Store child standard for later use
        print(f"Storing child standard '{slug}'. Will be processed later.",end='')
        childs.append( {'parent': fm['tags']['slugMaster'], 'slug': fm['slug'], 'theForm': deepcopy(theForm)} )

    print("\n---\n")


# Process the child standards, if any
if len(childs) > 0:
  print("=== Processing child standards ===")

  # Get workpackages again  
  wp = op.getProjectWorkpackages(project_id)

  # Process all children
  for child in childs:
    slug = child.get('slug')
    parent_slug = child.get('parent')
    theForm = child.get('theForm')

    # Find the parent work package
    wpParent = op.getWorkpackageBySlug(parent_slug)
    if wpParent is None:
      print(f"Can't get parent work package '{parent_slug}' for standard '{slug}'.")
      sys.exit(1)

    print(f"Creating child standard '{slug}' with parent '{parent_slug}'...",end='')
    theForm['_embedded']['payload']['_links']['parent'] = wpParent['_links']['self']

    wpStandard = op.createWorkpackage(theForm)
    if wpStandard is None:
      print(f"\n  Can't create work package for standard '{slug}'.")
      sys.exit(1)
    print("\n---")

print("Done.")





    