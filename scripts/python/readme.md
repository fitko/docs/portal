# Read tailored data from an OpenProject instance and prepare it to be used in Informationsplattform

## Overview
The „Informationsplattform“ is the directory of Federal IT Standards and a part of the „Federal Development Portal“, which serves as a central point of contact for software developers in the governmental environment.  


The scripts in this folder performing the job mentioned in the document title.

![Transfer data - block diagram](img/Transfer_data_-_block_diagram.png "Transfer data - block diagram")

## Application topology

The application consists of three script files:

- **transfer_fit-standards.py**  
conducts the transfer
- **class_OpenProject.py**  
  deals with the OP related issues, e.g. API requests and reading data values from the OP data structures
- **class_OP2Infoplattform.py**  
  Prepares the objects and provides them in a file in a dedicated folder, ready to be read from the Informationsplattform application (Next.js)

The following diagram provides an overview of the topology:

```mermaid
classDiagram
    Hauptprogramm --|> OpenProject
    Hauptprogramm --|> OpenProject2Infoplattform
    class Hauptprogramm {
        +String OpenProject HOST
        +int OpenProject PROJECT_ID
        +String MAP_TABLE
        +getMappedValue()
        +get_second_part()
        +convert_date_to_utc_string()
    }
    class OpenProject {
      -String PROTOCOL
      -String API
      -JSON currentWP
      -JSON updateWP
      +getAttributByName(elements, search_name, attribut_name)
      +getProjectId(identifier)
      +getProjectWorkpackages(project_id, filters[]) JSON[]
      +getTypes(project_id, filters[]) JSON[]
      +setCurrentWP(JSON)
      +find_attribute_key(schema_data, target_name)
      +getValue(param_name, sub, wp)
      +setValueUponCreate(param_name, sub, value, wp)
      +setValueUponUpdate(param_name, sub, value, wp)
      +commitUpdate()
      +fetchUpdateForm(href, method)
      +fetchCreateWorkpackageForm(project_id)
      +createWorkpackage(theForm)
      +getWorkpackageById(id)
      +getWorkpackageBySlug(slug)
      +getRootWorkpackage()
      +getChildWorkpackageByTitle(root_wp, title)
      +getChildResources()
      +deleteWorkpackage(id)
    }
    class OpenProject2Infoplattform {
      -JSON EMPTY_IP_OBJECT
      -JSON work_packages[]
      -JSON wp_array[]
      -JSON ip_array[]
      +getInfoPlattformObjects()
      +readLastResult(filename)
      +useLastResult(slug)
      +createNewObject()
      +getObjectBySlug(slug)
      +setValue(par_name, value)
      +setContactValue(contact_type, par_name, value)
      +setTag(tag_name, par_name, value)
      +setArtefact(tag_name, par_name, value, force_add)
      +addChildResource(name, slug)
      +addSupportResource(title, url)
    }
```

## Calling transfer_fit-standards.py

To run the Python script _transfer_fit-standards.py_, you must call it like so:

```console
py transfer_fit-standards.py [-h] [--outfile OUTFILE] PROJECT_ID TOKEN
```

The parameters are described in the table below:

| Parameter  | Meaning                                                                                                                                                 |
|------------|---------------------------------------------------------------------------------------------------------------------------------------------------------|
| PROJECT_ID | The mandatory parameter represents the OpenProject project ID where data is read from. The numeric ID is allowed as well as the project name. |
| TOKEN      | The mandatory parameter represents the API token to access the OpenProject API. |
| OUTFILE    | Optional. The name (incl. path) of the file the output shall be written to.<br/>Defaults to 'from_openproject.json'.                                    |

### Exit codes

The script performs extensive checks on the validity of the data structures read and the data itself:

- A slug cannot be empty
- A slug must be unique
- Organisation types must be unique
- Artefacts:
  - If an URL is specified, there must also be a name
  - If a license is specified, there must also be an URL
  - For specific artefacts, there must also be a name and an URL given
- Liability: if an URL is given, also the liability must be stated

Whenever an error occurs, the exit code will be 1.

An exit code of 0 means all went well, the resulting JSON file has been updated in the dedicated folder.

## Other scripts in this folder

### reset_status_in_OP.py

This script resets the publishing status on the information platform of all work packages in the specified project.

It is used to reset the status of all work packages with status 'Preview' to 'Published' after an 'Update FIT-Standards' merge request has been merged into the main branch (not triggered right now!)

To run the Python script _reset_status_in_OP.py_, you must call it like so:

```console
py reset_status_in_OP.py [-h] PROJECT_ID TOKEN
```

The parameters are the same as in the table above.

### md_to_op.py

This script is used only internally to transfer data from the information platform to OpenProject work packages.  
It does this by

- deleting the existing workpackages in OpenProject
- reading the md files on information platform
- creating new workpackages in OpenProject

To run the Python script _md_to_op.py_, you must call it like so:

```console
py md_to_op.py [-h] PROJECT_ID TOKEN
```

The parameters are the same as in the table above.
