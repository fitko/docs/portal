#!/usr/bin/env python

"""	reset_status_in_OP.py: Resets all FIT-Standards which are in Preview mode to Productive mode.

    Security
    ========
    An OpenProject apikey is used for authorization.
    To secure the API access in the production environment, 
    a CI/CD variable must be used.

    Usage
    ===== 
    reset_status_in_OP.py [-h] [project_id] token
    
    Parameters
    ==========
    project_id	The project ID (numeric or identifier) in Openproject the script shall deal with. Mandatory.
                
    token       OpenProject apikey. Mandatory.
          	
"""
import os
import sys
from typing import Union, List, Any
import argparse
import json
from datetime import datetime, timezone

from class_OpenProject import OpenProject

__author__ = "Jürgen Voskuhl, itcv GmbH"
__license__ = "GPL"
__version__ = "2024-12-14"
__maintainer__ = "Jürgen Voskuhl"
__email__ = "jvo@itcv-software.com"
__status__ = "Produktiv"

# ========== Define constants ==========

HOST = 'project.fitko.opendesk.live'

# ANSI escape codes for colors
GREEN = "\033[92m"
RED = "\033[91m"
YELLOW = "\033[93m"
RESET = "\033[0m"



# Let's get the OpenProject API token and optional filename
parser = argparse.ArgumentParser(		
		description='Resets all FIT-Standards which are in Preview mode to Productive mode.'
)
parser.add_argument("project_id", help="project ID in OpenProject (project id or project name).")
parser.add_argument("token", help="OpenProject API Token. Required.")
try:
    args = parser.parse_args()
except SystemExit:
    # argparse automatically exits if required args are missing
    sys.exit(1)


# Create our OpenProject object
op = OpenProject(HOST, args.token)

project_id = args.project_id

if isinstance(project_id, str) and not project_id.isdigit():
  project_id = op.getProjectId(project_id)

if not project_id:
    print(f"{RED}Can't access project with ID '{args.project_id}'{RESET}")
    sys.exit(1)

# The ID of the project where we read data from
print(f"Project ID = '{project_id}'")


# get all types related to the project in question
types = op.getTypes(project_id)

# We want only the list of elements
elements = types['_embedded']['elements']

# Extract ids where the name starts with "FIT-STD"
fit_std_ids = [element['id'] for element in elements if element['name'].startswith('FIT-STD')]


# We only want work packages of type FIT-STD, FIT-STD-Verbund and FIT-STD-Modul
# and work packages where the status is not 'new'.
myFilter = [
  { "type": { "operator": "=", "values": fit_std_ids } },
  { "status": { "operator": "!", "values": [1] } }	
]
wp = op.getProjectWorkpackages(project_id, myFilter)

# Check if the result is None
if wp is None:
    print(f"{RED}Can't get work packages from OpenProject.{RESET}")
    sys.exit(1)

"""
# store response in JSON file
out_file = open("response.json", "w", encoding='utf-8')
json.dump(wp, out_file, ensure_ascii=False, indent = 2)
out_file.close()
"""

# Process all FIT-Standards read
for myWorkPackage in wp['_embedded']['elements']:

  # We're working with the actual work package
  op.setCurrentWP(myWorkPackage)
  
  id = op.getValue("id")
  name = op.getValue("subject")
  current_status = op.getValue("Status auf Infoplattform")
  
  print(f"Name ='{name} ({id}), Status = {current_status}")
  if (current_status == 'Preview'):
    result = op.setValueUponUpdate("Status auf Infoplattform", '', 'Produktiv')
    op.commitUpdate()
    print(f"{GREEN}  Set Status 'Produktiv': {result}{RESET}")
  