#!/usr/bin/env python

"""	transfer_fit-standards.py: Reads the FIT-Standards from an OpenProject instance
    and serves them for Informationsplattform (docs.fitko.de/fit-standards/).

    Security
    ========
    An OpenProject apikey is used for authorization.
    To secure the API access in the production environment, 
    a CI/CD variable must be used.

    Usage
    ===== 
    transfer_fit-standards.py [-h] [--outfile OUTFILE] token
    
    Parameters
    ==========
    OUTFILE     Path and filename for the output. Optional.
                Defaults to 'from_openproject.json'.
                
    project_id	The ID (numerical or project name) of the OpenProject project 
                to retrieve the data from.

    token       OpenProject apikey. Mandatory.
      
      
"""
import os
import sys
import requests
import hashlib
from typing import Union, List, Any
import argparse
import json
from datetime import datetime, timezone
from urllib.parse import urlparse

from class_OpenProject import OpenProject
from class_OP2Infoplattform import OpenProject2Infoplattform

__author__ = "Jürgen Voskuhl, itcv GmbH"
__license__ = "GPL"
__version__ = "2024-12-14"
__maintainer__ = "Jürgen Voskuhl"
__email__ = "jvo@itcv-software.com"
__status__ = "Production"

# ========== Define constants ==========

HOST = 'project.fitko.opendesk.live'
URL_ENTWICKLUNGSPORTAL = 'https://docs.fitko.de/'
IMAGES_PATH = '../../public/img/fit-standards/icons/'

# ANSI escape codes for colors
GREEN = "\033[92m"
RED = "\033[91m"
YELLOW = "\033[93m"
RESET = "\033[0m"


# Our table for any required translations
MAP_TABLE = {
    "Art": [
        {"OP": "Initiierung", "IP": "initiator"},
        {"OP": "Bedarfsträger", "IP": "productOwner"},
        {"OP": "Steuerung", "IP": "steeringCommittee"},
        {"OP": "Umsetzung", "IP": "developer"},
        {"OP": "Betrieb", "IP": "operator"},        
        {"OP": "Support", "IP": "contactInformation"},
        {"OP": "Finanzierung", "IP": "funder"},
    ],
    "Kontakt": [
        {"OP": "Bezeichnung", "IP": "organisation"},
        {"OP": "Website", "IP": "url"},
        {"OP": "E-Mail", "IP": "mail"},
        {"OP": "Kontaktformular (Link)", "IP": "contactform"},
        {"OP": "Rufnummer", "IP": "phone"},
    ],
    "Status": [
      {"OP": "Identifizierung und Bedarfsmeldung", "IP": "bedarfsmeldung"},
      {"OP": "Prüfung und Bewertung", "IP": "prüfung"},
      {"OP": "Umsetzung", "IP": "umsetzung"},
      {"OP": "Überführung in Regelbetrieb", "IP": "genehmigung_plr"},
      {"OP": "Regelbetrieb und Monitoring", "IP": "regelbetrieb"},
      {"OP": "Dekommissionierungsempfehlung", "IP": "dekommissionierung"},
      {"OP": "Dekommissionierung", "IP": "dekommissionierung"},
      {"OP": "Dekommissioniert", "IP": "dekommissionierung"},
    ],
    "Type": [
      {"OP": "Architektur-Standard", "IP": "architektur"},
      {"OP": "Taxonomie", "IP": "taxonomie"},
      {"OP": "Techn.-fachliche Klassifizierungen", "IP": "tech_fach"},
      {"OP": "Datenstruktur", "IP": "datenstruktur"},
      {"OP": "Designsprachen und -systeme", "IP": "design"},
      {"OP": "Datenaustausch-Standard", "IP": "datenaustausch"},
      {"OP": "Transport- und Sicherheits-Standard", "IP": "tsp_sich"},
      {"OP": "APIs", "IP": "api"},
    ],
    "ContextRelation": [
      { "OP": "B2B", "IP": "B2B"},
      { "OP": "B2G", "IP": "B2G"},
      { "OP": "C2G", "IP": "C2G"},
      { "OP": "G2G", "IP": "G2G"},
      { "OP": "C2G", "IP": "C2G"},
      { "OP": "Nicht definiert", "IP": "undefined"},
    ],
    "ContextContent": [
       { "OP": "Beschaffung", "IP": "procurement"},
       { "OP": "Onlineservice (OZG)", "IP": "ozg"},
       { "OP": "Registermodernisierung", "IP": "reg_modernisation"},
       { "OP": "Anderer Kontext", "IP": "other_context"},
       { "OP": "Nicht definiert", "IP": "undefined"},
    ],
}

# ========== No editing required below this line! ==========

# Sometimes we must "translate" a value. This function performs the operation.
def getMappedValue(type: str, opValue: str, falseOnError: bool = False) -> Union[str, bool]:
    try:
        # Check if the type exists in MAP_TABLE
        if type not in MAP_TABLE:
            if (falseOnError):
              return False
            else:
              print(f"Error: Type '{type}' not found in MAP_TABLE.")
              sys.exit(1)

        # Lookup opValue in the list of dictionaries under the given type
        for mapping in MAP_TABLE[type]:
            if mapping["OP"] == opValue:
                return mapping["IP"]

        # If opValue not found, handle the error
        if (falseOnError):
            return False
        else:
            print(f"Error: OP value '{opValue}' not found for type '{type}'.")
            sys.exit(1)

    except Exception as e:
        # General error handling
        if (falseOnError):
            return False
        else:
            print(f"An unexpected error occurred: {e}")
            sys.exit(1)


# Contact data field names consist of two values:
# the no. of the current contact and the different values for the contact.
# They are separated by a colon. This function extracts the 2nd part.
def get_second_part(input_string):
    try:
        # Split the input string by the colon
        parts = input_string.split(":", 1)
        
        # Check if there are at least two parts resulting from the split
        if len(parts) < 2:
            raise ValueError("Input string does not contain a colon or a second part.")
        
        # Get the second part and trim any leading or trailing whitespace
        second_part = parts[1].strip()
        
        return second_part

    except Exception as e:
        print(f"An error occurred: {e}")
        sys.exit(1)


# Doing what it's named
def convert_date_to_utc_string(date_string):
    # Parse the input date string into a date object
    date_object = datetime.strptime(date_string, "%Y-%m-%d")
    
    # Convert to a datetime object in UTC, assuming midnight as time
    datetime_utc = datetime.combine(date_object, datetime.min.time(), tzinfo=timezone.utc)
    
    # Format as an ISO 8601 UTC string and replace '+00:00' with 'Z'
    utc_string = datetime_utc.isoformat().replace('+00:00', 'Z')
    
    return utc_string

def get_slug_from_url(url):
    # Split the URL by '/' and get the last part
    parts = url.split('/')
    if len(parts) > 1:
        return parts[-1]
    else:
        return url

# Handle the logo file:
# - If internal URL (https://docs.fitko.de/img/fit-standards/...), do nothing
# - If external URL, check if it's already in the images folder and content is the same.
#   - If yes, do nothing
#   - If no, download the file, store it in the images folder 
#     and return the internal URL as the logo file
#
def processLogo(slug: str, logo_path: str) -> str:
   
    # Internal URL already?
    if logo_path.startswith(f"{URL_ENTWICKLUNGSPORTAL}img/fit-standards/icons"):
        # print(f"  Logo URL is internal already. Nothing to do.")
        return logo_path  # We're already done here!

    # External URL!
    # Create base name for the logo file
    base_name = f"{slug}_{get_slug_from_url(logo_path)}"
    filename = os.path.join(IMAGES_PATH, base_name)

    # Check if the file exists
    if os.path.exists(filename):
        # Read the content of the existing file
        with open(filename, 'rb') as file:
            existing_content = file.read()

        # Download the content from the URL
        try:
            response = requests.get(logo_path)
            response.raise_for_status()  # Raise an error for bad responses
            url_content = response.content

            # Compare the content
            if hashlib.md5(existing_content).hexdigest() == hashlib.md5(url_content).hexdigest():
                # print(f"  Logo file '{filename}' already exists and content is the same.")
                return logo_path
        except requests.RequestException as e:
            print(f"  Error downloading logo from {logo_path}:\n  {e}")
            sys.exit(1)

    # If the file doesn't exist or content differs, download and save it
    try:
        response = requests.get(logo_path)
        response.raise_for_status()  # Raise an error for bad responses
        url_content = response.content

        # Save the content to the file
        with open(filename, 'wb') as file:
            file.write(url_content)
        # print(f"  Logo downloaded and saved as '{filename}'.")
        return filename
    
    except requests.RequestException as e:
        print(f"  Error downloading logo from {logo_path}:\n  {e}")
        sys.exit(1)




# Let's get the OpenProject API token and optional filename
parser = argparse.ArgumentParser(		
    description='Reads information stored in OpenProject via API and prepare it to be used in Informationsplattform.'
)
parser.add_argument("project_id", help="project ID in OpenProject (project id or project name).")
parser.add_argument("token", help="OpenProject API Token. Required.")
parser.add_argument("--outfile", help="Optional: file for output of the result.")
try:
    args = parser.parse_args()
except SystemExit:
    # argparse automatically exits if required args are missing
    sys.exit(1)


# Create our OpenProject object
op = OpenProject(HOST, args.token)

project_id = args.project_id

if isinstance(project_id, str) and not project_id.isdigit():
  project_id = op.getProjectId(project_id)

if not project_id:
    print(f"Project ID '{args.project_id}' not found!")
    sys.exit(1)

# The ID of the project where we read data from
print(f"Project ID = '{project_id}'")


# get all types related to the project in question
types = op.getTypes(project_id)

# We want only the list of elements
elements = types['_embedded']['elements']

# Extract ids where the name starts with "FIT-STD"
fit_std_ids = [element['id'] for element in elements if element['name'].startswith('FIT-STD')]


# We only want work packages of type FIT-STD, FIT-STD-Verbund and FIT-STD-Modul
# and work packages where the status is not 'new'.
myFilter = [
  { "type": { "operator": "=", "values": fit_std_ids } },
  { "status": { "operator": "!", "values": [1] } }	
]
wp = op.getProjectWorkpackages(project_id, myFilter)

# Check if the result is None
if wp is None:
    print("Can't get work packages from OpenProject.")
    sys.exit(1)

# store response in JSON file
out_file = open("response.json", "w", encoding='utf-8')
json.dump(wp, out_file, ensure_ascii=False, indent = 2)
out_file.close()

# Save filename for later use
filename = "from_openproject.json"
if args.outfile:
  filename = args.outfile


# Create instance of our converter class and provide data
o2i = OpenProject2Infoplattform(wp)

# Read old data from file, if it exists
o2i.readLastResult(filename)

# Process every OP work package
for myWorkPackage in wp['_embedded']['elements']:

  # We're working with the actual work package
  op.setCurrentWP(myWorkPackage)
  
  # Get the slug of the current work package
  id = op.getValue("id")
  status = op.getValue("_links", "status")
  subject = op.getValue("subject")
  slug = op.getValue("Slug")

  # Are we processing the root or the parent for all standalone FIT-Standards?
  if status['title'] == 'Permanent':
     continue # Skip this work package
     
     
  # === Error checks ===
  #

  # Check for empty slug
  if not slug:
    print(f"Work package #{id} ({subject}): Slug cannot be empty.\n")
    sys.exit(1)
  else:
    print(f"Processing work package #{id} '{subject}' (slug '{slug}')...", end="")

  # Check, if slug already exists
  existingStd = o2i.getObjectBySlug(slug)
  if existingStd != None:
    print("")
    print(f"  Slug '{existingStd['slug']}' exists already! Must be unique.")
    sys.exit(1)
    
    
  # - Organisation 1: Art / Organisation 2: Art: Check for not the same value
  c_type = op.getValue("Organisation 1: Art")
  if c_type == op.getValue("Organisation 2: Art"):
    print("")
    print(f"  Organisation type cannot be the same for both contacts ('{c_type}').")
    sys.exit(1)
  
  # 
  # Insert code here to check for skip update/insert current work package as InfoP-Object!
  #
  publish_mode = op.getValue("Status auf Infoplattform") 
  # print(f"  Publish mode = '{publish_mode}'")
  if (publish_mode == 'Nicht veröffentlichen'):
    print("")
    print(f"  Do NOT publish (publish_mode = '{publish_mode}')!\n")
    continue
  elif (publish_mode == 'Produktiv'):
    print("")
    print(f"  Try using previous data (publish_mode = '{publish_mode}')...", end="")
    if (not o2i.useLastResult(slug)):
      print(f"\n  {YELLOW}WARNING: No previous data available for slug '{slug}'!{RESET}\n  Using current data instead.")
    else:
      print("\n  Previous data found for slug '{slug}'!\n")
      continue
  else:
    print("")		
    print(f"  Update standard (publish_mode = '{publish_mode}')")
        

  # Check status
  status = getMappedValue("Status", op.getValue("Status"), True)
  if (not status):
    print("")
    print(f"  Do NOT publish (Status = '{status}')!\n")
    continue
  

  # Create a new FIT-Standard object
  o2i.createNewObject()
  
  # Name
  std_name = op.getValue("subject")
  o2i.setValue("name", std_name)

  # Kurzbeschreibung
  o2i.setValue("shortDescription", op.getValue("Kurzbeschreibung", "raw"))
  
  # Langtext
  o2i.setValue("text", op.getValue("description", "raw"))

  # Slug
  slug = op.getValue("Slug")
  o2i.setValue("slug", slug)
  
  # Logo
  logo_path = op.getValue("Link zum Logo")
  if logo_path:
    logo_path = processLogo(slug, logo_path) # Maybe move the logo file around
    o2i.setValue("logo", { "path": logo_path, "title": "Logo " + std_name})

  # Datum letztes Update
  o2i.setValue("dateLastUpdate", convert_date_to_utc_string(op.getValue("Letzte Aktualisierung")))

  # Status
  status = getMappedValue("Status", op.getValue("Status"))
  # print("Status = " + status)
  o2i.setValue("status", status)
  o2i.setTag("status", "status", status)
  o2i.setTag("status", "value", status)
  
  # Kontakt 1
  c_type = op.getValue("Organisation 1: Art")
  contactObject = getMappedValue("Art", c_type)
  
  temp = "Organisation 1: Bezeichnung"
  par_name = getMappedValue("Kontakt", get_second_part(temp))
  value = op.getValue(temp)
  o2i.setContactValue(contactObject, par_name, value)

  temp = "Organisation 1: Website"
  par_name = getMappedValue("Kontakt", get_second_part(temp))
  value = op.getValue(temp)
  o2i.setContactValue(contactObject, par_name, value)

  temp = "Organisation 1: E-Mail"
  par_name = getMappedValue("Kontakt", get_second_part(temp))
  value = op.getValue(temp)
  if value.startswith('mailto:'):     # Remove mailto: at the beginning
    value = value[len('mailto:'):]
  o2i.setContactValue(contactObject, par_name, value)

  temp = "Organisation 1: Kontaktformular (Link)"
  par_name = getMappedValue("Kontakt", get_second_part(temp))
  value = op.getValue(temp)
  o2i.setContactValue(contactObject, par_name, value)

  temp = "Organisation 1: Rufnummer"
  par_name = getMappedValue("Kontakt", get_second_part(temp))
  value = op.getValue(temp)
  o2i.setContactValue(contactObject, par_name, value)

  # Kontakt 2
  c_type = op.getValue("Organisation 2: Art")
  if(c_type):
    # print("2: c_type => contactObject: " + c_type + " => " + contactObject)

    contactObject = getMappedValue("Art", c_type)
    
    temp = "Organisation 2: Bezeichnung"
    par_name = getMappedValue("Kontakt", get_second_part(temp))
    value = op.getValue(temp)
    o2i.setContactValue(contactObject, par_name, value)

    temp = "Organisation 2: Website"
    par_name = getMappedValue("Kontakt", get_second_part(temp))
    value = op.getValue(temp)
    o2i.setContactValue(contactObject, par_name, value)

    temp = "Organisation 2: E-Mail"
    par_name = getMappedValue("Kontakt", get_second_part(temp))
    value = op.getValue(temp)
    if value.startswith('mailto:'):     # Remove mailto: at the beginning
      value = value[len('mailto:'):]
    o2i.setContactValue(contactObject, par_name, value)

    temp = "Organisation 2: Kontaktformular (Link)"
    par_name = getMappedValue("Kontakt", get_second_part(temp))
    value = op.getValue(temp)
    o2i.setContactValue(contactObject, par_name, value)

    temp = "Organisation 2: Rufnummer"
    par_name = getMappedValue("Kontakt", get_second_part(temp))
    value = op.getValue(temp)
    o2i.setContactValue(contactObject, par_name, value)

  # Kontakt 3
  c_type = op.getValue("Organisation 3: Art")
  if(c_type):
    # print("3: c_type => contactObject: " + c_type + " => " + contactObject)

    contactObject = getMappedValue("Art", c_type)
    
    temp = "Organisation 3: Bezeichnung"
    par_name = getMappedValue("Kontakt", get_second_part(temp))
    value = op.getValue(temp)
    o2i.setContactValue(contactObject, par_name, value)

    temp = "Organisation 3: Website"
    par_name = getMappedValue("Kontakt", get_second_part(temp))
    value = op.getValue(temp)
    o2i.setContactValue(contactObject, par_name, value)

    temp = "Organisation 3: E-Mail"
    par_name = getMappedValue("Kontakt", get_second_part(temp))
    value = op.getValue(temp)
    if value.startswith('mailto:'):     # Remove mailto: at the beginning
      value = value[len('mailto:'):]
    o2i.setContactValue(contactObject, par_name, value)

    temp = "Organisation 3: Kontaktformular (Link)"
    par_name = getMappedValue("Kontakt", get_second_part(temp))
    value = op.getValue(temp)
    o2i.setContactValue(contactObject, par_name, value)

    temp = "Organisation 3: Rufnummer"
    par_name = getMappedValue("Kontakt", get_second_part(temp))
    value = op.getValue(temp)
    o2i.setContactValue(contactObject, par_name, value)


  # Type
  type = op.getValue("Typ des Standards")
  if (mapped_type := getMappedValue("Type", type, True)) != False:
    type = mapped_type
  # print("  Type = " + type)
  o2i.setTag("type", "type", type)
  o2i.setTag("type", "value", type)


  # Beziehungskontext
  temp = op.getValue("Beziehungskontext")
  if temp != None:
    for value in temp:
      temp2 = getMappedValue("ContextRelation", value)
      o2i.setValue("contextRelation", temp2 , True)


  # Inhaltlicher Kontext
  temp = op.getValue("Inhaltlicher Kontext")
  if temp != None:
    for value in temp:
      if (temp2 := getMappedValue("ContextContent", value, True)) != False:
        o2i.setValue("contextContent", temp2 , True)


  # Finanzierung
  temp = op.getValue("Finanzierung")
  if temp != None:
    o2i.setValue("funder", temp)


  # Support 1
  temp = "Support 1: Bezeichnung"
  name = op.getValue(temp)
  temp = "Support 1: Link"
  url = op.getValue(temp)
  
  # Do we want to write data?
  if (name and url):
    # Yes!
    o2i.setValue("support", { "title": name, "url": url}, True)
  elif (name):
    print("")
    print(f"  Support 1: name set to '{name}', but no url found.")
    sys.exit(1)
  elif (url):
    print("")
    print(f"  Support 1: name not set, but found an url.")
    sys.exit(1)
  
  # If data has been found in Support 1, then look in Support 2 as well
  if (name):
    # Support 2
    temp = "Support 2: Bezeichnung"
    name = op.getValue(temp)
    temp = "Support 2: Link"
    url = op.getValue(temp)
    
    # Do we want to write data?
    if (name and url):
      # Yes!
      o2i.setValue("support", { "title": name, "url": url}, True)
    elif (name):
      print(f"  Support 2: name set to '{name}', but no url found.")
      sys.exit(1)
    elif (url):
      print(f"  Support 2: name not set, but found an url.")
      sys.exit(1)

  # If data has been found in Support 2, then look in Support 3 as well
  if (name):
    # Support 3
    temp = "Support 3: Bezeichnung"
    name = op.getValue(temp)
    temp = "Support 3: Link"
    url = op.getValue(temp)
    
    # Do we want to write data?
    if (name and url):
      # Yes!
      o2i.setValue("support", { "title": name, "url": url}, True)
    elif (name):
      print(f"  Support 3: name set to '{name}', but no url found.")
      sys.exit(1)
    elif (url):
      print(f"  Support 3: name not set, but found an url.")
      sys.exit(1)
    
  # If data has been found in Support 3, then look in Support 4 as well
  if (name):
    # Support 4
    temp = "Support 4: Bezeichnung"
    name = op.getValue(temp)
    temp = "Support 4: Link"
    url = op.getValue(temp)
    
    # Do we want to write data?
    if (name and url):
      # Yes!
      o2i.setValue("support", { "title": name, "url": url}, True)
    elif (name):
      print(f"  Support 4: name set to '{name}', but no url found.")
      sys.exit(1)
    elif (url):
      print(f"  Support 4: name not set, but found an url.")
      sys.exit(1)
    

  # Specification	    	
  temp = "Spezifikation: Link"
  url = op.getValue(temp)
  temp = "Spezifikation: Lizenz"
  license = op.getValue(temp)
  
  if (url):
    o2i.setArtefact("artefaktSpec", "url", url)
    if (license):
      o2i.setArtefact("artefaktSpec", "license", license)

  # Benutzerdokumentation
  artefact_infoplattform = "artefaktDocumentation"
  temp = "Benutzerdokumentation: Link"
  url = op.getValue(temp) if op.getValue(temp) != "" else "n.v."
  temp = "Benutzerdokumentation: Lizenz"
  license = op.getValue(temp)

  if (url):
    o2i.setArtefact(artefact_infoplattform, "url", url)
    if (license):
      o2i.setArtefact(artefact_infoplattform, "license", license)

  # Testumgebung
  artefact_infoplattform = "artefaktTestumgebung"
  temp = "Testumgebung: Link"
  url = op.getValue(temp) if op.getValue(temp) != "" else "n.v."
  temp = "Testumgebung: Lizenz"
  license = op.getValue(temp)

  if (url):
    o2i.setArtefact(artefact_infoplattform, "url", url)
    if (license):
      o2i.setArtefact(artefact_infoplattform, "license", license)
  elif (license):
    print(f"  {artefact_infoplattform}: url not set, but found a license!")
    sys.exit(1)

  # Referenzimplementierung
  artefact_infoplattform = "artefaktRefImplementation"
  temp = "Referenzimplementierung: Link"
  url = op.getValue(temp) if op.getValue(temp) != "" else "n.v."
  temp = "Referenzimplementierung: Lizenz"
  license = op.getValue(temp)

  if (url):
    o2i.setArtefact(artefact_infoplattform, "url", url)
    if (license):
      o2i.setArtefact(artefact_infoplattform, "license", license)
  elif (license):
    print(f"  {artefact_infoplattform}: url not set, but found a license!")
    sys.exit(1)
  
  # Betriebskonzept
  artefact_infoplattform = "artefaktBetriebskonzept"
  temp = "Betriebskonzept: Link"
  url = op.getValue(temp).strip()
  temp = "Betriebskonzept: Lizenz"
  license = op.getValue(temp)

  if (len(url)):
    o2i.setArtefact(artefact_infoplattform, "url", url)
    if (license):
      o2i.setArtefact(artefact_infoplattform, "license", license)
  elif (license):
    print("")
    print(f"  {artefact_infoplattform}: url not set, but found a license!")
    sys.exit(1)

  # Artefakt 1
  temp = "Artefakt 1: Bezeichnung"
  name = op.getValue(temp)
  temp = "Artefakt 1: Link"	
  url = op.getValue(temp)
  temp = "Artefakt 1: Lizenz"
  license = op.getValue(temp)

  if (name and url):
    artefact_infoplattform = name
    o2i.setArtefact(artefact_infoplattform, "name", name, True)
    o2i.setArtefact(artefact_infoplattform, "url", url, True)
    o2i.setArtefact(artefact_infoplattform, "license", license, True)
  elif (url):
    print("")
    print(f"  Artefakt 1: found url, but name not set!")
    sys.exit(1)
  elif (name):
    print("")
    print(f"  Artefakt 1: found name, but link not set!")
    sys.exit(1)
  

  # Artefakt 2
  temp = "Artefakt 2: Bezeichnung"
  name = op.getValue(temp)
  temp = "Artefakt 2: Link"	
  url = op.getValue(temp)
  temp = "Artefakt 2: Lizenz"
  license = op.getValue(temp)

  if (name and url):
    artefact_infoplattform = name
    o2i.setArtefact(artefact_infoplattform, "name", name, True)
    o2i.setArtefact(artefact_infoplattform, "url", url, True)
    o2i.setArtefact(artefact_infoplattform, "license", license, True)
  elif (url):
    print("")
    print(f"  Artefakt 2: found url, but name not set!")
    sys.exit(1)
  elif (name):
    print("")
    print(f"  Artefakt 2: found name, but link not set!")
    sys.exit(1)

  # Artefakt 3
  temp = "Artefakt 3: Bezeichnung"
  name = op.getValue(temp)
  temp = "Artefakt 3: Link"	
  url = op.getValue(temp)
  temp = "Artefakt 3: Lizenz"
  license = op.getValue(temp)

  if (name and url):
    artefact_infoplattform = name
    o2i.setArtefact(artefact_infoplattform, "name", name, True)
    o2i.setArtefact(artefact_infoplattform, "url", url, True)
    o2i.setArtefact(artefact_infoplattform, "license", license, True)
  elif (url):
    print("")
    print(f"  Artefakt 3: found url, but name not set!")
    sys.exit(1)
  elif (name):
    print("")
    print(f"  Artefakt 3: found name, but link not set!")
    sys.exit(1)


  # Framework
  temp = "Framework"
  framework_id = op.getValue(temp)
  
  # Do we want to write data?
  if (framework_id):
    # Yes!
    o2i.setValue("framework", {"id": framework_id})

  # Liability 1

  temp = "Gültigkeit 1: Verbindlichkeit"
  liability = op.getValue(temp)
  temp = "Gültigkeit 1: Gültig ab/seit"	
  date = op.getValue(temp)
  temp ="Gültigkeit 1: Link zum Beschluss"
  url = op.getValue(temp)
  
  # Do we want to write data?
  if (liability):
    # Yes!
    o2i.setValue("liability", { "liability": liability, "date": date, "url": url}, True)
  elif (url):
    print("")
    print(f"  Gültigkeit 1: Verbindlichkeit: liability not set, but found url.")
    sys.exit(1)

  # Liability 2
  if (liability):
    temp = "Gültigkeit 2: Verbindlichkeit"
    liability2 = op.getValue(temp)
    temp = "Gültigkeit 2: Gültig ab/seit"	
    date = op.getValue(temp)
    temp ="Gültigkeit 2: Link zum Beschluss"
    url = op.getValue(temp)
  
    # Do we want to write data?
    if (liability2):
      # Yes!
      o2i.setValue("liability", { "liability": liability2, "date": date, "url": url}, True)
    elif (url):
      print("")
      print(f"  Gültigkeit 2: Verbindlichkeit: liability not set, but found url.")
      sys.exit(1)

  # Handle related FIT-Standards
  childResources = op.getChildResources()
  # print(f"childresources = '{childResources}'")

  if (childResources):
    o2i.setValue("slugMaster", childResources[0]['slug'])
    o2i.setTag("slug_master", "slug", childResources[0]['slug'])
    o2i.setTag("slug_master", "value", childResources[0]['slug'])
    o2i.setValue("childResources", childResources)

  # Final checks & warning messages
  messages = [] # We start with no message at all

  # Check for liablity = not defined and status = in operation
  if liability == "Nicht definiert" and status == "regelbetrieb":
    messages.append(
        { 
          'type': 'WARNING', 
          'msg': "Liability is 'not defined', but status is 'regelbetrieb'",
        }
    )

  # Output messages, if any
  if len(messages):
    for message in messages:
       if message['type'] == 'WARNING':
          print(f"  {YELLOW}{message['type']}: ", end= '')
       elif message['type'] == 'ERROR':
          print(f"  {RED}{message['type']}: ", end= '')
       print(f"{message['msg']}{RESET}")
    print("")
  else:
    print("  O.K.\n")


# Get the converted data
ipData = o2i.getInfoPlattformObjects()

try:
    # Store converted data in JSON file
    with open(filename, "w", encoding='utf-8') as out_file:
        json.dump(ipData, out_file, ensure_ascii=False, indent=2)

    # Get the number of elements in the list
    num_elements = len(ipData)
    print(f"{num_elements} FIT-Standards exported successfully to '{filename}'.\n")
    
except Exception as e:
    print(f"An error occurred while writing to {filename}: {e}")    	
