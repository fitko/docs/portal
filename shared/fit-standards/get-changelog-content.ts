/*
 * Reads the content from the markdown file /content/fit-standards/bedienungshilfe.md
 * and returns it as HTML string
 */

import fs from 'fs'
import path from 'path'
import { remark } from 'remark'
import html from 'remark-html'

export default function loadAndPrepareMarkdown() {
    const markdownFileContent: string = loadMarkdown()
    const content: string = prepareContentFromMarkdown(markdownFileContent)
    return content
}

function loadMarkdown() {
    const fullPath: string = path.join(process.cwd(), 'content/fit-standards/changelog.md')
    const markdownContent: string = fs.readFileSync(fullPath, 'utf8')
    return markdownContent
}

function prepareContentFromMarkdown(markdownContent: string) {
    // eslint-disable-next-line no-unused-vars
    const [header, metadata, text] = markdownContent.replace(/\r\n/g, '\n').split('---')
    return parseTextToHTMLString(text)
}

function parseTextToHTMLString(markdownText: string) {
    const sanitizedMarkdownText: string = sanitizeMarkdownText(markdownText)
    const processedContent = remark().use(html, { sanitize: false }).processSync(sanitizedMarkdownText)
    const htmlContent: string = processedContent.toString()

    // Add 'rel="noopener" target="_blank"' to external links
    // return htmlContent
    return htmlContent.replaceAll(/<a href="http(\S+)">/mg, '<a href="http$1" rel="noopener" target="_blank">')
}

function sanitizeMarkdownText(markdownText: string) {
    // trim whitespaces; replace line breaks with and remove multiple whitespaces
    return markdownText.trim().replace(/ +(?= )/g, ' ')
}
