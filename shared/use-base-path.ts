import { useState } from 'react'

export default function useBasePath() {
    const [basePath] = useState(process.env.NEXT_PUBLIC_BASE_PATH ? process.env.NEXT_PUBLIC_BASE_PATH : '')

    const localizePath = (path, language?) => {
        const normalizedPath = path.startsWith('/') ? path : `/${path}`
        if (!language || language === 'de') {
            return removeDuplicateSlashes(`/${normalizedPath}`)
        }
        return removeDuplicateSlashes(`/${language}${normalizedPath}`)
    }

    const relocalizePath = (path, language) => {
        const cleanPath = path.split(/[?#]/)[0]
        const pathWithoutLang = cleanPath.replace(/^\/(de|en)/, '')

        if (!language || language === 'de') {
            return removeDuplicateSlashes(pathWithoutLang ? `${basePath}${pathWithoutLang}` : `${basePath}/${pathWithoutLang}`)
        } else {
            const newPath = `/${language}${pathWithoutLang.startsWith('/') ? '' : '/'}${pathWithoutLang}`
            return removeDuplicateSlashes(`${basePath}${newPath}`)
        }
    }

    const removeDuplicateSlashes = (path) => {
        return path.replace(/\/{2,}/g, '/')
    }

    const getLocaleFromPath = (path) => {
        const pathSegments = path.split('/')
        const potentialLocale = pathSegments[1]

        const supportedLocales = ['de', 'en']
        if (supportedLocales.includes(potentialLocale)) {
            return potentialLocale
        }

        return 'de'
    }

    const getImgPath = (imgResource): string => {
        return `${basePath}/img/${imgResource}`
    }

    return { basePath, localizePath, relocalizePath, getLocaleFromPath, getImgPath }
}
