import { ResourceContent } from 'types/ResourceContent'
import { loadAndPrepareMarkdown, parseMarkdown } from './use-markdown-parser'
import { loadAndPrepareJson } from './use-json-parser'
import fitStandardsData from '../content/fit-standards/from_openproject.json'
import fs from 'fs'
import path from 'path'

const DEFAULT_LANGUAGE = 'de'

export const useResourceContent = (directory = 'resources', lang: string = DEFAULT_LANGUAGE) => {
    const contents: ResourceContent[] = (directory == 'resources'
        ? readMarkdownFilesFromResources(directory, lang)
        : readMarkdownFilesFromResources(directory, lang)
    )

    const contentByResourceName = (selectedResourceName: string, lang: string = DEFAULT_LANGUAGE) => {
        const content: ResourceContent = (directory == 'resources'
            ? loadAndPrepareMarkdown(selectedResourceName, directory, lang)
            : loadAndPrepareJson(selectedResourceName, directory, lang)
        )
        return content
    }

    return {
        contents,
        contentByResourceName,
    }
}

export const useLandingPageContent = (lang: string = DEFAULT_LANGUAGE) => {
    const blogContent = loadContentFromDirectory('landingpage/blog', lang)
    const modulesContent = loadContentFromDirectory('landingpage/modules', lang)

    return {
        blog: blogContent,
        modules: modulesContent,
    }
}

const loadContentFromDirectory = (directoryPath: string, lang: string = DEFAULT_LANGUAGE) => {
    const fullPath = path.join(process.cwd(), 'content', directoryPath)
    const filenames = fs.readdirSync(fullPath)

    const contentObj = {}

    filenames.forEach((filename) => {
        const languageSuffix = `.${lang}.md`
        if (filename.endsWith(languageSuffix) && filename.endsWith('.md')) {
            const filePath = path.join(fullPath, filename)
            const fileContents = fs.readFileSync(filePath, 'utf8')
            const slug = filename.replace(/\.md$/, '').replace(languageSuffix, '')
            contentObj[slug] = parseMarkdown(fileContents)
        }
    })

    return contentObj
}

function readMarkdownFilesFromResources(directory: string = 'resources', lang: string): ResourceContent[] {
    const resourcesDirectory = path.join(process.cwd(), 'content', directory)
    if (directory == 'fit-standards') {
        // return loadAndPrepareJson(resourcesDirectory, lang)
        const fitStandards: object[] = fitStandardsData
        const serviceDirectories: string[] = fitStandards.map((standard: any) => standard.slug)

        return serviceDirectories.map((serviceDirectory: string) =>
            loadAndPrepareJson(serviceDirectory, directory, lang)
        )
    } else {
        const serviceDirectories: string[] = fs.readdirSync(resourcesDirectory, { withFileTypes: true })
            .filter((serviceDirectory: fs.Dirent) => serviceDirectory.isDirectory())
            .map((serviceDirectory: fs.Dirent) => serviceDirectory.name)

        return serviceDirectories.map((serviceDirectory: string) => {
            return loadAndPrepareMarkdown(serviceDirectory, directory, lang)
        })
    }
}

export default { useResourceContent, useLandingPageContent }
