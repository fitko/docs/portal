export default function useImgPath() {
    const gitBranch = process.env.CURRENT_BRANCH || 'main'
    const baseUrl = `https://gitlab.opencode.de/-/ide/project/fitko/docs/portal/edit/${gitBranch}/-/`

    function getContentEditUrl(resourceSlug: string, language: string = 'de'):string {
        const contentDirPath = 'content/resources'
        return `${baseUrl}${contentDirPath}/${resourceSlug}/${resourceSlug}.${language}.md`
    }

    function getPageEditUrl(pagePath: string):string {
        const pagesDirPath = 'pages'

        // Home page
        if (pagePath === '/') {
            return `${baseUrl}${pagesDirPath}/index.tsx`
        }

        // Other pages
        return `${baseUrl}${pagesDirPath}${pagePath}/index.tsx`
    }

    return { getContentEditUrl, getPageEditUrl }
}
