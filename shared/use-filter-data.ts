import filterDataDe from '../public/locales/de/currentFilter.json'
import filterDataEn from '../public/locales/en/currentFilter.json'

const DEFAULT_LANGUAGE = 'de'

export const useFilterData = (lang: string = DEFAULT_LANGUAGE) => {
    let filterData
    switch (lang) {
    case 'en':
        filterData = filterDataEn
        break
    case 'de':
        filterData = filterDataDe
        break
    default:
        filterData = filterDataDe
    }
    return filterData
}

export default useFilterData
