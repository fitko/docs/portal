export default function useImgPath() {
    const basePath = process.env.NEXT_PUBLIC_BASE_PATH ? process.env.NEXT_PUBLIC_BASE_PATH : ''
    const absoluteUrlPattern = /^https?:\/\//i

    function getImgPath(fileName: string): string {
        if (fileName == '') {
            return ''
        } else if (absoluteUrlPattern.test(fileName)) {
            return fileName
        } else {
            const imgPath = basePath + fileName
            return imgPath.replace('public', '')
        }
    }

    return { getImgPath }
}
