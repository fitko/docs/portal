import { ResourceContent } from 'types/ResourceContent'
import fs from 'fs'
import path from 'path'
import { remark } from 'remark'
import html from 'remark-html'
import { parse } from 'yaml'
import { LabelTag, StatusTag, Tag, TypeTag, SlugMasterTag } from 'types/content/'
import fitStandardsData from '../content/fit-standards/from_openproject.json'

const DEFAULT_LANGUAGE = 'de'

export function loadAndPrepareJson(selectedResourceName: string, directory = 'resources', lang: string = DEFAULT_LANGUAGE): ResourceContent {
    const jsonFileContent: string = loadJson(selectedResourceName, directory, lang)
    const content: ResourceContent = prepareContentFromJson(jsonFileContent, selectedResourceName)
    return content
}

function loadJson(selectedResourceName: string, directory: string, lang: string): string {
    // Using find to get the object with the matching slug
    const fitStandards: object[] = fitStandardsData
    const jsonObject = fitStandards.find((standard: { slug: string }) => standard.slug === selectedResourceName)

    // Safely handle the case where the object is not found
    if (jsonObject) {
        return JSON.stringify(jsonObject)
    } else {
        console.warn(`No matching standard found for slug: ${selectedResourceName}`)
        return JSON.stringify({ error: 'Resource not found' })
    }
}

function prepareContentFromJson(jsonContent: string, selectedResourceName: string): ResourceContent {
    const jsonObj = JSON.parse(jsonContent)
    const content: ResourceContent = parseMetadata(jsonContent)
    content.slug = selectedResourceName
    content.text = parseTextToHTMLString(jsonObj.text)
    return content
}

function parseTextToHTMLString(markdownText: string): string {
    const sanitizedMarkdownText: string = sanitizeMarkdownText(markdownText)
    const processedContent = remark().use(html).processSync(sanitizedMarkdownText)
    const htmlContent: string = processedContent.toString()

    // Add 'rel="noopener" target="_blank"' to external links
    // return htmlContent
    return htmlContent.replaceAll(/<a href="http(\S+)">/mg, '<a href="http$1" rel="noopener" target="_blank">')
}

function parseMetadata(metadata: string): ResourceContent {
    const contentData: any = parse(metadata)
    const content: ResourceContent = new ResourceContent(contentData)
    if (contentData.tags.length) {
        content.tags = contentData.tags.map((tagData: any) => createTag(tagData))
    }
    return content
}

function createTag(tagData): Tag {
    let key = ''
    let value = ''
    if (typeof tagData === 'string') {
        [key, value] = tagData.split(':')
    } else {
        key = tagData.name
        value = tagData.value
    }
    if (key.includes('type')) {
        return new TypeTag(value)
    } else if (key.includes('status')) {
        return new StatusTag(value)
    } else if (key.includes('label')) {
        return new LabelTag(value)
    } else if (key.includes('slug_master')) {
        return new SlugMasterTag(value)
    }
}

function sanitizeMarkdownText(markdownText: string): string {
    // Remove Javascript comment
    markdownText = markdownText.replace(/\/\*[\s\S]*\*\//g, '')
    return markdownText.trim().replace(/ +(?= )/g, ' ')
}

export function parseJson(jsonContent) {
    const contentData = jsonContent
    const htmlContent = jsonContent.text

    return {
        ...contentData,
        content: htmlContent,
    }
}
export default { loadAndPrepareJson, parseJson }
