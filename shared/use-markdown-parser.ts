import { ResourceContent } from 'types/ResourceContent'
import fs from 'fs'
import path from 'path'
import { remark } from 'remark'
import html from 'remark-html'
import { parse } from 'yaml'
import { LabelTag, StatusTag, Tag, TypeTag, SlugMasterTag, Logo } from 'types/content/'

const DEFAULT_LANGUAGE = 'de'

export function loadAndPrepareMarkdown(selectedResourceName: string, directory = 'resources', lang: string = DEFAULT_LANGUAGE): ResourceContent {
    const markdownFileContent: string = loadMarkdown(selectedResourceName, directory, lang)
    const content: ResourceContent = prepareContentFromMarkdown(markdownFileContent, selectedResourceName, directory, lang)
    return content
}

function loadMarkdown(selectedResourceName: string, directory: string, lang: string): string {
    const fullPath: string = getMarkdownFilePath(selectedResourceName, directory, lang)
    const markdownContent: string = fs.readFileSync(fullPath, 'utf8')
    return markdownContent
}

function getLogoFromSlug(slug: string, directory: string, lang: string): Logo {
    const markdownFileContent: string = loadMarkdown(slug, directory, lang)
    const [metadata, _] = markdownFileContent.split('---').slice(1)
    const parsedMetadata: ResourceContent = parseMetadata(metadata)
    return parsedMetadata.logo
}

function prepareContentFromMarkdown(markdownContent: string, selectedResourceName: string, directory, lang): ResourceContent {
    const [metadata, text] = markdownContent.split('---').slice(1)
    const content: ResourceContent = parseMetadata(metadata)
    content.slug = selectedResourceName
    content.text = parseTextToHTMLString(text)

    // add logos for childResources
    if (content.childResources && content.childResources.length > 0) {
        content.childResources.forEach(child => {
            child.logo = getLogoFromSlug(child.slug, directory, lang)
        });
    }
    return content
}

function getMarkdownFilePath(selectedResourceName: string, directory: string, lang: string = DEFAULT_LANGUAGE): string {
    const baseFilePath: string = path.join(process.cwd(), 'content', directory, selectedResourceName)
    const localizedFilePath = path.join(baseFilePath, `${selectedResourceName}.${lang}.md`)
    if (fs.existsSync(localizedFilePath)) {
        return localizedFilePath
    }
    const defaultFilePath = path.join(baseFilePath, `${selectedResourceName}.${DEFAULT_LANGUAGE}.md`)
    return defaultFilePath
}

function parseTextToHTMLString(markdownText: string): string {
    const sanitizedMarkdownText: string = sanitizeMarkdownText(markdownText)
    const processedContent = remark().use(html).processSync(sanitizedMarkdownText)
    const htmlContent: string = processedContent.toString()

    // Add 'rel="noopener" target="_blank"' to external links
    // return htmlContent
    return htmlContent.replaceAll(/<a href="http(\S+)">/mg, '<a href="http$1" rel="noopener" target="_blank">')
}

function parseMetadata(metadata: string): ResourceContent {
    const contentData: any = parse(metadata)
    const content: ResourceContent = new ResourceContent(contentData)
    if (contentData.tags.length) {
        content.tags = contentData.tags.map((tagData: any) => createTag(tagData))
    }
    return content
}

function createTag(tagData): Tag {
    let key = ''
    let value = ''
    if (typeof tagData === 'string') {
        [key, value] = tagData.split(':')
    } else {
        key = tagData.name
        value = tagData.value
    }
    if (key.includes('type')) {
        return new TypeTag(value)
    } else if (key.includes('status')) {
        return new StatusTag(value)
    } else if (key.includes('label')) {
        return new LabelTag(value)
    } else if (key.includes('slug_master')) {
        return new SlugMasterTag(value)
    }
}

function sanitizeMarkdownText(markdownText: string): string {
    // Remove Javascript comment
    markdownText = markdownText.replace(/\/\*[\s\S]*\*\//g, '')
    return markdownText.trim().replace(/ +(?= )/g, ' ')
}

export function parseMarkdown(markdownContent) {
    const [metadata, content] = markdownContent.split('---').slice(1)
    const contentData = parse(metadata)
    const htmlContent = remark().use(html).processSync(content).toString()

    return {
        ...contentData,
        content: htmlContent,
    }
}
export default { loadAndPrepareMarkdown, parseMarkdown }
