import { Tag } from 'types/content'
import translationsDe from '../public/locales/de/common.json'
import translationsEn from '../public/locales/en/common.json'

import { TranslationSegment } from '../types/TranslationSegment'
import useFilterData from './use-filter-data'

export const useTranslation = (currentLanguage = 'de') => {
    const TRANSLATION_NOT_SET = ''
    const filterData = useFilterData(currentLanguage)?.filter

    let translations
    switch (currentLanguage) {
    case 'en':
        translations = translationsEn
        break
    case 'de':
    default:
        translations = translationsDe
    }
    const getTranslationText = (translationKey: string): string => {
        const keys = translationKey.split('.')
        let translationText: any = translations

        for (const key of keys) {
            translationText = translationText?.[key] ?? translationKey
        }

        return translationText
    }

    const translate = (translationKey: string, substitutions?: Record<string, string>): string => {
        let translation = getTranslationText(translationKey)

        if (translation === translationKey) {
            console.warn(`Warning: i18n - Translation ${translationKey} not found.`)
        } else {
            if (substitutions) {
                Object.keys(substitutions).forEach(substitution => {
                    const regex = new RegExp(`{${substitution}}`, 'g')
                    translation = translation.replace(regex, substitutions[substitution])
                })
            }
            translation = translation.replace(/{\w+}/g, TRANSLATION_NOT_SET)
        }

        return translation
    }

    const translateStrongFormatted = (translationKey: string, substitutions?: Record<string, string>): string => {
        let translation = getTranslationText(translationKey)

        if (substitutions) {
            Object.keys(substitutions).forEach(key => {
                const value = substitutions[key]
                translation = translation.replace(`{${key}}`, `<strong>${value}</strong>`)
            })
        }

        return translation
    }

    const translateToArray = (translationKey: string, substitutions?: Record<string, string>): (string)[] => {
        const translation: string = getTranslationText(translationKey)

        if (substitutions == undefined) {
            return [translation]
        }

        if (!Object.keys(substitutions).length) {
            return [translation]
        }

        const regex = new RegExp(`{(${Object.keys(substitutions).join('|')})}`, 'g')
        const translationParts = translation.split(regex)

        return translationParts.map((translationPart) => {
            if (translationPart in substitutions) {
                return substitutions[translationPart]
            }
            return translationPart
        })
    }

    const translateSegmented = (translationKey: string, substitutions?: Record<string, string>): TranslationSegment => {
        if (substitutions && Object.keys(substitutions).length == 1) {
            const [prefix = '', substitution = '', suffix = ''] = translateToArray(translationKey, substitutions)
            return new TranslationSegment(prefix, substitution, suffix)
        } else {
            console.warn('TranslationSegment unterstützt nur eine Substitution.')
        }
        return new TranslationSegment()
    }

    const translateLabel = (label: string): string => {
        const filterCategoriesForTranslation = filterData.filter(filterCategory => {
            return filterCategory.id == label
        })
        return filterCategoriesForTranslation.length > 0 ? filterCategoriesForTranslation[0].label : ''
    }

    const translateTag = (tag: Tag): string => {
        const filterCategoriesForTranslation = filterData.filter(filterCategory => {
            return filterCategory.id == tag.name
        })
        if (filterCategoriesForTranslation.length > 0) {
            const filterCategory = filterCategoriesForTranslation[0]
            return translateLabelValue(filterCategory.options, tag.value)
        }
        return ''
    }

    const translateLabelValue = (options, labelValue) => {
        const labelOption = options.filter(option => option.id === labelValue)
        return labelOption.length > 0 ? labelOption[0].label : ''
    }

    const translateStatusLableValue = (labelValue: string): string => {
        const statusLabels = filterData.filter(filter => filter.label === 'Status')[0]
        return translateLabelValue(statusLabels.options, labelValue)
    }

    return { translate, translateToArray, translateStrongFormatted, translateSegmented, translateLabel, translateTag, translateStatusLableValue }
}

export default useTranslation
