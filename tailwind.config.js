module.exports = {
    mode: 'jit',
    content: [
        './pages/**/*.{js,ts,jsx,tsx}',
        './components/**/*.{js,ts,jsx,tsx}',
        './views/**/*.{js,jsx,ts,tsx}',
        './lib/constants-standards.js',
    ],
    safelist: [
    ],
    theme: {
        extend: {
            colors: {
                yellow: {
                    400: '#FFC819',
                },
                // IT-PLR colors
                'itplr_blau-1': '#003B5C',
                'itplr_blau-2': '#1A658F',
                'itplr_blau-3': '#0092CB',
                'itplr_blau-4': '#56B7E6',
                'itplr_blau-5': '#ABDBF3',
                'itplr_blau-6': '#D5EDF9',
                'itplr_grau-1': '#BCBCBC',
                'itplr_grau-2': '#CACACA',
                'itplr_grau-3': '#D6D6D6',
                'itplr_grau-4': '#E3E3E3',
                'itplr_senf': '#E4C166',
                'itplr_malve': '#B5838D',
                'itplr_opal': '#52796F',
                'itplr_muskat': '#A9927D',
                'itplr_ampel_rot': '#EF233C',
                'itplr_ampel_gelb': '#FCBF49',
                'itplr_ampel_gruen': '#90BE6D',
            },
            borderColor: theme => ({
                ...theme('colors')
            }),

            boxShadow: {
                xs: '0 0 0 1px rgba(0, 0, 0, 0.16)',
                sm: '0 1px 2px 0 rgba(0, 0, 0, 0.16)',
                default:
                    '0 1px 3px 0 rgba(0, 0, 0, 0.12), 0 1px 2px 0 rgba(0, 0, 0, 0.03)',
                md: '0 4px 6px -1px rgba(0, 0, 0, 0.04), 0 2px 4px -1px rgba(0, 0, 0, 0.03)',
                lg: '0 10px 15px -3px rgba(0, 0, 0, 0.04), 0 4px 6px -2px rgba(0, 0, 0, 0.02)',
                xl: '0 20px 25px -5px rgba(0, 0, 0, 0.12), 0 10px 10px -5px rgba(0, 0, 0, 0.02)',
                '2xl': '0 25px 50px -12px rgba(0, 0, 0, 0.15)',
                inner: 'inset 0 2px 4px 0 rgba(0, 0, 0, 0.04)',
                outline: '0 0 0 3px rgba(66, 153, 225, 0.5)',
                none: 'none',
            },
            spacing: {
                '9/16': '56.25%',
                '3/4': '75%',
                '1/1': '100%',
            },
            fontFamily: {
                montserrat: ['Montserrat', 'sans-serif'],
            },
            fontSize: {
                xs: '0.75rem',
                sm: '0.875rem',
                base: '1rem',
                lg: '1.125rem',
                xl: '1.25rem',
                '2xl': '1.5rem',
                '3xl': '1.75rem',
                '4xl': '2.5rem',
                '5xl': '3.25rem',
                '6xl': '5.5rem',
            },
            inset: {
                '1/2': '50%',
                full: '100%',
            },
            letterSpacing: {
                tighter: '-0.02em',
                tight: '-0.01em',
                normal: '0',
                wide: '0.01em',
                wider: '0.02em',
                widest: '0.4em',
            },
            lineHeight: {
                none: '1',
                tighter: '1.125',
                tight: '1.25',
                snug: '1.375',
                normal: '1.5',
                relaxed: '1.625',
                loose: '2',
                3: '.75rem',
                4: '1rem',
                5: '1.2rem',
                6: '1.5rem',
                7: '1.75rem',
                8: '2rem',
                9: '2.25rem',
                10: '2.5rem',
            },
            minWidth: {
                10: '2.5rem',
                48: '12rem',
            },
            opacity: {
                90: '0.9',
            },
            scale: {
                98: '.98',
            },
            backgroundImage: {
                'banner-texture': `url('${process.env.NEXT_PUBLIC_BASE_PATH ? process.env.NEXT_PUBLIC_BASE_PATH : ''}/img/connectwork.png')`,
            },
            customForms: (theme) => ({
                default: {
                    'input, textarea, multiselect, select, checkbox, radio': {
                        backgroundColor: theme('colors.white'),
                        borderColor: theme('colors.gray.300'),
                        borderRadius: theme('borderRadius.default'),
                        '&:focus': {
                            outline: undefined,
                            boxShadow: undefined,
                            borderColor: theme('colors.gray.500'),
                        },
                    },
                    'input, textarea, multiselect, select': {
                        backgroundColor: theme('colors.white'),
                        fontSize: undefined,
                        lineHeight: undefined,
                        paddingTop: theme('spacing.3'),
                        paddingRight: theme('spacing.4'),
                        paddingBottom: theme('spacing.3'),
                        paddingLeft: theme('spacing.4'),
                    },
                    'input, textarea': {
                        '&::placeholder': {
                            color: theme('colors.gray.500'),
                        },
                    },
                    select: {
                        paddingRight: theme('spacing.10'),
                        iconColor: theme('colors.gray.400'),
                    },
                    'checkbox, radio': {
                        color: theme('colors.gray.800'),
                        backgroundColor: theme('colors.white'),
                        borderRadius: theme('borderRadius.sm'),
                    },
                },
            }),
        },
    },
    plugins: [
        require('@tailwindcss/typography'),
        require('@tailwindcss/forms'),
    ],
}
