import { ChildResource, ProductOwner, ContactInformation, Developer, SteeringCommittee, 
          Initiator, Framework, Operator, Context, Tag, Logo, Liability } from './content'
import { LabelTag, StatusTag, TypeTag, SlugMasterTag } from 'types/content/'
import { LinkItem } from './content/LinkItem'
import { Funder } from './content/Funder'

function removeDelimiter(text: string) :string {
    if (Array.from(text)[0] === "'" && text.slice(-1) === "'") {
        return text.slice(1, -1)
    }
    return text
}
class ResourceContent {
    funder: Funder = new Funder() 
    initiator: Initiator = new Initiator()
    productOwner: ProductOwner = new ProductOwner()
    steeringCommittee: SteeringCommittee = new SteeringCommittee()
    developer: Developer = new Developer()
    operator: Operator = new Operator()
    contactInformation: ContactInformation = new ContactInformation()
    docsUrl: string = ''
    apiSpecUrl: string = ''
    name: string = ''
    shortDescription: string = ''
    sourceCodeUrl: string = ''
    status: string = ''
    contextRelation: string[] = []
    contextContent: string[] = []
    tags: Tag[] = []
    initialized: boolean = false
    text: string = ''
    childResources: ChildResource[] = []
    support: LinkItem[] = []
    framework: Framework
    liability: Liability[] = []
    artefacts: LinkItem[] = []
    slugMaster = ''
    logo: Logo = new Logo()
    slug: string = ''
    dateLastUpdate: Date

    artefactNames: string[] = [
        'artefaktSpec',
        'artefaktDocumentation',
        'artefaktTestumgebung',
        'artefaktRefImplementation',
        'artefaktBetriebskonzept',
    ]

    constructor(newContent?: any) {
        if (newContent) {
            this.initialize(newContent)
            this.initialized = true
        }
    }

    getPropertyValue(obj1: any, dataToRetrieve: string): any {
        return dataToRetrieve
            .split('.') // split string based on `.`
            .reduce(function (object, value) {
                return object && object[value] // get inner property if `o` is defined else get `o` and return
            }, obj1) // set initial value as object
    }

    initialize(newContent: ResourceContent) {
        if (typeof newContent.funder === 'string') {
          this.funder.organisation = removeDelimiter(newContent.funder)
        } else {
            this.funder = newContent.funder || new Funder()
        }
        this.productOwner = newContent.productOwner || new ProductOwner()
        this.contactInformation = newContent.contactInformation || new ContactInformation()
        this.developer = newContent.developer || new Developer()
        this.operator = newContent.operator || new Operator()
        this.steeringCommittee = newContent.steeringCommittee || new SteeringCommittee()
        this.initiator = newContent.initiator || new Initiator()
        this.docsUrl = newContent.docsUrl || ''
        this.apiSpecUrl = newContent.apiSpecUrl || ''
        this.name = newContent.name || ''
        this.shortDescription = newContent.shortDescription || ''
        this.sourceCodeUrl = newContent.sourceCodeUrl || ''
        this.contextRelation = Array.isArray(newContent.contextRelation) 
        ? newContent.contextRelation.filter(item => item !== 'undefined')
        : []
        this.contextContent = Array.isArray(newContent.contextContent)
        ? newContent.contextContent.filter(item => item !== 'undefined')
        : []
        this.tags = newContent.tags || []
        this.text = newContent.text || ''
        this.childResources = newContent.childResources || []
        this.slugMaster = newContent.slugMaster
        this.support = newContent.support || []
        this.framework = newContent.framework || undefined
        this.liability = newContent.liability || undefined
        this.artefacts = newContent.artefacts || []
        this.slug = newContent.slug || ''
        this.logo = newContent.logo || new Logo()

        const statusTags:any = this.tags?.filter((tag: Tag) => tag.name == 'status')
        if (statusTags.length > 0) {
            this.status = statusTags[0].status
        }

        // Check child resources
        if (this.childResources.length == 1 &&
            this.childResources[0].name == null &&
            this.childResources[0].slug == null) {
            this.childResources = []
        }

        // Check logo
        if (this.logo.path == null && this.logo.title == null) {
            this.logo = new Logo()
        }

        // Make tags work
        let delIndex = 0
        for (let index = 0; index < this.tags.length; index++) {
            let key: string, value: string
            if (typeof this.tags[delIndex] !== 'string') {
                key = Object.keys(this.tags[delIndex])[0]
                value = Object.values(this.tags[delIndex])[0] == null ? '' : Object.values(this.tags[delIndex])[0]
            } else {
                [key, value] = String(this.tags[delIndex]).split(':')
            }
            if (key.includes('type')) {
                this.tags.push(new TypeTag(value))
                this.tags.splice(delIndex, 1)
            } else if (key.includes('status')) {
                this.tags.push(new StatusTag(value))
                this.status = value
                this.tags.splice(delIndex, 1)
            } else if (key.includes('label')) {
                this.tags.push(new LabelTag(value))
                this.tags.splice(delIndex, 1)
            } else if (key.includes('slugMaster')) {
                this.tags.push(new SlugMasterTag(value))
                this.slugMaster = value
                this.tags.splice(delIndex, 1)
            } else {
                delIndex++
            }
        }

        // Make artefacts work
        this.artefactNames.forEach(artefact => {
            const value = this.getPropertyValue(newContent, artefact)
            if (typeof value === 'object' && value !== null) {
                let myUrl = this.getPropertyValue(value, 'url')
                if (myUrl == null) {
                    myUrl = ''
                }
                let myLicense = this.getPropertyValue(value, 'license')
                if (myLicense == null) {
                    myLicense = ''
                }
                this.artefacts.push(new LinkItem({ name: artefact, url: myUrl, license: myLicense }))
            }
        })

        // Now add user defined artefacts
        if (Object.hasOwn(newContent, 'artefaktUserDefined')) {
            const userDefinedArtefacts = this.getPropertyValue(newContent, 'artefaktUserDefined')
            userDefinedArtefacts.forEach(element => {
                if (element.name != null &&
                    element.name != '' &&
                    element.url != null &&
                    element.url != '') {
                    this.artefacts.push(new LinkItem({ name: element.name, url: element.url, license: element.license }))
                }
            })
        }

        if (newContent.dateLastUpdate !== undefined) {
            this.dateLastUpdate = new Date(newContent.dateLastUpdate)
        } else {
            this.dateLastUpdate = undefined
        }

        this.shortDescription = removeDelimiter(this.shortDescription)
        if (this.productOwner.organisation) {
            this.productOwner.organisation = removeDelimiter(this.productOwner.organisation)
        }
        if (this.contactInformation.organisation) {
            this.contactInformation.organisation = removeDelimiter(this.contactInformation.organisation)
        }
        if (this.developer.organisation) {
            this.developer.organisation = removeDelimiter(this.developer.organisation)
        }
    }
}
export { ResourceContent }
