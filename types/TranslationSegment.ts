class TranslationSegment {
    prefix: string
    substitution: string
    suffix: string

    constructor(prefix: string = '', substitution: string = '', suffix: string = '') {
        this.prefix = prefix
        this.substitution = substitution
        this.suffix = suffix
    }

    getPrefix() {
        return this.prefix
    }

    getSubstitution() {
        return this.substitution
    }

    getSuffix() {
        return this.suffix
    }
}

export { TranslationSegment }
