export abstract class BaseContact {
  name: string = '';
  organisation: string = '';
  url: string = '';
  mail: string = '';
  phone: string = '';
  contactform: string = '';

  constructor(data?: Partial<BaseContact>) {
      if (data) {
          this.initialize(data);
      }
  }

  protected initialize(data: Partial<BaseContact>): void {
      Object.assign(this, data);
  }

  public get initialized(): boolean {
      return (this.name !== '' || this.organisation !== '') && this.url !== '';
  }
}