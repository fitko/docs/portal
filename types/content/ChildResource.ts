import { Logo } from '.'

export class ChildResource {
    name: string = ''
    slug: string = ''
    logo: Logo

    constructor(childResource?: any) {
        if (childResource) {
            this.initialize(childResource)
        }
    }

    initialize(childResource: ChildResource) {
        this.name = childResource.name
        this.slug = childResource.slug
    }

    public get initialized(): boolean {
        return this.name !== '' && this.slug !== ''
    }
}
