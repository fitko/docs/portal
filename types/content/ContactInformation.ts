export class ContactInformation {
    name: string = ''
    organisation: string = ''
    url: string = ''
    mail: string = ''
    phone: string = ''
    contactform: string = ''

    constructor(contactInformation?: any) {
        if (contactInformation) {
            this.initialize(contactInformation)
        }
    }

    initialize(contactInformation: ContactInformation) {
        this.name = contactInformation.name
        this.organisation = contactInformation.organisation
        this.url = contactInformation.url
        this.mail = contactInformation.mail
        this.phone = contactInformation.phone
        this.contactform = contactInformation.contactform
    }

    public get initialized(): boolean {
        return (this.name !== '' || this.organisation !== '') && this.mail !== ''
    }
}
