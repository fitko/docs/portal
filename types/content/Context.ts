/* eslint-disable no-unused-vars */
export class Context {
    id: string = ''

    constructor(Context?: any) {
        if (Context) {
            this.initialize(Context)
        }
    }

    initialize(newContext: Context) {
        this.id = newContext.id
    }

    public get initialized(): boolean {
        return (this.id !== undefined && this.id !== '')
    }
}
