/* eslint-disable no-unused-vars */
export class Framework {
    id: string = ''
    url: string = ''

    constructor(Framework?: any) {
        if (Framework) {
            this.initialize(Framework)
        }
    }

    initialize(linkItem: Framework) {
        this.id = linkItem.id
        this.url = linkItem.url
    }

    public get initialized(): boolean {
        return (this.id !== undefined && this.id !== '')
    }

    public get hasLink(): boolean {
        return (this.url !== undefined && this.url != null && this.url !== '' && this.url !== 'n.v.')
    }
}
