import { BaseContact } from './BaseContact'

export class Funder extends BaseContact {
    // Override the initialize method properly
    protected override initialize(data: Partial<BaseContact> | string): void {
        if (typeof data === 'string') {
            // Handle string case
            super.initialize({
                name: data
            });
        } else {
            // Handle object case
            super.initialize(data);
        }
    }

    public override get initialized(): boolean {
        return (this.name !== '' || this.organisation !== '')
    }
}

