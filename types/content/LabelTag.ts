import { Tag } from './Tag'

export class LabelTag extends Tag {
    label: string
    name: string = 'label'
    value: string

    constructor(label: string) {
        super()
        this.label = label
        this.value = label
    }
}
