/* eslint-disable no-unused-vars */
export enum LiabilityType {
    Verbindlich_PLR = 'Verbindlich (IT-PLR)',
    Empfohlen_PLR = 'Empfohlen (IT-PLR)',
    Empfohlen_FitSb = 'Empfohlen (FIT-SB)',
    OzgRv = 'OZG RV - Rechtsverordnung nach §6 OZG',
    Rv = 'Rechtsverordnung',
    Other = 'Andere Vorgabe',
    NOT_DEFINED = 'Nicht definiert'
}
export const UNDEFINED_LIABILITY = 'Nicht definiert' as const

export class Liability {
    liability: LiabilityType
    url: string = ''
    date: string = ''
    dateDate: Date

    constructor(Liability?: any) {
        if (Liability) {
            this.initialize(Liability)
        }
    }

    initialize(linkItem: Liability) {
        this.liability = linkItem.liability
        this.url = linkItem.url
        this.date = linkItem.date
        if (linkItem.date !== undefined) {
            this.dateDate = new Date(linkItem.date)
        }
    }

    public get initialized(): boolean {
        return (this.liability !== undefined)
    }

    public get hasLink(): boolean {
        return (this.url !== undefined && this.url != null && this.url !== '' && this.url !== 'n.v.')
    }

    public get hasDate(): boolean {
        return (this.date !== undefined && this.date != null && this.date !== '' && this.date !== 'n.v.')
    }
}
