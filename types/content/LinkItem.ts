export class LinkItem {
    title: string = ''
    name: string = ''
    url: string = ''
    license: string = ''

    constructor(LinkItem?: any) {
        if (LinkItem) {
            this.initialize(LinkItem)
        }
    }

    initialize(linkItem: LinkItem) {
        this.title = linkItem.title
        this.name = linkItem.name
        this.url = linkItem.url
        this.license = linkItem.license
    }

    public get initialized(): boolean {
        return this.name != null || this.name !== '' ||
               this.title != null || this.title !== ''
    }

    public get hasLink(): boolean {
        return (this.url != null && this.url !== '' && this.url !== 'n.v.')
    }

    public get hasLicense(): boolean {
        return (this.license != null && this.license !== '' && this.license !== 'n.v.')
    }
}
