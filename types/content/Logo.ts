export class Logo {
    path: string = ''
    title: string = ''

    constructor(newLogo?: any) {
        if (newLogo) {
            this.initialize(newLogo)
        }
    }

    initialize(newDeveloper: Logo) {
        this.path = newDeveloper.path
        this.title = newDeveloper.title
    }

    public get initialized(): boolean {
        return this.path !== '' && this.title !== ''
    }
}
