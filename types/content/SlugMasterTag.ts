import { Tag } from './Tag'

export class SlugMasterTag extends Tag {
    slug: string
    name: string = 'slug_master'
    value: string

    constructor(type: string) {
        super()
        this.slug = type
        this.value = type != null ? type : ''
    }
}
