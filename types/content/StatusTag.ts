import { Tag } from './Tag'

export class StatusTag extends Tag {
    status: string
    name: string = 'status'
    value: string

    constructor(status: string) {
        super()
        this.status = status
        this.value = status
    }
}
