export class SteeringCommittee {
    name: string = ''
    organisation: string = ''
    url: string = ''
    mail: string = ''
    phone: string = ''
    contactform: string = ''

    constructor(newSteeringCommittee?: any) {
        if (newSteeringCommittee) {
            this.initialize(newSteeringCommittee)
        }
    }

    initialize(newSteeringCommittee: SteeringCommittee) {
        this.name = newSteeringCommittee.name
        this.organisation = newSteeringCommittee.organisation
        this.url = newSteeringCommittee.url
        this.phone = newSteeringCommittee.phone
        this.mail = newSteeringCommittee.mail
        this.contactform = newSteeringCommittee.contactform
    }

    public get initialized(): boolean {
        return (this.name !== '' || this.organisation !== '') && this.url !== ''
    }
}
