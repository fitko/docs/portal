export abstract class Tag {
    abstract name: string;
    abstract value: string;
}
