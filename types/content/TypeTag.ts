import { Tag } from './Tag'

export class TypeTag extends Tag {
    type: string
    name: string = 'type'
    value: string

    constructor(type: string) {
        super()
        this.type = type
        this.value = type
    }
}
