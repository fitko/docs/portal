import { BooleanOption } from './BooleanOption'
import { FilterGroup, OpenClose } from './FilterGroup'

export type GetLabelsFunction = (obj: unknown) => string[]
export class BoolAutoFilterGroup extends FilterGroup {
    id: string = ''
    type: string = 'BoolAuto'
    label: string = ''
    color: string = ''
    icon: string = ''
    openCloseMode: OpenClose = OpenClose.ForcedOpen
    getLabelsOfElementFunc: GetLabelsFunction = () => []
    options: BooleanOption[] = []

    constructor(init?:Partial<BoolAutoFilterGroup>) {
        super()
        Object.assign(this, init)
        this.type = 'BoolAuto'
    }

    public getElements(object: any): Array<string> {
        return this.getLabelsOfElementFunc(object)
    }

    public checkLabel(object: any): boolean {
        let elements: Array<string> = []

        if (this.getLabelsOfElementFunc !== undefined) {
            elements = this.getLabelsOfElementFunc(object)
        }
        return elements.includes(this.label)
    }

    public getElementsAll(theObjects: Array<any>): Array<string> {
      let elements: Array<string> = []
  
      theObjects.forEach((object) => {
          const labels = this.getLabelsOfElementFunc(object)
          // Handle both single strings and arrays of strings
          if (Array.isArray(labels)) {
              elements = elements.concat(labels)
          } else {
              elements.push(labels)
          }
      })
  
      return elements
          .filter(element => element !== '')  // Remove empty strings
          .filter(this.onlyUnique)            // Remove duplicates
          .sort((a, b) => a.localeCompare(b)) // Sort elements
    }

    private onlyUnique(value: string, index:number, array:Array<string>) {
        return array.indexOf(value) === index
    }
}
