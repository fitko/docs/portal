import { BooleanOption } from './BooleanOption'
import { FilterGroup, OpenClose } from './FilterGroup'

export class BoolForcedFilterGroup extends FilterGroup {
    id: string = ''
    type: string = 'BoolForced'
    label: string = ''
    color: string = ''
    icon: string = ''
    openCloseMode: OpenClose = OpenClose.ForcedOpen
    options: BooleanOption[] = []

    constructor(init?:Partial<BoolForcedFilterGroup>) {
        super()
        Object.assign(this, init)
    }
}
