export class BooleanOption {
    id: string = ''
    label: string = ''
    evalFunc: (elem:any) => boolean | Array<string> = function func() { return false }
    parentFilterType: string = ''
    initialCount: number = 0

    public constructor(init?:Partial<BooleanOption>) {
        Object.assign(this, init)
        this.initialCount = 0
    }
}
