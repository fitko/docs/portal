/* eslint-disable no-unused-vars */
export enum OpenClose {
    DefaultClose = 1,
    DefaultOpen,
    ForcedOpen
 }
export abstract class FilterGroup {
    abstract id: string
    abstract type: string
    abstract color?: string
    abstract icon?: string
    abstract label: string
    abstract openCloseMode: OpenClose
    abstract options: any[]
}
