declare module 'react-if' {
  import { ReactNode } from 'react';
  
  interface IfProps {
    condition: boolean;
    children: ReactNode;
  }
  
  export const If: React.FC<IfProps>;
  export const Then: React.FC<{ children: ReactNode }>;
  export const Else: React.FC<{ children: ReactNode }>;
}