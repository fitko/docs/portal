import { IconSend, IconRoute, IconSteeringWheel, IconManualGearbox, IconBrandOpenSource, IconRocket, IconArrowBigRight } from '@tabler/icons-react'
import React, { useEffect, useState } from 'react'

import Link from 'next/link'
import IconRenderer from '@/components/IconRenderer'

const iconMapping = {
    IconRoute,
    IconSend,
    IconSteeringWheel,
    IconManualGearbox,
    IconBrandOpenSource,
    IconRocket,
    IconArrowBigRight,
}

interface LandingPageContent {
    icon?: keyof typeof iconMapping;
    title: string;
    content: string;
    link?: string;
    linkText?: string;
  }
  interface FeaturedApiModulesProps {
    content: {
        blog: Record<string, LandingPageContent>;
        modules: Record<string, LandingPageContent>;
    },
    lang: string
}

const FeaturedApiModules: React.FC<FeaturedApiModulesProps> = ({ content, lang }) => {
    const { blog, modules } = content

    const [isContentLoaded, setIsContentLoaded] = useState(false)

    useEffect(() => {
        setIsContentLoaded(true)
    }, [content])
    return (
        <div className="bg-white">
            <div className="relative pb-32 bg-gray-800">
                <div className="absolute inset-0 bg-yellow-400">
                    <div className="h-full bg-[length:250px] bg-banner-texture" />
                    <div
                        className="absolute inset-0 bg-gray-200 mix-blend-multiply"
                        aria-hidden="true"
                    />
                </div>
                {isContentLoaded && (
                    <section className="relative max-w-7xl mx-auto">
                        {Object.entries(blog).map(([slug, blogEntry]: [string, LandingPageContent], index) => (
                            <div key={index} className='pt-24 pb-14 px-4 sm:px-6 lg:px-8'>
                                <div className='flex justify-start'>
                                    <div className="h-16 w-16 m-2.5 p-5 bg-yellow-400 rounded-xl shadow-lg">
                                        <IconRenderer iconName={blogEntry?.icon} />
                                    </div>
                                    <h1 className="overflow-hidden max-w-[60vw] md:max-w-prose py-1 sm:py-0.5 m-4 text-2xl sm:text-3xl tracking-tight text-white drop-shadow-md">
                                        {blogEntry.title}
                                    </h1>
                                </div>
                                <p className="cms-blog-text p-8 mt-6 max-w-7xl text-lg text-gray-800 bg-white rounded-2xl" dangerouslySetInnerHTML={{ __html: blogEntry.content }} />
                                {blogEntry?.link &&
                            <div className="p-6 -mt-4 bg-gray-50 rounded-bl-2xl rounded-br-2xl md:px-8 flex">
                                <Link href={ blogEntry.link } passHref legacyBehavior>
                                    <a className="justify-start text-base text-yellow-400 hover:text-yellow-600 inline-flex space-x-2">
                                        <span className="font-bold xl:py-0.5">{ blogEntry.linkText }</span>
                                        <IconArrowBigRight
                                            className="h-7 w-7 xl:py-1"
                                            aria-hidden="true"
                                        />
                                    </a>
                                </Link>
                            </div>
                                }
                            </div>
                        ))}
                    </section>
                )}
            </div>

            {/* Overlapping cards */}
            <section className="-mt-32 max-w-7xl mx-auto relative z-10 pb-32 px-4 sm:px-6 lg:px-8">
                {isContentLoaded && (
                    <div className="grid grid-cols-1 gap-y-20 lg:grid-cols-3 lg:gap-y-0 lg:gap-x-8">
                        {Object.entries(modules).map(([slug, module]: [string, LandingPageContent], index) => (
                            <div
                                key={index}
                                className="flex flex-col bg-white rounded-2xl shadow-xl"
                            >
                                <div className="flex-1 relative pt-16 px-6 pb-8 md:px-8">
                                    <div className="absolute top-0 p-5 inline-block bg-yellow-400 rounded-xl shadow-lg transform -translate-y-1/2">
                                        <IconRenderer iconName={module?.icon} />
                                    </div>
                                    <h2 className="text-xl font-medium text-gray-900">
                                        {module.title}
                                    </h2>
                                    <p className="mt-4 text-base text-gray-500" dangerouslySetInnerHTML={{ __html: module.content }} />
                                </div>
                                <div className="p-6 bg-gray-50 rounded-bl-2xl rounded-br-2xl md:px-8 flex">
                                    <a
                                        href={module.link}
                                        className="justify-start text-base text-yellow-400 hover:text-yellow-600 inline-flex space-x-2"
                                    >
                                        <span className="font-medium xl:py-0.5">{module.linkText}</span>
                                        <IconArrowBigRight
                                            className="h-7 w-7 xl:py-1"
                                            aria-hidden="true"
                                        />
                                    </a>
                                </div>
                            </div>
                        ))}
                    </div>
                )}
            </section>
        </div>
    )
}

export default FeaturedApiModules
