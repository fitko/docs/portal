import Link from 'next/link'
import useBasePath from '../shared/use-base-path'
import useTranslation from '../shared/use-translation'

interface HeroProps {
    lang: string;
}

const Hero = ({ lang }: HeroProps) => {
    const { getImgPath, localizePath } = useBasePath()
    const { translate } = useTranslation(lang)

    return (
        <div className="relative bg-white overflow-hidden">
            <section className="relative pb-6 sm:pb-12 lg:pb-16">
                <div className="mt-10 mx-auto max-w-7xl px-4 sm:mt-24 sm:px-6 lg:mt-20">
                    <div className="lg:grid lg:grid-cols-12 lg:gap-8">
                        <div className=" order-last mb-8 relative sm:max-w-md sm:mx-auto lg:mt-0 lg:max-w-none lg:mx-0 lg:col-span-4 lg:flex lg:items-center">
                            <div className="relative mx-auto w-full max-w-[60%] lg:max-w-md">
                                <img src={`${getImgPath('kabelmaennchen_construct.svg')}`} alt={'Kabelmaennchen_Construct'} />
                            </div>
                        </div>
                        <div className="text-center md:max-w-4xl md:mx-auto lg:col-span-8 lg:text-left">
                            <h1 className="xl:pb-1">
                                <span className="mt-1 block text-3xl tracking-tight sm:text-5xl xl:text-7xl">
                                    <span className="block text-gray-900 leading-6 lg:leading-none">{ translate('landingpage.headlines.title1') }</span>
                                    <span className="block text-yellow-400">{ translate('landingpage.headlines.title2') }</span>
                                </span>
                            </h1>
                            <p className="mt-4 text-base text-left text-gray-500 sm:mt-5 sm:text-xl lg:text-lg xl:text-2xl">
                                { translate('landingpage.paragraphs.title') }
                            </p>
                            <div className="mt-8 lg:mxw-0 flex flex-col items-stretch grow lg:w-2/3">
                                <Link href={localizePath('/resources', lang)} passHref legacyBehavior>
                                    <a className="justify-center text-lg -ml-px relative inline-flex space-x-2 px-4 py-2 font-medium rounded-md text-white bg-yellow-400 hover:bg-yellow-300 hover:cursor-pointer shadow-sm w-full">
                                        { translate('landingpage.buttons.toresources') }
                                    </a>
                                </Link>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    )
}

export default Hero
