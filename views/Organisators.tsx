import useTranslation from '../shared/use-translation'
import useBasePath from '../shared/use-base-path'

interface OrganisatorsProps {
    lang: string;
}

const Organisators = ({ lang }: OrganisatorsProps) => {
    const { getImgPath } = useBasePath()
    const { translate } = useTranslation(lang)

    return (
        <div className="bg-white bg-opacity-25">
            <div className="max-w-7xl mx-auto py-10 sm:px-6">
                <div className="lg:grid lg:grid-cols-2 lg:gap-8 items-center">
                    <div className="flow-root self-center">
                        <div className="-mt-4 flex flex-wrap justify-between lg:-ml-4">
                            <div className="mt-4 ml-4 lg:ml-8 flex flex-grow flex-shrink-0 justify-center lg:flex-grow-0 lg:ml-4 items-center">
                                <img
                                    className="h-20 sm:h-24 lg:h-28"
                                    src={`${getImgPath('it-planungsrat-logo.svg')}`}
                                    alt={translate('alt.itPlanungsrat')}
                                />
                            </div>
                            <div className="mt-4 mr-4 lg:mr-0 lg:ml-8 flex flex-grow flex-shrink-0 justify-center lg:flex-grow-0 lg:ml-4 items-center">
                                <img
                                    className="h-20 sm:h-24 lg:h-28"
                                    src={`${getImgPath('fitko-main-logo.svg')}`}
                                    alt={translate('alt.fitko')}
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Organisators
