import React from 'react'
import Link from 'next/link'
import ResourceCard from '@/components/ResourceCard'
import useEditPath from 'shared/use-edit-path'
import useTranslation from '../shared/use-translation'
import { IconBook, IconPencil } from '@tabler/icons-react'
const { getContentEditUrl } = useEditPath()

type ResourceContentProperties = {
  title: string
  slug: string
  description: string
  logo?: {
    path: string
    title?: string
  }
  developer: {
    name: string
    link: string
  }
  contact: {
    name: string
    mail: string
  }
  statusLabel: string
  documentationURL: string
  apiSpecURL?: string
  sourceURL?: string
  tosURL?: string
  issuesURL?: string
  childResources: any[],
  lang?: string
}

export function Resource({
    title,
    slug,
    lang,
    description,
    logo,
    developer,
    contact,
    tosURL,
    statusLabel,
    documentationURL,
    apiSpecURL,
    sourceURL,
    issuesURL,
    childResources,
    // todo: Add Resource Content class here
}: ResourceContentProperties) {
    const { translate } = useTranslation(lang)
    return (
        <ResourceCard title={title} description={description} logo={logo} slug={slug} lang={lang} apiSpecURL={apiSpecURL} childResources={childResources}>
            {developer && (
                <div className="grow md:grow-0 text-left">
                    <dt className="text-sm font-medium text-gray-500">{ translate('resourcedetail.labels.responsible') }</dt>
                    <dd className="ml-0 mt-1 text-sm text-gray-900 hover:cursor-pointer">
                        <a href={developer.link} target={'_blank'}>
                            {developer.name}
                        </a>
                    </dd>
                </div>
            )}
            {(contact?.name || contact?.mail) && (
                <div className="grow md:grow-0">
                    <dt className="text-sm font-medium text-gray-500">{ translate('resourcedetail.labels.contact') }</dt>
                    <dd className="ml-0 mt-1 text-sm text-gray-900 hover:cursor-pointer">
                        <a
                            target={'_blank'}
                            onClick={(event) => {
                                window.open(`mailto:${contact.mail}`, '_blank')
                                event.preventDefault()
                            }}
                        >
                            {contact.name ? contact.name : contact.mail}
                        </a>
                    </dd>
                </div>
            )}
            {statusLabel && (
                <div className="grow md:grow-0">
                    <dt className="text-sm font-medium text-gray-500">{ translate('resourcedetail.labels.status') }</dt>
                    <dd className="ml-0 mt-1 text-sm text-gray-900">
                        {statusLabel}
                    </dd>
                </div>
            )}
            {sourceURL && (
                <div className="grow md:grow-0">
                    <dt className="text-sm font-medium text-gray-500">{ translate('resourcedetail.labels.sourcecode') }</dt>
                    <dd className="ml-0 mt-1 text-sm text-gray-900 hover:cursor-pointer">
                        <a href={sourceURL} target={'_blank'}>
                            {sourceURL}
                        </a>
                    </dd>
                </div>
            )}
            {tosURL && (
                <div className="grow md:grow-0">
                    <dt className="text-sm font-medium text-gray-500">{ translate('resourcedetail.labels.tosurl') }</dt>
                    <dd className="ml-0 mt-1 text-sm text-gray-900 hover:cursor-pointer">
                        <a href={tosURL} target={'_blank'}>
                            {tosURL}
                        </a>
                    </dd>
                </div>
            )}
            {issuesURL && (
                <div className="grow md:grow-0">
                    <dt className="text-sm font-medium text-gray-500">{ translate('resourcedetail.labels.issueurl') }</dt>
                    <dd className="ml-0 mt-1 text-sm text-gray-900 hover:cursor-pointer">
                        <a href={issuesURL} target={'_blank'}>
                            {issuesURL}
                        </a>
                    </dd>
                </div>
            )}
            {documentationURL && (
                <div className="grow md:grow-0 ml-auto">
                    <Link href={documentationURL} passHref legacyBehavior>
                        <a href={documentationURL} target={'_blank'} className="flex w-full md:min-w-0 justify-center whitespace-nowrap bg-yellow-400 hover:bg-yellow-500 text-gray-700 font-medium py-2 px-4 rounded focus:outline-none focus:ring-2 focus:ring-yellow-500">
                            <IconBook className="w-5 h-5 mr-2" />
                            { translate('resourcedetail.buttons.toresource') }
                        </a>

                    </Link>
                </div>
            )}
        </ResourceCard>
    )
}
