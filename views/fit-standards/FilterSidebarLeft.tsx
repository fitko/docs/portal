import useServiceFilter from 'shared/use-service-filter'
import { ResourceContent } from 'types/ResourceContent'
import { useRouter } from 'next/router'
import useFilterData from 'shared/use-filter-data'

export default function ({ contentsString }) {
    const contents: ResourceContent[] = JSON.parse(contentsString)
    const filterData = useFilterData()
    const filters = filterData['filter-it-standards']
    const router = useRouter()
    const { activeTagFilters, filterToParams } =
    useServiceFilter(contents, router.query.filter)

    function handleFilters(event) {
        const filter = event.target.id.split('-')[0]

        if (event.target.checked) {
            activeTagFilters[filter].push(event.target.value)
        } else if (activeTagFilters[filter].indexOf(event.target.value) >= 0) {
            activeTagFilters[filter].splice(
                activeTagFilters[filter].indexOf(event.target.value),
                1
            )
        }
        router.push(`?filter=${encodeURIComponent(filterToParams())}`, undefined, {
            shallow: true,
        })
    }

    return (
        <nav aria-label="Sidebar" className="sticky top-4 divide-y divide-gray-300">
            {filters.map((filter) => (
                <div className="pt-6 mt-4" key={filter.id}>
                    <p
                        className="text-xs font-semibold text-gray-500 uppercase tracking-wider"
                        id={`${filter.id}-headline`}
                    >
                        {filter.label}
                    </p>
                    <div
                        className="pl-3 mt-3 space-y-2"
                        aria-labelledby={`${filter.id}-headline`}
                    >
                        {filter.options.map((option) => (
                            <div key={option.value} className="flex items-center">
                                <input
                                    onClick={handleFilters}
                                    id={`${filter.id}-${option.id}`}
                                    name={`${filter.id}[]`}
                                    defaultValue={`${filter.id}:${option.id}`}
                                    defaultChecked={
                                        router.query.filter
                                            ? router.query.filter.includes(
                                                `${filter.id}:${option.id}`
                                            )
                                            : false
                                    }
                                    type="checkbox"
                                    className="h-4 w-4 border-gray-300 rounded text-indigo-600 focus:ring-indigo-500"
                                />
                                <label
                                    htmlFor={`${filter.id}-${option.id}`}
                                    className="ml-3 text-sm text-gray-500"
                                >
                                    {option.label}
                                </label>
                            </div>
                        ))}
                    </div>
                </div>
            ))}
        </nav>
    )
}
