import React, { useEffect } from 'react'
import Link from 'next/link'
import { useRouter } from 'next/router'
import {
    // IconArrowBigRight,
    // IconMail,
    // IconBooks, IconBook2,
    IconChevronLeft
} from '@tabler/icons-react'
import useTranslation from '../../shared/use-translation'

const { translate } = useTranslation()

const LifeCycleModel = (props) => (
    <svg
        xmlns='http://www.w3.org/2000/svg'
        id='lifecyle-svg'
        aria-hidden='true'
        viewBox="0 0 336.901 21.5"
        {...props}
    >
        <path
            d='M-114.694 50.867c-1.7-.002-27.755-.004-29.461 0 2.044 3.575 4.106 7.146 6.164 10.718-2.052 3.575-4.12 7.15-6.167 10.727 1.706 0 27.76.002 29.464 0 1.952-3.579 3.928-7.152 5.87-10.733-1.962-3.569-3.915-7.14-5.87-10.712z'
            className='svg-elem-1'
            transform='translate(144.158 -50.814)'
        />
        <path
            d='M-73.216 50.867c-1.7-.002-40.037-.004-41.743 0 2.044 3.575 4.106 7.146 6.163 10.718-2.05 3.575-4.119 7.15-6.166 10.727 1.706 0 40.044.002 41.746 0 1.953-3.579 3.929-7.152 5.87-10.733-1.962-3.569-3.914-7.14-5.87-10.712z'
            className='svg-elem-2'
            transform='translate(144.158 -50.814)'
        />
        <path
            d='M-5.276 50.867c-1.7-.002-66.692-.004-68.399 0 2.045 3.575 4.106 7.146 6.164 10.718-2.051 3.575-4.12 7.15-6.167 10.727 1.706 0 66.699.002 68.402 0 1.952-3.579 3.928-7.152 5.87-10.733-1.963-3.569-3.915-7.14-5.87-10.712z'
            className='svg-elem-3'
            transform='translate(144.158 -50.814)'
        />
        <path
            d='M20.436 50.867c-1.7-.002-24.294-.004-26 0C-3.52 54.442-1.46 58.013.599 61.585c-2.051 3.575-4.12 7.15-6.167 10.727 1.707 0 24.3.002 26.004 0 1.952-3.579 3.928-7.152 5.87-10.733-1.963-3.569-3.915-7.14-5.87-10.712z'
            className='svg-elem-4'
            transform='translate(144.158 -50.814)'
        />
        <path
            d='M53.031 50.867c-1.7-.002-31.473-.004-33.18 0 2.045 3.575 4.106 7.146 6.164 10.718-2.05 3.575-4.119 7.15-6.167 10.727 1.707 0 31.48.002 33.183 0 1.952-3.579 3.928-7.152 5.87-10.733-1.962-3.569-3.914-7.14-5.87-10.712z'
            className='svg-elem-5'
            transform='translate(144.158 -50.814)'
        />
        <path
            d='M100.934 50.867c-2.567-.002-45.619-.004-48.185 0 2.061 3.575 4.12 7.15 6.167 10.727-2.055 3.573-4.11 7.145-6.167 10.716 2.57.002 45.622.006 48.188 0 1.966-3.573 3.911-7.148 5.877-10.718-1.966-3.573-3.904-7.154-5.88-10.725z'
            className='svg-elem-6'
            transform='translate(144.158 -50.814)'
        />
        <path
            d='M135.971 50.867c-1.7-.002-33.603-.004-35.31 0 2.045 3.575 4.106 7.146 6.164 10.718-2.051 3.575-4.12 7.15-6.167 10.727 1.706 0 33.61.002 35.313 0 1.953-3.579 3.929-7.152 5.87-10.733-1.962-3.569-3.914-7.14-5.87-10.712z'
            className='svg-elem-7'
            transform='translate(144.158 -50.814)'
        />
        <path
            d='M161.464 50.867c-1.7-.002-24.155-.004-25.862 0 2.044 3.575 4.106 7.146 6.164 10.718-2.051 3.575-4.12 7.15-6.167 10.727 1.706 0 24.162.002 25.865 0 1.952-3.579 3.928-7.152 5.87-10.733-1.963-3.569-3.915-7.14-5.87-10.712z'
            className='svg-elem-8'
            transform='translate(144.158 -50.814)'
        />
        <path
            d='M186.873 50.817c-1.7-.002-24.156-.004-25.862 0 2.044 3.575 4.105 7.145 6.163 10.718-2.05 3.575-4.119 7.15-6.167 10.727 1.707 0 24.162.002 25.866 0 1.952-3.58 3.928-7.152 5.87-10.733-1.963-3.569-3.915-7.14-5.87-10.712z'
            className='svg-elem-9'
            transform='translate(144.158 -50.814)'
        />
        <text
            x='100.16156'
            y='15.85995'
            className='lebenszyklusmodell'
        >
            Lebenszyklusmodell
        </text>
    </svg>
)

export default () => {
    useEffect(() => {
        const intervalId = setInterval(invertSvgClassActive, 2500)
        return () => {
            clearInterval(intervalId)
        }
    })
    function invertSvgClassActive() {
        document.getElementById('lifecyle-svg').classList.toggle('active')
    }

    // Für Link auf Detailseite root ermitteln
    const router = useRouter()
    const currentPath = router.asPath // Aktuellen Pfad holen
    const pathSegments = currentPath.split('/') // Pfad in Segmente aufteilen

    // Der Ordnername befindet sich in pathSegments[1]
    const rootFolder = '/' + pathSegments[1] + '/'

    return (
        <div className="">
            <div className="flex hidden">
                <Link href='/' passHref legacyBehavior>
                    <a>
                        <IconChevronLeft className="mr-7 mt-6 text-gray-400 hover:text-gray-900" aria-hidden="true"/>
                    </a>
                </Link>
            </div>
            <h1 className="text-lg sm:text-xl" aria-label="on" dangerouslySetInnerHTML={{ __html: translate('fitstandards.listview.labels.welcome') }}/>
            <p className="pt-1 text-sm md:text-base">
                Die Informationsplattform der <a href="https://fitko.de" target="_blank" rel="noopener">FITKO</a> gibt einen Überblick über die Föderalen IT-Standards (FIT-Standards) des IT-Planungsrates und berücksichtigt den gesamten Lebenszyklus – von der Bedarfsmeldung über die Umsetzung und den Regelbetrieb bis hin zur endgültigen Dekommissionierung.
            </p>
            <div className="grid justify-items-center my-4 sm:my-6">
                <Link href={`${rootFolder}hilfe/#bedeutung_lz`} className="" id="dynamic-lifecycle" aria-label="Mehr Informationen zum Lebenszyklusmodell der Standardisierungsagenda">
                    <LifeCycleModel alt="Lebenszyklusmodell der Standardisierungsagenda" className=""/>
                </Link>
            </div>
            <p className="pt-1 text-sm md:text-base">
                Weitere Erläuterungen zur Bedienung der Informationsplattform erhalten Sie unter&nbsp;
                <Link href="/fit-standards/hilfe">Bedienungshilfe</Link>.
            </p>
            <p className="pt-1  text-sm md:text-base md:mb-6">
                Diese Plattform wird kontinuierlich weiterentwickelt, um über die FIT-Standards gebündelt, übersichtlich und transparent zu informieren.
            </p>
            <hr className="my-4 border-t-2 border-gray-300" aria-hidden="true"/>
        </div>
    )
}
