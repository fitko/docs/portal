import React from 'react'
import PageHeaderCard from '@/components/fit-standards/PageHeaderCard'
import { Context, Tag } from 'types/content'

type HeaderOptions = {
  title: string
  contextContent: string[]
  contextRelation: string[]
  description: string
  img?: {
    src: string
    alt?: string
  }
  tags: Tag[]
  developer: {
    name: string
    link: string
  }
  contact: {
    name: string
    mail: string
  }
  statusLabel: string
  documentationURL: string
  sourceURL?: string
  tosURL?: string
  issuesURL?: string
  resources: any[]
}

export function Header({
    title,
    contextContent,
    contextRelation,
    description,
    img,
    tags,
    developer,
    contact,
    tosURL,
    statusLabel,
    documentationURL,
    sourceURL,
    issuesURL,
    resources,
}: HeaderOptions) {
    return (
        <PageHeaderCard 
            title={title} 
            contextRelation={contextRelation}
            contextContent={contextContent}
            description={description} 
            img={img} 
            status={statusLabel}
        />
    )
}
