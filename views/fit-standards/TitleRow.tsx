import * as React from 'react'
import { useRouter } from 'next/router'

export default () => {
    // Für Link zur Homepage root ermitteln
    const router = useRouter()
    const rootA = router.basePath + '/fit-standards/'
    return (
        <div className="px-3 lg:px-8  max-w-7xl md:mx-auto pt-8 pb-2 text-2xl md:text-3xl font-medium" tabIndex={0}>
            {/* <a href={`${rootA}`}>Informationsplattform für Föderale IT-Standards</a> */}
            <h1>Informationsplattform für Föderale IT-Standards</h1>
            <hr className="h-px" aria-hidden="true"/>
        </div>
    )
}
