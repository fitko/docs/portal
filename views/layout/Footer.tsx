import React from 'react'
import Link from 'next/link'
import useBasePath from '../../shared/use-base-path'
import useTranslation from 'shared/use-translation'

export type FooterOptions = {
    lang: string
    logo: {
        img: string
        alt?: string
    }
}

export type LinkEntry = {
    href: string
    label: string
}

export type FooterColumnOptions = {
    title: string
    links: LinkEntry[]
}

export function Footer({ lang, logo }: FooterOptions) {
    const { getImgPath } = useBasePath()
    const { translate, translateSegmented, translateToArray } = useTranslation(lang)

    const privacyPolicy = translateSegmented('footer.privacyPolicy', { link: '' })
    const contentLicense = translateSegmented('footer.contentLicense', { link: '' })
    const sourceCodeLicense = translateToArray('footer.sourceCodeLicense', { link1: '', link2: '' })
    const backgroundGraphics = translateToArray('footer.backgroundGraphics', { link1: '', link2: '' })
    return (
        <footer className="bg-white" aria-labelledby="footer-heading">
            <h2 id="footer-heading" className="sr-only">
                Footer
            </h2>
            <div className="mt-8">
                <hr aria-hidden='true'/>
                <div className="max-w-7xl mx-auto flex flex-row pt-6 pb-8 px-2 sm:px-4">
                    <div className="flex-1 w-64 flex justify-center self-center">
                        <Link href="https://www.it-planungsrat.de/" target="_blank" rel="noopener">
                            <img src={`${getImgPath('it-planungsrat-logo.svg')}`} alt="Logo des IT-Planungsrats" className="w-48" />
                        </Link>
                    </div>
                    <div className="flex-1 w-64 flex justify-center self-center">
                        <Link href="https://www.fitko.de/" target="_blank" rel="noopener">
                            <img src={`${getImgPath('fitko-main-logo-no-claim.svg')}`} alt="Logo der Fitko" className="w-32" />
                        </Link>
                    </div>
                </div>
                <div className="bg-neutral-200 text-black px-4 py-10 gap-1 shadow shadow-gray-800/80 flex flex-col lg:flex-row space-around items-start font-bold underline-offset-2">
                    <div className="md:basis-1/3">
                        <div className="flex flex-row gap-9 p-5 pb-10">
                            <a href="https://www.fitko.de/impressum" target="_blank" rel="noopener" className="hover:underline text-sm">Impressum</a>
                            <a href="https://docs.fitko.de/meta/privacy" target="_blank" rel="noopener" className="hover:underline text-sm">Datenschutz</a>
                            <a href="https://www.fitko.de/barrierefreiheitserklaerung" target="_blank" rel="noopener" className="hover:underline text-sm">Barrierefreiheit</a>
                        </div>
                    </div>
                    <div className="md:basis-2/3">
                        <div className="flex flex-col px-5 md:flex-row grow justify-between text-[0.8rem] text-justify font-[400] gap-9">
                            <p className="basis-1/3 py-1">
                                {backgroundGraphics[0]}
                                <a className="underline" href="https://github.com/atlemo/SubtlePatterns">{translate('footer.linktexts.subtlePatterns')}</a>
                                {backgroundGraphics[2]}
                                <a className="underline" href="https://creativecommons.org/licenses/by-sa/3.0/">{translate('footer.linktexts.ccBySaLicense')}</a>
                                {backgroundGraphics[5]}
                            </p>
                            <p className="basis-1/3 py-1">
                                {contentLicense.getPrefix()}
                                <a className="underline" href="https://creativecommons.org/licenses/by/4.0/deed.de">{translate('footer.linktexts.ccLicense')}</a>
                                {contentLicense.getSuffix()}
                            </p>
                            <p className="basis-1/3 py-1">
                                {sourceCodeLicense[0]}
                                <a className="underline" href="https://gitlab.opencode.de/fitko/docs/portal/-/blob/main/LICENSES/EUPL-1.2.txt">{translate('footer.linktexts.euplLicense')}</a>
                                {sourceCodeLicense[2]}
                                <a className="underline" href="https://gitlab.opencode.de/fitko/docs/portal">{translate('footer.linktexts.openCodePlatform')}</a>
                                {sourceCodeLicense[4]}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    )
}
