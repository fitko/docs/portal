import React, { ReactElement } from 'react'
import { useRouter } from 'next/router'
import Link from 'next/link'
import useBasePath from '../../shared/use-base-path'
import FitStdFooter from './fit-standards/FitStdFooter'
import useTranslation from 'shared/use-translation'

export type FooterOptions = {
    lang: string
    copyright: string
    logo: {
        img: string
        alt?: string
    }
    children?: ReactElement[]
}

export type LinkEntry = {
    href: string
    label: string
}

export type FooterColumnOptions = {
    title: string
    links: LinkEntry[]
}

export function FooterColumn({ title, links }: FooterColumnOptions) {
    return (
        <>
            <div>
                <h3 className="text-sm font-semibold text-gray-400 tracking-wider uppercase">
                    {title}
                </h3>
                <ul role="list" className="list-none m-0 p-0 mt-4 space-y-4">
                    {links.map(({ href, label }, key) => (
                        <li key={key}>
                            <Link href={href} passHref legacyBehavior>
                                <a className="text-base text-gray-500 hover:text-gray-900">
                                    {label}
                                </a>
                            </Link>
                        </li>
                    ))}
                </ul>
            </div>
        </>
    )
}

export function Footer({ lang, logo, copyright, children }: FooterOptions) {
    const { getImgPath } = useBasePath()
    const { translate, translateSegmented, translateToArray } = useTranslation(lang)
    const isFitStandard = useRouter().asPath.match(/\/fit-standards\//gi)
    const currentYear = new Date().getFullYear()
    const copyText = translate('footer.copy', { year: currentYear.toString() })

    const privacyPolicy = translateSegmented('footer.privacyPolicy', { link: '' })
    const contentLicense = translateSegmented('footer.contentLicense', { link: '' })
    const claim = translateSegmented('footer.fitForFuture', { break: '' })
    const sourceCodeLicense = translateToArray('footer.sourceCodeLicense', { link1: '', link2: '' })
    const backgroundGraphics = translateToArray('footer.backgroundGraphics', { link1: '', link2: '' })

    return (
        <footer className="bg-white" aria-labelledby="footer-heading">
            <h2 id="footer-heading" className="sr-only">
                {translate('footer.heading')}
            </h2>
            { isFitStandard
                ? <FitStdFooter />
                : <div className="max-w-7xl mx-auto pt-12 pb-8 px-4">
                    <div className="justify-items-center xl:grid xl:grid-cols-5 xl:gap-8">
                        <div className="space-y-4 xl:col-span-1">
                            <Link href='/'>
                                <img
                                    className="h-20 w-auto hover:cursor-pointer"
                                    src={`${getImgPath(logo.img)}`}
                                    alt={logo.alt ? logo.alt : 'Logo'}
                                />
                            </Link>
                            <p className="text-gray-500 text-base">{claim.getPrefix()}<br />{claim.getSuffix()}</p>
                        </div>

                        <div className="mt-12 col-span-3 grid grid-cols-3 gap-16 xl:mt-0">
                            {children}
                        </div>
                    </div>

                    {copyright
                        ? (
                            <div className="mt-12 border-t border-gray-200 pt-6">
                                <p className="text-xs text-gray-400 text-center">{copyText}</p>
                            </div>
                        )
                        : null}
                    <p className="text-xs text-gray-400 text-center">
                        {privacyPolicy.getPrefix()}
                        <a className="underline" href="https://fitko.de/datenschutz">{translate('footer.linktexts.privacyPolicy')}</a>
                        {privacyPolicy.getSuffix()}
                    </p>
                    <p className="text-xs text-gray-400 text-center">
                        {contentLicense.getPrefix()}
                        <a className="underline" href="https://creativecommons.org/licenses/by/4.0/deed.de">{translate('footer.linktexts.ccLicense')}</a>
                        {contentLicense.getSuffix()}
                    </p>
                    <p className="text-xs text-gray-400 text-center">
                        {sourceCodeLicense[0]}
                        <a className="underline" href="https://gitlab.opencode.de/fitko/docs/portal/-/blob/main/LICENSES/EUPL-1.2.txt">{translate('footer.linktexts.euplLicense')}</a>
                        {sourceCodeLicense[2]}
                        <a className="underline" href="https://gitlab.opencode.de/fitko/docs/portal">{translate('footer.linktexts.openCodePlatform')}</a>
                        {sourceCodeLicense[4]}
                    </p>
                    <p className="text-xs text-gray-400 text-center">
                        {backgroundGraphics[0]}
                        <a className="underline" href="https://github.com/atlemo/SubtlePatterns">{translate('footer.linktexts.subtlePatterns')}</a>{backgroundGraphics[2]}
                        <a className="underline" href="https://creativecommons.org/licenses/by-sa/3.0/">{translate('footer.linktexts.ccBySaLicense')}</a>
                        {backgroundGraphics[5]}
                    </p>
                </div>
            }
        </footer>
    )
}
