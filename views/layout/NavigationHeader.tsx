import * as React from 'react'
import { useEffect, useState } from 'react'
import CSS from 'csstype'
import Link from 'next/link'
import { useRouter } from 'next/router'
import { IconMenu2, IconX, IconConfetti } from '@tabler/icons-react'
import useTranslation from 'shared/use-translation'
import useBasePath from 'shared/use-base-path'
import LanguageChooser from '@/components/LanguageChooser'

const isBrowser = () => typeof window !== 'undefined'

const elementIds = ['req-close', 'req-open']

// Home icon for NavBar
const IconHome = (props) => (
    <svg
        xmlns="http://www.w3.org/2000/svg"
        height="1em"
        viewBox="0 0 576 512"
        aria-hidden='true'
        {...props}
    >
        <path d="M575.8 255.5c0 18-15 32.1-32 32.1h-32l.7 160.2c0 2.7-.2 5.4-.5 8.1V472c0 22.1-17.9 40-40 40h-16c-1.1 0-2.2 0-3.3-.1-1.4.1-2.8.1-4.2.1H392c-22.1 0-40-17.9-40-40v-88c0-17.7-14.3-32-32-32h-64c-17.7 0-32 14.3-32 32v88c0 22.1-17.9 40-40 40h-55.9c-1.5 0-3-.1-4.5-.2-1.2.1-2.4.2-3.6.2h-16c-22.1 0-40-17.9-40-40V360c0-.9 0-1.9.1-2.8v-69.6H32c-18 0-32-14-32-32.1 0-9 3-17 10-24L266.4 8c7-7 15-8 22-8s15 2 21 7l255.4 224.5c8 7 12 15 11 24z" />
    </svg>
)

export default () => {
    const router = useRouter()
    const { localizePath, getLocaleFromPath } = useBasePath()
    const lang = getLocaleFromPath(router.asPath)
    const { translate } = useTranslation(lang)
    const routeHome = localizePath('/', lang)
    const routeDevelopmentResources = localizePath('/resources', lang)
    const routeStandards = localizePath('/fit-standards/')
    const routeStandardsHelp = localizePath('/fit-standards/hilfe/')
    const routeStandardsChangelog = localizePath('/fit-standards/changelog/')

    const onResourcesPath = router.pathname.includes('/resources')
    const onStandardsPath = router.pathname.includes('/fit-standards')
    const onStandardsSubPath = router.asPath.includes('/fit-standards/')
    const hiddenOnFitStandards = !onStandardsPath

    const feedbackUrl = 'https://docs.fitko.de/feedback'
    const sourceCodeUrl = 'https://gitlab.opencode.de/fitko/docs/portal'

    const style: CSS.Properties = {
        height: '14px',
        paddingTop: '2px',
    }

    const [isMenuOpen, setIsMenuOpen] = useState(false)

    const handleOpenCloseMenu = (evnt: React.MouseEvent<HTMLButtonElement>) => {
        if (!isBrowser()) return

        // Get clicked element
        const thisElement: Element = evnt.target as HTMLElement

        // Which one was it?
        let foundIdx = 1
        if (thisElement && thisElement.closest('button')) {
            foundIdx = elementIds.findIndex((element) => element === thisElement.closest('button').id)
        } else {
            console.warn('No button found for the clicked element')
        }
        // Adjust icon visibility depending on requested operation (open/close menu)
        let elem: HTMLElement
        elem = document.getElementById(elementIds[foundIdx])
        elem.style.display = 'none'
        elem = document.getElementById(elementIds[1 - foundIdx])
        elem.style.display = 'block'

        // Show/hide menu (move outside view area)
        elem = document.getElementById('theMenu')
        if (elem) {
            elem.style.display = (foundIdx == 1 ? 'block' : 'none')
            elem.style.left = (foundIdx == 1 ? '0' : '100%')
        }
        setIsMenuOpen(!isMenuOpen)
    }

    const closeMenu = (evnt: React.MouseEvent<HTMLButtonElement> | React.MouseEvent<HTMLAnchorElement>) => {
        const target = evnt.target as HTMLElement
        const link = target.closest('a') // Find the closest <a> tag

        // Check if URL is the same
        if (link) {
            if (link.href === window.location.href) {
                // Same URL => Do NOT reload page!
                // Event.preventDefault()
                const elem = document.getElementById('theMenu')
                elem.style.left = '100%'
            } else { // Different URL!
                const thisUrl = new URL(link.href)
                if (thisUrl.hostname !== window.location.hostname) { // Another server?
                    // Page will be opened in new window, so we need to hide our menu
                    const elem = document.getElementById('theMenu')
                    elem.style.left = '100%'
                }
            }
        }

        // Reset menu icons
        let elem = document.getElementById(elementIds[0])
        elem.style.display = 'none'
        elem = document.getElementById(elementIds[1])
        elem.style.display = 'block'

        setIsMenuOpen(false)
    }

    useEffect(() => {
        // Any client-side logic can be added here
    }, [])

    // Calculate breakpoint for mobile menu
    const breakpoint = router.asPath.includes('/fit-standards/') ? 'xl' : 'md'

    // Do NOT use LanguageChooser for FIT-Standards!
    const languageChooser = !router.asPath.includes('/fit-standards/')

    return (
        <>
            {/* Ensure Tailwind classes are loaded */}
            <div className="sm:block xl:block hidden xl:hidden sm:hidden"></div>
            {/* Navbar for small screens (different break point for info plattform) */}
            <div className={`${breakpoint}:hidden pb-2`}>
                <div className="bg-gray-100 text-black p-2 shadow shadow-gray-500/50 flex flex-row justify-end">
                    <button type='button' title='Menü anzeigen' tabIndex={0} id="req-open" onClick={handleOpenCloseMenu}>
                        <IconMenu2 aria-hidden='true' className="cursor-pointer"/>
                    </button>
                    <button type='button' title='Menüanzeige schließen' tabIndex={0} id="req-close" className="hidden cursor-pointer" onClick={handleOpenCloseMenu}>
                        <IconX aria-hidden='true' />
                    </button>
                </div>
            </div>

            {/* Menu for small screens */}
            {isMenuOpen && (
                <nav id="theMenu" className="absolute top-11 z-20 bg-white flex flex-col w-screen h-screen shadow-gray-500/50 duration-500 ease-out">
                    <ul className="text-lg ml-16">
                        {/* home */}
                        <li className="my-4">
                            <Link href={routeHome} passHref legacyBehavior>
                                <a className="hover:underline" title="Home" onClick={closeMenu}>
                                    <div className="inline-flex">
                                        <div className="pr-2">
                                            <IconHome className="h-5 pt-1"/>
                                        </div>
                                        { translate('navbar.labels.overview')}
                                    </div>
                                </a>
                            </Link>
                        </li>
                        {/* resources/ */}
                        <li className='my-4'>
                            <Link href={routeDevelopmentResources} passHref legacyBehavior>
                                <a className={`hover:underline ${onResourcesPath ? 'bg-yellow-400' : ''}`} onClick={closeMenu}>{ translate('navbar.labels.resources')}</a>
                            </Link>
                        </li>
                        {/* fit-standards/ */}
                        <li className='mt-4'>
                            <Link href={routeStandards} passHref legacyBehavior>
                                <a className={`hover:underline ${onStandardsPath ? 'bg-yellow-400' : ''}`} onClick={closeMenu}>{ translate('navbar.labels.fitstandards')}</a>
                            </Link>
                        </li>
                        {/* additional menu entries in fit-standards/ subdirectory */}
                        { router.asPath.includes('/fit-standards/')
                            ? <li>
                                <ul>
                                    <li><Link href={'https://docs.fitko.de/standardisierungsagenda/docs/'} passHref legacyBehavior><a target="_blank" rel="noopener" className="hover:underline py-1 px-3" onClick={closeMenu}>Standardisierungsagenda</a></Link></li>
                                    <li><Link href={routeStandardsHelp} passHref legacyBehavior><a className="hover:underline py-1 px-3" onClick={closeMenu}>Bedienungshilfe</a></Link></li>
                                    <li><Link href={routeStandardsChangelog} passHref legacyBehavior><a className="hover:underline py-1 px-3" onClick={closeMenu}>Changelog</a></Link></li>
                                    <li><Link href={'mailto:it-standards@fitko.de'} passHref legacyBehavior><a className="hover:underline py-1 px-3" onClick={closeMenu}>Kontakt</a></Link></li>
                                </ul>
                            </li>
                            : ''
                        }
                        {
                            languageChooser
                                ? <li className="my-4">
                                    <LanguageChooser />
                                </li>
                                : null
                        }

                        <li className="my-4">
                            <Link href={sourceCodeUrl} passHref legacyBehavior>
                                <a href={sourceCodeUrl} className="hover:underline" onClick={closeMenu}>
                                    { translate('navbar.labels.sourcecode')}
                                </a>
                            </Link>
                        </li>
                        <li className="my-4">
                            <Link href={feedbackUrl} passHref legacyBehavior>
                                <a href={feedbackUrl} className="hover:underline" onClick={closeMenu}>
                                    { translate('navbar.labels.feedback')}
                                </a>
                            </Link>
                        </li>
                    </ul>
                </nav>
            )}

            {/* navbar for large screens  (different break point for info plattform) */}
            <div className={`hidden ${breakpoint}:block pb-2 md:pb-4`}>
                <div className="fixed top-0 left-0 right-0 z-20 ">
                    <nav className="bg-gray-100 text-black px-8 gap-8 shadow shadow-gray-500/50 flex flex-row justify-between text-xs underline-offset-2 uppercase ">
                        <div className="flex gap-4 items-center">
                            {/* home */}
                            <Link href={routeHome} passHref legacyBehavior>
                                <a className="hover:underline p-1" title="Home" >
                                    <IconHome style={style} aria-hidden='true'/>
                                </a>
                            </Link>
                            {/* resources/ */}
                            <Link href={`${routeDevelopmentResources}`}passHref legacyBehavior>
                                <a className={`hover:underline ${onResourcesPath ? 'bg-yellow-400' : ''} py-1 px-2`}>
                                    { translate('navbar.labels.resources')}
                                </a>
                            </Link>
                            {/* fit-standards/ */}
                            <Link href={`${routeStandards}`} passHref legacyBehavior>
                                <a className={`hover:underline ${onStandardsPath ? 'bg-yellow-400' : ''} py-1 px-2`}>
                                    { translate('navbar.labels.fitstandards')}
                                </a>
                            </Link>
                        </div>
                        <div className="flex gap-4 items-center">
                            {/* additional menu entries in fit-standards/ subdirectory */}
                            { onStandardsSubPath &&
                                <div>
                                    <Link href="https://docs.fitko.de/standardisierungsagenda/docs/" passHref legacyBehavior>
                                        <a target="_blank" rel="noopener" className="hover:underline py-1 px-3">Standardisierungsagenda</a>
                                    </Link>
                                    <Link href={`${routeStandardsHelp}`} passHref legacyBehavior>
                                        <a className="hover:underline py-1 px-3">Bedienungshilfe</a>
                                    </Link>
                                    <Link href={`${routeStandardsChangelog}`} passHref legacyBehavior>
                                        <a className="hover:underline py-1 px-3">Changelog</a>
                                    </Link>
                                    <Link href="mailto:it-standards@fitko.de" passHref legacyBehavior>
                                        <a className="hover:underline py-1 px-3">Kontakt</a>
                                    </Link>
                                    <span aria-hidden='true'>|</span>
                                </div>
                            }
                            { hiddenOnFitStandards &&
                                <LanguageChooser />
                            }
                            <a href={sourceCodeUrl} target="_blank" rel="noopener noreferrer" className="hover:underline">
                                { translate('navbar.labels.sourcecode')}
                            </a>
                            <a href={feedbackUrl} className="hover:underline flex items-center">
                                <IconConfetti className="mr-1" style={{ height: '1em' }} aria-hidden='true'/>
                                { translate('navbar.labels.feedback')}
                            </a>
                        </div>
                    </nav>
                </div>
                <hr className="h-px" aria-hidden='true'/>
            </div>
        </>
    )
}
