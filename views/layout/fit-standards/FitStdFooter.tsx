/*
 *
 *  Footer for FIT standards
 *  2023-10-19/JVo
 *
 */
import React, { SVGProps } from 'react'
import Link from 'next/link'
import useImgPath from 'shared/use-img-path'

// eslint-disable-next-line no-unused-vars
const FitkoLogoSvg = (props: SVGProps<SVGSVGElement>) => (
    <svg
        xmlns='http://www.w3.org/2000/svg'
        xmlSpace='preserve'
        id="Ebene_1"
        x={0}
        y={0}
        viewBox="0 0 73.9 14.4"
        {...props}
    >
        <style>{'.st0{fill-rule:evenodd;clip-rule:evenodd}'}</style>
        <path
            d="M.2 14.1h2.5V8h4.7V5.8H2.7v-3h8.5V.3H.2zM21.5.3v13.8h-2.6V.3zM33.9 14.1V2.8h-4.8V.3H41v2.5h-4.5v11.3zM54 12.5l-1.8 1.8L45 7.2l7-7.1 1.8 1.8-5.2 5.3zM66.9.3C63.1.3 60 3.4 60 7.2c0 3.8 3.1 6.9 6.9 6.9 3.8 0 6.9-3.1 6.9-6.9 0-3.8-3.1-6.9-6.9-6.9zm0 11.3c-2.4 0-4.4-2-4.4-4.4 0-2.4 2-4.4 4.4-4.4 2.4 0 4.4 2 4.4 4.4 0 2.4-2 4.4-4.4 4.4z"
            className="st0"
        />
    </svg>
)

export default function FitStdFooter() {
    const { getImgPath } = useImgPath()

    return (
        <div className="mt-8">
            <hr aria-hidden='true'/>
            <div className="max-w-7xl mx-auto flex flex-row pt-6 pb-8 px-2 sm:px-4">
                <div className="flex-1 w-64 flex justify-center self-center">
                    <Link href="https://www.it-planungsrat.de/" target="_blank" rel="noopener">
                        <img src={getImgPath('/img/it-planungsrat-logo.svg')} alt="Logo des IT-Planungsrats" className="w-48" />
                    </Link>
                </div>
                <div className="flex-1 w-64 flex justify-center self-center">
                    <Link href="https://www.fitko.de/" target="_blank" rel="noopener">
                        <img src={getImgPath('/img/fitko-main-logo-no-claim.svg')} alt="Logo der Fitko" className="w-32" />
                        {/* <FitkoLogoSvg className="w-48"/> */}
                    </Link>
                </div>
            </div>
            <div className="bg-neutral-200 text-black sm:px-8 py-10 gap-6 sm:gap-8 shadow shadow-gray-800/80 flex flex-row flex justify-center sm:justify-start text-sm font-bold underline-offset-2">
                <Link href="https://www.fitko.de/impressum" target="_blank" rel="noopener" className="hover:underline">Impressum</Link>
                <Link href="https://www.fitko.de/datenschutz" target="_blank" rel="noopener" className="hover:underline">Datenschutz</Link>
                <Link href="https://www.fitko.de/barrierefreiheitserklaerung" target="_blank" rel="noopener" className="hover:underline">Barrierefreiheit</Link>
            </div>
        </div>
    )
}
